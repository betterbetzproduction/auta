{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "Yu Gothic UI",
  "styleName": "Bold",
  "size": 12.0,
  "bold": true,
  "italic": false,
  "charset": 0,
  "AntiAlias": 0,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 0,
  "glyphs": {
    "32": {"x":2,"y":2,"w":5,"h":26,"character":32,"shift":5,"offset":0,},
    "33": {"x":137,"y":58,"w":2,"h":26,"character":33,"shift":6,"offset":2,},
    "34": {"x":130,"y":58,"w":5,"h":26,"character":34,"shift":7,"offset":1,},
    "35": {"x":120,"y":58,"w":8,"h":26,"character":35,"shift":10,"offset":1,},
    "36": {"x":110,"y":58,"w":8,"h":26,"character":36,"shift":10,"offset":1,},
    "37": {"x":96,"y":58,"w":12,"h":26,"character":37,"shift":13,"offset":1,},
    "38": {"x":83,"y":58,"w":11,"h":26,"character":38,"shift":12,"offset":1,},
    "39": {"x":79,"y":58,"w":2,"h":26,"character":39,"shift":4,"offset":1,},
    "40": {"x":73,"y":58,"w":4,"h":26,"character":40,"shift":6,"offset":2,},
    "41": {"x":66,"y":58,"w":5,"h":26,"character":41,"shift":6,"offset":0,},
    "42": {"x":141,"y":58,"w":6,"h":26,"character":42,"shift":8,"offset":1,},
    "43": {"x":54,"y":58,"w":10,"h":26,"character":43,"shift":12,"offset":1,},
    "44": {"x":41,"y":58,"w":2,"h":26,"character":44,"shift":4,"offset":1,},
    "45": {"x":34,"y":58,"w":5,"h":26,"character":45,"shift":7,"offset":1,},
    "46": {"x":30,"y":58,"w":2,"h":26,"character":46,"shift":4,"offset":1,},
    "47": {"x":20,"y":58,"w":8,"h":26,"character":47,"shift":8,"offset":0,},
    "48": {"x":11,"y":58,"w":7,"h":26,"character":48,"shift":9,"offset":1,},
    "49": {"x":2,"y":58,"w":7,"h":26,"character":49,"shift":9,"offset":1,},
    "50": {"x":245,"y":30,"w":7,"h":26,"character":50,"shift":9,"offset":1,},
    "51": {"x":236,"y":30,"w":7,"h":26,"character":51,"shift":9,"offset":1,},
    "52": {"x":226,"y":30,"w":8,"h":26,"character":52,"shift":9,"offset":1,},
    "53": {"x":45,"y":58,"w":7,"h":26,"character":53,"shift":9,"offset":1,},
    "54": {"x":149,"y":58,"w":7,"h":26,"character":54,"shift":9,"offset":1,},
    "55": {"x":158,"y":58,"w":7,"h":26,"character":55,"shift":9,"offset":1,},
    "56": {"x":167,"y":58,"w":8,"h":26,"character":56,"shift":9,"offset":1,},
    "57": {"x":143,"y":86,"w":7,"h":26,"character":57,"shift":9,"offset":1,},
    "58": {"x":138,"y":86,"w":3,"h":26,"character":58,"shift":5,"offset":1,},
    "59": {"x":133,"y":86,"w":3,"h":26,"character":59,"shift":5,"offset":1,},
    "60": {"x":121,"y":86,"w":10,"h":26,"character":60,"shift":12,"offset":1,},
    "61": {"x":109,"y":86,"w":10,"h":26,"character":61,"shift":12,"offset":1,},
    "62": {"x":97,"y":86,"w":10,"h":26,"character":62,"shift":12,"offset":1,},
    "63": {"x":88,"y":86,"w":7,"h":26,"character":63,"shift":9,"offset":1,},
    "64": {"x":74,"y":86,"w":12,"h":26,"character":64,"shift":14,"offset":1,},
    "65": {"x":61,"y":86,"w":11,"h":26,"character":65,"shift":11,"offset":0,},
    "66": {"x":49,"y":86,"w":10,"h":26,"character":66,"shift":11,"offset":1,},
    "67": {"x":37,"y":86,"w":10,"h":26,"character":67,"shift":11,"offset":1,},
    "68": {"x":24,"y":86,"w":11,"h":26,"character":68,"shift":12,"offset":1,},
    "69": {"x":13,"y":86,"w":9,"h":26,"character":69,"shift":10,"offset":1,},
    "70": {"x":2,"y":86,"w":9,"h":26,"character":70,"shift":10,"offset":1,},
    "71": {"x":238,"y":58,"w":10,"h":26,"character":71,"shift":12,"offset":1,},
    "72": {"x":226,"y":58,"w":10,"h":26,"character":72,"shift":12,"offset":1,},
    "73": {"x":221,"y":58,"w":3,"h":26,"character":73,"shift":5,"offset":1,},
    "74": {"x":214,"y":58,"w":5,"h":26,"character":74,"shift":7,"offset":0,},
    "75": {"x":202,"y":58,"w":10,"h":26,"character":75,"shift":11,"offset":1,},
    "76": {"x":192,"y":58,"w":8,"h":26,"character":76,"shift":10,"offset":1,},
    "77": {"x":177,"y":58,"w":13,"h":26,"character":77,"shift":15,"offset":1,},
    "78": {"x":214,"y":30,"w":10,"h":26,"character":78,"shift":13,"offset":1,},
    "79": {"x":202,"y":30,"w":10,"h":26,"character":79,"shift":12,"offset":1,},
    "80": {"x":191,"y":30,"w":9,"h":26,"character":80,"shift":11,"offset":1,},
    "81": {"x":225,"y":2,"w":10,"h":26,"character":81,"shift":12,"offset":1,},
    "82": {"x":207,"y":2,"w":10,"h":26,"character":82,"shift":11,"offset":1,},
    "83": {"x":196,"y":2,"w":9,"h":26,"character":83,"shift":11,"offset":1,},
    "84": {"x":185,"y":2,"w":9,"h":26,"character":84,"shift":11,"offset":1,},
    "85": {"x":173,"y":2,"w":10,"h":26,"character":85,"shift":12,"offset":1,},
    "86": {"x":160,"y":2,"w":11,"h":26,"character":86,"shift":11,"offset":0,},
    "87": {"x":144,"y":2,"w":14,"h":26,"character":87,"shift":15,"offset":0,},
    "88": {"x":131,"y":2,"w":11,"h":26,"character":88,"shift":11,"offset":0,},
    "89": {"x":118,"y":2,"w":11,"h":26,"character":89,"shift":11,"offset":0,},
    "90": {"x":107,"y":2,"w":9,"h":26,"character":90,"shift":10,"offset":1,},
    "91": {"x":219,"y":2,"w":4,"h":26,"character":91,"shift":6,"offset":2,},
    "92": {"x":96,"y":2,"w":9,"h":26,"character":92,"shift":10,"offset":0,},
    "93": {"x":82,"y":2,"w":4,"h":26,"character":93,"shift":6,"offset":0,},
    "94": {"x":72,"y":2,"w":8,"h":26,"character":94,"shift":8,"offset":0,},
    "95": {"x":62,"y":2,"w":8,"h":26,"character":95,"shift":8,"offset":0,},
    "96": {"x":57,"y":2,"w":3,"h":26,"character":96,"shift":8,"offset":2,},
    "97": {"x":48,"y":2,"w":7,"h":26,"character":97,"shift":9,"offset":1,},
    "98": {"x":38,"y":2,"w":8,"h":26,"character":98,"shift":10,"offset":1,},
    "99": {"x":29,"y":2,"w":7,"h":26,"character":99,"shift":9,"offset":1,},
    "100": {"x":19,"y":2,"w":8,"h":26,"character":100,"shift":10,"offset":1,},
    "101": {"x":9,"y":2,"w":8,"h":26,"character":101,"shift":9,"offset":1,},
    "102": {"x":88,"y":2,"w":6,"h":26,"character":102,"shift":6,"offset":0,},
    "103": {"x":237,"y":2,"w":8,"h":26,"character":103,"shift":9,"offset":1,},
    "104": {"x":83,"y":30,"w":8,"h":26,"character":104,"shift":10,"offset":1,},
    "105": {"x":247,"y":2,"w":2,"h":26,"character":105,"shift":5,"offset":1,},
    "106": {"x":176,"y":30,"w":4,"h":26,"character":106,"shift":5,"offset":0,},
    "107": {"x":166,"y":30,"w":8,"h":26,"character":107,"shift":9,"offset":1,},
    "108": {"x":162,"y":30,"w":2,"h":26,"character":108,"shift":5,"offset":1,},
    "109": {"x":148,"y":30,"w":12,"h":26,"character":109,"shift":15,"offset":1,},
    "110": {"x":138,"y":30,"w":8,"h":26,"character":110,"shift":10,"offset":1,},
    "111": {"x":128,"y":30,"w":8,"h":26,"character":111,"shift":9,"offset":1,},
    "112": {"x":118,"y":30,"w":8,"h":26,"character":112,"shift":10,"offset":1,},
    "113": {"x":108,"y":30,"w":8,"h":26,"character":113,"shift":10,"offset":1,},
    "114": {"x":101,"y":30,"w":5,"h":26,"character":114,"shift":6,"offset":1,},
    "115": {"x":182,"y":30,"w":7,"h":26,"character":115,"shift":9,"offset":1,},
    "116": {"x":93,"y":30,"w":6,"h":26,"character":116,"shift":6,"offset":0,},
    "117": {"x":73,"y":30,"w":8,"h":26,"character":117,"shift":10,"offset":1,},
    "118": {"x":63,"y":30,"w":8,"h":26,"character":118,"shift":9,"offset":0,},
    "119": {"x":48,"y":30,"w":13,"h":26,"character":119,"shift":13,"offset":0,},
    "120": {"x":38,"y":30,"w":8,"h":26,"character":120,"shift":9,"offset":0,},
    "121": {"x":28,"y":30,"w":8,"h":26,"character":121,"shift":9,"offset":0,},
    "122": {"x":20,"y":30,"w":6,"h":26,"character":122,"shift":8,"offset":1,},
    "123": {"x":13,"y":30,"w":5,"h":26,"character":123,"shift":6,"offset":1,},
    "124": {"x":9,"y":30,"w":2,"h":26,"character":124,"shift":6,"offset":2,},
    "125": {"x":2,"y":30,"w":5,"h":26,"character":125,"shift":6,"offset":0,},
    "126": {"x":152,"y":86,"w":9,"h":26,"character":126,"shift":11,"offset":1,},
    "9647": {"x":163,"y":86,"w":9,"h":26,"character":9647,"shift":16,"offset":3,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":32,"upper":127,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "fnt_Menu",
  "tags": [],
  "resourceType": "GMFont",
}
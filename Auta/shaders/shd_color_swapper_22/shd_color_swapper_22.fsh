// Okay, So obviously we have a whole bunch of variables for the different colors.
// This only supports 22 colors for a single sprite.  If you need more colors, download the new and
// Improved version.
// Available here: http://gmc.yoyogames.com/index.php?showtopic=587773&page=1

uniform vec3 f_Colour0;uniform vec3 f_Colour1;uniform vec3 f_Colour2;uniform vec3 f_Colour3;
uniform vec3 f_Colour4;uniform vec3 f_Colour5;uniform vec3 f_Colour6;uniform vec3 f_Colour7;
uniform vec3 f_Colour8;uniform vec3 f_Colour9;uniform vec3 f_Colour10;uniform vec3 f_Colour11;
uniform vec3 f_Colour12;uniform vec3 f_Colour13;uniform vec3 f_Colour14;uniform vec3 f_Colour15;
uniform vec3 f_Colour16;uniform vec3 f_Colour17;uniform vec3 f_Colour18;uniform vec3 f_Colour19;
uniform vec3 f_Colour20;uniform vec3 f_Colour21;

uniform vec3 s_Colour0;uniform vec3 s_Colour1;uniform vec3 s_Colour2;uniform vec3 s_Colour3;
uniform vec3 s_Colour4;uniform vec3 s_Colour5;uniform vec3 s_Colour6;uniform vec3 s_Colour7;
uniform vec3 s_Colour8;uniform vec3 s_Colour9;uniform vec3 s_Colour10;uniform vec3 s_Colour11;
uniform vec3 s_Colour12;uniform vec3 s_Colour13;uniform vec3 s_Colour14;uniform vec3 s_Colour15;
uniform vec3 s_Colour16;uniform vec3 s_Colour17;uniform vec3 s_Colour18;uniform vec3 s_Colour19;
uniform vec3 s_Colour20;uniform vec3 s_Colour21;

varying vec2 v_vTexcoord;
varying vec4 v_vColour;

float ColorDistance(vec3 c1, vec3 c2)
{
    return distance(c1.rgb, c2.rgb);
}

void main()
{
    float threshold = 0.00000005;
    vec4 col = texture2D( gm_BaseTexture, v_vTexcoord );

    if(ColorDistance(col.rgb, f_Colour0.rgb) <=threshold )
        col.rgb = s_Colour0.rgb;
    else if(ColorDistance(col.rgb, f_Colour1.rgb) <=threshold )
        col.rgb = s_Colour1.rgb;
    else if(ColorDistance(col.rgb, f_Colour2.rgb) <=threshold )
        col.rgb = s_Colour2.rgb;
    else if(ColorDistance(col.rgb, f_Colour3.rgb) <=threshold )
        col.rgb = s_Colour3.rgb;
    else if(ColorDistance(col.rgb, f_Colour4.rgb) <=threshold )
        col.rgb = s_Colour4.rgb;
    else if(ColorDistance(col.rgb, f_Colour5.rgb) <=threshold )
        col.rgb = s_Colour5.rgb;
    else if(ColorDistance(col.rgb, f_Colour6.rgb) <=threshold )
        col.rgb = s_Colour6.rgb;
    else if(ColorDistance(col.rgb, f_Colour7.rgb) <=threshold )
        col.rgb = s_Colour7.rgb;
    else if(ColorDistance(col.rgb, f_Colour8.rgb) <=threshold )
        col.rgb = s_Colour8.rgb;
    else if(ColorDistance(col.rgb, f_Colour9.rgb) <=threshold )
        col.rgb = s_Colour9.rgb;
    else if(ColorDistance(col.rgb, f_Colour10.rgb) <=threshold )
        col.rgb = s_Colour10.rgb;
    else if(ColorDistance(col.rgb, f_Colour11.rgb) <=threshold )
        col.rgb = s_Colour11.rgb;
    else if(ColorDistance(col.rgb, f_Colour12.rgb) <=threshold )
        col.rgb = s_Colour12.rgb;
    else if(ColorDistance(col.rgb, f_Colour13.rgb) <=threshold )
        col.rgb = s_Colour13.rgb;
    else if(ColorDistance(col.rgb, f_Colour14.rgb) <=threshold )
        col.rgb = s_Colour14.rgb;
    else if(ColorDistance(col.rgb, f_Colour15.rgb) <=threshold )
        col.rgb = s_Colour15.rgb;
    else if(ColorDistance(col.rgb, f_Colour16.rgb) <=threshold )
        col.rgb = s_Colour16.rgb;
    else if(ColorDistance(col.rgb, f_Colour17.rgb) <=threshold )
        col.rgb = s_Colour17.rgb;
    else if(ColorDistance(col.rgb, f_Colour18.rgb) <=threshold )
        col.rgb = s_Colour18.rgb;
    else if(ColorDistance(col.rgb, f_Colour19.rgb) <=threshold )
        col.rgb = s_Colour19.rgb;
    else if(ColorDistance(col.rgb, f_Colour20.rgb) <=threshold )
        col.rgb = s_Colour20.rgb;
    else if(ColorDistance(col.rgb, f_Colour21.rgb) <=threshold )
        col.rgb = s_Colour21.rgb;
    
    gl_FragColor = v_vColour * col;
    
}

{
  "compression": 1,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_SmallBreak",
  "duration": 0.302166,
  "parent": {
    "name": "SpiderBelly",
    "path": "folders/Sounds/Enemy/SpiderBelly.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_SmallBreak",
  "tags": [],
  "resourceType": "GMSound",
}
{
  "compression": 1,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_Punch.wav",
  "duration": 0.235295,
  "parent": {
    "name": "Player",
    "path": "folders/Sounds/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_Punch",
  "tags": [],
  "resourceType": "GMSound",
}
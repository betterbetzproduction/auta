{
  "compression": 1,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_WandUp",
  "duration": 0.588175,
  "parent": {
    "name": "Items",
    "path": "folders/Sounds/Items.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_WandUp",
  "tags": [],
  "resourceType": "GMSound",
}
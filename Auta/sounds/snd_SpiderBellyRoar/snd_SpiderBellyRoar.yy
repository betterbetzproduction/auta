{
  "compression": 1,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_SpiderBellyRoar",
  "duration": 0.519195,
  "parent": {
    "name": "SpiderBelly",
    "path": "folders/Sounds/Enemy/SpiderBelly.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_SpiderBellyRoar",
  "tags": [],
  "resourceType": "GMSound",
}
{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_OrbThrow",
  "duration": 0.187857,
  "parent": {
    "name": "Player",
    "path": "folders/Sounds/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_OrbThrow",
  "tags": [],
  "resourceType": "GMSound",
}
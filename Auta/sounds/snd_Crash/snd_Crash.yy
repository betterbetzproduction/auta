{
  "compression": 1,
  "volume": 0.24,
  "preload": true,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_Crash",
  "duration": 0.558333,
  "parent": {
    "name": "Player",
    "path": "folders/Sounds/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_Crash",
  "tags": [],
  "resourceType": "GMSound",
}
{
  "compression": 3,
  "volume": 0.14,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "mus_HustlePW.mp3",
  "duration": 118.7054,
  "parent": {
    "name": "Music",
    "path": "folders/Sounds/Music.yy",
  },
  "resourceVersion": "1.0",
  "name": "mus_HustlePW",
  "tags": [],
  "resourceType": "GMSound",
}
{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_BreakBG",
  "duration": 0.163526,
  "parent": {
    "name": "Objects",
    "path": "folders/Sounds/Environment/Objects.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_BreakBG",
  "tags": [],
  "resourceType": "GMSound",
}
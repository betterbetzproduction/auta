{
  "compression": 1,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_CH_Suspense.mp3",
  "duration": 5.789,
  "parent": {
    "name": "Cutscene",
    "path": "folders/Sounds/Cutscene.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_CH_Suspense",
  "tags": [],
  "resourceType": "GMSound",
}
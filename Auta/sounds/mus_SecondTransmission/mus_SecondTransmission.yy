{
  "compression": 3,
  "volume": 0.35,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "mus_SecondTransmission.mp3",
  "duration": 185.108673,
  "parent": {
    "name": "Music",
    "path": "folders/Sounds/Music.yy",
  },
  "resourceVersion": "1.0",
  "name": "mus_SecondTransmission",
  "tags": [],
  "resourceType": "GMSound",
}
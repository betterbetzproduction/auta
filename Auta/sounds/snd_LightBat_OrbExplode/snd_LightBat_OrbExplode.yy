{
  "compression": 1,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_LightBat_OrbExplode",
  "duration": 3.487993,
  "parent": {
    "name": "Attacks",
    "path": "folders/Sounds/Attacks.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_LightBat_OrbExplode",
  "tags": [],
  "resourceType": "GMSound",
}
{
  "compression": 3,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_Err.wav",
  "duration": 0.243617,
  "parent": {
    "name": "Objects",
    "path": "folders/Sounds/Environment/Objects.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_Err",
  "tags": [],
  "resourceType": "GMSound",
}
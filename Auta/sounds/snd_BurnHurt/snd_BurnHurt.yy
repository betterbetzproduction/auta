{
  "compression": 1,
  "volume": 0.68,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_BurnHurt",
  "duration": 0.399444,
  "parent": {
    "name": "snd_BurnHurt",
    "path": "folders/Sounds/Player/Attack/snd_BurnHurt.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_BurnHurt",
  "tags": [],
  "resourceType": "GMSound",
}
{
  "compression": 1,
  "volume": 0.73,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_Lightbat_Charge",
  "duration": 4.532891,
  "parent": {
    "name": "Attacks",
    "path": "folders/Sounds/Attacks.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_Lightbat_Charge",
  "tags": [],
  "resourceType": "GMSound",
}
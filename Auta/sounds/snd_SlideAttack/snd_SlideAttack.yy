{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_SlideAttack.wav",
  "duration": 0.277041,
  "parent": {
    "name": "Attack",
    "path": "folders/Sounds/Player/Attack.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_SlideAttack",
  "tags": [],
  "resourceType": "GMSound",
}
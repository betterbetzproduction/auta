{
  "compression": 1,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_NoHurt",
  "duration": 0.370034,
  "parent": {
    "name": "Enemy",
    "path": "folders/Sounds/Enemy.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_NoHurt",
  "tags": [],
  "resourceType": "GMSound",
}
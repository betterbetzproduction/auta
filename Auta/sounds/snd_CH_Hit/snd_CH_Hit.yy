{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_CH_Hit.wav",
  "duration": 5.111395,
  "parent": {
    "name": "Cutscene",
    "path": "folders/Sounds/Cutscene.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_CH_Hit",
  "tags": [],
  "resourceType": "GMSound",
}
{
  "compression": 1,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_EnemyHurt",
  "duration": 0.22076,
  "parent": {
    "name": "Enemy",
    "path": "folders/Sounds/Enemy.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_EnemyHurt",
  "tags": [],
  "resourceType": "GMSound",
}
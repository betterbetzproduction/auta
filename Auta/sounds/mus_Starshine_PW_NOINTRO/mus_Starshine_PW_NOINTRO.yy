{
  "compression": 3,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "mus_Starshine_PW_NOINTRO",
  "duration": 93.52337,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "mus_Starshine_PW_NOINTRO",
  "tags": [],
  "resourceType": "GMSound",
}
{
  "compression": 1,
  "volume": 0.74,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_MagicSlice",
  "duration": 0.317585,
  "parent": {
    "name": "Attack",
    "path": "folders/Sounds/Player/Attack.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_MagicSlice",
  "tags": [],
  "resourceType": "GMSound",
}
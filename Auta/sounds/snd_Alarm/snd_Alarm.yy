{
  "compression": 1,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_Alarm",
  "duration": 39.317,
  "parent": {
    "name": "Environment",
    "path": "folders/Sounds/Environment.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_Alarm",
  "tags": [],
  "resourceType": "GMSound",
}
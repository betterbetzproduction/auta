
if global.storyBeat < -2 {
	////////////////////////////////////
	type[0] =	"Terminal Activated ~~~~~~~~~~~~~~~"
	type[1] =	@"Running system diagnostics..."
	type[2] =	@"Interstellar Teleportation Matrix:
	Online
	General Power Grid:
	Online
	Elevator Power Grid:
	Offline [Please restart breakers!]
	Days since maintenence visit:
	10.4 yrs [Maintenence visit overdue!]"
	type[3] =	@"Terminal Closed ~~~~~~~~~~~~~~~"
} else {
	////////////////////////////////////
	type[0] =	"Terminal Activated ~~~~~~~~~~~~~~~"
	type[1] =	@"Running system diagnostics..."
	type[2] =	@"Interstellar Teleportation Matrix:
	Online
	General Power Grid:
	Online
	Elevator Power Grid:
	Online
	Days since maintenence visit:
	10.4 yrs [Maintenence visit overdue!]"
	type[3] =	@"Terminal Closed ~~~~~~~~~~~~~~~"
	
	layer_set_visible(layer_get_id("B4Elev1"),false)
	layer_set_visible(layer_get_id("B4Elev2"),true)
	layer_set_visible(layer_get_id("AfElev1"),false)
	layer_set_visible(layer_get_id("AfElev2"),true)
	
	instance_destroy(inst_5E553224)
}
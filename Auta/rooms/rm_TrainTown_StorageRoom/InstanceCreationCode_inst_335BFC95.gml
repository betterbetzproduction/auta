
if global.storyBeat = 5 {
	state = "cutscene"
	global.cutscene = true
	timeline_index = tl_StorageRoom
	timeline_running = true
	timeline_position = 0
	timeline_speed = 1

	animstate="override"
	audio_play_sound(snd_Crash,4,false)
	sprite_index = spr_AutaCollapse
}


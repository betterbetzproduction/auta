vWidth = 320
vHeight = 180

var vm = matrix_build_lookat(x,y,-10,x,y,0,0,1,0);
var pm = matrix_build_projection_ortho(vWidth,vHeight,1,10000);

camera_set_view_mat(camera,vm);
camera_set_proj_mat(camera,pm);

global.screenshake = -0.3
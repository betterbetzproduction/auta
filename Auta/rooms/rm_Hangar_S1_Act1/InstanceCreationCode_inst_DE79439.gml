
////////////////////////////////////
type[0] =	"Terminal Activated ~~~~~~~~~~~~~~~"
type[1] =	@"Your location: Eclipse Interstellar
Transport connection zone.
Train Status: Delayed by 256 hrs.
Last admin login: T-5 years."
type[2] =	@"Relaying most recent admin log..."
type[3] =	@"The station is in utter disrepair.
By all measure, it ought to be shut
down immediately. But, at this point
we're all aware that the state of
things here is simply to keep
travellers away."
type[4] =	@"The number of trains in effect
is decreasing by the day. Trains 
have been leaving the station 
regularly, and never being seen 
again."
type[5] =	@"I suspect this is also
a ploy to keep travellers out, but
this seems like a strangely lowkey
way to accomplish that."
type[6] =	@"Terminal Closed ~~~~~~~~~~~~~~~"
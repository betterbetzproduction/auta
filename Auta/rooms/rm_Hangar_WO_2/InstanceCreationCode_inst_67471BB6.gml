camera_destroy(camera);
camera = camera_create();
vWidth = 368
vHeight = 207

var vm = matrix_build_lookat(x,y,-10,x,y,0,0,1,0);
var pm = matrix_build_projection_ortho(vWidth,vHeight,1,10000);

camera_set_view_mat(camera,vm);
camera_set_proj_mat(camera,pm);

view_camera[0] = camera;

follow = obj_PlayerParent;

xTo = x
yTo = y

xmin = 0+vWidth/2
xmax = room_width-vWidth/2
ymin = 0+vHeight/2
ymax = room_height-vHeight/2
x = obj_PlayerParent.x
y = obj_PlayerParent.y
x = clamp(x,xmin,xmax)
y = clamp(y,ymin,ymax)

standardfollow = true
{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 7,
  "bbox_right": 22,
  "bbox_top": 8,
  "bbox_bottom": 23,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 30,
  "height": 50,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 8,
  "gridY": 8,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"2be0ea37-c468-4af7-ae45-119e78e2c9a7","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2be0ea37-c468-4af7-ae45-119e78e2c9a7","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":{"name":"b996a66a-7e14-4c1e-8d6d-ccd57ef9624f","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"2be0ea37-c468-4af7-ae45-119e78e2c9a7","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":{"name":"da80e996-d4d0-4253-9688-e8104afb2876","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"2be0ea37-c468-4af7-ae45-119e78e2c9a7","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":{"name":"336fff77-794a-4a96-8dad-fef45bffcc88","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Propeller","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"2be0ea37-c468-4af7-ae45-119e78e2c9a7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"10b85b77-356a-4b4b-8eda-8c94f803a9ef","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"10b85b77-356a-4b4b-8eda-8c94f803a9ef","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":{"name":"b996a66a-7e14-4c1e-8d6d-ccd57ef9624f","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"10b85b77-356a-4b4b-8eda-8c94f803a9ef","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":{"name":"da80e996-d4d0-4253-9688-e8104afb2876","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"10b85b77-356a-4b4b-8eda-8c94f803a9ef","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":{"name":"336fff77-794a-4a96-8dad-fef45bffcc88","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Propeller","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"10b85b77-356a-4b4b-8eda-8c94f803a9ef","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eebcf76f-e8fb-4807-ac3f-f82d6c86fda3","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eebcf76f-e8fb-4807-ac3f-f82d6c86fda3","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":{"name":"b996a66a-7e14-4c1e-8d6d-ccd57ef9624f","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"eebcf76f-e8fb-4807-ac3f-f82d6c86fda3","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":{"name":"da80e996-d4d0-4253-9688-e8104afb2876","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"eebcf76f-e8fb-4807-ac3f-f82d6c86fda3","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":{"name":"336fff77-794a-4a96-8dad-fef45bffcc88","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Propeller","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"eebcf76f-e8fb-4807-ac3f-f82d6c86fda3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"674b84d7-445f-49e2-b395-70e4772ece16","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"674b84d7-445f-49e2-b395-70e4772ece16","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":{"name":"b996a66a-7e14-4c1e-8d6d-ccd57ef9624f","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"674b84d7-445f-49e2-b395-70e4772ece16","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":{"name":"da80e996-d4d0-4253-9688-e8104afb2876","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"674b84d7-445f-49e2-b395-70e4772ece16","path":"sprites/spr_Propeller/spr_Propeller.yy",},"LayerId":{"name":"336fff77-794a-4a96-8dad-fef45bffcc88","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Propeller","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","name":"674b84d7-445f-49e2-b395-70e4772ece16","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Propeller","path":"sprites/spr_Propeller/spr_Propeller.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"8c755a03-23e5-4cae-9ae0-ab6b17025bc5","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2be0ea37-c468-4af7-ae45-119e78e2c9a7","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"407857fd-89de-4ead-a011-8248e104831e","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"10b85b77-356a-4b4b-8eda-8c94f803a9ef","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"16c03040-0af0-4baa-812f-775687c732ff","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eebcf76f-e8fb-4807-ac3f-f82d6c86fda3","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d1eea5c4-48b7-4644-b213-d240c78f96e1","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"674b84d7-445f-49e2-b395-70e4772ece16","path":"sprites/spr_Propeller/spr_Propeller.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 14,
    "yorigin": 17,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Propeller","path":"sprites/spr_Propeller/spr_Propeller.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"336fff77-794a-4a96-8dad-fef45bffcc88","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"b996a66a-7e14-4c1e-8d6d-ccd57ef9624f","tags":[],"resourceType":"GMImageLayer",},
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"da80e996-d4d0-4253-9688-e8104afb2876","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Grab",
    "path": "folders/Sprites/Sprites/Game/Characters/Auta/Grab.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Propeller",
  "tags": [],
  "resourceType": "GMSprite",
}
{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 10,
  "bbox_top": 0,
  "bbox_bottom": 10,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 11,
  "height": 11,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"9bfb1a07-eafc-49af-8c17-bb680b32c8ff","path":"sprites/spr_DuoDroid_Walker/spr_DuoDroid_Walker.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9bfb1a07-eafc-49af-8c17-bb680b32c8ff","path":"sprites/spr_DuoDroid_Walker/spr_DuoDroid_Walker.yy",},"LayerId":{"name":"47454008-d949-4b1a-8287-67601c65ea35","path":"sprites/spr_DuoDroid_Walker/spr_DuoDroid_Walker.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DuoDroid_Walker","path":"sprites/spr_DuoDroid_Walker/spr_DuoDroid_Walker.yy",},"resourceVersion":"1.0","name":"9bfb1a07-eafc-49af-8c17-bb680b32c8ff","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_DuoDroid_Walker","path":"sprites/spr_DuoDroid_Walker/spr_DuoDroid_Walker.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"1daa15a1-9891-4b1c-9c5c-c7f2d92ab301","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9bfb1a07-eafc-49af-8c17-bb680b32c8ff","path":"sprites/spr_DuoDroid_Walker/spr_DuoDroid_Walker.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 5,
    "yorigin": 5,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_DuoDroid_Walker","path":"sprites/spr_DuoDroid_Walker/spr_DuoDroid_Walker.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"47454008-d949-4b1a-8287-67601c65ea35","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "DuoDroid",
    "path": "folders/Sprites/Sprites/Game/Enemies/Hangar/DuoDroid.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_DuoDroid_Walker",
  "tags": [],
  "resourceType": "GMSprite",
}
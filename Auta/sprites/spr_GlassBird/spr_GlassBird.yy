{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 5,
  "bbox_right": 25,
  "bbox_top": 10,
  "bbox_bottom": 22,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"3b58a48f-ee40-4826-b69d-e639b85c41d2","path":"sprites/spr_GlassBird/spr_GlassBird.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3b58a48f-ee40-4826-b69d-e639b85c41d2","path":"sprites/spr_GlassBird/spr_GlassBird.yy",},"LayerId":{"name":"4554112a-5f12-4131-8897-256508fb2910","path":"sprites/spr_GlassBird/spr_GlassBird.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"3b58a48f-ee40-4826-b69d-e639b85c41d2","path":"sprites/spr_GlassBird/spr_GlassBird.yy",},"LayerId":{"name":"f4ed5b86-503b-4988-bf97-f8d95871ad25","path":"sprites/spr_GlassBird/spr_GlassBird.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GlassBird","path":"sprites/spr_GlassBird/spr_GlassBird.yy",},"resourceVersion":"1.0","name":"3b58a48f-ee40-4826-b69d-e639b85c41d2","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_GlassBird","path":"sprites/spr_GlassBird/spr_GlassBird.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"df69255e-9244-445f-8586-d613a61f457e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3b58a48f-ee40-4826-b69d-e639b85c41d2","path":"sprites/spr_GlassBird/spr_GlassBird.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 15,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_GlassBird","path":"sprites/spr_GlassBird/spr_GlassBird.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"4554112a-5f12-4131-8897-256508fb2910","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"f4ed5b86-503b-4988-bf97-f8d95871ad25","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GlassBird",
    "path": "folders/Sprites/Sprites/Game/Enemies/GlassBird.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_GlassBird",
  "tags": [],
  "resourceType": "GMSprite",
}
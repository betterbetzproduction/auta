{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 16,
  "bbox_right": 79,
  "bbox_top": 11,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"7177f5d2-0bf9-4e2f-bc18-e86c38b612df","path":"sprites/spr_TrainTown_IntDecor/spr_TrainTown_IntDecor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7177f5d2-0bf9-4e2f-bc18-e86c38b612df","path":"sprites/spr_TrainTown_IntDecor/spr_TrainTown_IntDecor.yy",},"LayerId":{"name":"c1c6aa12-ca21-4856-9bb4-d8cbffa1755d","path":"sprites/spr_TrainTown_IntDecor/spr_TrainTown_IntDecor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TrainTown_IntDecor","path":"sprites/spr_TrainTown_IntDecor/spr_TrainTown_IntDecor.yy",},"resourceVersion":"1.0","name":"7177f5d2-0bf9-4e2f-bc18-e86c38b612df","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_TrainTown_IntDecor","path":"sprites/spr_TrainTown_IntDecor/spr_TrainTown_IntDecor.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"767e05a5-6b53-4b70-bcb5-f12213d3a7a5","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7177f5d2-0bf9-4e2f-bc18-e86c38b612df","path":"sprites/spr_TrainTown_IntDecor/spr_TrainTown_IntDecor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_TrainTown_IntDecor","path":"sprites/spr_TrainTown_IntDecor/spr_TrainTown_IntDecor.yy",},
    "resourceVersion": "1.3",
    "name": "spr_TrainTown_IntDecor",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"c1c6aa12-ca21-4856-9bb4-d8cbffa1755d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Tilesets",
    "path": "folders/Sprites/Sprites/Game/Tilesets.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_TrainTown_IntDecor",
  "tags": [],
  "resourceType": "GMSprite",
}
{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 5,
  "bbox_right": 13,
  "bbox_top": 4,
  "bbox_bottom": 25,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 30,
  "height": 30,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 8,
  "gridY": 8,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"5cc7b30e-aa71-4b47-91d3-03926b5ef477","path":"sprites/spr_AutaCrouch/spr_AutaCrouch.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5cc7b30e-aa71-4b47-91d3-03926b5ef477","path":"sprites/spr_AutaCrouch/spr_AutaCrouch.yy",},"LayerId":{"name":"0dbb0791-146c-4f74-906d-963f9fc4b321","path":"sprites/spr_AutaCrouch/spr_AutaCrouch.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"5cc7b30e-aa71-4b47-91d3-03926b5ef477","path":"sprites/spr_AutaCrouch/spr_AutaCrouch.yy",},"LayerId":{"name":"c83db71e-56a1-4499-a65e-0bbcd1452efc","path":"sprites/spr_AutaCrouch/spr_AutaCrouch.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaCrouch","path":"sprites/spr_AutaCrouch/spr_AutaCrouch.yy",},"resourceVersion":"1.0","name":"5cc7b30e-aa71-4b47-91d3-03926b5ef477","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_AutaCrouch","path":"sprites/spr_AutaCrouch/spr_AutaCrouch.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"1ed7055c-ba59-4885-8f34-a65ce230ca9d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5cc7b30e-aa71-4b47-91d3-03926b5ef477","path":"sprites/spr_AutaCrouch/spr_AutaCrouch.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 9,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_AutaCrouch","path":"sprites/spr_AutaCrouch/spr_AutaCrouch.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"0dbb0791-146c-4f74-906d-963f9fc4b321","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":51.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"c83db71e-56a1-4499-a65e-0bbcd1452efc","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Regular",
    "path": "folders/Sprites/Sprites/Game/Characters/Auta/Regular.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_AutaCrouch",
  "tags": [],
  "resourceType": "GMSprite",
}
{
  "bboxMode": 0,
  "collisionKind": 4,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 15,
  "bbox_top": 0,
  "bbox_bottom": 15,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 16,
  "height": 16,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"b83e98e1-2e12-4d01-875e-4bb252979c68","path":"sprites/spr_SlopeCollision/spr_SlopeCollision.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b83e98e1-2e12-4d01-875e-4bb252979c68","path":"sprites/spr_SlopeCollision/spr_SlopeCollision.yy",},"LayerId":{"name":"8b9455b5-6b8c-48f6-a0e0-ce277f512a69","path":"sprites/spr_SlopeCollision/spr_SlopeCollision.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"b83e98e1-2e12-4d01-875e-4bb252979c68","path":"sprites/spr_SlopeCollision/spr_SlopeCollision.yy",},"LayerId":{"name":"b0e4b68f-8a4e-4047-b6ad-a1b975e8e951","path":"sprites/spr_SlopeCollision/spr_SlopeCollision.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_SlopeCollision","path":"sprites/spr_SlopeCollision/spr_SlopeCollision.yy",},"resourceVersion":"1.0","name":"b83e98e1-2e12-4d01-875e-4bb252979c68","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_SlopeCollision","path":"sprites/spr_SlopeCollision/spr_SlopeCollision.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7e2bf5f9-421e-4b0c-bced-ab67d8c7360c","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b83e98e1-2e12-4d01-875e-4bb252979c68","path":"sprites/spr_SlopeCollision/spr_SlopeCollision.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_SlopeCollision","path":"sprites/spr_SlopeCollision/spr_SlopeCollision.yy",},
    "resourceVersion": "1.3",
    "name": "spr_SlopeCollision",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"8b9455b5-6b8c-48f6-a0e0-ce277f512a69","tags":[],"resourceType":"GMImageLayer",},
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"b0e4b68f-8a4e-4047-b6ad-a1b975e8e951","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Collision",
    "path": "folders/Sprites/Sprites/Game/Collision.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_SlopeCollision",
  "tags": [],
  "resourceType": "GMSprite",
}
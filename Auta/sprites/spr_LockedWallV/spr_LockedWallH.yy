{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 15,
  "bbox_top": 0,
  "bbox_bottom": 79,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 16,
  "height": 80,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"12dc7a34-ef5e-440e-9eeb-e5cb15d00245","path":"sprites/spr_LockedWallH/spr_LockedWallH.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"12dc7a34-ef5e-440e-9eeb-e5cb15d00245","path":"sprites/spr_LockedWallH/spr_LockedWallH.yy",},"LayerId":{"name":"633443fa-a117-4599-80cb-1b1a594c1a04","path":"sprites/spr_LockedWallH/spr_LockedWallH.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"12dc7a34-ef5e-440e-9eeb-e5cb15d00245","path":"sprites/spr_LockedWallH/spr_LockedWallH.yy",},"LayerId":{"name":"26eb8771-caa1-4803-8de0-10649845ee07","path":"sprites/spr_LockedWallH/spr_LockedWallH.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"12dc7a34-ef5e-440e-9eeb-e5cb15d00245","path":"sprites/spr_LockedWallH/spr_LockedWallH.yy",},"LayerId":{"name":"003afb8f-90b0-4f07-aa5f-b75479dc8af3","path":"sprites/spr_LockedWallH/spr_LockedWallH.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_LockedWallH","path":"sprites/spr_LockedWallH/spr_LockedWallH.yy",},"resourceVersion":"1.0","name":"12dc7a34-ef5e-440e-9eeb-e5cb15d00245","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_LockedWallH","path":"sprites/spr_LockedWallH/spr_LockedWallH.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"286b1028-ee92-4187-a81d-2f8bf221afc7","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"12dc7a34-ef5e-440e-9eeb-e5cb15d00245","path":"sprites/spr_LockedWallH/spr_LockedWallH.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_LockedWallH","path":"sprites/spr_LockedWallH/spr_LockedWallH.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"633443fa-a117-4599-80cb-1b1a594c1a04","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"26eb8771-caa1-4803-8de0-10649845ee07","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"003afb8f-90b0-4f07-aa5f-b75479dc8af3","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "TeleportChamber",
    "path": "folders/Sprites/Sprites/Game/BG/Backgrounds/TeleportChamber.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_LockedWallH",
  "tags": [],
  "resourceType": "GMSprite",
}
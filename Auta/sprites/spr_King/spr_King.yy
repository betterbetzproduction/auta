{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 29,
  "bbox_top": 0,
  "bbox_bottom": 25,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 30,
  "height": 26,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"053dff1a-2166-4658-a613-e6e736112072","path":"sprites/spr_King/spr_King.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"053dff1a-2166-4658-a613-e6e736112072","path":"sprites/spr_King/spr_King.yy",},"LayerId":{"name":"ea6cc09f-5b36-423c-aadf-dde12a5c324e","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"053dff1a-2166-4658-a613-e6e736112072","path":"sprites/spr_King/spr_King.yy",},"LayerId":{"name":"01761667-de9f-4613-ad45-d9bcd501d38d","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"053dff1a-2166-4658-a613-e6e736112072","path":"sprites/spr_King/spr_King.yy",},"LayerId":{"name":"6d4bf2c6-f46c-47fb-a7bf-77bdfcba7d8c","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_King","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"053dff1a-2166-4658-a613-e6e736112072","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2598bdac-9a65-4e75-b413-e839d13d1065","path":"sprites/spr_King/spr_King.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2598bdac-9a65-4e75-b413-e839d13d1065","path":"sprites/spr_King/spr_King.yy",},"LayerId":{"name":"ea6cc09f-5b36-423c-aadf-dde12a5c324e","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"2598bdac-9a65-4e75-b413-e839d13d1065","path":"sprites/spr_King/spr_King.yy",},"LayerId":{"name":"01761667-de9f-4613-ad45-d9bcd501d38d","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"2598bdac-9a65-4e75-b413-e839d13d1065","path":"sprites/spr_King/spr_King.yy",},"LayerId":{"name":"6d4bf2c6-f46c-47fb-a7bf-77bdfcba7d8c","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_King","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"2598bdac-9a65-4e75-b413-e839d13d1065","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c3a9741b-1364-4309-b267-126666ed7040","path":"sprites/spr_King/spr_King.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c3a9741b-1364-4309-b267-126666ed7040","path":"sprites/spr_King/spr_King.yy",},"LayerId":{"name":"ea6cc09f-5b36-423c-aadf-dde12a5c324e","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"c3a9741b-1364-4309-b267-126666ed7040","path":"sprites/spr_King/spr_King.yy",},"LayerId":{"name":"01761667-de9f-4613-ad45-d9bcd501d38d","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"c3a9741b-1364-4309-b267-126666ed7040","path":"sprites/spr_King/spr_King.yy",},"LayerId":{"name":"6d4bf2c6-f46c-47fb-a7bf-77bdfcba7d8c","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_King","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"c3a9741b-1364-4309-b267-126666ed7040","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6b36099d-0240-4b30-a597-f8d3128b208e","path":"sprites/spr_King/spr_King.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6b36099d-0240-4b30-a597-f8d3128b208e","path":"sprites/spr_King/spr_King.yy",},"LayerId":{"name":"ea6cc09f-5b36-423c-aadf-dde12a5c324e","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"6b36099d-0240-4b30-a597-f8d3128b208e","path":"sprites/spr_King/spr_King.yy",},"LayerId":{"name":"01761667-de9f-4613-ad45-d9bcd501d38d","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"6b36099d-0240-4b30-a597-f8d3128b208e","path":"sprites/spr_King/spr_King.yy",},"LayerId":{"name":"6d4bf2c6-f46c-47fb-a7bf-77bdfcba7d8c","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_King","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","name":"6b36099d-0240-4b30-a597-f8d3128b208e","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_King","path":"sprites/spr_King/spr_King.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 5.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"fbc99097-de02-47c8-ac90-7032847603fe","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"053dff1a-2166-4658-a613-e6e736112072","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2bb9132c-48c2-43b8-a66d-018bedd72520","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2598bdac-9a65-4e75-b413-e839d13d1065","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f6f9adab-c0e4-409c-8406-c31bb923d00d","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c3a9741b-1364-4309-b267-126666ed7040","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"00385eaf-eef8-4df3-80c8-33f0de7a8c3a","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6b36099d-0240-4b30-a597-f8d3128b208e","path":"sprites/spr_King/spr_King.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 22,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_King","path":"sprites/spr_King/spr_King.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"6d4bf2c6-f46c-47fb-a7bf-77bdfcba7d8c","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"ea6cc09f-5b36-423c-aadf-dde12a5c324e","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"01761667-de9f-4613-ad45-d9bcd501d38d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "King",
    "path": "folders/Sprites/Sprites/Game/Characters/NPCs/King.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_King",
  "tags": [],
  "resourceType": "GMSprite",
}
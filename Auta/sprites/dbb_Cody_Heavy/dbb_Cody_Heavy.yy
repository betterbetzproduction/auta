{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 25,
  "bbox_top": 0,
  "bbox_bottom": 19,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 26,
  "height": 20,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"517862d1-3ec8-46cd-8901-e42492b9e3f3","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"517862d1-3ec8-46cd-8901-e42492b9e3f3","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"LayerId":{"name":"38d86717-7b9a-42e9-8028-cabba27e0bfc","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Cody_Heavy","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","name":"517862d1-3ec8-46cd-8901-e42492b9e3f3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"405f80f9-19e7-4b05-8209-7fba27494d8a","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"405f80f9-19e7-4b05-8209-7fba27494d8a","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"LayerId":{"name":"38d86717-7b9a-42e9-8028-cabba27e0bfc","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Cody_Heavy","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","name":"405f80f9-19e7-4b05-8209-7fba27494d8a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cd759091-f5a6-43d8-9259-61afe0494383","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cd759091-f5a6-43d8-9259-61afe0494383","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"LayerId":{"name":"38d86717-7b9a-42e9-8028-cabba27e0bfc","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Cody_Heavy","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","name":"cd759091-f5a6-43d8-9259-61afe0494383","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"466c0cc5-e6e1-48b3-8aaf-4667d94fd3d0","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"466c0cc5-e6e1-48b3-8aaf-4667d94fd3d0","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"LayerId":{"name":"38d86717-7b9a-42e9-8028-cabba27e0bfc","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Cody_Heavy","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","name":"466c0cc5-e6e1-48b3-8aaf-4667d94fd3d0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5ccc62dd-4e69-47ee-9065-4cc50be48e49","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5ccc62dd-4e69-47ee-9065-4cc50be48e49","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"LayerId":{"name":"38d86717-7b9a-42e9-8028-cabba27e0bfc","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Cody_Heavy","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","name":"5ccc62dd-4e69-47ee-9065-4cc50be48e49","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0d73e510-edc2-4c05-b741-4e6ebee00aa9","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0d73e510-edc2-4c05-b741-4e6ebee00aa9","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"LayerId":{"name":"38d86717-7b9a-42e9-8028-cabba27e0bfc","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Cody_Heavy","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","name":"0d73e510-edc2-4c05-b741-4e6ebee00aa9","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"dbb_Cody_Heavy","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"d7bcf68e-8fbc-4b92-90cd-68d9d32e2b6b","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"517862d1-3ec8-46cd-8901-e42492b9e3f3","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"da628206-cb0a-47ae-89ee-46b8d48cc0b3","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"405f80f9-19e7-4b05-8209-7fba27494d8a","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1e92a0f8-0b12-414b-ae09-45e7a8025ea2","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cd759091-f5a6-43d8-9259-61afe0494383","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6cfdd60e-09b1-4b15-b366-e7f5cfc0a45d","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"466c0cc5-e6e1-48b3-8aaf-4667d94fd3d0","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"020aca7d-f42d-43f8-a46a-a7609a62f2f7","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5ccc62dd-4e69-47ee-9065-4cc50be48e49","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"19b48b40-5896-41c5-b013-a6f56ac0d925","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0d73e510-edc2-4c05-b741-4e6ebee00aa9","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"dbb_Cody_Heavy","path":"sprites/dbb_Cody_Heavy/dbb_Cody_Heavy.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"38d86717-7b9a-42e9-8028-cabba27e0bfc","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Cody",
    "path": "folders/Sequences/Sprites/SuperDevBrosBrawl/Cody.yy",
  },
  "resourceVersion": "1.0",
  "name": "dbb_Cody_Heavy",
  "tags": [],
  "resourceType": "GMSprite",
}
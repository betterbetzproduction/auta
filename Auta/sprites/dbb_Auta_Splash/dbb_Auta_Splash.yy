{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 41,
  "bbox_right": 142,
  "bbox_top": 3,
  "bbox_bottom": 90,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 195,
  "height": 100,
  "textureGroupId": {
    "name": "Unused",
    "path": "texturegroups/Unused",
  },
  "swatchColours": [
    4278190335,
    4278255615,
    4278255360,
    4294967040,
    4294901760,
    4294902015,
    4294967295,
    4293717228,
    4293059298,
    4292335575,
    4291677645,
    4290230199,
    4287993237,
    4280556782,
    4278252287,
    4283540992,
    4293963264,
    4287770926,
    4287365357,
    4287203721,
    4286414205,
    4285558896,
    4284703587,
    4283782485,
    4281742902,
    4278190080,
    4286158839,
    4286688762,
    4287219453,
    4288280831,
    4288405444,
    4288468131,
    4288465538,
    4291349882,
    4294430829,
    4292454269,
    4291466115,
    4290675079,
    4290743485,
    4290943732,
    4288518390,
    4283395315,
    4283862775,
    4284329979,
    4285068799,
    4285781164,
    4285973884,
    4286101564,
    4290034460,
    4294164224,
    4291529796,
    4289289312,
    4289290373,
    4289291432,
    4289359601,
    4286410226,
    4280556782,
    4280444402,
    4280128760,
    4278252287,
    4282369933,
    4283086137,
    4283540992,
    4288522496,
    4293963264,
    4290540032,
    4289423360,
    4289090560,
    4287770926,
    4287704422,
    4287571858,
    4287365357,
    4284159214,
    4279176094,
    4279058848,
    4278870691,
    4278231211,
    4281367321,
  ],
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e83326ae-b0ec-4495-b0e4-1df67502e500","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e83326ae-b0ec-4495-b0e4-1df67502e500","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},"LayerId":{"name":"a3ac47ad-7cb0-421f-8227-a42cdc31ade4","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e83326ae-b0ec-4495-b0e4-1df67502e500","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},"LayerId":{"name":"7bf0703c-1961-4316-9e7b-355e1fc5f832","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e83326ae-b0ec-4495-b0e4-1df67502e500","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},"LayerId":{"name":"6d28d5a4-709b-4e17-9647-c8f5be488b52","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e83326ae-b0ec-4495-b0e4-1df67502e500","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},"LayerId":{"name":"d4641723-a1db-4ef2-9cf1-4f56b52b069e","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e83326ae-b0ec-4495-b0e4-1df67502e500","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},"LayerId":{"name":"a9720d26-3c5d-473b-a37f-d0b590f4a87d","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Auta_Splash","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},"resourceVersion":"1.0","name":"e83326ae-b0ec-4495-b0e4-1df67502e500","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"dbb_Auta_Splash","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7b6462c0-631b-4d04-94dd-801756cb7bb3","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e83326ae-b0ec-4495-b0e4-1df67502e500","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"dbb_Auta_Splash","path":"sprites/dbb_Auta_Splash/dbb_Auta_Splash.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 4","resourceVersion":"1.0","name":"a9720d26-3c5d-473b-a37f-d0b590f4a87d","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 3","resourceVersion":"1.0","name":"d4641723-a1db-4ef2-9cf1-4f56b52b069e","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"a3ac47ad-7cb0-421f-8227-a42cdc31ade4","tags":[],"resourceType":"GMImageLayer",},
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":19.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"7bf0703c-1961-4316-9e7b-355e1fc5f832","tags":[],"resourceType":"GMImageLayer",},
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"6d28d5a4-709b-4e17-9647-c8f5be488b52","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Auta",
    "path": "folders/Sequences/Sprites/SuperDevBrosBrawl/Auta.yy",
  },
  "resourceVersion": "1.0",
  "name": "dbb_Auta_Splash",
  "tags": [],
  "resourceType": "GMSprite",
}
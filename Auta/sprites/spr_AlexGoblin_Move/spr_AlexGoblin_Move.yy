{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 5,
  "bbox_right": 26,
  "bbox_top": 5,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Unused",
    "path": "texturegroups/Unused",
  },
  "swatchColours": null,
  "gridX": 8,
  "gridY": 8,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"f0b34beb-7332-4b90-9bc6-eeb0a88f26c0","path":"sprites/spr_AlexGoblin_Move/spr_AlexGoblin_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f0b34beb-7332-4b90-9bc6-eeb0a88f26c0","path":"sprites/spr_AlexGoblin_Move/spr_AlexGoblin_Move.yy",},"LayerId":{"name":"3a879f34-cf55-4ac6-8b32-3f6b4b40c875","path":"sprites/spr_AlexGoblin_Move/spr_AlexGoblin_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AlexGoblin_Move","path":"sprites/spr_AlexGoblin_Move/spr_AlexGoblin_Move.yy",},"resourceVersion":"1.0","name":"f0b34beb-7332-4b90-9bc6-eeb0a88f26c0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0126ee98-720b-4e0d-9563-74062d8f3a4d","path":"sprites/spr_AlexGoblin_Move/spr_AlexGoblin_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0126ee98-720b-4e0d-9563-74062d8f3a4d","path":"sprites/spr_AlexGoblin_Move/spr_AlexGoblin_Move.yy",},"LayerId":{"name":"3a879f34-cf55-4ac6-8b32-3f6b4b40c875","path":"sprites/spr_AlexGoblin_Move/spr_AlexGoblin_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AlexGoblin_Move","path":"sprites/spr_AlexGoblin_Move/spr_AlexGoblin_Move.yy",},"resourceVersion":"1.0","name":"0126ee98-720b-4e0d-9563-74062d8f3a4d","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_AlexGoblin_Move","path":"sprites/spr_AlexGoblin_Move/spr_AlexGoblin_Move.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 5.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"30ac6b57-f588-43a8-9531-b31dcacbcf3d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f0b34beb-7332-4b90-9bc6-eeb0a88f26c0","path":"sprites/spr_AlexGoblin_Move/spr_AlexGoblin_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b22421f3-e651-4162-af24-57462238a2d5","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0126ee98-720b-4e0d-9563-74062d8f3a4d","path":"sprites/spr_AlexGoblin_Move/spr_AlexGoblin_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 14,
    "yorigin": 18,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_AlexGoblin_Move","path":"sprites/spr_AlexGoblin_Move/spr_AlexGoblin_Move.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"3a879f34-cf55-4ac6-8b32-3f6b4b40c875","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "AlexGoblin",
    "path": "folders/Sprites/Sprites/Game/Enemies/AlexGoblin.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_AlexGoblin_Move",
  "tags": [],
  "resourceType": "GMSprite",
}
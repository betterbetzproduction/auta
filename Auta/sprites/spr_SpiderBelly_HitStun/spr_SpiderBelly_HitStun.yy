{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 7,
  "bbox_right": 88,
  "bbox_top": 0,
  "bbox_bottom": 72,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 100,
  "height": 73,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"ead72224-b491-4d8f-bae3-ef4d6cbf4990","path":"sprites/spr_SpiderBelly_HitStun/spr_SpiderBelly_HitStun.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ead72224-b491-4d8f-bae3-ef4d6cbf4990","path":"sprites/spr_SpiderBelly_HitStun/spr_SpiderBelly_HitStun.yy",},"LayerId":{"name":"21d09b56-b086-46ad-aac1-c9f72d669cf4","path":"sprites/spr_SpiderBelly_HitStun/spr_SpiderBelly_HitStun.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"ead72224-b491-4d8f-bae3-ef4d6cbf4990","path":"sprites/spr_SpiderBelly_HitStun/spr_SpiderBelly_HitStun.yy",},"LayerId":{"name":"2658d222-0f29-495d-a60f-fa72dc17934c","path":"sprites/spr_SpiderBelly_HitStun/spr_SpiderBelly_HitStun.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"ead72224-b491-4d8f-bae3-ef4d6cbf4990","path":"sprites/spr_SpiderBelly_HitStun/spr_SpiderBelly_HitStun.yy",},"LayerId":{"name":"59c04223-93c4-4483-b5d7-59597ac274c2","path":"sprites/spr_SpiderBelly_HitStun/spr_SpiderBelly_HitStun.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_SpiderBelly_HitStun","path":"sprites/spr_SpiderBelly_HitStun/spr_SpiderBelly_HitStun.yy",},"resourceVersion":"1.0","name":"ead72224-b491-4d8f-bae3-ef4d6cbf4990","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_SpiderBelly_HitStun","path":"sprites/spr_SpiderBelly_HitStun/spr_SpiderBelly_HitStun.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ec6a744b-12b8-4b40-af8f-3ab5e143a52e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ead72224-b491-4d8f-bae3-ef4d6cbf4990","path":"sprites/spr_SpiderBelly_HitStun/spr_SpiderBelly_HitStun.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 50,
    "yorigin": 36,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_SpiderBelly_HitStun","path":"sprites/spr_SpiderBelly_HitStun/spr_SpiderBelly_HitStun.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"21d09b56-b086-46ad-aac1-c9f72d669cf4","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"2658d222-0f29-495d-a60f-fa72dc17934c","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"59c04223-93c4-4483-b5d7-59597ac274c2","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "HangarBoss",
    "path": "folders/Sprites/Sprites/Game/Enemies/Minibosses/HangarBoss.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_SpiderBelly_HitStun",
  "tags": [],
  "resourceType": "GMSprite",
}
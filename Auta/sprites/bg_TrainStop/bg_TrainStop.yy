{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 127,
  "bbox_top": 0,
  "bbox_bottom": 127,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"6d9b32e0-80fa-4a4a-b077-fcb7de822ea5","path":"sprites/bg_TrainStop/bg_TrainStop.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6d9b32e0-80fa-4a4a-b077-fcb7de822ea5","path":"sprites/bg_TrainStop/bg_TrainStop.yy",},"LayerId":{"name":"34d3eb34-3bdd-4792-a78e-2c9bb29cbdfc","path":"sprites/bg_TrainStop/bg_TrainStop.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"6d9b32e0-80fa-4a4a-b077-fcb7de822ea5","path":"sprites/bg_TrainStop/bg_TrainStop.yy",},"LayerId":{"name":"36ba56ae-520d-4c59-8f45-a3a0650e4b67","path":"sprites/bg_TrainStop/bg_TrainStop.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"bg_TrainStop","path":"sprites/bg_TrainStop/bg_TrainStop.yy",},"resourceVersion":"1.0","name":"6d9b32e0-80fa-4a4a-b077-fcb7de822ea5","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"bg_TrainStop","path":"sprites/bg_TrainStop/bg_TrainStop.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"d0275340-c422-40b1-b919-34d26c351ae5","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6d9b32e0-80fa-4a4a-b077-fcb7de822ea5","path":"sprites/bg_TrainStop/bg_TrainStop.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"bg_TrainStop","path":"sprites/bg_TrainStop/bg_TrainStop.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"34d3eb34-3bdd-4792-a78e-2c9bb29cbdfc","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"36ba56ae-520d-4c59-8f45-a3a0650e4b67","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "BG",
    "path": "folders/Sprites/Sprites/Game/BG/Backgrounds/TrainCenter/BG.yy",
  },
  "resourceVersion": "1.0",
  "name": "bg_TrainStop",
  "tags": [],
  "resourceType": "GMSprite",
}
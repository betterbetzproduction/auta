{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 6,
  "bbox_right": 18,
  "bbox_top": 0,
  "bbox_bottom": 26,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 30,
  "height": 27,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"370e8724-e0bf-453f-9d67-f43c11a1ae6a","path":"sprites/spr_ChiefMask_Shock/spr_ChiefMask_Shock.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"370e8724-e0bf-453f-9d67-f43c11a1ae6a","path":"sprites/spr_ChiefMask_Shock/spr_ChiefMask_Shock.yy",},"LayerId":{"name":"1d9bfbc8-f6ca-4856-83a2-b81b68301c81","path":"sprites/spr_ChiefMask_Shock/spr_ChiefMask_Shock.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ChiefMask_Shock","path":"sprites/spr_ChiefMask_Shock/spr_ChiefMask_Shock.yy",},"resourceVersion":"1.0","name":"370e8724-e0bf-453f-9d67-f43c11a1ae6a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"413f57bf-1e2a-4cfa-b157-28fdff4e707e","path":"sprites/spr_ChiefMask_Shock/spr_ChiefMask_Shock.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"413f57bf-1e2a-4cfa-b157-28fdff4e707e","path":"sprites/spr_ChiefMask_Shock/spr_ChiefMask_Shock.yy",},"LayerId":{"name":"1d9bfbc8-f6ca-4856-83a2-b81b68301c81","path":"sprites/spr_ChiefMask_Shock/spr_ChiefMask_Shock.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ChiefMask_Shock","path":"sprites/spr_ChiefMask_Shock/spr_ChiefMask_Shock.yy",},"resourceVersion":"1.0","name":"413f57bf-1e2a-4cfa-b157-28fdff4e707e","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_ChiefMask_Shock","path":"sprites/spr_ChiefMask_Shock/spr_ChiefMask_Shock.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5a44b580-7311-40cb-b6dc-f10514a68e2f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"370e8724-e0bf-453f-9d67-f43c11a1ae6a","path":"sprites/spr_ChiefMask_Shock/spr_ChiefMask_Shock.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"81266386-b84d-4072-9945-8455909b50d3","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"413f57bf-1e2a-4cfa-b157-28fdff4e707e","path":"sprites/spr_ChiefMask_Shock/spr_ChiefMask_Shock.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 13,
    "yorigin": 14,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_ChiefMask_Shock","path":"sprites/spr_ChiefMask_Shock/spr_ChiefMask_Shock.yy",},
    "resourceVersion": "1.3",
    "name": "spr_ChiefMask_Shock",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"1d9bfbc8-f6ca-4856-83a2-b81b68301c81","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "TrainCenter",
    "path": "folders/Sprites/Sprites/Game/Characters/NPCs/TrainCenter.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_ChiefMask_Shock",
  "tags": [],
  "resourceType": "GMSprite",
}
{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 5,
  "bbox_right": 12,
  "bbox_top": 7,
  "bbox_bottom": 25,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 40,
  "height": 60,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 8,
  "gridY": 8,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"9e184115-59b9-49e6-9fb0-fc28927789b4","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9e184115-59b9-49e6-9fb0-fc28927789b4","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},"LayerId":{"name":"91d7521a-fd7e-453c-a08c-9da7ee55bca8","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"9e184115-59b9-49e6-9fb0-fc28927789b4","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},"LayerId":{"name":"baa4772e-bcf5-4a94-89f1-523b166d876d","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ScavIdle","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},"resourceVersion":"1.0","name":"9e184115-59b9-49e6-9fb0-fc28927789b4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2d3fe428-f1a6-48fb-abac-e3a50638dcbe","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2d3fe428-f1a6-48fb-abac-e3a50638dcbe","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},"LayerId":{"name":"91d7521a-fd7e-453c-a08c-9da7ee55bca8","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"2d3fe428-f1a6-48fb-abac-e3a50638dcbe","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},"LayerId":{"name":"baa4772e-bcf5-4a94-89f1-523b166d876d","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ScavIdle","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},"resourceVersion":"1.0","name":"2d3fe428-f1a6-48fb-abac-e3a50638dcbe","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_ScavIdle","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"dfde8fdf-d503-47ab-9fef-af69976ba398","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9e184115-59b9-49e6-9fb0-fc28927789b4","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fd7de4f0-97c9-475f-932c-d642eeee68fd","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2d3fe428-f1a6-48fb-abac-e3a50638dcbe","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 9,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_ScavIdle","path":"sprites/spr_ScavIdle/spr_ScavIdle.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"layers":[],"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer Group 1","resourceVersion":"1.0","name":"31ae03fc-7665-4d48-959a-d2f729033e83","tags":[],"resourceType":"GMImageFolderLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"91d7521a-fd7e-453c-a08c-9da7ee55bca8","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"baa4772e-bcf5-4a94-89f1-523b166d876d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Scav",
    "path": "folders/Sprites/Sprites/Game/Characters/Scav.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_ScavIdle",
  "tags": [],
  "resourceType": "GMSprite",
}
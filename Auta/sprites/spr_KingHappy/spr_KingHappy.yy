{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 37,
  "bbox_top": 0,
  "bbox_bottom": 28,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 38,
  "height": 29,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"9d360e05-f0a7-4c7f-af1b-ac67e6243ac9","path":"sprites/spr_KingHappy/spr_KingHappy.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9d360e05-f0a7-4c7f-af1b-ac67e6243ac9","path":"sprites/spr_KingHappy/spr_KingHappy.yy",},"LayerId":{"name":"45e4cb9b-967f-4eee-b930-7e6396b72f60","path":"sprites/spr_KingHappy/spr_KingHappy.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"9d360e05-f0a7-4c7f-af1b-ac67e6243ac9","path":"sprites/spr_KingHappy/spr_KingHappy.yy",},"LayerId":{"name":"d2eeac05-7fc3-4e95-b5a9-0177fc4a8371","path":"sprites/spr_KingHappy/spr_KingHappy.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"9d360e05-f0a7-4c7f-af1b-ac67e6243ac9","path":"sprites/spr_KingHappy/spr_KingHappy.yy",},"LayerId":{"name":"00a89bcd-7a66-4700-9754-597ed7963b3d","path":"sprites/spr_KingHappy/spr_KingHappy.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_KingHappy","path":"sprites/spr_KingHappy/spr_KingHappy.yy",},"resourceVersion":"1.0","name":"9d360e05-f0a7-4c7f-af1b-ac67e6243ac9","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_KingHappy","path":"sprites/spr_KingHappy/spr_KingHappy.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 5.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"c151bca0-b89f-4763-8edb-e7eac48eebea","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9d360e05-f0a7-4c7f-af1b-ac67e6243ac9","path":"sprites/spr_KingHappy/spr_KingHappy.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 20,
    "yorigin": 18,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_KingHappy","path":"sprites/spr_KingHappy/spr_KingHappy.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"00a89bcd-7a66-4700-9754-597ed7963b3d","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"45e4cb9b-967f-4eee-b930-7e6396b72f60","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"d2eeac05-7fc3-4e95-b5a9-0177fc4a8371","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "King",
    "path": "folders/Sprites/Sprites/Game/Characters/NPCs/King.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_KingHappy",
  "tags": [],
  "resourceType": "GMSprite",
}
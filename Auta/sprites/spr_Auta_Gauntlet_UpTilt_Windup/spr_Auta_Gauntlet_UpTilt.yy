{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 5,
  "bbox_right": 12,
  "bbox_top": 4,
  "bbox_bottom": 22,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 26,
  "height": 44,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 8,
  "gridY": 8,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"188d1c0a-e3fc-4739-9e63-c0735188e521","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"188d1c0a-e3fc-4739-9e63-c0735188e521","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"4c0dc38a-2cd5-46ac-a1d6-c2fd35874517","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"188d1c0a-e3fc-4739-9e63-c0735188e521","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"d7c7754b-8464-4875-ad41-fede7bc9dc98","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_Gauntlet_UpTilt","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"188d1c0a-e3fc-4739-9e63-c0735188e521","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b69a2679-fcaf-4b67-a438-fa2b308fc99b","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b69a2679-fcaf-4b67-a438-fa2b308fc99b","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"4c0dc38a-2cd5-46ac-a1d6-c2fd35874517","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"b69a2679-fcaf-4b67-a438-fa2b308fc99b","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"d7c7754b-8464-4875-ad41-fede7bc9dc98","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_Gauntlet_UpTilt","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"b69a2679-fcaf-4b67-a438-fa2b308fc99b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4c1ffb57-5a47-4341-86ed-af75921b7540","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4c1ffb57-5a47-4341-86ed-af75921b7540","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"4c0dc38a-2cd5-46ac-a1d6-c2fd35874517","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"4c1ffb57-5a47-4341-86ed-af75921b7540","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"d7c7754b-8464-4875-ad41-fede7bc9dc98","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_Gauntlet_UpTilt","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"4c1ffb57-5a47-4341-86ed-af75921b7540","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9b656e9e-0dc0-43e0-8405-4099e53d082e","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9b656e9e-0dc0-43e0-8405-4099e53d082e","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"4c0dc38a-2cd5-46ac-a1d6-c2fd35874517","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"9b656e9e-0dc0-43e0-8405-4099e53d082e","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"d7c7754b-8464-4875-ad41-fede7bc9dc98","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_Gauntlet_UpTilt","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"9b656e9e-0dc0-43e0-8405-4099e53d082e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"44f4d140-a01d-4995-8a5d-06cfef0c3fa1","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"44f4d140-a01d-4995-8a5d-06cfef0c3fa1","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"4c0dc38a-2cd5-46ac-a1d6-c2fd35874517","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"44f4d140-a01d-4995-8a5d-06cfef0c3fa1","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"d7c7754b-8464-4875-ad41-fede7bc9dc98","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_Gauntlet_UpTilt","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"44f4d140-a01d-4995-8a5d-06cfef0c3fa1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"20b1dfa9-fb69-410c-9cd9-f52b06e52594","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"20b1dfa9-fb69-410c-9cd9-f52b06e52594","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"4c0dc38a-2cd5-46ac-a1d6-c2fd35874517","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"20b1dfa9-fb69-410c-9cd9-f52b06e52594","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"d7c7754b-8464-4875-ad41-fede7bc9dc98","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_Gauntlet_UpTilt","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"20b1dfa9-fb69-410c-9cd9-f52b06e52594","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e2acea27-960d-4c44-9593-e8ba4315090a","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e2acea27-960d-4c44-9593-e8ba4315090a","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"d7c7754b-8464-4875-ad41-fede7bc9dc98","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e2acea27-960d-4c44-9593-e8ba4315090a","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"4c0dc38a-2cd5-46ac-a1d6-c2fd35874517","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_Gauntlet_UpTilt","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"e2acea27-960d-4c44-9593-e8ba4315090a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ea088d6a-402e-48c9-914c-237fd65c67af","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ea088d6a-402e-48c9-914c-237fd65c67af","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"4c0dc38a-2cd5-46ac-a1d6-c2fd35874517","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"ea088d6a-402e-48c9-914c-237fd65c67af","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"d7c7754b-8464-4875-ad41-fede7bc9dc98","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_Gauntlet_UpTilt","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"ea088d6a-402e-48c9-914c-237fd65c67af","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"754d434c-9898-4270-a2dc-13f03cbffbec","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"754d434c-9898-4270-a2dc-13f03cbffbec","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"d7c7754b-8464-4875-ad41-fede7bc9dc98","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"754d434c-9898-4270-a2dc-13f03cbffbec","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"4c0dc38a-2cd5-46ac-a1d6-c2fd35874517","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_Gauntlet_UpTilt","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"754d434c-9898-4270-a2dc-13f03cbffbec","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"231bad47-5b64-4305-b99d-c67d237cc161","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"231bad47-5b64-4305-b99d-c67d237cc161","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"d7c7754b-8464-4875-ad41-fede7bc9dc98","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"231bad47-5b64-4305-b99d-c67d237cc161","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"LayerId":{"name":"4c0dc38a-2cd5-46ac-a1d6-c2fd35874517","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_Gauntlet_UpTilt","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","name":"231bad47-5b64-4305-b99d-c67d237cc161","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Auta_Gauntlet_UpTilt","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 22.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 10.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"11777425-28ba-4958-b543-977d569b64f7","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"188d1c0a-e3fc-4739-9e63-c0735188e521","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f8fb27aa-c846-4a89-9126-2e7f7cb169fa","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b69a2679-fcaf-4b67-a438-fa2b308fc99b","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7eae3a2c-3f7e-40a4-93af-5e7ddb04589d","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4c1ffb57-5a47-4341-86ed-af75921b7540","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b34fb36a-597a-434a-91b7-c4ad89694f05","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9b656e9e-0dc0-43e0-8405-4099e53d082e","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1113d2a1-657f-480d-a558-f7e040f1a838","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"44f4d140-a01d-4995-8a5d-06cfef0c3fa1","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4c767a36-8ff8-40d7-a945-9d0122879e6a","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"20b1dfa9-fb69-410c-9cd9-f52b06e52594","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"46417785-e956-4ef3-a986-a030779c166e","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e2acea27-960d-4c44-9593-e8ba4315090a","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fbeded35-0d55-4d4d-827c-1ad09df3f438","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ea088d6a-402e-48c9-914c-237fd65c67af","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"da60e854-84a3-48e3-a304-d594647fead9","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"754d434c-9898-4270-a2dc-13f03cbffbec","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a07e78c6-2d47-4945-8959-aad63e3b6e07","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"231bad47-5b64-4305-b99d-c67d237cc161","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 9,
    "yorigin": 29,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Auta_Gauntlet_UpTilt","path":"sprites/spr_Auta_Gauntlet_UpTilt/spr_Auta_Gauntlet_UpTilt.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Auta_Gauntlet_UpTilt",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"d7c7754b-8464-4875-ad41-fede7bc9dc98","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"4c0dc38a-2cd5-46ac-a1d6-c2fd35874517","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Gauntlets",
    "path": "folders/Sprites/Sprites/Game/Characters/Auta/Attacks/Gauntlets.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Auta_Gauntlet_UpTilt",
  "tags": [],
  "resourceType": "GMSprite",
}
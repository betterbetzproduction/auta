{
  "bboxMode": 2,
  "collisionKind": 4,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 42,
  "bbox_top": 0,
  "bbox_bottom": 34,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 50,
  "height": 35,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 8,
  "gridY": 8,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"fc6622e9-f0c8-4770-8cef-386725d5bd09","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fc6622e9-f0c8-4770-8cef-386725d5bd09","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"878fdcf3-f599-46e4-a975-c2e89a6151f8","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"fc6622e9-f0c8-4770-8cef-386725d5bd09","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"2bfae7ce-7590-43db-85f7-1daf90a21c95","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"fc6622e9-f0c8-4770-8cef-386725d5bd09","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"d2b39024-0d44-443f-b724-1d49f6bf734b","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaUpTilt_Hitbox","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"fc6622e9-f0c8-4770-8cef-386725d5bd09","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d215c065-d960-419f-aec9-6ceb9483815f","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d215c065-d960-419f-aec9-6ceb9483815f","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"878fdcf3-f599-46e4-a975-c2e89a6151f8","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"d215c065-d960-419f-aec9-6ceb9483815f","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"2bfae7ce-7590-43db-85f7-1daf90a21c95","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"d215c065-d960-419f-aec9-6ceb9483815f","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"d2b39024-0d44-443f-b724-1d49f6bf734b","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaUpTilt_Hitbox","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"d215c065-d960-419f-aec9-6ceb9483815f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e2148744-7f0d-4f83-ace6-82d79158cd95","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e2148744-7f0d-4f83-ace6-82d79158cd95","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"878fdcf3-f599-46e4-a975-c2e89a6151f8","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e2148744-7f0d-4f83-ace6-82d79158cd95","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"2bfae7ce-7590-43db-85f7-1daf90a21c95","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e2148744-7f0d-4f83-ace6-82d79158cd95","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"d2b39024-0d44-443f-b724-1d49f6bf734b","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaUpTilt_Hitbox","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"e2148744-7f0d-4f83-ace6-82d79158cd95","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"26d64992-39bb-4917-92c0-1d7fc1890686","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"26d64992-39bb-4917-92c0-1d7fc1890686","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"878fdcf3-f599-46e4-a975-c2e89a6151f8","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"26d64992-39bb-4917-92c0-1d7fc1890686","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"2bfae7ce-7590-43db-85f7-1daf90a21c95","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"26d64992-39bb-4917-92c0-1d7fc1890686","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"d2b39024-0d44-443f-b724-1d49f6bf734b","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaUpTilt_Hitbox","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"26d64992-39bb-4917-92c0-1d7fc1890686","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3b0cc47a-0480-4f93-8457-c46363e4ae31","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3b0cc47a-0480-4f93-8457-c46363e4ae31","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"878fdcf3-f599-46e4-a975-c2e89a6151f8","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"3b0cc47a-0480-4f93-8457-c46363e4ae31","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"2bfae7ce-7590-43db-85f7-1daf90a21c95","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"3b0cc47a-0480-4f93-8457-c46363e4ae31","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"d2b39024-0d44-443f-b724-1d49f6bf734b","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaUpTilt_Hitbox","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"3b0cc47a-0480-4f93-8457-c46363e4ae31","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bad2cd71-7867-4ad1-8a64-3d06651e6b4a","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bad2cd71-7867-4ad1-8a64-3d06651e6b4a","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"878fdcf3-f599-46e4-a975-c2e89a6151f8","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"bad2cd71-7867-4ad1-8a64-3d06651e6b4a","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"2bfae7ce-7590-43db-85f7-1daf90a21c95","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"bad2cd71-7867-4ad1-8a64-3d06651e6b4a","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"d2b39024-0d44-443f-b724-1d49f6bf734b","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaUpTilt_Hitbox","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"bad2cd71-7867-4ad1-8a64-3d06651e6b4a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a861c418-3958-4b05-a534-d8bf36b1d415","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a861c418-3958-4b05-a534-d8bf36b1d415","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"878fdcf3-f599-46e4-a975-c2e89a6151f8","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"a861c418-3958-4b05-a534-d8bf36b1d415","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"2bfae7ce-7590-43db-85f7-1daf90a21c95","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"a861c418-3958-4b05-a534-d8bf36b1d415","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"LayerId":{"name":"d2b39024-0d44-443f-b724-1d49f6bf734b","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaUpTilt_Hitbox","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","name":"a861c418-3958-4b05-a534-d8bf36b1d415","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_AutaUpTilt_Hitbox","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 7.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"960fe2fa-ed31-4089-b959-b21c7dfc6884","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fc6622e9-f0c8-4770-8cef-386725d5bd09","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cb292f29-b10b-4411-b27e-ebc489e7d05d","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d215c065-d960-419f-aec9-6ceb9483815f","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2fa8b911-a33d-4966-8bfe-cee6195efdf7","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e2148744-7f0d-4f83-ace6-82d79158cd95","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fa9f3686-ba5b-4bfb-88d6-a3dbdf11751e","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"26d64992-39bb-4917-92c0-1d7fc1890686","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5133f930-52bb-4b71-bf46-2046b9c0ba68","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3b0cc47a-0480-4f93-8457-c46363e4ae31","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7796c07f-2a41-46fa-8617-f5f6a5bde5d3","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bad2cd71-7867-4ad1-8a64-3d06651e6b4a","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"98dd675e-2fb6-4fd6-8801-a3f82d7fa296","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a861c418-3958-4b05-a534-d8bf36b1d415","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 18,
    "yorigin": 24,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_AutaUpTilt_Hitbox","path":"sprites/spr_AutaUpTilt_Hitbox/spr_AutaUpTilt_Hitbox.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"878fdcf3-f599-46e4-a975-c2e89a6151f8","tags":[],"resourceType":"GMImageLayer",},
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"2bfae7ce-7590-43db-85f7-1daf90a21c95","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":51.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"d2b39024-0d44-443f-b724-1d49f6bf734b","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sword",
    "path": "folders/Sprites/Sprites/Game/Characters/Auta/Attacks/Sword.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_AutaUpTilt_Hitbox",
  "tags": [],
  "resourceType": "GMSprite",
}
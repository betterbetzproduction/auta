{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 15,
  "bbox_top": 0,
  "bbox_bottom": 11,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 16,
  "height": 12,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e1ba1528-1e64-48ca-b463-c3957fc259ec","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e1ba1528-1e64-48ca-b463-c3957fc259ec","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"LayerId":{"name":"5fd3a5bd-7046-4f5d-9842-3f367df00ca4","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Exclamation","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","name":"e1ba1528-1e64-48ca-b463-c3957fc259ec","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2240061b-2c1f-4fb6-aa8e-895f67e98754","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2240061b-2c1f-4fb6-aa8e-895f67e98754","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"LayerId":{"name":"5fd3a5bd-7046-4f5d-9842-3f367df00ca4","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Exclamation","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","name":"2240061b-2c1f-4fb6-aa8e-895f67e98754","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cd3339c4-cc5b-4c49-b6c4-12e77de3c343","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cd3339c4-cc5b-4c49-b6c4-12e77de3c343","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"LayerId":{"name":"5fd3a5bd-7046-4f5d-9842-3f367df00ca4","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Exclamation","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","name":"cd3339c4-cc5b-4c49-b6c4-12e77de3c343","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1effa930-cf01-4c3f-ad65-6eded9ad961e","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1effa930-cf01-4c3f-ad65-6eded9ad961e","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"LayerId":{"name":"5fd3a5bd-7046-4f5d-9842-3f367df00ca4","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Exclamation","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","name":"1effa930-cf01-4c3f-ad65-6eded9ad961e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4ea8daf3-1e3d-4508-bafa-c35ed6d4ae51","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4ea8daf3-1e3d-4508-bafa-c35ed6d4ae51","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"LayerId":{"name":"5fd3a5bd-7046-4f5d-9842-3f367df00ca4","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Exclamation","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","name":"4ea8daf3-1e3d-4508-bafa-c35ed6d4ae51","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Exclamation","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 5.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"b7122ea0-394d-4731-886e-a101916add4a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e1ba1528-1e64-48ca-b463-c3957fc259ec","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"46650066-d521-4a92-8590-11423f75bcb9","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2240061b-2c1f-4fb6-aa8e-895f67e98754","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"622858b7-b58f-4735-8799-22a8a01bbae0","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cd3339c4-cc5b-4c49-b6c4-12e77de3c343","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3ef32822-0afd-4f18-899f-e22f1ac10bec","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1effa930-cf01-4c3f-ad65-6eded9ad961e","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"21fb5acc-f16f-48e8-a620-23cb86c7b628","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4ea8daf3-1e3d-4508-bafa-c35ed6d4ae51","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 8,
    "yorigin": 13,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Exclamation","path":"sprites/spr_Exclamation/spr_Exclamation.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"5fd3a5bd-7046-4f5d-9842-3f367df00ca4","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "FC_DialogueSystem",
    "path": "folders/Sequences/Sprites/FC_DialogueSystem.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Exclamation",
  "tags": [],
  "resourceType": "GMSprite",
}
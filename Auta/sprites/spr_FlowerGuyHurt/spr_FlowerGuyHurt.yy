{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 5,
  "bbox_right": 31,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"094f5a19-7c4c-4ce2-9587-d900709c7408","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"094f5a19-7c4c-4ce2-9587-d900709c7408","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"LayerId":{"name":"3eb02ce5-ec04-4080-85b8-9f2300e09915","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_FlowerGuyHurt","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"resourceVersion":"1.0","name":"094f5a19-7c4c-4ce2-9587-d900709c7408","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8bb053a0-485d-4497-87ac-f3fc1856a712","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8bb053a0-485d-4497-87ac-f3fc1856a712","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"LayerId":{"name":"3eb02ce5-ec04-4080-85b8-9f2300e09915","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_FlowerGuyHurt","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"resourceVersion":"1.0","name":"8bb053a0-485d-4497-87ac-f3fc1856a712","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e21dbf93-7c0e-4ca5-891d-8e046e26f21c","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e21dbf93-7c0e-4ca5-891d-8e046e26f21c","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"LayerId":{"name":"3eb02ce5-ec04-4080-85b8-9f2300e09915","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_FlowerGuyHurt","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"resourceVersion":"1.0","name":"e21dbf93-7c0e-4ca5-891d-8e046e26f21c","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_FlowerGuyHurt","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"1d4e393d-360d-4c46-a7cf-0a2cdd6bd9d9","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"094f5a19-7c4c-4ce2-9587-d900709c7408","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dee6ced1-fb7c-45c8-8a79-4ac453920686","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8bb053a0-485d-4497-87ac-f3fc1856a712","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"38611a37-50ba-4fed-a521-a208cf79f0d8","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e21dbf93-7c0e-4ca5-891d-8e046e26f21c","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 11,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_FlowerGuyHurt","path":"sprites/spr_FlowerGuyHurt/spr_FlowerGuyHurt.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"3eb02ce5-ec04-4080-85b8-9f2300e09915","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "FlowerGuy",
    "path": "folders/Sprites/Sprites/Game/Enemies/FlowerGuy.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_FlowerGuyHurt",
  "tags": [],
  "resourceType": "GMSprite",
}
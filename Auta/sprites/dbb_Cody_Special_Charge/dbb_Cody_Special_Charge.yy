{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 20,
  "bbox_top": 0,
  "bbox_bottom": 20,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 22,
  "height": 21,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"2ba12c04-265e-4dc8-bc67-83ab1084598a","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2ba12c04-265e-4dc8-bc67-83ab1084598a","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"LayerId":{"name":"9934bd51-1d9b-4065-a386-ac7d5540858c","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Cody_Special_Charge","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","name":"2ba12c04-265e-4dc8-bc67-83ab1084598a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"73dc639a-8763-4862-b167-0c5538f83655","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"73dc639a-8763-4862-b167-0c5538f83655","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"LayerId":{"name":"9934bd51-1d9b-4065-a386-ac7d5540858c","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Cody_Special_Charge","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","name":"73dc639a-8763-4862-b167-0c5538f83655","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"98e0e3cc-880c-4fae-a0e6-13cbae786962","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"98e0e3cc-880c-4fae-a0e6-13cbae786962","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"LayerId":{"name":"9934bd51-1d9b-4065-a386-ac7d5540858c","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Cody_Special_Charge","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","name":"98e0e3cc-880c-4fae-a0e6-13cbae786962","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4b91a598-52e5-4eb9-a8eb-9e4ea6f8ad0b","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4b91a598-52e5-4eb9-a8eb-9e4ea6f8ad0b","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"LayerId":{"name":"9934bd51-1d9b-4065-a386-ac7d5540858c","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Cody_Special_Charge","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","name":"4b91a598-52e5-4eb9-a8eb-9e4ea6f8ad0b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"734987cf-b6b4-48aa-a070-2c620b56db66","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"734987cf-b6b4-48aa-a070-2c620b56db66","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"LayerId":{"name":"9934bd51-1d9b-4065-a386-ac7d5540858c","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Cody_Special_Charge","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","name":"734987cf-b6b4-48aa-a070-2c620b56db66","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"dbb_Cody_Special_Charge","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4526a417-95f1-44f5-b1ff-5c06204da847","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2ba12c04-265e-4dc8-bc67-83ab1084598a","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"501ab2f8-b147-4bc1-bf9a-5977bff24f7c","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"73dc639a-8763-4862-b167-0c5538f83655","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3f768c81-40f2-4a9a-8409-6f3f5c77566a","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"98e0e3cc-880c-4fae-a0e6-13cbae786962","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4bdad416-a1b3-4808-827b-a25b990b3b16","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4b91a598-52e5-4eb9-a8eb-9e4ea6f8ad0b","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3b6b3687-e647-4589-96ed-bbf78888b8b2","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"734987cf-b6b4-48aa-a070-2c620b56db66","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"dbb_Cody_Special_Charge","path":"sprites/dbb_Cody_Special_Charge/dbb_Cody_Special_Charge.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"9934bd51-1d9b-4065-a386-ac7d5540858c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Cody",
    "path": "folders/Sequences/Sprites/SuperDevBrosBrawl/Cody.yy",
  },
  "resourceVersion": "1.0",
  "name": "dbb_Cody_Special_Charge",
  "tags": [],
  "resourceType": "GMSprite",
}
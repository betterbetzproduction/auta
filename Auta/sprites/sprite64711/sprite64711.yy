{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 95,
  "bbox_top": 0,
  "bbox_bottom": 127,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 112,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 16,
  "gridY": 16,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"b8200f1b-a65d-4b64-b839-06df43f5c893","path":"sprites/sprite64711/sprite64711.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b8200f1b-a65d-4b64-b839-06df43f5c893","path":"sprites/sprite64711/sprite64711.yy",},"LayerId":{"name":"dfdbda98-f898-4a9d-85ab-177d1d8405bd","path":"sprites/sprite64711/sprite64711.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"b8200f1b-a65d-4b64-b839-06df43f5c893","path":"sprites/sprite64711/sprite64711.yy",},"LayerId":{"name":"aea94c5e-8541-461e-b210-dbe15304e2b2","path":"sprites/sprite64711/sprite64711.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"b8200f1b-a65d-4b64-b839-06df43f5c893","path":"sprites/sprite64711/sprite64711.yy",},"LayerId":{"name":"d12b8aa9-18df-4cd2-94b4-beb72a08daa4","path":"sprites/sprite64711/sprite64711.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite64711","path":"sprites/sprite64711/sprite64711.yy",},"resourceVersion":"1.0","name":"b8200f1b-a65d-4b64-b839-06df43f5c893","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprite64711","path":"sprites/sprite64711/sprite64711.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"fe341b09-16f7-4a84-8ee0-76700823026f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b8200f1b-a65d-4b64-b839-06df43f5c893","path":"sprites/sprite64711/sprite64711.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprite64711","path":"sprites/sprite64711/sprite64711.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"dfdbda98-f898-4a9d-85ab-177d1d8405bd","tags":[],"resourceType":"GMImageLayer",},
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"aea94c5e-8541-461e-b210-dbe15304e2b2","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d12b8aa9-18df-4cd2-94b4-beb72a08daa4","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "TeleportChamber",
    "path": "folders/Sprites/Sprites/Game/BG/Backgrounds/TeleportChamber.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprite64711",
  "tags": [],
  "resourceType": "GMSprite",
}
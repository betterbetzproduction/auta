{
  "bboxMode": 2,
  "collisionKind": 4,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 250,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 70,
  "bbox_top": 4,
  "bbox_bottom": 61,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 60,
  "height": 80,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": [
    4278190335,
    4278255615,
    4278255360,
    4294967040,
    4294901760,
    4294902015,
    4294967295,
    4293717228,
    4293059298,
    4292335575,
    4291677645,
    4290230199,
    4287993237,
    4280556782,
    4278252287,
    4283540992,
    4293963264,
    4287770926,
    4287365357,
    4287203721,
    4286414205,
    4285558896,
    4284703587,
    4283782485,
    4281742902,
    4278190080,
    4286158839,
    4286688762,
    4287219453,
    4288280831,
    4288405444,
    4288468131,
    4288465538,
    4291349882,
    4294430829,
    4292454269,
    4291466115,
    4290675079,
    4290743485,
    4290943732,
    4288518390,
    4283395315,
    4283862775,
    4284329979,
    4285068799,
    4285781164,
    4285973884,
    4286101564,
    4290034460,
    4294164224,
    4291529796,
    4289289312,
    4289290373,
    4289291432,
    4289359601,
    4286410226,
    4280556782,
    4280444402,
    4280128760,
    4278252287,
    4282369933,
    4283086137,
    4283540992,
    4288522496,
    4293963264,
    4290540032,
    4289423360,
    4289090560,
    4287770926,
    4287704422,
    4287571858,
    4287365357,
    4284159214,
    4279176094,
    4279058848,
    4278870691,
    4278231211,
    4281367321,
  ],
  "gridX": 8,
  "gridY": 8,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"1131372a-490d-4767-be50-164998575456","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1131372a-490d-4767-be50-164998575456","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"53a9a1ce-4d56-4c87-ab1b-26e1862fb016","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"1131372a-490d-4767-be50-164998575456","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"79c71165-a672-4082-acf5-ddca8b4bf555","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"1131372a-490d-4767-be50-164998575456","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"97e80400-caa9-49fe-bed8-9c9e6462c102","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_ComboAttackC_Hitbox","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"1131372a-490d-4767-be50-164998575456","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6c5402e3-7135-45b9-bcc0-7cc13709af5d","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6c5402e3-7135-45b9-bcc0-7cc13709af5d","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"53a9a1ce-4d56-4c87-ab1b-26e1862fb016","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"6c5402e3-7135-45b9-bcc0-7cc13709af5d","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"79c71165-a672-4082-acf5-ddca8b4bf555","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"6c5402e3-7135-45b9-bcc0-7cc13709af5d","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"97e80400-caa9-49fe-bed8-9c9e6462c102","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_ComboAttackC_Hitbox","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"6c5402e3-7135-45b9-bcc0-7cc13709af5d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a61420a8-f998-4814-826d-45672b9cff6a","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a61420a8-f998-4814-826d-45672b9cff6a","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"97e80400-caa9-49fe-bed8-9c9e6462c102","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"a61420a8-f998-4814-826d-45672b9cff6a","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"79c71165-a672-4082-acf5-ddca8b4bf555","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"a61420a8-f998-4814-826d-45672b9cff6a","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"53a9a1ce-4d56-4c87-ab1b-26e1862fb016","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_ComboAttackC_Hitbox","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"a61420a8-f998-4814-826d-45672b9cff6a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f6634049-9dc5-4d53-8fed-3bf335d0f3df","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f6634049-9dc5-4d53-8fed-3bf335d0f3df","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"97e80400-caa9-49fe-bed8-9c9e6462c102","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"f6634049-9dc5-4d53-8fed-3bf335d0f3df","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"79c71165-a672-4082-acf5-ddca8b4bf555","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"f6634049-9dc5-4d53-8fed-3bf335d0f3df","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"53a9a1ce-4d56-4c87-ab1b-26e1862fb016","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_ComboAttackC_Hitbox","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"f6634049-9dc5-4d53-8fed-3bf335d0f3df","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7d98bb5b-0ff1-4e19-8e0d-756e7b53c742","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7d98bb5b-0ff1-4e19-8e0d-756e7b53c742","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"53a9a1ce-4d56-4c87-ab1b-26e1862fb016","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"7d98bb5b-0ff1-4e19-8e0d-756e7b53c742","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"79c71165-a672-4082-acf5-ddca8b4bf555","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"7d98bb5b-0ff1-4e19-8e0d-756e7b53c742","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"97e80400-caa9-49fe-bed8-9c9e6462c102","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_ComboAttackC_Hitbox","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"7d98bb5b-0ff1-4e19-8e0d-756e7b53c742","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d008f330-c218-4ec2-be7e-22c40558de7b","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d008f330-c218-4ec2-be7e-22c40558de7b","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"53a9a1ce-4d56-4c87-ab1b-26e1862fb016","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"d008f330-c218-4ec2-be7e-22c40558de7b","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"79c71165-a672-4082-acf5-ddca8b4bf555","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"d008f330-c218-4ec2-be7e-22c40558de7b","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"LayerId":{"name":"97e80400-caa9-49fe-bed8-9c9e6462c102","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_ComboAttackC_Hitbox","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","name":"d008f330-c218-4ec2-be7e-22c40558de7b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Auta_ComboAttackC_Hitbox","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 20.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"24c22874-18ff-4f3e-940a-6f2e11c7a330","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1131372a-490d-4767-be50-164998575456","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c52ec43f-f735-4a6a-b520-30cc27e2f5ad","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6c5402e3-7135-45b9-bcc0-7cc13709af5d","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"475b7888-8561-4369-ae18-84eb8dbf6d01","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a61420a8-f998-4814-826d-45672b9cff6a","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"13d16c1d-5182-4f92-88a8-5c29daf32076","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f6634049-9dc5-4d53-8fed-3bf335d0f3df","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"52daf21e-8f64-440e-8dc1-7b3c24bc0636","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7d98bb5b-0ff1-4e19-8e0d-756e7b53c742","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2eb7aedf-aee1-4aec-8358-bf7290463e96","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d008f330-c218-4ec2-be7e-22c40558de7b","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 24,
    "yorigin": 41,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Auta_ComboAttackC_Hitbox","path":"sprites/spr_Auta_ComboAttackC_Hitbox/spr_Auta_ComboAttackC_Hitbox.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Auta_ComboAttackC_Hitbox",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"53a9a1ce-4d56-4c87-ab1b-26e1862fb016","tags":[],"resourceType":"GMImageLayer",},
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":21.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"79c71165-a672-4082-acf5-ddca8b4bf555","tags":[],"resourceType":"GMImageLayer",},
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"97e80400-caa9-49fe-bed8-9c9e6462c102","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sword",
    "path": "folders/Sprites/Sprites/Game/Characters/Auta/Attacks/Sword.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Auta_ComboAttackC_Hitbox",
  "tags": [],
  "resourceType": "GMSprite",
}
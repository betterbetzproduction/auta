{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"ea54a3fd-3cd9-41c7-b0e9-3084845a6279","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ea54a3fd-3cd9-41c7-b0e9-3084845a6279","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"81e3c116-49d6-49f7-880c-bae1e9715312","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"ea54a3fd-3cd9-41c7-b0e9-3084845a6279","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"33014e53-cdb0-4c86-a148-e07485f4a1d5","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"ea54a3fd-3cd9-41c7-b0e9-3084845a6279","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"a4b7c635-dc72-4766-87fd-e2ca84ace3bc","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Portrait_Tess","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"ea54a3fd-3cd9-41c7-b0e9-3084845a6279","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ed19ce70-7412-4580-9c6b-43b5062bd103","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ed19ce70-7412-4580-9c6b-43b5062bd103","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"81e3c116-49d6-49f7-880c-bae1e9715312","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"ed19ce70-7412-4580-9c6b-43b5062bd103","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"33014e53-cdb0-4c86-a148-e07485f4a1d5","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"ed19ce70-7412-4580-9c6b-43b5062bd103","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"a4b7c635-dc72-4766-87fd-e2ca84ace3bc","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Portrait_Tess","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"ed19ce70-7412-4580-9c6b-43b5062bd103","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5a681764-38f5-450f-8a4f-72e47f94cf89","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5a681764-38f5-450f-8a4f-72e47f94cf89","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"81e3c116-49d6-49f7-880c-bae1e9715312","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"5a681764-38f5-450f-8a4f-72e47f94cf89","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"33014e53-cdb0-4c86-a148-e07485f4a1d5","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"5a681764-38f5-450f-8a4f-72e47f94cf89","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"a4b7c635-dc72-4766-87fd-e2ca84ace3bc","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Portrait_Tess","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"5a681764-38f5-450f-8a4f-72e47f94cf89","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8478bebb-e0a7-4efe-9be6-f29c55904077","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8478bebb-e0a7-4efe-9be6-f29c55904077","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"81e3c116-49d6-49f7-880c-bae1e9715312","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"8478bebb-e0a7-4efe-9be6-f29c55904077","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"33014e53-cdb0-4c86-a148-e07485f4a1d5","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"8478bebb-e0a7-4efe-9be6-f29c55904077","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"a4b7c635-dc72-4766-87fd-e2ca84ace3bc","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Portrait_Tess","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"8478bebb-e0a7-4efe-9be6-f29c55904077","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f3d7ca3a-9b13-44bb-b457-ac377c423d31","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f3d7ca3a-9b13-44bb-b457-ac377c423d31","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"81e3c116-49d6-49f7-880c-bae1e9715312","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"f3d7ca3a-9b13-44bb-b457-ac377c423d31","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"33014e53-cdb0-4c86-a148-e07485f4a1d5","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"f3d7ca3a-9b13-44bb-b457-ac377c423d31","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"LayerId":{"name":"a4b7c635-dc72-4766-87fd-e2ca84ace3bc","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Portrait_Tess","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","name":"f3d7ca3a-9b13-44bb-b457-ac377c423d31","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Portrait_Tess","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"9872455a-bd35-4b8c-b6dd-0521f9a023d8","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ea54a3fd-3cd9-41c7-b0e9-3084845a6279","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8e1a44b8-9359-4f1b-9321-6b1aa069a329","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ed19ce70-7412-4580-9c6b-43b5062bd103","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4ae0c346-d59c-4ecc-86ff-0160efa485a8","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5a681764-38f5-450f-8a4f-72e47f94cf89","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9e159bc7-d35b-4116-8282-b560394d8ef6","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8478bebb-e0a7-4efe-9be6-f29c55904077","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"333ea341-8d4e-49c1-ad9d-950fb6777482","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f3d7ca3a-9b13-44bb-b457-ac377c423d31","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Portrait_Tess","path":"sprites/spr_Portrait_Tess/spr_Portrait_Tess.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Portrait_Tess",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"81e3c116-49d6-49f7-880c-bae1e9715312","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"33014e53-cdb0-4c86-a148-e07485f4a1d5","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"a4b7c635-dc72-4766-87fd-e2ca84ace3bc","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Auta",
    "path": "folders/Sequences/Sprites/FC_DialogueSystem/Dialogue/Auta.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Portrait_Tess",
  "tags": [],
  "resourceType": "GMSprite",
}
{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 16,
  "bbox_right": 46,
  "bbox_top": 0,
  "bbox_bottom": 23,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 50,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"529fedc9-cf05-4902-a7e7-c5282c930034","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"529fedc9-cf05-4902-a7e7-c5282c930034","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"LayerId":{"name":"4c728cf2-58b2-4a3d-b0d4-a884e861c66f","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_ComboAttackA","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","name":"529fedc9-cf05-4902-a7e7-c5282c930034","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"40efeadc-d793-49e5-8197-667ee2d29229","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"40efeadc-d793-49e5-8197-667ee2d29229","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"LayerId":{"name":"4c728cf2-58b2-4a3d-b0d4-a884e861c66f","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_ComboAttackA","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","name":"40efeadc-d793-49e5-8197-667ee2d29229","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"88bdf182-aa3d-4b62-9efb-79ae613ba5a2","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"88bdf182-aa3d-4b62-9efb-79ae613ba5a2","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"LayerId":{"name":"4c728cf2-58b2-4a3d-b0d4-a884e861c66f","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_ComboAttackA","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","name":"88bdf182-aa3d-4b62-9efb-79ae613ba5a2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"02ef4e19-3183-4294-a131-5b933cc191c0","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"02ef4e19-3183-4294-a131-5b933cc191c0","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"LayerId":{"name":"4c728cf2-58b2-4a3d-b0d4-a884e861c66f","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_ComboAttackA","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","name":"02ef4e19-3183-4294-a131-5b933cc191c0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cc6aea1f-b1fe-49b6-9918-34b404813f45","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cc6aea1f-b1fe-49b6-9918-34b404813f45","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"LayerId":{"name":"4c728cf2-58b2-4a3d-b0d4-a884e861c66f","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_ComboAttackA","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","name":"cc6aea1f-b1fe-49b6-9918-34b404813f45","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ab3a06a3-ca6b-4af5-9ba0-839d4ee3d1ce","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ab3a06a3-ca6b-4af5-9ba0-839d4ee3d1ce","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"LayerId":{"name":"4c728cf2-58b2-4a3d-b0d4-a884e861c66f","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_ComboAttackA","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","name":"ab3a06a3-ca6b-4af5-9ba0-839d4ee3d1ce","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Auta_ComboAttackA","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 20.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"d9efb61e-abf8-4823-bf6a-5bedd09c87c9","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"529fedc9-cf05-4902-a7e7-c5282c930034","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"081c1e15-3117-496e-b34a-7158158fff3c","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"40efeadc-d793-49e5-8197-667ee2d29229","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"42da347d-3e11-4cad-bf3a-0ae9dabd67a6","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"88bdf182-aa3d-4b62-9efb-79ae613ba5a2","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c47bb26b-510f-4a74-a6e8-41b6ad1009fc","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"02ef4e19-3183-4294-a131-5b933cc191c0","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"aa13da5e-7925-456c-a376-ae1c2fe5ab2e","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cc6aea1f-b1fe-49b6-9918-34b404813f45","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f30e9667-5891-465c-af08-4eadfc72eb50","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ab3a06a3-ca6b-4af5-9ba0-839d4ee3d1ce","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 25,
    "yorigin": 13,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Auta_ComboAttackA","path":"sprites/spr_Auta_ComboAttackA/spr_Auta_ComboAttackA.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"4c728cf2-58b2-4a3d-b0d4-a884e861c66f","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sword",
    "path": "folders/Sprites/Sprites/Game/Characters/Auta/Attacks/Sword.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Auta_ComboAttackA",
  "tags": [],
  "resourceType": "GMSprite",
}
{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 10,
  "bbox_top": 2,
  "bbox_bottom": 23,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 18,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 8,
  "gridY": 8,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a677584e-108f-4f76-afb3-66ac761a55d7","path":"sprites/spr_Auta_FightStance/spr_Auta_FightStance.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a677584e-108f-4f76-afb3-66ac761a55d7","path":"sprites/spr_Auta_FightStance/spr_Auta_FightStance.yy",},"LayerId":{"name":"c7a33211-cf52-4348-9c83-7e60eb718959","path":"sprites/spr_Auta_FightStance/spr_Auta_FightStance.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"a677584e-108f-4f76-afb3-66ac761a55d7","path":"sprites/spr_Auta_FightStance/spr_Auta_FightStance.yy",},"LayerId":{"name":"35a3b10e-9f0b-4ffa-8eac-78146570975b","path":"sprites/spr_Auta_FightStance/spr_Auta_FightStance.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Auta_FightStance","path":"sprites/spr_Auta_FightStance/spr_Auta_FightStance.yy",},"resourceVersion":"1.0","name":"a677584e-108f-4f76-afb3-66ac761a55d7","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Auta_FightStance","path":"sprites/spr_Auta_FightStance/spr_Auta_FightStance.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"176b598d-b3c6-498d-98be-fc06d7476cf2","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a677584e-108f-4f76-afb3-66ac761a55d7","path":"sprites/spr_Auta_FightStance/spr_Auta_FightStance.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 8,
    "yorigin": 14,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Auta_FightStance","path":"sprites/spr_Auta_FightStance/spr_Auta_FightStance.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Auta_FightStance",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"c7a33211-cf52-4348-9c83-7e60eb718959","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":51.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"35a3b10e-9f0b-4ffa-8eac-78146570975b","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Maneuvers",
    "path": "folders/Sprites/Sprites/Game/Characters/Auta/Maneuvers.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Auta_FightStance",
  "tags": [],
  "resourceType": "GMSprite",
}
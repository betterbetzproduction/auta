{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 21,
  "bbox_top": 0,
  "bbox_bottom": 16,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 25,
  "height": 29,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"bc52a789-6425-4388-89e7-8174d2ea9407","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bc52a789-6425-4388-89e7-8174d2ea9407","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"LayerId":{"name":"b362c0a6-86a7-49ae-acb3-65d6e733fd4a","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DuoDroid_Fly","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"resourceVersion":"1.0","name":"bc52a789-6425-4388-89e7-8174d2ea9407","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"646c0981-73d0-449d-9689-db787e5c9081","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"646c0981-73d0-449d-9689-db787e5c9081","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"LayerId":{"name":"b362c0a6-86a7-49ae-acb3-65d6e733fd4a","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DuoDroid_Fly","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"resourceVersion":"1.0","name":"646c0981-73d0-449d-9689-db787e5c9081","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5c4567a8-4d3b-4cb3-bd00-4e76fc533566","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5c4567a8-4d3b-4cb3-bd00-4e76fc533566","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"LayerId":{"name":"b362c0a6-86a7-49ae-acb3-65d6e733fd4a","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DuoDroid_Fly","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"resourceVersion":"1.0","name":"5c4567a8-4d3b-4cb3-bd00-4e76fc533566","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_DuoDroid_Fly","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4a10e5e7-c08f-4f61-b34e-1ce30426c338","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bc52a789-6425-4388-89e7-8174d2ea9407","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"254c793a-0f7f-46b0-a4d6-0cf8ff91299d","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"646c0981-73d0-449d-9689-db787e5c9081","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3076c7f5-e51e-4d1c-8161-562d1fbd5d1b","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5c4567a8-4d3b-4cb3-bd00-4e76fc533566","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 10,
    "yorigin": 8,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_DuoDroid_Fly","path":"sprites/spr_DuoDroid_Fly/spr_DuoDroid_Fly.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"b362c0a6-86a7-49ae-acb3-65d6e733fd4a","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "DuoDroid",
    "path": "folders/Sprites/Sprites/Game/Enemies/Hangar/DuoDroid.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_DuoDroid_Fly",
  "tags": [],
  "resourceType": "GMSprite",
}
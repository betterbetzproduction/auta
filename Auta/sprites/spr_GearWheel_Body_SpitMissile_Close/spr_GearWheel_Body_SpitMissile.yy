{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 12,
  "bbox_right": 61,
  "bbox_top": 16,
  "bbox_bottom": 40,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 74,
  "height": 77,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 16,
  "gridY": 16,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"87d19710-1f23-4666-9713-a2172cffe511","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"87d19710-1f23-4666-9713-a2172cffe511","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"87d19710-1f23-4666-9713-a2172cffe511","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":{"name":"35a3b10e-9f0b-4ffa-8eac-78146570975b","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"87d19710-1f23-4666-9713-a2172cffe511","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"87d19710-1f23-4666-9713-a2172cffe511","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"87d19710-1f23-4666-9713-a2172cffe511","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"87d19710-1f23-4666-9713-a2172cffe511","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":{"name":"9ea3d998-a2c7-49eb-8043-9b52679f0626","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GearWheel_Body_SpitMissile","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"87d19710-1f23-4666-9713-a2172cffe511","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"92a0958d-1483-4ab4-bf01-244483bc2619","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"92a0958d-1483-4ab4-bf01-244483bc2619","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":{"name":"9ea3d998-a2c7-49eb-8043-9b52679f0626","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"92a0958d-1483-4ab4-bf01-244483bc2619","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":{"name":"35a3b10e-9f0b-4ffa-8eac-78146570975b","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GearWheel_Body_SpitMissile","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"92a0958d-1483-4ab4-bf01-244483bc2619","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f51e235f-e2e8-441a-a229-ba5a00cdbc34","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f51e235f-e2e8-441a-a229-ba5a00cdbc34","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":{"name":"9ea3d998-a2c7-49eb-8043-9b52679f0626","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"f51e235f-e2e8-441a-a229-ba5a00cdbc34","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":{"name":"35a3b10e-9f0b-4ffa-8eac-78146570975b","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GearWheel_Body_SpitMissile","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"f51e235f-e2e8-441a-a229-ba5a00cdbc34","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7efe7092-9783-4f75-bc26-b51519d30702","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7efe7092-9783-4f75-bc26-b51519d30702","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":{"name":"9ea3d998-a2c7-49eb-8043-9b52679f0626","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"7efe7092-9783-4f75-bc26-b51519d30702","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":{"name":"35a3b10e-9f0b-4ffa-8eac-78146570975b","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GearWheel_Body_SpitMissile","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"7efe7092-9783-4f75-bc26-b51519d30702","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ee3517c9-7648-4ff5-896b-8355dd7605cc","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ee3517c9-7648-4ff5-896b-8355dd7605cc","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":{"name":"9ea3d998-a2c7-49eb-8043-9b52679f0626","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"ee3517c9-7648-4ff5-896b-8355dd7605cc","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":{"name":"35a3b10e-9f0b-4ffa-8eac-78146570975b","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GearWheel_Body_SpitMissile","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"ee3517c9-7648-4ff5-896b-8355dd7605cc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"53a66f99-538d-4271-9ba8-9b0a71b3a0b9","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"53a66f99-538d-4271-9ba8-9b0a71b3a0b9","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":{"name":"9ea3d998-a2c7-49eb-8043-9b52679f0626","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"53a66f99-538d-4271-9ba8-9b0a71b3a0b9","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"LayerId":{"name":"35a3b10e-9f0b-4ffa-8eac-78146570975b","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GearWheel_Body_SpitMissile","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","name":"53a66f99-538d-4271-9ba8-9b0a71b3a0b9","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_GearWheel_Body_SpitMissile","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"9e2fa3c6-8949-454b-a566-09d758c122c1","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"87d19710-1f23-4666-9713-a2172cffe511","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"089fd4fe-c8bf-41af-b86a-2434614c6722","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"92a0958d-1483-4ab4-bf01-244483bc2619","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fbdba10f-216d-4bc1-9471-97d3981a4715","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f51e235f-e2e8-441a-a229-ba5a00cdbc34","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"836bed5f-3b5b-41e6-9277-6c73b3eafc11","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7efe7092-9783-4f75-bc26-b51519d30702","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"03b1cabe-cc7b-4143-92db-21f60c704a07","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ee3517c9-7648-4ff5-896b-8355dd7605cc","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1a94f445-12fb-4b98-bbb8-2f371684b93a","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"53a66f99-538d-4271-9ba8-9b0a71b3a0b9","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 37,
    "yorigin": 38,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_GearWheel_Body_SpitMissile","path":"sprites/spr_GearWheel_Body_SpitMissile/spr_GearWheel_Body_SpitMissile.yy",},
    "resourceVersion": "1.3",
    "name": "spr_GearWheel_Body_SpitMissile",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default (2) (2)","resourceVersion":"1.0","name":"9ea3d998-a2c7-49eb-8043-9b52679f0626","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"35a3b10e-9f0b-4ffa-8eac-78146570975b","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GearWheel",
    "path": "folders/Sprites/Sprites/Game/Enemies/Minibosses/GearWheel.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_GearWheel_Body_SpitMissile",
  "tags": [],
  "resourceType": "GMSprite",
}
{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 9,
  "bbox_top": 2,
  "bbox_bottom": 18,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 12,
  "height": 15,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 8,
  "gridY": 8,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"834ac066-bb55-4888-b854-1d034a91b52c","path":"sprites/spr_Tess_BallToFall/spr_Tess_BallToFall.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"834ac066-bb55-4888-b854-1d034a91b52c","path":"sprites/spr_Tess_BallToFall/spr_Tess_BallToFall.yy",},"LayerId":{"name":"c62265b8-0ad3-4f0b-9869-831a947809ff","path":"sprites/spr_Tess_BallToFall/spr_Tess_BallToFall.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Tess_BallToFall","path":"sprites/spr_Tess_BallToFall/spr_Tess_BallToFall.yy",},"resourceVersion":"1.0","name":"834ac066-bb55-4888-b854-1d034a91b52c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e1fed5cf-07e0-4755-ba6e-479a1434f99e","path":"sprites/spr_Tess_BallToFall/spr_Tess_BallToFall.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e1fed5cf-07e0-4755-ba6e-479a1434f99e","path":"sprites/spr_Tess_BallToFall/spr_Tess_BallToFall.yy",},"LayerId":{"name":"c62265b8-0ad3-4f0b-9869-831a947809ff","path":"sprites/spr_Tess_BallToFall/spr_Tess_BallToFall.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Tess_BallToFall","path":"sprites/spr_Tess_BallToFall/spr_Tess_BallToFall.yy",},"resourceVersion":"1.0","name":"e1fed5cf-07e0-4755-ba6e-479a1434f99e","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Tess_BallToFall","path":"sprites/spr_Tess_BallToFall/spr_Tess_BallToFall.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 20.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"069c4aee-23ef-4a1c-8798-4d5fdcb836da","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"834ac066-bb55-4888-b854-1d034a91b52c","path":"sprites/spr_Tess_BallToFall/spr_Tess_BallToFall.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e9b4b95f-fbf0-4f46-9adf-68e9ca8a1975","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e1fed5cf-07e0-4755-ba6e-479a1434f99e","path":"sprites/spr_Tess_BallToFall/spr_Tess_BallToFall.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 7,
    "yorigin": 8,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Tess_BallToFall","path":"sprites/spr_Tess_BallToFall/spr_Tess_BallToFall.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Tess_BallToFall",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"c62265b8-0ad3-4f0b-9869-831a947809ff","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Tess",
    "path": "folders/Sprites/Sprites/Game/Characters/Tess.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Tess_BallToFall",
  "tags": [],
  "resourceType": "GMSprite",
}
{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 13,
  "bbox_top": 0,
  "bbox_bottom": 20,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 14,
  "height": 21,
  "textureGroupId": {
    "name": "Unused",
    "path": "texturegroups/Unused",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"44739331-fb5e-4ced-91b5-be20b26ddb71","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"44739331-fb5e-4ced-91b5-be20b26ddb71","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":{"name":"1eca68bd-9bba-4d39-86df-85b58920240b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"44739331-fb5e-4ced-91b5-be20b26ddb71","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"703d09c4-69f2-4439-9c51-8b6785677566","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"703d09c4-69f2-4439-9c51-8b6785677566","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":{"name":"1eca68bd-9bba-4d39-86df-85b58920240b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"703d09c4-69f2-4439-9c51-8b6785677566","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"22aaefb0-5e85-4ed0-8e4d-936f170540ed","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"22aaefb0-5e85-4ed0-8e4d-936f170540ed","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":{"name":"1eca68bd-9bba-4d39-86df-85b58920240b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"22aaefb0-5e85-4ed0-8e4d-936f170540ed","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3916bb30-13da-43a5-8769-d6f348f17f3c","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3916bb30-13da-43a5-8769-d6f348f17f3c","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":{"name":"1eca68bd-9bba-4d39-86df-85b58920240b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"3916bb30-13da-43a5-8769-d6f348f17f3c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a4878c3a-d191-4604-a3d2-ca2d9128dea4","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a4878c3a-d191-4604-a3d2-ca2d9128dea4","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":{"name":"1eca68bd-9bba-4d39-86df-85b58920240b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"a4878c3a-d191-4604-a3d2-ca2d9128dea4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b70b6983-f99e-40ea-b6bb-342a32d70700","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b70b6983-f99e-40ea-b6bb-342a32d70700","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":{"name":"1eca68bd-9bba-4d39-86df-85b58920240b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"b70b6983-f99e-40ea-b6bb-342a32d70700","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0176d2c7-ffb9-41c3-ba5e-5bac031905cd","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0176d2c7-ffb9-41c3-ba5e-5bac031905cd","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":{"name":"1eca68bd-9bba-4d39-86df-85b58920240b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"0176d2c7-ffb9-41c3-ba5e-5bac031905cd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a620867b-8377-47e6-8324-d328c9409265","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a620867b-8377-47e6-8324-d328c9409265","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":{"name":"1eca68bd-9bba-4d39-86df-85b58920240b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"a620867b-8377-47e6-8324-d328c9409265","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"96080e9d-e2d3-4b42-bec7-516779558354","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"96080e9d-e2d3-4b42-bec7-516779558354","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":{"name":"1eca68bd-9bba-4d39-86df-85b58920240b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"96080e9d-e2d3-4b42-bec7-516779558354","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0f447fb0-79d6-41cb-b7b7-148aebda3285","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0f447fb0-79d6-41cb-b7b7-148aebda3285","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":{"name":"1eca68bd-9bba-4d39-86df-85b58920240b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"0f447fb0-79d6-41cb-b7b7-148aebda3285","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"34f3e2d8-ebc7-48f5-9431-79b33f712a65","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"34f3e2d8-ebc7-48f5-9431-79b33f712a65","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":{"name":"1eca68bd-9bba-4d39-86df-85b58920240b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"34f3e2d8-ebc7-48f5-9431-79b33f712a65","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"49ed4719-fc4d-4033-8bca-235cacf2f9cc","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"49ed4719-fc4d-4033-8bca-235cacf2f9cc","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":{"name":"1eca68bd-9bba-4d39-86df-85b58920240b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"49ed4719-fc4d-4033-8bca-235cacf2f9cc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3509de3c-8c65-487d-8076-242dbff2ae3b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3509de3c-8c65-487d-8076-242dbff2ae3b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"LayerId":{"name":"1eca68bd-9bba-4d39-86df-85b58920240b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","name":"3509de3c-8c65-487d-8076-242dbff2ae3b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 13.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"173941d7-4deb-4892-87c8-a1d2d8adc0fd","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"44739331-fb5e-4ced-91b5-be20b26ddb71","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"654f0c93-be79-4c69-afcd-0101e4273b75","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"703d09c4-69f2-4439-9c51-8b6785677566","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8297499a-4ecd-48c8-9cbc-c12a66091ec4","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"22aaefb0-5e85-4ed0-8e4d-936f170540ed","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d8f98f64-6f62-4ecd-9149-2e452b125d16","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3916bb30-13da-43a5-8769-d6f348f17f3c","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a6702f83-6f74-42eb-9d85-8fe5d6295233","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a4878c3a-d191-4604-a3d2-ca2d9128dea4","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bc088179-b5a6-4a8e-b057-f0f851594bb4","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b70b6983-f99e-40ea-b6bb-342a32d70700","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c06c58cc-c69a-4d31-9800-e658d6d94860","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0176d2c7-ffb9-41c3-ba5e-5bac031905cd","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2a4bb283-df3f-4f6e-8c00-2f2e434bd73d","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a620867b-8377-47e6-8324-d328c9409265","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cb06ffb8-5bc8-4ae0-9cd7-31c9703e9a1f","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"96080e9d-e2d3-4b42-bec7-516779558354","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b8681c09-fba8-4ba1-a114-51fe641729e7","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0f447fb0-79d6-41cb-b7b7-148aebda3285","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"35ac730d-0367-439c-9bbf-a87f365dba9b","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"34f3e2d8-ebc7-48f5-9431-79b33f712a65","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0ee806cd-d2d3-4739-9697-892eecd57449","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"49ed4719-fc4d-4033-8bca-235cacf2f9cc","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"706737fc-4403-4217-9979-d513e45348dc","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3509de3c-8c65-487d-8076-242dbff2ae3b","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"dbb_Auta_Victory","path":"sprites/dbb_Auta_Victory/dbb_Auta_Victory.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"1eca68bd-9bba-4d39-86df-85b58920240b","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Auta",
    "path": "folders/Sequences/Sprites/SuperDevBrosBrawl/Auta.yy",
  },
  "resourceVersion": "1.0",
  "name": "dbb_Auta_Victory",
  "tags": [],
  "resourceType": "GMSprite",
}
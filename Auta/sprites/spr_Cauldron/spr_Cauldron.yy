{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 5,
  "bbox_right": 27,
  "bbox_top": 0,
  "bbox_bottom": 27,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"834f731e-5e48-4f35-b1c3-78a3170f089c","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"834f731e-5e48-4f35-b1c3-78a3170f089c","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"LayerId":{"name":"d9df04c8-63fc-481c-bbaa-c847c1eca23a","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"834f731e-5e48-4f35-b1c3-78a3170f089c","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"LayerId":{"name":"ff7878f9-e49e-4944-bf13-38f6485f9a24","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"834f731e-5e48-4f35-b1c3-78a3170f089c","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"LayerId":{"name":"8872289d-6cd4-4835-97b0-4d0dde81ec96","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Cauldron","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"resourceVersion":"1.0","name":"834f731e-5e48-4f35-b1c3-78a3170f089c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"184a9f09-0cd2-4346-855a-f6ab8fa43b4f","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"184a9f09-0cd2-4346-855a-f6ab8fa43b4f","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"LayerId":{"name":"ff7878f9-e49e-4944-bf13-38f6485f9a24","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"184a9f09-0cd2-4346-855a-f6ab8fa43b4f","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"LayerId":{"name":"d9df04c8-63fc-481c-bbaa-c847c1eca23a","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"184a9f09-0cd2-4346-855a-f6ab8fa43b4f","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"LayerId":{"name":"8872289d-6cd4-4835-97b0-4d0dde81ec96","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Cauldron","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"resourceVersion":"1.0","name":"184a9f09-0cd2-4346-855a-f6ab8fa43b4f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Cauldron","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"66389ff7-1eaa-4f8e-ba80-6eda5ba0a37e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"834f731e-5e48-4f35-b1c3-78a3170f089c","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"347262e5-bfaa-4455-80ec-193484f476b0","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"184a9f09-0cd2-4346-855a-f6ab8fa43b4f","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Cauldron","path":"sprites/spr_Cauldron/spr_Cauldron.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"ff7878f9-e49e-4944-bf13-38f6485f9a24","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d9df04c8-63fc-481c-bbaa-c847c1eca23a","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"8872289d-6cd4-4835-97b0-4d0dde81ec96","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Enemies",
    "path": "folders/Sprites/Sprites/Game/Enemies.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Cauldron",
  "tags": [],
  "resourceType": "GMSprite",
}
{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 5,
  "bbox_right": 12,
  "bbox_top": 4,
  "bbox_bottom": 22,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 16,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": [
    4278190335,
    4278255615,
    4278255360,
    4294967040,
    4294901760,
    4294902015,
    4294967295,
    4293717228,
    4293059298,
    4292335575,
    4291677645,
    4290230199,
    4287993237,
    4280556782,
    4278252287,
    4283540992,
    4293963264,
    4287770926,
    4287365357,
    4287203721,
    4286414205,
    4285558896,
    4284703587,
    4283782485,
    4281742902,
    4278190080,
    4286158839,
    4286688762,
    4287219453,
    4288280831,
    4288405444,
    4288468131,
    4288465538,
    4291349882,
    4294430829,
    4292454269,
    4291466115,
    4290675079,
    4290743485,
    4290943732,
    4288518390,
    4283395315,
    4283862775,
    4284329979,
    4285068799,
    4285781164,
    4285973884,
    4286101564,
    4290034460,
    4294164224,
    4291529796,
    4289289312,
    4289290373,
    4289291432,
    4289359601,
    4286410226,
    4280556782,
    4280444402,
    4280128760,
    4278252287,
    4282369933,
    4283086137,
    4283540992,
    4288522496,
    4293963264,
    4290540032,
    4289423360,
    4289090560,
    4287770926,
    4287704422,
    4287571858,
    4287365357,
    4284159214,
    4279176094,
    4279058848,
    4278870691,
    4278231211,
    4281367321,
  ],
  "gridX": 8,
  "gridY": 8,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"21cfbc98-1577-4df9-b192-9908c1653444","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"21cfbc98-1577-4df9-b192-9908c1653444","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"LayerId":{"name":"a53c248a-e307-42da-9cb4-dcf575b6d58e","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaRun_Jump655","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","name":"21cfbc98-1577-4df9-b192-9908c1653444","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e05e20de-73d5-42c7-b317-ff8b26e72cb6","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e05e20de-73d5-42c7-b317-ff8b26e72cb6","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"LayerId":{"name":"a53c248a-e307-42da-9cb4-dcf575b6d58e","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaRun_Jump655","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","name":"e05e20de-73d5-42c7-b317-ff8b26e72cb6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"982008e6-00ed-4377-a6fa-11d51a78d553","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"982008e6-00ed-4377-a6fa-11d51a78d553","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"LayerId":{"name":"a53c248a-e307-42da-9cb4-dcf575b6d58e","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaRun_Jump655","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","name":"982008e6-00ed-4377-a6fa-11d51a78d553","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"62199704-1a57-43a1-8ee9-864f49d1d3ea","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"62199704-1a57-43a1-8ee9-864f49d1d3ea","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"LayerId":{"name":"a53c248a-e307-42da-9cb4-dcf575b6d58e","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaRun_Jump655","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","name":"62199704-1a57-43a1-8ee9-864f49d1d3ea","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"251a201d-501f-47a9-ba00-d599165c6677","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"251a201d-501f-47a9-ba00-d599165c6677","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"LayerId":{"name":"a53c248a-e307-42da-9cb4-dcf575b6d58e","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaRun_Jump655","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","name":"251a201d-501f-47a9-ba00-d599165c6677","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9017a012-b500-4732-b1af-976a748ea7b8","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9017a012-b500-4732-b1af-976a748ea7b8","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"LayerId":{"name":"a53c248a-e307-42da-9cb4-dcf575b6d58e","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaRun_Jump655","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","name":"9017a012-b500-4732-b1af-976a748ea7b8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3b9cebd8-30e2-4c22-8072-32699ee80c37","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3b9cebd8-30e2-4c22-8072-32699ee80c37","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"LayerId":{"name":"a53c248a-e307-42da-9cb4-dcf575b6d58e","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AutaRun_Jump655","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","name":"3b9cebd8-30e2-4c22-8072-32699ee80c37","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_AutaRun_Jump655","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 12.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 7.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"b7e552df-ccbf-46c4-8461-385dc234db3b","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"21cfbc98-1577-4df9-b192-9908c1653444","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"398bd061-9fb6-4e1d-af57-0df6fa29cbd7","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e05e20de-73d5-42c7-b317-ff8b26e72cb6","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fb6557fd-1c91-4260-a051-0d03bad45451","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"982008e6-00ed-4377-a6fa-11d51a78d553","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"51c2fa9c-db47-46c0-ac48-3fec9cc9ba96","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"62199704-1a57-43a1-8ee9-864f49d1d3ea","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"47c5dca6-d69e-4200-8dee-a1f65755f269","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"251a201d-501f-47a9-ba00-d599165c6677","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d17a7e0c-7305-4ff6-b76d-99f12e03e00b","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9017a012-b500-4732-b1af-976a748ea7b8","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"48b38bd0-c798-45a3-81fd-243a860ac653","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3b9cebd8-30e2-4c22-8072-32699ee80c37","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 44,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_AutaRun_Jump655","path":"sprites/spr_AutaRun_Jump655/spr_AutaRun_Jump655.yy",},
    "resourceVersion": "1.3",
    "name": "spr_AutaRun_Jump",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"a53c248a-e307-42da-9cb4-dcf575b6d58e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Regular",
    "path": "folders/Sprites/Sprites/Game/Characters/Auta/Regular.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_AutaRun_Jump655",
  "tags": [],
  "resourceType": "GMSprite",
}
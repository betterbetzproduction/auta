{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 111,
  "bbox_top": 0,
  "bbox_bottom": 79,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 112,
  "height": 80,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 16,
  "gridY": 8,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"597b6566-be31-463f-a4b9-fca7faa49f3e","path":"sprites/sprite647/sprite647.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"597b6566-be31-463f-a4b9-fca7faa49f3e","path":"sprites/sprite647/sprite647.yy",},"LayerId":{"name":"cae64a31-508f-431e-9e50-71325fa17b2d","path":"sprites/sprite647/sprite647.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"597b6566-be31-463f-a4b9-fca7faa49f3e","path":"sprites/sprite647/sprite647.yy",},"LayerId":{"name":"3ee29913-86d3-40f1-b2a3-306e938f592b","path":"sprites/sprite647/sprite647.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"597b6566-be31-463f-a4b9-fca7faa49f3e","path":"sprites/sprite647/sprite647.yy",},"LayerId":{"name":"202b6844-40a6-41a0-9221-86a803b75863","path":"sprites/sprite647/sprite647.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprite647","path":"sprites/sprite647/sprite647.yy",},"resourceVersion":"1.0","name":"597b6566-be31-463f-a4b9-fca7faa49f3e","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprite647","path":"sprites/sprite647/sprite647.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"01a25c4f-3b0c-4262-a817-25e58ee0228b","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"597b6566-be31-463f-a4b9-fca7faa49f3e","path":"sprites/sprite647/sprite647.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprite647","path":"sprites/sprite647/sprite647.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"202b6844-40a6-41a0-9221-86a803b75863","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"3ee29913-86d3-40f1-b2a3-306e938f592b","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"cae64a31-508f-431e-9e50-71325fa17b2d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "TeleportChamber",
    "path": "folders/Sprites/Sprites/Game/BG/Backgrounds/TeleportChamber.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprite647",
  "tags": [],
  "resourceType": "GMSprite",
}
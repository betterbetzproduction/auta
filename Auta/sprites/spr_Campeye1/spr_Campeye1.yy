{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 4,
  "bbox_right": 16,
  "bbox_top": 8,
  "bbox_bottom": 24,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 21,
  "height": 25,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"5b52a128-21b0-4fc1-9ec7-c4e27f976eaf","path":"sprites/spr_Campeye1/spr_Campeye1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5b52a128-21b0-4fc1-9ec7-c4e27f976eaf","path":"sprites/spr_Campeye1/spr_Campeye1.yy",},"LayerId":{"name":"fef42962-2954-4aff-9a1c-e43093266742","path":"sprites/spr_Campeye1/spr_Campeye1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Campeye1","path":"sprites/spr_Campeye1/spr_Campeye1.yy",},"resourceVersion":"1.0","name":"5b52a128-21b0-4fc1-9ec7-c4e27f976eaf","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Campeye1","path":"sprites/spr_Campeye1/spr_Campeye1.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"13c2e58c-245e-4b0d-aad9-ebc443ee3d6a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5b52a128-21b0-4fc1-9ec7-c4e27f976eaf","path":"sprites/spr_Campeye1/spr_Campeye1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 13,
    "yorigin": 13,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Campeye1","path":"sprites/spr_Campeye1/spr_Campeye1.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"fef42962-2954-4aff-9a1c-e43093266742","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "NPCs",
    "path": "folders/Sprites/Sprites/Game/Characters/NPCs.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Campeye1",
  "tags": [],
  "resourceType": "GMSprite",
}
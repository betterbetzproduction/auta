{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 7,
  "bbox_top": 0,
  "bbox_bottom": 2,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 8,
  "height": 3,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"17ec83b4-b4c2-4c02-9aa3-eaa40bb5dbd8","path":"sprites/dbb_Cody_Weak_Bullet/dbb_Cody_Weak_Bullet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"17ec83b4-b4c2-4c02-9aa3-eaa40bb5dbd8","path":"sprites/dbb_Cody_Weak_Bullet/dbb_Cody_Weak_Bullet.yy",},"LayerId":{"name":"9ea64fc4-2464-4bcf-a38a-e4e4165aea12","path":"sprites/dbb_Cody_Weak_Bullet/dbb_Cody_Weak_Bullet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"dbb_Cody_Weak_Bullet","path":"sprites/dbb_Cody_Weak_Bullet/dbb_Cody_Weak_Bullet.yy",},"resourceVersion":"1.0","name":"17ec83b4-b4c2-4c02-9aa3-eaa40bb5dbd8","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"dbb_Cody_Weak_Bullet","path":"sprites/dbb_Cody_Weak_Bullet/dbb_Cody_Weak_Bullet.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"1e5b4dd7-eb89-432a-97cc-0bb9ff6fb48c","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"17ec83b4-b4c2-4c02-9aa3-eaa40bb5dbd8","path":"sprites/dbb_Cody_Weak_Bullet/dbb_Cody_Weak_Bullet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"dbb_Cody_Weak_Bullet","path":"sprites/dbb_Cody_Weak_Bullet/dbb_Cody_Weak_Bullet.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"9ea64fc4-2464-4bcf-a38a-e4e4165aea12","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Cody",
    "path": "folders/Sequences/Sprites/SuperDevBrosBrawl/Cody.yy",
  },
  "resourceVersion": "1.0",
  "name": "dbb_Cody_Weak_Bullet",
  "tags": [],
  "resourceType": "GMSprite",
}
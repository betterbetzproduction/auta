{
  "bboxMode": 2,
  "collisionKind": 0,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 15,
  "bbox_top": -1,
  "bbox_bottom": 41,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 16,
  "height": 40,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": [
    4278190335,
    4278255615,
    4278255360,
    4294967040,
    4294901760,
    4294902015,
    4294967295,
    4293717228,
    4293059298,
    4292335575,
    4291677645,
    4290230199,
    4287993237,
    4280556782,
    4278252287,
    4283540992,
    4293963264,
    4287770926,
    4287365357,
    4287203721,
    4286414205,
    4285558896,
    4284703587,
    4283782485,
    4281742902,
    4278190080,
    4286158839,
    4286688762,
    4287219453,
    4288280831,
    4288405444,
    4288468131,
    4288465538,
    4291349882,
    4294430829,
    4292454269,
    4291466115,
    4290675079,
    4290743485,
    4290943732,
    4288518390,
    4283395315,
    4283862775,
    4284329979,
    4285068799,
    4285781164,
    4285973884,
    4286101564,
    4290034460,
    4294164224,
    4291529796,
    4289289312,
    4289290373,
    4289291432,
    4289359601,
    4286410226,
    4280556782,
    4280444402,
    4280128760,
    4278252287,
    4282369933,
    4283086137,
    4283540992,
    4288522496,
    4293963264,
    4290540032,
    4289423360,
    4289090560,
    4287770926,
    4287704422,
    4287571858,
    4287365357,
    4284159214,
    4279176094,
    4279058848,
    4278870691,
    4278231211,
    4281367321,
  ],
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a51fc101-440b-44cb-ae30-abdd6349ccf9","path":"sprites/spr_Wire/spr_Wire.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a51fc101-440b-44cb-ae30-abdd6349ccf9","path":"sprites/spr_Wire/spr_Wire.yy",},"LayerId":{"name":"96694f1e-0e4d-4f8a-b939-d28393199fe0","path":"sprites/spr_Wire/spr_Wire.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"a51fc101-440b-44cb-ae30-abdd6349ccf9","path":"sprites/spr_Wire/spr_Wire.yy",},"LayerId":{"name":"99b6587e-0859-43dd-9e01-9a3534a1f413","path":"sprites/spr_Wire/spr_Wire.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Wire","path":"sprites/spr_Wire/spr_Wire.yy",},"resourceVersion":"1.0","name":"a51fc101-440b-44cb-ae30-abdd6349ccf9","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Wire","path":"sprites/spr_Wire/spr_Wire.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"bd4a1c40-8e18-453a-bb18-be16e9c6252d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a51fc101-440b-44cb-ae30-abdd6349ccf9","path":"sprites/spr_Wire/spr_Wire.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Wire","path":"sprites/spr_Wire/spr_Wire.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"96694f1e-0e4d-4f8a-b939-d28393199fe0","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":1.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"99b6587e-0859-43dd-9e01-9a3534a1f413","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Wire",
  "tags": [],
  "resourceType": "GMSprite",
}
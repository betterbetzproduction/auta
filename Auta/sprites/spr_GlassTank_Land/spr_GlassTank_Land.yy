{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 7,
  "bbox_right": 56,
  "bbox_top": 14,
  "bbox_bottom": 43,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"d2d80812-ccf1-482c-8fa5-9e7bf22ba0ad","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d2d80812-ccf1-482c-8fa5-9e7bf22ba0ad","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"LayerId":{"name":"025a113e-32ef-47fa-bde9-1ffe714f4fca","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GlassTank_Land","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"resourceVersion":"1.0","name":"d2d80812-ccf1-482c-8fa5-9e7bf22ba0ad","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d5dff8c8-e8b1-4df1-98e1-3ef11c9f1a59","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d5dff8c8-e8b1-4df1-98e1-3ef11c9f1a59","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"LayerId":{"name":"025a113e-32ef-47fa-bde9-1ffe714f4fca","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GlassTank_Land","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"resourceVersion":"1.0","name":"d5dff8c8-e8b1-4df1-98e1-3ef11c9f1a59","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"25015260-492a-4067-adf9-d849eda81748","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"25015260-492a-4067-adf9-d849eda81748","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"LayerId":{"name":"025a113e-32ef-47fa-bde9-1ffe714f4fca","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GlassTank_Land","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"resourceVersion":"1.0","name":"25015260-492a-4067-adf9-d849eda81748","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_GlassTank_Land","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"df015cf0-f5f4-4aef-89e7-4b7191db851f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d2d80812-ccf1-482c-8fa5-9e7bf22ba0ad","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3d7c2da6-c68b-42b9-94af-6dcfa6f8835f","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d5dff8c8-e8b1-4df1-98e1-3ef11c9f1a59","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"839fa6c2-13bd-4c05-8155-732999c067fa","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"25015260-492a-4067-adf9-d849eda81748","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_GlassTank_Land","path":"sprites/spr_GlassTank_Land/spr_GlassTank_Land.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"025a113e-32ef-47fa-bde9-1ffe714f4fca","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GlassTank",
    "path": "folders/Sprites/Sprites/Game/Enemies/GlassTank.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_GlassTank_Land",
  "tags": [],
  "resourceType": "GMSprite",
}
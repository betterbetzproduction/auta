{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 21,
  "bbox_top": 0,
  "bbox_bottom": 16,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 25,
  "height": 29,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"cdabf5e9-e198-4702-a3cb-4793a5eb9319","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cdabf5e9-e198-4702-a3cb-4793a5eb9319","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"LayerId":{"name":"c874fefc-1283-4b47-b81a-0fd8c0ddd6d8","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"cdabf5e9-e198-4702-a3cb-4793a5eb9319","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"LayerId":{"name":"bec9e166-cb5c-4b86-ae22-e619cc86712e","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DuoDroid_Fall","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"resourceVersion":"1.0","name":"cdabf5e9-e198-4702-a3cb-4793a5eb9319","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f2aa39ca-aa98-4ba3-adb5-848d89672ad9","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f2aa39ca-aa98-4ba3-adb5-848d89672ad9","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"LayerId":{"name":"c874fefc-1283-4b47-b81a-0fd8c0ddd6d8","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"f2aa39ca-aa98-4ba3-adb5-848d89672ad9","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"LayerId":{"name":"bec9e166-cb5c-4b86-ae22-e619cc86712e","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DuoDroid_Fall","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"resourceVersion":"1.0","name":"f2aa39ca-aa98-4ba3-adb5-848d89672ad9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5dad10ec-2736-4e08-82d0-03cb33e0631a","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5dad10ec-2736-4e08-82d0-03cb33e0631a","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"LayerId":{"name":"c874fefc-1283-4b47-b81a-0fd8c0ddd6d8","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"5dad10ec-2736-4e08-82d0-03cb33e0631a","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"LayerId":{"name":"bec9e166-cb5c-4b86-ae22-e619cc86712e","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DuoDroid_Fall","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"resourceVersion":"1.0","name":"5dad10ec-2736-4e08-82d0-03cb33e0631a","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_DuoDroid_Fall","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6770e724-5bcf-45a5-becd-80792f8032b3","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cdabf5e9-e198-4702-a3cb-4793a5eb9319","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3af910c9-b504-49d7-a9a7-e7cb6dd173ed","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f2aa39ca-aa98-4ba3-adb5-848d89672ad9","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"362e83ed-4877-44ac-8f3b-75a19b15a52e","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5dad10ec-2736-4e08-82d0-03cb33e0631a","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 10,
    "yorigin": 8,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_DuoDroid_Fall","path":"sprites/spr_DuoDroid_Fall/spr_DuoDroid_Fall.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"c874fefc-1283-4b47-b81a-0fd8c0ddd6d8","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"bec9e166-cb5c-4b86-ae22-e619cc86712e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "DuoDroid",
    "path": "folders/Sprites/Sprites/Game/Enemies/Hangar/DuoDroid.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_DuoDroid_Fall",
  "tags": [],
  "resourceType": "GMSprite",
}
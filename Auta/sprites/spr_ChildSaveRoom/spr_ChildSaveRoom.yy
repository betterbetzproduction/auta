{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 226,
  "bbox_top": 0,
  "bbox_bottom": 94,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 227,
  "height": 95,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"42087745-70b8-4793-8124-3a94dfc512e3","path":"sprites/spr_ChildSaveRoom/spr_ChildSaveRoom.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"42087745-70b8-4793-8124-3a94dfc512e3","path":"sprites/spr_ChildSaveRoom/spr_ChildSaveRoom.yy",},"LayerId":{"name":"a6c597a4-bba3-4e7d-b7f2-6a443631c8e8","path":"sprites/spr_ChildSaveRoom/spr_ChildSaveRoom.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ChildSaveRoom","path":"sprites/spr_ChildSaveRoom/spr_ChildSaveRoom.yy",},"resourceVersion":"1.0","name":"42087745-70b8-4793-8124-3a94dfc512e3","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_ChildSaveRoom","path":"sprites/spr_ChildSaveRoom/spr_ChildSaveRoom.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"1a9d5a19-d0b4-4395-ba9a-f25f87ac9d9d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"42087745-70b8-4793-8124-3a94dfc512e3","path":"sprites/spr_ChildSaveRoom/spr_ChildSaveRoom.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_ChildSaveRoom","path":"sprites/spr_ChildSaveRoom/spr_ChildSaveRoom.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"a6c597a4-bba3-4e7d-b7f2-6a443631c8e8","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Flourish",
    "path": "folders/Sprites/Sprites/Game/BG/Backgrounds/TrainCenter/Flourish.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_ChildSaveRoom",
  "tags": [],
  "resourceType": "GMSprite",
}
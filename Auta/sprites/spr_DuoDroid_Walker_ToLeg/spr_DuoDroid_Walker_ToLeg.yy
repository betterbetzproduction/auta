{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 4,
  "bbox_right": 14,
  "bbox_top": 0,
  "bbox_bottom": 10,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 22,
  "height": 23,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"54ef8af2-6065-42a7-b28a-550e794d30b9","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"54ef8af2-6065-42a7-b28a-550e794d30b9","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":{"name":"3b7ef5af-abea-45db-b9a2-b9cb604d12b0","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"54ef8af2-6065-42a7-b28a-550e794d30b9","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":{"name":"8a92e087-5ea9-4bbf-b888-976579bc4f7e","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DuoDroid_Walker_ToLeg","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"54ef8af2-6065-42a7-b28a-550e794d30b9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7b40bf8b-be12-49fb-888e-bebce10d533e","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7b40bf8b-be12-49fb-888e-bebce10d533e","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":{"name":"3b7ef5af-abea-45db-b9a2-b9cb604d12b0","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"7b40bf8b-be12-49fb-888e-bebce10d533e","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":{"name":"8a92e087-5ea9-4bbf-b888-976579bc4f7e","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DuoDroid_Walker_ToLeg","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"7b40bf8b-be12-49fb-888e-bebce10d533e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bae66f69-15c0-404f-b2e2-d2958c11dee9","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bae66f69-15c0-404f-b2e2-d2958c11dee9","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":{"name":"3b7ef5af-abea-45db-b9a2-b9cb604d12b0","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"bae66f69-15c0-404f-b2e2-d2958c11dee9","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":{"name":"8a92e087-5ea9-4bbf-b888-976579bc4f7e","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DuoDroid_Walker_ToLeg","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"bae66f69-15c0-404f-b2e2-d2958c11dee9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9abc158f-99f1-4f9b-bb71-ebfb3414c919","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9abc158f-99f1-4f9b-bb71-ebfb3414c919","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":{"name":"3b7ef5af-abea-45db-b9a2-b9cb604d12b0","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"9abc158f-99f1-4f9b-bb71-ebfb3414c919","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":{"name":"8a92e087-5ea9-4bbf-b888-976579bc4f7e","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DuoDroid_Walker_ToLeg","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"9abc158f-99f1-4f9b-bb71-ebfb3414c919","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"53807311-3259-4a59-8427-00b28871b15e","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"53807311-3259-4a59-8427-00b28871b15e","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":{"name":"3b7ef5af-abea-45db-b9a2-b9cb604d12b0","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"53807311-3259-4a59-8427-00b28871b15e","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":{"name":"8a92e087-5ea9-4bbf-b888-976579bc4f7e","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DuoDroid_Walker_ToLeg","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"53807311-3259-4a59-8427-00b28871b15e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"55be2bbd-c936-4c17-9dba-ae24866ede2b","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"55be2bbd-c936-4c17-9dba-ae24866ede2b","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":{"name":"3b7ef5af-abea-45db-b9a2-b9cb604d12b0","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"55be2bbd-c936-4c17-9dba-ae24866ede2b","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"LayerId":{"name":"8a92e087-5ea9-4bbf-b888-976579bc4f7e","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DuoDroid_Walker_ToLeg","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","name":"55be2bbd-c936-4c17-9dba-ae24866ede2b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_DuoDroid_Walker_ToLeg","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"d0d752dd-499d-4f63-8b10-7fab4bbfc551","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"54ef8af2-6065-42a7-b28a-550e794d30b9","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d19520cf-678b-4f34-ba61-abb46a74a469","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7b40bf8b-be12-49fb-888e-bebce10d533e","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5d512433-d2f7-4b69-87ca-d09b6f09d28a","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bae66f69-15c0-404f-b2e2-d2958c11dee9","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bf68e579-2318-4242-ab06-5db8c961bcec","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9abc158f-99f1-4f9b-bb71-ebfb3414c919","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"68a17d0a-47d1-4749-b3c6-daf56961a5c7","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"53807311-3259-4a59-8427-00b28871b15e","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"475c9aca-d403-4aa6-bc01-fa66c1522ad1","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"55be2bbd-c936-4c17-9dba-ae24866ede2b","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 9,
    "yorigin": 5,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_DuoDroid_Walker_ToLeg","path":"sprites/spr_DuoDroid_Walker_ToLeg/spr_DuoDroid_Walker_ToLeg.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"3b7ef5af-abea-45db-b9a2-b9cb604d12b0","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"8a92e087-5ea9-4bbf-b888-976579bc4f7e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "DuoDroid",
    "path": "folders/Sprites/Sprites/Game/Enemies/Hangar/DuoDroid.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_DuoDroid_Walker_ToLeg",
  "tags": [],
  "resourceType": "GMSprite",
}
/// @description Insert description here
// You can write your code in this editor
if checkDelay = 1 {
	if index != -1 {
		if global.healthCollectable[index] = 0 {
			instance_destroy()	
		}
	}
}
if state = "bye" {
	image_alpha = 0
	
}
if drop {
	if !place_meeting(x,y+1,obj_GroundParent) {
		ySpeed += 0.15
	}
}

if drop {
	if stallTime <= -600 {
		instance_destroy();	
	}
}

if place_meeting(x,y+ySpeed,obj_GroundParent) {
	ySpeed = 0
}

if drop {
	y+=ySpeed
	x+=xSpeed
}
stallTime -= global.gameSpeed
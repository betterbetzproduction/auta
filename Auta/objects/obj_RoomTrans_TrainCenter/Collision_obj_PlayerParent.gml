/// @description Insert description here
// You can write your code in this editor

if keyboard_check_pressed(global.kUp) && !active && visible {
	if scr_HasItem("Staircase Key") != -1 {
		textID = "UnlockGate"
		scr_DialogueList()
		active = true
		visible = false
		audio_play_sound(snd_MetalDoor,4,false)
		scr_RemoveItem(scr_HasItem("Staircase Key"))
		global.storyBeat = 9
		
		inst_1C35C1C7.talkList[0] = "HA_NPC_Sad_3"
	}
	else {
		textID = "LockedDoor"
		scr_DialogueList()	
		active = true
		
	}
}



if keyboard_check_pressed(global.kUp) && !active && !visible {
	active = true
	with instance_create_depth(x,y,depth-100,obj_FadeTrans) {
		toRoom = other.toRoom
		other.state = "cutscene"
		if other.toInst = noone {
			toX = other.toX
			toY = other.toY
		} else {
			toInst = other.toInst
		}
	
	}
}
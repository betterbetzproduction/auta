Size = image_xscale*300
if flicker = true {
    var z = choose(0.05,-0.05,0);
    lightSize += z;
    
    lightSize = clamp(lightSize, Size, Size + flickerdepth);
}

/*if lightStrength != 1 {
    lightStrength += 0.01;
}


/// @description Insert description here
// You can write your code in this editor
if sprite_index = spr_RockThrower_Windup {
	waitCounter = 0
	
	if obj_PlayerParent.x > x {
		dir = 1	
	}
	else {dir = -1}
	
	xSpeed = maxSpeed*dir
	ySpeed = -2
	
	sprite_index = spr_RockThrower_Throw
	
	grounded = false
	
	attacked = false
}

if sprite_index = spr_RockThrower_Land {
	waitCounter = 0
	
	sprite_index = spr_RockThrower
}
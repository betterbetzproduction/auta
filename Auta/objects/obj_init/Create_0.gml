 /// @description Insert description here
// You can write your code in this editor
global.screenshake = 0
global.currentweapon = "Brush"
global.respawnx = 0
global.respawny = 0
global.musictrack = snd_NoSound
global.fbskip = false
global.coins = 0
global.gameSpeed = 1 
global.aberTime = 0
   
//STORY SPECIFIC SETTINGS


//Get ready to insert some negatives here
//0 = Game Begins
//1 = Police guy interacted with
//2 = Seen morpheus guy

ini_open("saveData.ini");
// ----------- PROGRESSION ------------- //

global.gottenFirstCard = false

global.cutscene = false

global.storyBeat = 2

global.bigEnemy[0] = 1 //ini_read_real("Progression","BigEnemies",1) //Spiderbelly in rm_Hangar_Act4
global.canWallRun = true

for(i=0;i<20;i++) {
	global.chest[i] = 1
}

global.equipCollectable[0] = 1

global.healthCollectable[0] = 1
global.healthCollectable[1] = 1

//Equipment

///Area Specific Variabls

//0 - Weapon
//1 - Throw
//2 - Armor
//3 - Mobility
//4 - Shield
//5 - SubWeapon
//10 - Acc1
//11 - Acc2
//12 - Acc3

global.respawnRoom = rm_TrainCenter_1
global.respawnx = 110
global.respawny = 245

global.equipment[0] = ini_read_string("Variables","Equip0","Brush Blade")
global.equipment[1] = ini_read_string("Variables","Equip1","Basic Bracer")
global.equipment[2] = ini_read_string("Variables","Equip2","Tough Sweater")
global.equipment[3] = ini_read_string("Variables","Equip3","Scrappy Cleats")
global.equipment[4] = ini_read_string("Variables","Equip4","Force Shield")
global.equipment[5] = ini_read_string("Variables","Equip4","Nothing")

global.equipment[10] = ini_read_string("Variables","Equip10","Nothing")
global.equipment[11] = ini_read_string("Variables","Equip11","Nothing")
global.equipment[12] = ini_read_string("Variables","Equip12","Nothing")


//global.inventory[0] = ini_read_string("Variables","Inventory0","")
//global.inventory[1] = ini_read_string("Variables","Inventory1","")
//global.inventory[2] = ini_read_string("Variables","Inventory2","")
//global.inventory[3] = ini_read_string("Variables","Inventory3","")
//global.inventory[4] = ini_read_string("Variables","Inventory4","")
//global.inventory[5] = ini_read_string("Variables","Inventory5","")
//global.inventory[6] = ini_read_string("Variables","Inventory6","")
//global.inventory[7] = ini_read_string("Variables","Inventory7","")

for(i=0;i<8;i++) {
	global.inventory[0][0] = ""		//Name
	global.inventory[0][1] = 0		//Number
	global.inventory[0][2][0] = 0		//Power
	global.inventory[0][2][1] = 1		//Level
	global.inventory[0][2][2] = 0		//BaseFXType
	global.inventory[0][2][3] = 0		//BaseFXLevel
	global.inventory[0][2][4] = 0		//Aug1Type
	global.inventory[0][2][5] = 0		//Aug1Lv
	global.inventory[0][2][6] = 0		//Aug2Type
	global.inventory[0][2][7] = 0		//Aug2Lv
	global.inventory[0][2][8] = 0		//Aug3Type
	global.inventory[0][2][9] = 0		//Aug3Lv
	
	/*
	Types list
	0 - none
	1 - critical hit
	2 - fire normal
	3 - fire strength
	*/
}

//Stats
global.exp = 0
global.level = 1
global.maxExp = 20

global.baseAtk = 4
global.maxhp = 20
global.HP = -20
global.maxmp = 20
global.MP = -20
//Upgrades
global.haveOrb = ini_read_real("Variables","Inventory0",0)
global.canActivateTerminals = ini_read_real("Variables","Inventory0",0)
//Hangar
global.sparkActive = false

global.mapX = 0
global.mapY = 0

//Other Stuff
global.pal = ini_read_real("Variables","Palette",0); //The third value here will set the score variable if there is no save file
global.beatgame = ini_read_real("Variables","BeatGame?",false); //The third value here will set the score variable if there is no save file
global.speedrun = false
global.secs = 0
global.besttime = ini_read_real("Variables","BestTime",0); //The third value here will set the score variable if there is no save file
global.hardwon = ini_read_real("Variables","HardWon?",false); //The third value here will set the score variable if there is no save file

//CONTROL SETUP

global.kRight		= ini_read_real("Controls","kRight",vk_right)
global.kLeft		= ini_read_real("Controls","kLeft",vk_left)
global.kUp			= ini_read_real("Controls","kUp",vk_up)
global.kDown		= ini_read_real("Controls","kDown",vk_down)
global.kJump		= ini_read_real("Controls","kJump",ord("A"))
global.kAttack		= ini_read_real("Controls","kAttack",ord("S"))
global.kOrb			= ini_read_real("Controls","kOrb",ord("D"))
global.kInteract	= ini_read_real("Controls","kInteract",vk_space)
global.kSpecial		= ini_read_real("Controls","kSpecial",ord("W"))
global.kShield		= ini_read_real("Controls","kShield",vk_shift)

global.pRight2		= ini_read_real("Controls","pRight2",gp_padr)
global.pLeft2		= ini_read_real("Controls","pLeft2",gp_padl)
global.pUp2			= ini_read_real("Controls","pUp2",gp_padu)
global.pDown2		= ini_read_real("Controls","pDown2",gp_padd)
global.pJump		= ini_read_real("Controls","pJump",gp_face1)
global.pAttack		= ini_read_real("Controls","pAttack",gp_face3)
global.pOrb			= ini_read_real("Controls","pOrb",gp_shoulderrb)
global.pInteract	= ini_read_real("Controls","pInteract",gp_face2)
global.pShield		= ini_read_real("Controls","pShield",gp_shoulderlb)
global.pSpecial		= ini_read_real("Controls","pSpecial",gp_face4)

global.currentControl = obj_Auta
ini_close();

global.pad = 0


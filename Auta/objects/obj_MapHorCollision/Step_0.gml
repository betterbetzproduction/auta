if !place_meeting(x,y,obj_Auta) {
	dirFrom = (x > obj_PlayerParent.x ? -1:1)
}

if place_meeting(x,y,obj_Auta) {
	if active = false {
		active = true	
		
		dirMoved = dirFrom
		global.mapX-= dirMoved
	}
} else {
	if active = true {
		active = false
		dirFrom = (x > obj_PlayerParent.x ? -1:1)
		
		if dirFrom = dirMoved {
			global.mapX += dirMoved
		}	
	}
}
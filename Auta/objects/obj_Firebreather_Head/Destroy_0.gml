/// @description Insert description here
// You can write your code in this editor

if mustDrop {
	with instance_create_layer(x,y,layer,obj_PowerCard) {
		item = "Fizzle Card"
		sprite_index = spr_FizzleCard
	}
} else {
	var rand = round(random_range(0,20))
	if rand = 20 {
		with instance_create_layer(x,y,layer,obj_PowerCard) {
			item = "Fizzle Card"
			sprite_index = spr_FizzleCard
		}
	}
}
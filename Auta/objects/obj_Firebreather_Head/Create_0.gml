/// @description Insert description here
// You can write your code in this editor
scr_StandardSetup()
maxSpeed = 0.6

type = "slice"

drops[0,0] = obj_Coin
drops[1,0] = obj_Coin
drops[2,0] = noone
drops[3,0] = obj_HealthPickup
drops[4,0] = noone

nudgeWeight = 0

shootTimer = 0

grabbable = false
maxhp = 20
hp = maxhp

recovercounter = 0

flinchThreshold = 5

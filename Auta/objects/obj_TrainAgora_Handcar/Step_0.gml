/// @description Insert description here
// You can write your code in this editor
if state = 1 {
	xSpeed = 0.2	
	if x > 112 {
		state = 2	
	}
	sprite_index = spr_TrainAgora_HandCar
}
if state = 2 {
	xSpeed = -0.5
	if x < 64 {
		state = 1	
	}
}
if state = 3 {
	sprite_index = spr_TrainAgora_HandCar_Hurt
	xSpeed = -2
	if x < -8 {
		state = 1	
	}
	if damageCounter%30 = 0 {
		with instance_create_depth(x+random_range(-15,15),y+random_range(-5,15),depth-1,obj_OneTimeFX) {
			sprite_index = spr_VatB_MetalBall_Explode
			global.screenshake = 3
			audio_play_sound(snd_Crash,4,false)
		}		
	}
}
damageCounter += global.gameSpeed
if damageCounter = 121 {
	damageCounter = 0	
}
x+=xSpeed*global.gameSpeed
/// @description Insert description here
// You can write your code in this editor
counter+=global.gameSpeed
xSpeed = 0.4*dcos(counter)*global.gameSpeed
ySpeed = -0.2*dsin(counter*2)*global.gameSpeed

rotSpeed = 0.3*dcos(counter)

x+=xSpeed
y+=ySpeed

if place_meeting(x,y-3,obj_PlayerParent) && obj_PlayerParent.grounded = true{
	if dcos(image_angle) != 0 {
		playerOffsetX = (x-obj_PlayerParent.x)
		playerOffsetXflat = (x-obj_PlayerParent.x)/dcos(image_angle)
	}
	image_angle+=rotSpeed*global.gameSpeed
	playerOffsetXnew = playerOffsetXflat*dcos(image_angle)
	//playerOffsetY = (y-obj_PlayerParent.y)/
} else {
	playerOffsetX = "none"
	image_angle+=rotSpeed*global.gameSpeed
}


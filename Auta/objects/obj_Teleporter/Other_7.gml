/// @description Insert description here
// You can write your code in this editor

if sprite_index = spr_TeleporterOut {
	sprite_index = spr_Teleporter
	obj_Auta.x = x+22
	obj_Auta.y = y+29
	obj_Auta.visible = true
	obj_Auta.state = "orb"
	obj_Auta.animstate = "constant"
	obj_Auta.sprite_index = spr_AutaOrb
	global.cutscene = false
	obj_Music.musictrack = mus_HomogenousStars_PW
}
if sprite_index = spr_TeleporterIn {
	sprite_index = spr_TeleporterOut
	image_index = 0
	
	global.storyBeat = -4
}
/// @description Insert description here
// You can write your code in this editor
if global.gameSpeed < 1 && global.cutscene = false{
	global.aberTime = 10
}
if global.aberTime > 0 {
	global.aberTime--
	shader_reset();
	dis = clamp(global.aberTime/10,0,4);
	shader_set(shd_aberration);
	shader_set_uniform_f(dis_u, dis);

}

draw_surface_stretched(application_surface, 0, 0, window_get_width(), window_get_height())

shader_reset()
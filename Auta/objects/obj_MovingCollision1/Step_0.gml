/// @description Insert description here
// You can write your code in this editor

if path != noone {
	direction = path.image_angle+90

	if !place_meeting(x+xSpeed,y+ySpeed,path) {
		dir = -dir	
	}
	xSpeed = dcos(direction)*dir*totalSpeed
	ySpeed = -dsin(direction)*dir*totalSpeed
}
x += xSpeed*global.gameSpeed
y += ySpeed*global.gameSpeed


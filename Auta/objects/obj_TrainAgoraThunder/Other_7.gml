/// @description Insert description here
// You can write your code in this editor
if sprite_index = spr_TrainAgora_Thunder_Beam_2 {
	instance_destroy();
	with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
		sprite_index = spr_LargeBurst	
		if !audio_is_playing(snd_EnemyDeath) {
			audio_play_sound(snd_EnemyDeath,4, false)
		}
	}
}

if sprite_index = spr_TrainAgora_Thunder_Charge {
	sprite_index = spr_TrainAgora_Thunder_Beam_2	
	image_index = 0
	audio_play_sound(snd_Lightbat_Slam,4,false)
}

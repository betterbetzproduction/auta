/// @description Insert description here
// You can write your code in this editor
x+=0.3*(toX-x)*global.gameSpeed
counter+=global.gameSpeed



image_speed = global.gameSpeed

if sprite_index = spr_TrainAgora_Thunder_Beam_2	{
	var yPlace = 0
	var q = 0
	do {
		yPlace += 16
		q+=1
	}
	until (place_meeting(x,y+yPlace,obj_Collision) || q = 20);
	yPlace+=16
	if collision_rectangle(x-1,y,x+1,y+yPlace,obj_PlayerParent,false,true) {
		scr_HurtKelly(4)	
	}
	if collision_rectangle(x-1,y,x+1,y+yPlace,obj_EnemyParent,false,true) {
		var q = 0 {
			do {
				q+=1
				var eInst = instance_place(x,y+q,obj_EnemyParent)
			} until (q=64||eInst != noone)
			if eInst != noone {
				with eInst {
					scr_EnemyDie()	
				}
			}
		}
	}
	if collision_rectangle(x-1,y,x+1,y+yPlace,obj_TrainAgora_Handcar,false,true) {
		obj_TrainAgora_Handcar.state = 3
	}
}
/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if respawnOnGround && global.currentControl.grounded && x = -100{
	if !place_meeting(baseX,y,obj_PlayerParent) {
	x = baseX	
		button.image_index = 0
		audio_play_sound(snd_Err,4,false)
		var ex = x+sprite_width/2*image_xscale
		var ey = y+sprite_height/2*image_yscale
		with instance_create_depth(ex,ey,depth-1,obj_OffScreenFX) {
			baseX = ex
			baseY = ey
		}	
	}
}
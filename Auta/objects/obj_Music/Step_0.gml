
if global.musictrack != musictrack {
	audio_sound_gain(global.musictrack,0,500)
	//temptrack = global.musictrack
	//global.musictrack = musictrack
	if audio_exists(global.musictrack) {
		if audio_sound_get_gain(global.musictrack) < 0.1 {
			audio_stop_sound(global.musictrack)
			global.musictrack = musictrack
			if musictrack != snd_NoSound {
				if musictrack = mus_Starshine_PW_INTRO {
					audio_play_sound(musictrack,100,false)
				} else {
					audio_play_sound(musictrack,100,true)
				}
				audio_sound_gain(musictrack,0,0)
			}
			audio_sound_gain(musictrack,1,1200)
		}
	}
	if global.musictrack = snd_NoSound {
		global.musictrack = musictrack
		if musictrack != snd_NoSound {
			audio_play_sound(musictrack,100,true)
			audio_sound_gain(musictrack,0,0)
		}
		audio_sound_gain(musictrack,1,1200)
	}
	
}
if !audio_exists(global.musictrack) && global.musictrack = mus_Starshine_PW_INTRO {
	musictrack = mus_Starshine_PW_NOINTRO
}
if audio_exists(temptrack) {
	if audio_sound_get_gain(temptrack) = 0 {
		audio_stop_sound(temptrack)
	}
}
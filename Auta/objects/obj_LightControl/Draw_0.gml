draw_set_alpha(1)
if darkness > visdark {
    visdark += 0.01
}
if darkness < visdark {
    visdark -= 0.01
}

if !surface_exists(surf) {
	surf = surface_create(room_width,room_height);	
}

surface_set_target(surf);

draw_clear(color)


with(obj_light)
{
	if clear = true {
        gpu_set_blendmode(bm_subtract);
        draw_sprite_ext(sprite_index,1,x*other.surfScale,y*other.surfScale,lightSize*other.surfScale,lightSize*other.surfScale,0,color,lightStrength);
        gpu_set_blendmode(bm_normal);
			
    }
    else {
			
        draw_sprite_ext(sprite_index,1,x*other.surfScale,y*other.surfScale,lightSize*other.surfScale,lightSize*other.surfScale,0,color,lightStrength);
				
    }
}

depthDiv = 1

if layer_get_name(layer) = "highLight" {
	with(obj_HDLight)
	{
		if clear = true {
	        gpu_set_blendmode(bm_subtract);
	        draw_sprite_ext(sprite_index,1,x*other.surfScale,y*other.surfScale,lightSizeX*other.surfScale,lightSizeY*other.surfScale,0,color,lightStrength);
	        gpu_set_blendmode(bm_normal);
			
	    }
	    else {

			draw_sprite_ext(sprite_index,1,x*other.surfScale,y*other.surfScale,lightSizeX*other.surfScale,lightSizeY*other.surfScale,0,color,lightStrength);
	    }
	}
}
else {
	with(obj_HDLight)
	{
		if clear = true {
	        gpu_set_blendmode(bm_subtract);
	        draw_sprite_ext(sprite_index,1,x*other.surfScale,y*other.surfScale,lightSizeX*other.surfScale*other.depthDiv,lightSizeY*other.depthDiv*other.surfScale,0,color,lightStrength);
	        gpu_set_blendmode(bm_normal);
			
	    }
	    else {

			draw_sprite_ext(sprite_index,1,x*other.surfScale,y*other.surfScale,lightSizeX*other.depthDiv*other.surfScale,lightSizeY*other.depthDiv*other.surfScale,0,color,lightStrength);
	    }
	}
}
if layer_get_name(layer) = "highLight" {
	with(obj_PlayerParent)
	{
	    gpu_set_blendmode(bm_subtract);
	    draw_sprite_ext(spr_HDLight1,1,x*other.surfScale,y*other.surfScale,lightSize*other.surfScale,lightSize*other.surfScale,0,c_white,1);
	    gpu_set_blendmode(bm_normal);

	}
} else {
	with(obj_PlayerParent)
	{
		
		depthDiv = 2/3
	    gpu_set_blendmode(bm_subtract);
	    draw_sprite_ext(spr_HDLight1,1,x*other.surfScale,y*other.surfScale,lightSize*other.depthDiv*other.surfScale,lightSize*other.depthDiv*other.surfScale,0,c_white,1);
	    gpu_set_blendmode(bm_normal);

	}
}

if color = c_black{
gpu_set_blendmode(bm_normal)

} else {
gpu_set_blendmode(bm_add)


}

surface_reset_target();
draw_surface_ext(surf,0,0,1/surfScale,1/surfScale,0,c_white,visdark);
gpu_set_blendmode(bm_normal)
/*
if room = rm_CrystalCaverns_3{
draw_surface_ext(surf,0,0,1/surfScale,1/surfScale,0,c_white,(view_yview[0]+100)/768);
}
else {

}
//just in case, draw_set_blend_mode(bm_src_color); draw_set_blend_mode(bm_normal);
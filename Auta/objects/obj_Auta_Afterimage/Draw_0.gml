/// @description Insert description here
// You can write your code in this editor
if obj_Auta.iframes <= 0 {
	if delay > 0 {
		shader_set(shd_ColorReplace);
		shader_set_uniform_f_array(_uniColor, [1*alpha, 1-alpha, 1, 1-alpha]);
		draw_self();
		shader_reset();
	}
} else {
	if delay > 0 {
		shader_set(shd_ColorReplace);
		shader_set_uniform_f_array(_uniColor, [1, 1, 1, 1]);
		draw_self();
		shader_reset();
	}
	
}
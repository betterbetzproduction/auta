/// @description Insert description here
// You can write your code in this editor

if delay = 0 {
	if instance_exists(creator) {
		
		sprite_index = creator.sprite_index
		image_index = creator.image_index
		image_xscale = creator.image_xscale
		image_angle = creator.image_angle

		delay++
	
		visible = true
	
		if creator = obj_Collision {
			instance_destroy();
		}	
	} else {
		instance_destroy();	
	}
} 
alpha -= 0.1*global.gameSpeed
if alpha <= 0 {
	instance_destroy();	
}
depth+=0.1
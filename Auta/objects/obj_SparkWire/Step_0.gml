counter += global.gameSpeed

var active = true
if global.sparkActive = false {active = false}

gdinst = instance_place(x,y,obj_CamRegionCollision)
if gdinst != noone {
	if gdinst.active = false {
		var active = false
	}
}

if counter > 60 {
	if active = true {
		visible = true
	} else {
		counter = 0	
	}
} else {
	visible = false	
}


if counter > 120 {
	visible = true
	
	if active {
		var yPlace = 0
		var q = 0
		do {
			yPlace += 16
			q+=1
		}
		until (place_meeting(x,y+yPlace,obj_Collision) || q = 20);
		yPlace+=16
		if collision_rectangle(x-1,y,x+1,y+yPlace,obj_PlayerParent,false,true) {
			scr_HurtKelly(4)	
		}
	
		if collision_rectangle(x-1,y,x+1,y+yPlace,obj_ShockCollision,false,true) {
			for(var i = 0; i < 20; i++) {
				var gdinst = instance_place(x,y+16*i,obj_ShockCollision)
				if gdinst != noone {
					gdinst.activated = true;
					gdinst.deactiveDelay = 2;
					break;
				}
			
			}
		}
		
		if collision_rectangle(x-1,y,x+1,y+yPlace,obj_Activator,false,true) {
			for(var i = 0; i < 20; i++) {
				var gdinst = instance_place(x,y+16*i,obj_Activator)
				if gdinst != noone {
					gdinst.active = true
					break;
				}
			
			}
		}
	
	}
}
if counter > 180 {
	counter = 0	
}
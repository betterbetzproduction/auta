/// @description Insert description here
// You can write your code in this editor
var spd = global.gameSpeed
canJumpCount--


//y += ySpeed*spd
//if ySpeed > 6 {
//	ySpeed = 6	
//}


if behaviour = "run" {
	if image_xscale = 1 {
		rightkey = true	
	}
	if image_xscale = -1 {
		leftkey = true	
	}
	
	if place_meeting(x+xSpeed,y,obj_Collision) {
		image_xscale = -image_xscale 
		xSpeed = -xSpeed
	}
}

if behaviour = "watch" {
	if obj_PlayerParent.x > x {
		image_xscale = 1	
	} else {
		image_xscale = -1	
	}
}
if behaviour = "bounce" {
	if grounded {
		grounded = false
		ySpeed = -2
	}
}

if state = "normal" {
	if grounded {
		var accel = 0.2*spd	
		var stop = 0.4*spd
		
	} else {
		var accel = 0.1*spd	
		var stop = 0.0*spd
		
		if jumpUpwardsCancel {
			if ySpeed < -0.5 && !jumpheld {
				ySpeed += 0.1*spd
			}
			if ySpeed >= -0.5 {
				jumpUpwardsCancel = false	
			}
		}
	}
	
	if canJumpCount > 0 {
		if jumpkey && !downkey {
			ySpeed = jumpforce
			if place_meeting(x,y+4,obj_WaterCollision) && !place_meeting(x,y+4,obj_Collision){
				ySpeed = jumpforce*0.75	
			}
			
			grounded = false
			audio_play_sound(snd_Jump,4,false)
			jumpUpwardsCancel = true
			jumpkey = false
		}	
	}
	
	if rightkey {
		if xSpeed < maxSpeed {
			if !place_meeting(x+maxSpeed,y,obj_Collision) {
				xSpeed += accel	
			}
		}
		if xSpeed > maxSpeed {
			xSpeed = maxSpeed	
		}
	}
	if leftkey {
		if xSpeed > -maxSpeed {
			if !place_meeting(x-maxSpeed,y,obj_Collision) {
				xSpeed -= accel	
			}
		}
		if xSpeed < -maxSpeed {
			xSpeed = -maxSpeed	
		}
	}
	if !leftkey && !rightkey {
		if xSpeed > 0 {
			if xSpeed > stop {
				xSpeed -= stop	
			} else {
				xSpeed = 0	
			}
		}
		if xSpeed < 0 {
			if xSpeed < -stop {
				xSpeed += stop	
			} else {
				xSpeed = 0	
			}
		}
	}
}



scr_Collision(false);
if grounded = true && sprite_index = spr_PoliceGuy1 {
	sprite_index = spr_PoliceGuy
	global.screenshake = 2
	audio_play_sound(snd_Crash,4,false)
}	

if active = true && !instance_exists(obj_textbox) {
		obj_PlayerParent.talkTarg = noone
	
	keyboard_clear(global.kInteract)
	with obj_PlayerParent {
		keyboard_clear(global.kInteract)	
		state = "normal"
		animstate = "normal"
	}

	active = false
	state = "bye"
	audio_play_sound(snd_o_O,4,false)
	sprite_index = spr_Campeye1

}
if state = "bye" {
	image_alpha -= 0.02
	
	if image_alpha <= 0 {
		instance_destroy()	
		obj_Auta.state = "normal"
	}
}
if behaviour != "cutscene" {
	rightkey = false
	leftkey = false
}
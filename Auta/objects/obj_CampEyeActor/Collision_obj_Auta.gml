/// @description Insert description here
// You can write your code in this editor

if other.aInteract {
	if !instance_exists(obj_textbox) {
		if obj_Auta.grounded = true {
			obj_PlayerParent.state = "cutscene"
			obj_PlayerParent.xSpeed = 0
			obj_PlayerParent.animstate = "normal"
			obj_PlayerParent.talkTarg = id
			
			with instance_create_layer(x,y,layer,obj_DialogueHelper) {
				var text = ""
				var mySpeaker = -1
				speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
	
				var t = 0//line 1
				text[t] = "...Hey kid. You found my humble abode.";
				mySpeaker[t] = speaker[0]
	
				t++
				text[t] = "What? No. No, I'm not upset.";
				mySpeaker[t] = speaker[0]
				
				t++
				text[t] = "You're gonna do good if you keep that up.";
				mySpeaker[t] = speaker[0]
				
				t++
				text[t] = "Keep an eye all over... that's what I say.";
				mySpeaker[t] = speaker[0]
				create_dialogue(text, mySpeaker,0,0,0,0,0,0,0);	
			}
			
			
			active = true
		}
	}
}


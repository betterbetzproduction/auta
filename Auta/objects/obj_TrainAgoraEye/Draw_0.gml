/// @description Insert description here
// You can write your code in this editor
if !instance_exists(obj_TrainAgora) {
	scr_EnemyDie()
}
if hp > 0 {
	var rot = point_direction(x,y,obj_TrainAgora.x,obj_TrainAgora.y)
	draw_sprite_ext(spr_TrainAgoraEye_Thread,1,x,y,distance_to_point(obj_TrainAgora.x,obj_TrainAgora.y)/24,1,rot,c_white,1)
}
scr_FocusDistort(c_red,c_blue)

scr_EnemyDraw();
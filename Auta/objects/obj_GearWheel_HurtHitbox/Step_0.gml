/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

x=obj_GearWheel_Main.x
y=obj_GearWheel_Main.y

if state = "hurt" {
	stuntimer--
	
	if obj_GearWheel_Main.sprite_index = spr_GearWheel_Body {
		obj_GearWheel_Main.sprite_index = spr_GearWheel_BodyStun	
	}
	
	if stuntimer = 0 {
		state = "normal"
		
		if obj_GearWheel_Main.sprite_index = spr_GearWheel_BodyStun {
			obj_GearWheel_Main.sprite_index = spr_GearWheel_Body	
		}
	}
}

obj_GearWheel_Main.hp = hp

if invincible {

	if !instance_exists(contactby){
		invincible = false	
	}
	if instance_exists(contactby) {
		if !collision_rectangle(bbox_left-5,bbox_top-5,bbox_right+5,bbox_bottom+5,contactby,true,true) {
			invincible = false
		}
	}
}
/// @description Insert description here
// You can write your code in this editor
if state = "halfDying" {
	display_set_gui_size(320,180)
	draw_set_color(c_white)
	draw_set_alpha(fadeAlpha*2)
	draw_sprite_stretched(spr_WhiteGradient_Sidways,0,0,0,320,180)
	if fadeAlpha > 0.5 {
		draw_set_alpha((fadeAlpha-0.5)*2)
		draw_sprite_stretched(spr_WhiteGradient_Sidways,0,0,0,320,180)	
	}
	if fadeAlpha > 1 {
		draw_set_alpha((fadeAlpha-1)*2)
		draw_sprite_stretched(spr_WhiteGradient_Sidways,0,0,0,320,180)	
	}
	draw_set_alpha(fadeAlpha)
	draw_rectangle(0,0,320,180,false)
	fadeAlpha += 0.008
	if fadeAlpha >= 1.5 {
		if room = rm_HangarTrainBattle_Outside {
			room = rm_FightTest
		}
		if room = rm_HangarTrainBattle {
			room = rm_HangarTrainBattle_Outside
		}
	}
	
	if !instance_exists(obj_FastSnowGen) {
		instance_create_layer(504,592, "Instances", obj_FastSnowGen)
	}
}

if state = "halfLiving" {
	sprite_index = spr_TrainAgora_HalfHealth
	display_set_gui_size(320,180)
	draw_set_color(c_white)
	draw_set_alpha(fadeAlpha)
	draw_sprite_stretched(spr_WhiteGradient,0,0,0,320,180)
	draw_rectangle(0,0,320,180,false)
	fadeAlpha -= 0.05
	if fadeAlpha < 0 {
		if fadeAlpha%1 = 0 {
			with instance_create_depth(x+random_range(-20,20),y+random_range(0,30),depth-1,obj_OneTimeFX) {
				sprite_index = spr_VatB_MetalBall_Explode
				global.screenshake = 3
				audio_play_sound(snd_Crash,4,false)
			}	
		}
	}
	if fadeAlpha = -5 {
		state = "normal"
		sprite_index = spr_TrainAgora_HalfHealth1
		audio_play_sound(snd_Crash,4,false)
		global.screenshake = 2
		phase = "choosePhase"
		lastPhase = "thunderBeams"
		
		with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
			sprite_index = spr_LargeBurst
			global.screenshake = 3
		}	
	}
}
if phase = "voidAttack" {
	if subphase = 0 {
		display_set_gui_size(320,180)
		draw_set_color(c_white)
		draw_set_alpha(fadeAlpha*2)
		draw_sprite_stretched(spr_WhiteGradient_Sidways,0,0,0,320,180)
		if fadeAlpha > 0.5 {
			draw_set_alpha((fadeAlpha-0.5)*2)
			draw_sprite_stretched(spr_WhiteGradient_Sidways,0,0,0,320,180)	
		}
		if fadeAlpha > 1 {
			draw_set_alpha((fadeAlpha-1)*2)
			draw_sprite_stretched(spr_WhiteGradient_Sidways,0,0,0,320,180)	
		}
		draw_set_alpha(fadeAlpha)
		draw_rectangle(0,0,320,180,false)
		fadeAlpha += 0.05
		if fadeAlpha >= 1.5 {
			subphase = 1
			distDist = 3
			
			layer_set_visible(layer_get_id("Backgrounds_2"),false)
			layer_set_visible(layer_get_id("Backgrounds_1"),false)
			layer_set_visible(layer_get_id("Mountains"),false)
			layer_set_visible(layer_get_id("Backgrounds_4"),false)
			layer_set_visible(layer_get_id("Train"),false)
			
			layer_set_visible(layer_get_id("Void1"),true)
			layer_set_visible(layer_get_id("TrainVoid"),true)
			//layer_set_visible(layer_get_id("Void2"),true)
			
			obj_Auta.sil = true
			obj_TrainAgora_Handcar.image_blend = c_black
			sprite_index = spr_TrainAgora_Void
			void = true
		}
	}
	if subphase = 1 {
		display_set_gui_size(320,180)
		draw_set_color(c_white)
		draw_set_alpha(fadeAlpha)
		draw_sprite_stretched(spr_WhiteGradient,0,0,0,320,180)
		draw_rectangle(0,0,320,180,false)
		fadeAlpha -= 0.05	
	}
}
draw_set_alpha(1)
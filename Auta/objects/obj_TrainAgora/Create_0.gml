/// @description Insert description here
// You can write your code in this editor
scr_StandardSetup()
maxSpeed = 0.6

type = "slice"
state = "normal"
grounded = false

drops[0,0] = obj_Coin
drops[1,0] = obj_Coin
drops[2,0] = noone
drops[3,0] = obj_HealthPickup
drops[4,0] = noone
distDist = 0
nudgeWeight = 2

shootTimer = 0

grabbable = false
maxhp = 100
hp = maxhp

void = false

recovercounter = 0

enemyIndex = 0

baseY = y
baseX = x
hovState = 0

shake = -100

phase = "choosePhase"
lastPhase = ""
subphase = 0
phaseCount = 0

obj_GUI.bossTarg = id


fadeAlpha = -1

if room = rm_HangarTrainBattle_Outside {hp = hp/2; state = "halfLiving"; phase = "halfLiving"; fadeAlpha = 1}
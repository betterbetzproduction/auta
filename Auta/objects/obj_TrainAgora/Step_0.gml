/// @description Insert description here
// You can write your code in this editor
var spd = global.gameSpeed


image_xscale = 1
scr_EnemyStandard(spr_ChuckerHurt);
grounded = true



if hitstunTimer > 0 {exit};
//if state = "normal" {
//	if grounded {
//		var accel = 0.15
//		var stop = 0.1
		
//	} else {
//		var accel = 0
//		var stop = 0
//	}
	
//	if xSpeed > 0 {
//		if xSpeed > stop {
//			xSpeed -= stop	
//		} else {
//			xSpeed = 0	
//		}
//	}
//	if xSpeed < 0 {
//		if xSpeed < -stop {
//			xSpeed += stop	
//		} else {
//			xSpeed = 0	
//		}
//	}

//}
switch phase {
	case "choosePhase":
		do {
			var pickRand = choose(0,1,2);	
			switch pickRand {
				case 0:
					phase = "thunderBeams"
					break;
				case 1:
					phase = "ElecRing"
					break;
				case 2:
					phase = "enemyEyes"
					break;
				case 3:
					phase = "voidAttack"
					break;
				
			}
		}
		until (phase != lastPhase)
		break;
	case "thunderBeams":
		switch subphase {
			case 0://288 624
				xSpeed = 0
				ySpeed = 0
				x+=0.1*(baseX-x)
				y+=0.1*((baseY-50)-y)
				if abs(baseX-x) < 1 && abs((baseY-50)-y) < 1 {
					subphase++	
					
					if room = rm_HangarTrainBattle_Outside {
						with instance_create_layer(x,y,"Instances",obj_TrainAgoraRunner) {
							ySpeed = -2
							xSpeed = 1
						}
						with instance_create_layer(x,y,"Instances",obj_TrainAgoraRunner) {
							ySpeed = -2
							xSpeed = -1
						}
					}
				}
				phaseCount = 0
				break;
			case 1:	
				xSpeed = 0
				ySpeed = 0
				phaseCount+=global.gameSpeed
				if phaseCount%45 = 0 && phaseCount < 360{
					with instance_create_layer(x,y, "instances", obj_TrainAgoraThunder) {
						toX = obj_PlayerParent.x
					}
				}
				if phaseCount >= 360 {
					if !instance_exists(obj_TrainAgoraThunder) {
						subphase = 0	
						phaseCount = 0
						lastPhase = phase
						phase = "choosePhase"	
					}
				}
				break;
		}
		break;
	case "enemyEyes":
		switch subphase {
			case 0:
				if room = rm_HangarTrainBattle_Outside {
					var rand = choose(0,1)
					switch rand {
						case 0:
							with instance_create_layer(x,y,"Collision",obj_TrainAgoraEye) {
								xSpeed = -2
							}
							with instance_create_layer(x,y,"Collision",obj_TrainAgoraMouth) {

								ySpeed = 2	
							}
				
							with instance_create_layer(x,y,"Collision",obj_TrainAgoraEye) {

								xSpeed = 2
							}
							break;
						case 1:
							with instance_create_layer(x,y,"Collision",obj_TrainAgoraMouth) {
								xSpeed = -2
							}
							with instance_create_layer(x,y,"Collision",obj_TrainAgoraEye) {

								ySpeed = 2	
							}
				
							with instance_create_layer(x,y,"Collision",obj_TrainAgoraMouth) {

								xSpeed = 2
							}
							break;
					}
				} else {
					with instance_create_layer(x,y,"Collision",obj_TrainAgoraEye) {
						xSpeed = -1
						ySpeed = 1
					}
					with instance_create_layer(x,y,"Collision",obj_TrainAgoraMouth) {
						xSpeed = 1
						ySpeed = 1	
					}
				}
				subphase = 1
				break;
			case 1:
				var thresh = 0.7
				var grav = 0.04
				if hovState = 0 {
					ySpeed += grav
					if ySpeed > thresh {
						hovState = 1	
						if y > baseY
						ySpeed = thresh
					}
				}
				if hovState = 1 {
					ySpeed -= grav
					if ySpeed < -thresh {
						ySpeed = -thresh	
					}
					if y < baseY {
						hovState = 0	
					}
				}
				if obj_PlayerParent.x > x {
					xSpeed += 0.03
					if xSpeed > 0.5 {
						xSpeed = 0.5	
					}
					image_xscale = -1
				} else {
					xSpeed -= 0.03
					if xSpeed < -0.5 {
						xSpeed = -0.5	
					}
					image_xscale = 1
				}
				
				if !instance_exists(obj_TrainAgoraEye) && !instance_exists(obj_TrainAgoraMouth) {
					subphase = 0	
					phaseCount++
					if phaseCount = 2 {
						subphase = 0	
						phaseCount = 0
						lastPhase = phase
						phase = "choosePhase"
					}
				}
				break;
		}
		break;
	case "voidAttack":
		switch subphase {
			case 0:
				
				break;
		}
		break;
	case "ElecRing":
		switch subphase {
			case 0://288 624
				xSpeed = 0
				ySpeed = 0
				x+=0.1*(baseX-x)
				y+=0.1*(baseY-y)
				if abs(baseX-x) < 1 && abs(baseY-y) < 1 {
					subphase++	
					
					if room = rm_HangarTrainBattle_Outside {
						with instance_create_layer(x,y,"Instances",obj_TrainAgoraRunner) {
							ySpeed = -2
							xSpeed = 1
						}
						with instance_create_layer(x,y,"Instances",obj_TrainAgoraRunner) {
							ySpeed = -2
							xSpeed = -1
						}
					}
				}
				break;
			case 1:
				phaseCount+=4*spd
				
				if phaseCount <= 1080 {
					if phaseCount%48 = 0 {
						with instance_create_depth(x,y,depth+1,obj_DirectionBullet) {
							sprite_index = spr_TrainAgoraEye_AttackParticle1
							direction = other.phaseCount
							if direction < 180 {
								direction += 180
							}
							speed = 2
						}
					}
				}
				x = baseX + 40*dsin(phaseCount/3)
				y = baseY + 10*dsin(phaseCount/1.5)
				if phaseCount >= 1260 {
					subphase = 0	
					phaseCount = 0
					lastPhase = phase
					phase = "choosePhase"
				}
				break;
				
			
		}
	
		break;
}
if sprite_index = spr_TrainAgora_HalfHealth1 {
	switch(shake) {
	case -100:
		shake = random_range(-1,1)
		x+=shake
		break;
	default:
		x-=shake
		shake = -100
		break;
	}
}

if state = "hurt" {
	//recovercounter -= spd
	//if recovercounter <= 0 {
		state = "normal"	
		sprite_index = spr_TrainAgora
		if room = rm_HangarTrainBattle_Outside {
			sprite_index = spr_TrainAgora_HalfHealth1	
		}
		if void {
			sprite_index = spr_TrainAgora_Void	
		}
	//}
}
y+=ySpeed*global.gameSpeed
x+=xSpeed*global.gameSpeed

if hp <= maxhp/2 && room = rm_HangarTrainBattle {
	hp = maxhp/2
	sprite_index = spr_TrainAgora_HalfHealth
	
	if instance_exists(obj_EnemyParent) {
		with obj_EnemyParent {
			if object_index != obj_TrainAgora {
				scr_EnemyDie()	
			}
		}
	}
	if instance_exists(obj_PainCollision) {
		with obj_PainCollision {
			instance_destroy();	
		}
	}
	
	phase = "halfDying"
	state = "halfDying"
	xSpeed = 0
	ySpeed = 0
	obj_Music.musictrack = snd_NoSound
}


if hp <= 1 && room = rm_HangarTrainBattle_Outside {
	hp =1
	sprite_index = spr_TrainAgora_HalfHealth
	
	if instance_exists(obj_EnemyParent) {
		with obj_EnemyParent {
			if object_index != obj_TrainAgora {
				scr_EnemyDie()	
			}
		}
	}
	if instance_exists(obj_PainCollision) {
		with obj_PainCollision {
			instance_destroy();	
		}
	}
	
	phase = "halfDying"
	state = "halfDying"
	xSpeed = 0
	ySpeed = 0
	obj_Music.musictrack = snd_NoSound
}

if state = "cannonball" {
	scr_EnemyCannonball(spr_Chad_Cannonball)
	var c = true
} else {
	var c = false
}


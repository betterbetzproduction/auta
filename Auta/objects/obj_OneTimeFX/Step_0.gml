/// @description Insert description here
// You can write your code in this editor
image_speed = global.gameSpeed
if fade = true {
	image_alpha -= 0.05	
}
if follow !=-1 && offsetX = -100 {
	offsetX = follow.x-x
	offsetY = follow.y-y
}
if follow != -1 {
	x = follow.x-offsetX
	y = follow.y-offsetY
}
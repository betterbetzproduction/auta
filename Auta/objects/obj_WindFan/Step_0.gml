/// @description Insert description here
// You can write your code in this editor
if image_angle = 0 {
	if collision_rectangle(bbox_left,bbox_bottom,bbox_right,bbox_top-dist,obj_PlayerParent,false,true) {
		obj_PlayerParent.ySpeed -= 0.2	
	}
}
if image_angle = 270 {
	if collision_rectangle(bbox_left,bbox_bottom,bbox_right+dist,bbox_top,obj_PlayerParent,false,true) {
		obj_PlayerParent.xSpeed += 0.2
	}
}
if image_angle = 90 {
	if collision_rectangle(bbox_left-dist,bbox_bottom,bbox_right,bbox_top,obj_PlayerParent,false,true) {
		if obj_PlayerParent.grounded = false {
			obj_PlayerParent.xSpeed -= 0.2
		}
		else {
			obj_PlayerParent.x-=0.6
		}
	}
}
if image_angle = 180 {
	if collision_rectangle(bbox_left,bbox_bottom+dist,bbox_right,bbox_top,obj_PlayerParent,false,true) {
		obj_PlayerParent.ySpeed += 0.2	
	}
}

if path != noone {
	direction = path.image_angle+90

	if !place_meeting(x+xSpeed,y+ySpeed,path) {
		dir = -dir	
	}
	xSpeed = dcos(direction)*dir*totalSpeed
	ySpeed = -dsin(direction)*dir*totalSpeed
}
x += xSpeed*global.gameSpeed
y += ySpeed*global.gameSpeed

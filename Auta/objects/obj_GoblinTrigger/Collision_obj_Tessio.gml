if state= "dormant" {
	layer_set_visible("Tiles_5",true)
	layer_set_visible("Tiles_6",true)
	state = "active"
	barrier1 = instance_create_layer(2440,96,layer,obj_Collision)
	with barrier1 {
		image_yscale = 11	
	}
	barrier2 = instance_create_layer(2608,96,layer,obj_Collision)
	with barrier2 {
		image_yscale = 11	
	}
	global.screenshake = 1.5
	audio_play_sound(snd_BlockBreak,4,false)
	instance_create_layer(2547,120,layer,obj_AlexGoblin)
	
	obj_DefaultCamera.xmin = 2511
	obj_DefaultCamera.xmax = 2550
	obj_DefaultCamera.ymin = 144
	obj_DefaultCamera.ymax = 144
}
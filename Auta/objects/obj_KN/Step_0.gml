/// @description Insert description here
// You can write your code in this editor
image_alpha = 1
if iframes > 0 {
	image_alpha = 0.7	
	iframes--
}
if state = "active" {
	if keyboard_check(vk_up) {
		if y > 32 {
			y -= 2	
		}
		if y < 32 {
			y = 32	
		}
	}
	if keyboard_check(vk_down) {
		if y < 160 {
			y += 2	
		}
		if y > 160 {
			y = 160	
		}
	}
	if keyboard_check(vk_right) {
		if x < 224 {
			x += 2	
		}
		if x > 224 {
			x = 224	
		}
	}
	if keyboard_check(vk_left) {
		if x > 32 {
			x-=2	
		}
		if x < 32 {
			x = 32	
		}
	}
	if (keyboard_check(ord("Z")) || keyboard_check(ord("X"))|| keyboard_check(vk_space)) && shootcounter = 0 {
		instance_create_depth(x,y,depth+1,obj_KNFireball)	
		shootcounter = 15
		audio_play_sound(snd_NKFire,4,false)
	}
	sprite_index = spr_KN
	if shootcounter > 0 {
		sprite_index = spr_KN_Fire	
		shootcounter--
	}
	if hp <= 0 {
		state = "dead"
		ySpeed = -1
	}
}
if state = "dead" {
	image_angle+=3
	ySpeed+=0.07
	y+=ySpeed
	x-=0.5
	if y > 200 {
		with instance_create_depth(x,y,depth-100,obj_FadeTrans) {
			toRoom = room
			toX = 0
			toY = 0
			global.fbskip = 1
			if instance_exists(obj_FBAlex) {
				if obj_FBAlex.hp <= obj_FBAlex.maxhp/2 {
					global.fbskip = 2	
				}
			} else {
				if obj_FBAlex_Hardmode.hp <= obj_FBAlex_Hardmode.maxhp/2 {
					global.fbskip = 2	
				}
			}
		}
	}
}
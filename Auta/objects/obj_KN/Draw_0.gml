/// @description Insert description here
// You can write your code in this editor
draw_set_alpha(image_alpha)

pal_swap_set(my_shader,spr_KellyPallettes,current_pal,true);
{
    draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha);
}
pal_swap_reset();

draw_set_alpha(1);

draw_set_alpha(hpalpha);
draw_set_color(c_black)
draw_rectangle(x-14,y-sprite_height/2-1,x+14,y-sprite_height/2-5,false)
draw_set_color(c_red)
if hp > 0 {
   draw_rectangle(x-13,y-sprite_height/2-2,x-13+(26*hp/maxhp),y-sprite_height/2-4,false)
}
draw_set_alpha(1);

if hptimer <= 0 && hpalpha > 0 {
    hpalpha -= 0.05
}
if hptimer > 0 && hpalpha < 1 {
    hpalpha += 0.2
}
hptimer--
/// @description Insert description here
// You can write your code in this editor
with other {
	if state != "slide" && state != "dead" {
		grounded= false
		ySpeed += other.ySpeedaccel
		if ySpeed < other.ySpeedmax {
			ySpeed = other.ySpeedmax	
		}
		if place_meeting(x,y+ySpeed,obj_Collision) {
			ySpeed = 0	
		}
		if state = "hurt" {
			state = "normal"	
			animstate = "normal"
		}
	}
}
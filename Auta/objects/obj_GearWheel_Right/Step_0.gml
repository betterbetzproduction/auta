/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
image_speed = xSpeed/maxSpeed

event_inherited();

var accel = 0.03*global.gameSpeed
var stop = 0.01*global.gameSpeed



if dir = 1 {
	if xSpeed < maxSpeed {
		if !place_meeting(x+maxSpeed,y-4,obj_Collision) {
			xSpeed += accel	
		}
	} else {
		if xSpeed > maxSpeed+stop {
			xSpeed -= stop	
		} else {
			xSpeed = maxSpeed
		}	
	}
	if place_meeting(x+dir,y+1,obj_PlayerParent) {
		obj_PlayerParent.xSpeed = 2*dir
		obj_PlayerParent.ySpeed = -1.5
		obj_PlayerParent.grounded = false
		obj_PlayerParent.state = "attack"
		obj_PlayerParent.animstate = "onedone"
		obj_PlayerParent.sprite_index = spr_Auta_Bump
		obj_PlayerParent.image_index = 0
	}
}
if dir = -1 {
	if xSpeed > -maxSpeed {
		if !place_meeting(x-maxSpeed,y-4,obj_Collision) {
			xSpeed -= accel	
		}
	} else {
		if xSpeed < -maxSpeed-stop {
			xSpeed += stop	
		} else {
			xSpeed = -maxSpeed	
		}
	}
	
	if place_meeting(x+dir,y+1,obj_PlayerParent) {
		obj_PlayerParent.xSpeed = 2*dir
		obj_PlayerParent.ySpeed = -1.5
		obj_PlayerParent.grounded = false
		obj_PlayerParent.state = "attack"
		obj_PlayerParent.animstate = "onedone"
		obj_PlayerParent.sprite_index = spr_Auta_Bump
		obj_PlayerParent.image_index = 0
	}
}
if dir = 0 {
	if xSpeed > 0 {
		if xSpeed > stop {
			xSpeed -= stop	
		} else {
			xSpeed = 0	
		}
	}
	if xSpeed < 0 {
		if xSpeed < -stop {
			xSpeed += stop	
		} else {
			xSpeed = 0	
		}
	}
}
maxSpeed = 0.7
if side = 1 {
	if dir = -1{
		if abs(x-obj_GearWheel_Left.x) < 92 {
			maxSpeed = 0.4	
		}
		else if abs(x-obj_GearWheel_Left.x) > 100 {
			maxSpeed = 0.9
		} else {
			maxSpeed = 0.7	
		}
	}
}

if side = -1 {
	if dir = 1 {
		if abs(x-obj_GearWheel_Right.x) < 92 {
			maxSpeed = 0.4
		}
		else if abs(x-obj_GearWheel_Right.x) > 100 {
			maxSpeed = 0.9
		} else {
			maxSpeed = 0.7	
		}
	}
}

if !place_meeting(x,y+1,obj_Collision) {
	y++	
}

if !place_meeting(x+xSpeed,y,obj_Collision) {
	x+=xSpeed*global.gameSpeed	
}
y+=ySpeed*global.gameSpeed
if !place_meeting(x,y+1,obj_Collision) {
	ySpeed+=grav	
}
if place_meeting(x,y+ySpeed,obj_Collision) && ySpeed > 0 {
	ySpeed = 0
	do {y+=0.2} until (place_meeting(x,y+1,obj_Collision))
	scr_PlaySFX(snd_Crash)
	global.screenshake = 2
	
	obj_Auta.ySpeed = -1.5
	obj_Auta.grounded = false
}
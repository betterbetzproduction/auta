/// @description Insert description here
// You can write your code in this editor
if wait = 1 {
	padding = 32

	xmax = x+64*image_xscale-obj_DefaultCamera.vWidth/2+padding
	xmin = x+obj_DefaultCamera.vWidth/2-padding
	ymax = y+64*image_yscale-obj_DefaultCamera.vHeight/2+padding
	ymin = y+obj_DefaultCamera.vHeight/2-padding
wait = 0
}
active = false

if place_meeting(x,y,obj_PlayerParent) {
	active = true	
}
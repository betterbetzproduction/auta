/// @description Insert description here
// You can write your code in this editor

switch sprite_index {
	case spr_GearWheel_Body:
		if missileCounter >= missileMaxTime {
			missileCounter = -100
			sprite_index = spr_GearWheel_Body_SpitMissile
			image_index = 0
		}
		break;
	case spr_GearWheel_Body_SpitMissile:
		sprite_index = spr_GearWheel_Body_SpitMissile_Close
		image_index = 0
		if collision_rectangle(x-48,bbox_top,x+48,y+100,obj_PlayerParent,false,true) {
			for(var i=2;i<5;i++) {
				with instance_create_layer(x,y,"Instances_1",obj_GearWheel_SparkBall) {
					ySpeed = 0.5*i+0.2
					accel = 0.015*i+0.01
				}
				with instance_create_layer(x,y,"Instances_1",obj_GearWheel_SparkBall) {
					ySpeed = 0.5*i+0.2
					accel = -(0.015*i+0.01)
				}
			}
		} else {
			for(var i=0;i<3;i++) {
				with instance_create_layer(x,y,"Instances_1",obj_GearWheel_Missile) {
					direction = 55+35*i
				}
			}
		}
		break;
	case spr_GearWheel_Body_SpitMissile_Close:
		sprite_index = spr_GearWheel_Body
		missileCounter = 0
		missileMaxTime = random_range(140,180)
		break;
	case spr_GearWheel_Body_Roar:
		sprite_index = spr_GearWheel_Body
		break;
}
/// @description Insert description here
// You can write your code in this editor
xTo = (lInst.x+rInst.x)/2
yTo = (lInst.y+rInst.y)/2-64

if yTo > y{
	ySpeed+=0.1	
}
if y >= yTo {
	ySpeed = -0.3	
}

x+=xSpeed*global.gameSpeed
y+=ySpeed*global.gameSpeed

var accel = 0.04*global.gameSpeed
var stop = 0.04*global.gameSpeed

aRight	= xTo-2 > x
aLeft	= xTo+2 < x


if state = "suspension" {exit;}
if aRight {
	if xSpeed < maxSpeed {
		if !place_meeting(x+maxSpeed,y-4,obj_Collision) {
			xSpeed += accel	
		}
	} else {
		if xSpeed > maxSpeed+stop {
			xSpeed -= stop	
		} else {
			xSpeed = maxSpeed
		}	
	}
				
}
if aLeft {
	if xSpeed > -maxSpeed {
		if !place_meeting(x-maxSpeed,y-4,obj_Collision) {
			xSpeed -= accel	
		}
	} else {
		if xSpeed < -maxSpeed-stop {
			xSpeed += stop	
		} else {
			xSpeed = -maxSpeed	
		}
	}
}
if !aLeft && !aRight {
	if xSpeed > 0 {
		if xSpeed > stop {
			xSpeed -= stop	
		} else {
			xSpeed = 0	
		}
	}
	if xSpeed < 0 {
		if xSpeed < -stop {
			xSpeed += stop	
		} else {
			xSpeed = 0	
		}
	}
}

moveCounter+=global.gameSpeed
var moveTime = 110
switch moveState {
	case 0:
		rInst.dir = 0
		lInst.dir = 0
		
		if moveCounter >= moveTime {
			moveState++
			moveCounter = 0
			
			dir = choose(-1,1)
			//dir = (obj_PlayerParent.x > x ? 1:-1)
			
			if place_meeting(x+100*dir,y,obj_Collision) {
				dir = -dir	
			}
			
			rInst.dir = dir
			lInst.dir = rInst.dir
		}
		break;
	case 1:
		
		if moveCounter >= moveTime {
			moveState = 0
			moveCounter = 0
		}
		break;
	case 2:
		rInst.dir = 0
		lInst.dir = 0
		if moveCounter >= moveTime {
			moveState++
			moveCounter = 0
		}
		break;
	case 3:
		rInst.dir = -1
		lInst.dir = -1
		if moveCounter >= moveTime {
			moveState = 0
			moveCounter = 0
		}
		break;
}

missileCounter+=global.gameSpeed

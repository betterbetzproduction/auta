/// @description Insert description here
// You can write your code in this editor

for(i=0;i<4;i++) {
	var log = 0
	switch(i) {
		case 0:
			log = 0.33
			break;
		case 1:
			log = 0.59
			break;
		case 2:
			log = 0.80
			break;
		case 3:
			log = 1
			break;
	}
	var xx = lerp(x+32,rInst.x,log)
	var yy = lerp(y,rInst.y,0.25*i)+8
	draw_sprite(spr_GearWheel_ArmSphere,0,xx,yy)	
}

for(i=0;i<4;i++) {
	switch(i) {
		case 0:
			log = 0.33
			break;
		case 1:
			log = 0.59
			break;
		case 2:
			log = 0.80
			break;
		case 3:
			log = 1
			break;
	}
	var xx = lerp(x-32,lInst.x,log)
	var yy = lerp(y,lInst.y,0.25*i)+8
	draw_sprite(spr_GearWheel_ArmSphere,0,xx,yy)	
}

draw_self()

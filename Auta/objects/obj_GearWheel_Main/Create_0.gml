/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

rInst = instance_create_layer(x+48,y+64,"Arms",obj_GearWheel_Right)
lInst = instance_create_layer(x-48,y+64,"Arms",obj_GearWheel_Left)
hInst = instance_create_layer(x,y,layer,obj_GearWheel_HurtHitbox)

moveState = 0
moveCounter = 0

maxSpeed = 0.7

missileCounter = 0
missileMaxTime = random_range(140,180)

hp = 120
maxhp = 120
obj_GUI.bossTarg = self

dir = 0

state = "normal"
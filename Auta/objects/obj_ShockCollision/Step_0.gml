/// @description Insert description here
// You can write your code in this editor
if activated {
	//visible = true	
	
	if deactiveDelay = 2 {
		scr_SpreadElectricity();
	}
	
	if collision_rectangle(x - 1, y - 1, x + 8*(image_xscale)+1, y + 8*(image_yscale)+1, obj_PlayerParent, false, true) {
		scr_HurtKelly(4)
	}
	
	
} else {
	//visible = false	
}

deactiveDelay--
if deactiveDelay = 0 {
	activated = false	
}

partCreateCounter++

if activated {
	if partCreateCounter > partCreateMax {
		partCreateCounter = 0
		partCreateMax = random_range(0,60);
	
		part_particles_create(psys,x+random_range(0,8*image_xscale),y+random_range(0,8*image_yscale),particle1,1);
	
	}
}
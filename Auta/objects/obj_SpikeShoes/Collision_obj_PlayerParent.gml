/// @description Insert description here
// You can write your code in this editor
if state != "bye" {
	obj_PlayerParent.canWallRun = true
	audio_play_sound(snd_Powerup,4,false)
	with instance_create_layer(x,y,"HighInstances",obj_TextBox) {
		text[0] = "You got the spike shoes!\nHold C and move towards a\nwall in the air to run up it."
		text[1] = "You can jump off the wall and run again\non a wall opposite the one you jumped off."
		text[2] = "end"
	}
}
state = "bye"

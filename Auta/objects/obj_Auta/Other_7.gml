/// @description Insert description here
// You can write your code in this editor
if sprite_index = spr_AutaJumpToFall {
	sprite_index = spr_AutaFall	
}
if sprite_index = spr_AutaRun_Jump_To_Fall {
	sprite_index = spr_AutaRun_Fall	
}
if sprite_index = spr_AutaToJump {
	sprite_index = spr_AutaJump
}
if sprite_index = spr_AutaRun_To_Jump {
	sprite_index = spr_AutaRun_Jump	
}
if sprite_index = spr_AutaRun_Stop {
	sprite_index = spr_AutaIdle
}
if sprite_index = spr_AutaTurn {
	sprite_index = spr_AutaRun
	image_index = 0
}
if sprite_index = spr_AutaToCrouch {
	sprite_index = spr_AutaCrouch	
}
if sprite_index = spr_Auta_To_FightStance {
	sprite_index = spr_Auta_FightStance	
}
if sprite_index = spr_Auta_Bump{
	sprite_index = spr_AutaFall
	if state = "attack" {
		state = "normal"	
	}
	if animstate = "onedone" {
		animstate = "normal"	
	}
}
if sprite_index = Auta_Knockback_Turn {
	sprite_index = Auta_Knockback
	image_index = 0
	animstate = "constant"
}
if sprite_index = spr_AutaStompWindup {
	sprite_index = spr_AutaStomp
	ySpeed = 6
	xSpeed = 1*image_xscale
	state = "attack"
	animstate = "constant"
	
	audio_play_sound(snd_MagicSwing,4,false);
	with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
		attack = other.Atk
		Xknockback = 0.5
		Yknockback = -2
		type = "sliceNoPull"
		stun = false
		stopme = true
		killsprite = other.sprite_index
		sprite_index = spr_AutaStomp_Hitbox
		visible = true
	}
	
}
if sprite_index = spr_Auta_Sword_To_DownAerial {
	sprite_index = spr_Auta_Sword_DownAerial
	atkStateGrav = 0
	state = "attack"
	image_index = 0
	audio_play_sound(snd_MagicSwing,4,false);
	with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
		attack = other.Atk
		Xknockback = 0.8
		Yknockback = 0
		audio_play_sound(snd_SlideAttack,4,false);
		image_xscale = obj_PlayerParent.image_xscale
		type = "slice"
		killsprite = other.sprite_index
		sprite_index = spr_Auta_Sword_DownAerial_Hitbox
		visible = true
		stopme = false
		image_xscale *= 0.95
		if other.equip[0] = "Swift Blade" {
			other.imgSpd = 1.15
			image_xscale *= 0.75
		}
		if other.equip[0] = "Heavy Blade" {
			other.imgSpd = 0.7
		}
	}	
	exit;
}
if sprite_index = spr_Auta_Gauntlet_UpTilt_Windup {
	atkStateGrav = 0
	image_index = 0
	grounded = false
	xSpeed = 0.5*image_xscale
	animstate = "onedone"
	sprite_index = spr_Auta_Gauntlet_UpTilt
	audio_play_sound(snd_UpwardsAttack,4,false);
	ySpeed = -2.6

	with instance_create_layer(x,y,layer,obj_PlayerParentHitbox) {
		attack = other.Atk
		Xknockback = 0.5
		Yknockback = 2.2
		type = "strike"
		killsprite = other.sprite_index
		sprite_index = spr_Auta_Gauntlet_UpTilt_Hitbox
		stopme = false
		visible = true
		shiftY = -6
	}	
	exit;
}

if sprite_index = spr_Auta_Gauntlet_DownTilt_Windup {
	atkStateGrav = 0
	image_index = 0
	grounded = false
	xSpeed = 3.2*image_xscale
	animstate = "onedone"
	sprite_index = spr_Auta_Gauntlet_DownTilt
	mask_index = spr_AutaIdle_ShortMask
	audio_play_sound(snd_SlideAttack,4,false);
	with instance_create_layer(x,y,layer,obj_PlayerParentHitbox) {
		attack = other.Atk
		Xknockback = 1
		Yknockback = 2
		type = "strike"
		killsprite = other.sprite_index
		sprite_index = spr_Auta_Gauntlet_DownTilt_Hitbox
		stopme = false
		visible = true
	}	
	exit;
}

if sprite_index = spr_AutaUppercutWindup {

	image_index = 0
	ySpeed = -4
	atkStateGrav = -0
	grounded = false
	xSpeed = 0.5*image_xscale
	animstate = "onedone"
	sprite_index = spr_Auta_AntiAerial
	if ySpeed > -1 && canAttackBoost {
		ySpeed = -1	
		canAttackBoost = false
	}
	with instance_create_layer(x,y,layer,obj_PlayerParentHitbox) {
		attack = other.Atk
		Xknockback = 0.5
		Yknockback = 2
		image_xscale *= 0.85
		type = "slice"
		killsprite = other.sprite_index
		sprite_index = spr_Auta_AntiAerial_Hitbox
		stopme = false
		visible = true
						
		if other.equip[0] = "Swift Blade" {
			other.imgSpd = 1.15
			image_xscale *= 0.75
		}
		if other.equip[0] = "Heavy Blade" {
			other.imgSpd = 0.7
		}
	}
	exit;
}

if animstate = "onedone" {
	if sprite_index = spr_Auta_Sword_DownAerial {
		exit;	
	}
	animstate = "normal"
	if state = "attack" {
		comboTimer = 10
		comboNumber++
	}
	
	if sprite_index = spr_AutaCrouch_Attack {
		state = "duck"	
		animstate = "constant"
		sprite_index = spr_AutaCrouch
	} else if sprite_index = spr_AutaCrouch_SpecialAttack1 {
		global.gameSpeed = 1
		iframes = 0
		state = "duck"	
		animstate = "constant"
		sprite_index = spr_AutaCrouch
	} 
	else if sprite_index = spr_Auta_Gauntlet_DownTilt {
		state = "duck"	
		animstate = "constant"
		sprite_index = spr_AutaCrouch
	}
	else {
		state = "normal"
	}
}



if sprite_index = spr_AutaToPrism {
	sprite_index = spr_AutaPrismDash
	xSpeed = 13*image_xscale
	animstate = "constant"
	state = "prismDash"
	slidecounter = 15	
	
	with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
		attack = other.Atk/2
		Xknockback = 0
		Yknockback = 1
		killsprite = other.sprite_index
		type = "lightSpeed"
		sprite_index = spr_AutaOutPrismDashHitbox
		visible = false
	}
}
if sprite_index = spr_Auta_Smek {
	sprite_index = spr_AutaIdle
}	
if state = "roll" {
	sprite_index = spr_AutaCrouch
	if aDown {
		state = "duck"
	} else {
		state = "normal"
		animstate = "normal"
	}
}
imgSpd = 1
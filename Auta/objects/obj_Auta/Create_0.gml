/// @description Insert description here
// You can write your code in this editor
//if !instance_exists(obj_GunAug) {
//	instance_create_layer(x,y,layer,obj_GunAug)	
//}
orbDelay = 0
hangTime = 0
if global.speedrun && obj_FadeTrans.toRoom = rm_MagicLand && obj_FadeTrans.toY = 352 {
	global.secs = 0	
}
haveOrb = global.haveOrb
canOrb = true
canJumpCount = 0
canAttackBoost = true

atkCounter = 0

atkStateGrav = 0

canWallRun = global.canWallRun

aimAngle = 0
aimX = 0
aimY = 0

xSpeed = 0
ySpeed = 0
state = "normal"
animstate = "normal"
maxSpeed = 1.5
grounded = true
jumpforce = -3.5
grav = 0.08
riseGrav = 0
slidecounter = 0
attack = 2
knockback = 1
orbPower = 3.4
oxygen = 100
floorAngle = 0
bufferjump = 0

weapondisplay = 0
jumpUpwardsCancel = false

jumpKey = ord("A")
attackKey = ord("S")
orbKey = ord("D")


wallrundirect = 0

iframes = 0
hurtcounter = 0

comboNumber = 1
comboTimer = 0

//visual
trail = false
trailcounter = 0

///Init palette system.
//You need to do this in whatever object you are drawing with a palette.  You'll need this later.
//---------IMPORTANT---------//
my_shader=shd_color_swapper_22; 
//---------------------------//


current_pal=global.pal;

//Aberation

dis_u = shader_get_uniform(shd_aberration, "u_Distance");
dis = 0.2;

shader = 1;

//ColorReplace

_uniColor = shader_get_uniform(shd_ColorReplace, "u_color");
_color    = [1.0, 1.0, 0.0, 1.0];

nudge = true
nudgeWeight = 1
imgSpd = 1


leftkey = false
rightkey = false
leftRightLock= 0
keyPressLast = vk_control
keyPressTimer = 0

talkTarg = noone

lightSize = 3

specCounter = 0

sil = false

stuntimer = 0
fallDist = 0

scr_Controls()

inputList = ds_list_create()
inputTimer = 0

aimMode = 0
// 0	= pointer
// 1	= direction


scr_EquipInit()



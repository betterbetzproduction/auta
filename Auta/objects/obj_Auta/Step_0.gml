 /// @description Insert description here
//show_debug_message(grav)
//show_debug_message(atkStateGrav)
if mouse_wheel_up() {
	global.gameSpeed+=0.1	
}
if mouse_wheel_down() {
	global.gameSpeed-=0.1	
}

spd = global.gameSpeed
image_speed = spd*imgSpd

nudge = true

// You can write your code in this editor
if stuntimer > 0 {
	stuntimer -= spd
	image_speed = 0
	exit;
	
}

jumpkey = false
jumpheld = false

if state != "cutscene" {
	var pad = global.pad
	
	aRight = false
	aLeft = false
	aUp = false
	aDown = false
	aRightTap = false
	aLeftTap = false
	aUpTap = false
	aDownTap = false
	aAttack = false
	aMenu = false
	aSpecial = false
	aOrb = false
	aInteract = false
	aShield = false
	
	if global.currentControl = obj_Auta {
	
		if gamepad_axis_value(pad, gp_axislh) > 0.3
			{ aRight = true} 
		if aRight = false {
			aRight = keyboard_check(kRight) || gamepad_button_check(pad,pRight2)
			aRightTap = keyboard_check_pressed(kRight) || gamepad_button_check_pressed(pad,pRight2)
		}


		if gamepad_axis_value(pad, gp_axislh) < -0.3
			{ aLeft = true} 
		if aLeft = false {
			aLeft = keyboard_check(kLeft) || gamepad_button_check(pad,pLeft2)
			aLeftTap = keyboard_check_pressed(kLeft) || gamepad_button_check_pressed(pad,pLeft2)
		}
	
		if gamepad_axis_value(pad, gp_axislv) < -0.3
			{ aUp = true} 
		if aUp = false {
			aUp = keyboard_check(kUp) || gamepad_button_check(pad,pUp2)
			aUpTap = keyboard_check_pressed(kUp) || gamepad_button_check_pressed(pad,pUp2)
		}
	
		if gamepad_axis_value(pad, gp_axislv) > 0.3
			{ aDown = true} 
		if aDown = false {
			aDown = keyboard_check(kDown) || gamepad_button_check(pad,pDown2)
			aDownTap = keyboard_check_pressed(kDown) || gamepad_button_check_pressed(pad,pDown2)
		}
	
		aAttack		= keyboard_check_pressed(kAttack) || gamepad_button_check_pressed(pad,pAttack)
		aJump		= keyboard_check_pressed(kJump) || gamepad_button_check_pressed(pad,pJump)
		aOrb		= keyboard_check_pressed(kOrb) ||  gamepad_button_check_pressed(pad,pOrb)
		aSpecial	= keyboard_check_pressed(kSpecial) || gamepad_button_check_pressed(pad,pSpecial)
		aMenu		= keyboard_check_pressed(kMenu) ||  gamepad_button_check_pressed(pad,pMenu)
		aShield		= keyboard_check(kShield) || gamepad_button_check(pad,pShield)
		aSpecial	= keyboard_check_pressed(kSpecial) || gamepad_button_check_pressed(pad,pSpecial)
		aInteract	= keyboard_check_pressed(kInteract) || gamepad_button_check_pressed(pad,pInteract)
		jumpkey = keyboard_check_pressed(kJump) || gamepad_button_check_pressed(pad,pJump)
		jumpheld = keyboard_check(kJump) || gamepad_button_check(pad,pJump)
	}

}

scr_AutaDeathState()

scr_PlayerDefaultNormal(spr_AutaIdle,spr_AutaJump);

scr_PlayerAttackState(spr_AutaIdle);



scr_AutaSkate()

scr_AutaOrbState()

scr_HurtState()

if state != "dead" {
	scr_CollisionNew(false);
}

scr_AutaAttack()

scr_AutaSlide()

scr_AutaSuspend()

scr_AutaGrab()

scr_AutaSpecial()


if sprite_index = spr_AutaFlipToAir && grounded {
	sprite_index = spr_AutaFlipLand	
}
if animstate = "normal" {
	trail = false
	if grounded {
		if xSpeed = 0 {
			if !(sprite_index = spr_AutaTurn || sprite_index = spr_AutaRun_Stop)  {
				if sprite_index = spr_AutaRun {
					sprite_index = spr_AutaRun_Stop
					image_index = 0
				} else {
					sprite_index = spr_AutaIdle
				}
			}
		} else {
			if sprite_index != spr_AutaTurn {
				sprite_index = spr_AutaRun	
			}
			
			
			if xSpeed > 0 {
				if image_xscale = -1 {
					sprite_index = spr_AutaTurn	
					image_index = 0
				}
				image_xscale = 1	
			}
			if xSpeed < 0 {
				if image_xscale = 1 {
					sprite_index = spr_AutaTurn	
				}
				image_xscale = -1	
			}
		}
	} else {
		if sprite_index != spr_AutaFall && sprite_index != spr_AutaRun_Fall {
			if ySpeed < -0.5 {
				if sprite_index != spr_AutaRun_Jump {
					sprite_index = spr_AutaJump	
				}
			} else {
				if sprite_index != spr_AutaJump && sprite_index != spr_AutaJumpToFall && sprite_index != spr_AutaRun_Jump && sprite_index != spr_AutaRun_Jump_To_Fall {
					if sprite_index = spr_AutaRun {
						sprite_index = spr_AutaRun_Jump_To_Fall	
						image_index = 2
					} else {
						sprite_index = spr_AutaFall	
					}
				}
				else {
					if ySpeed > 0.8 && sprite_index = spr_AutaJump {
						image_index = 0
						sprite_index = spr_AutaJumpToFall
					}
					if ySpeed > -0.5 && sprite_index = spr_AutaRun_Jump {
						image_index = 0
						sprite_index = spr_AutaRun_Jump_To_Fall
					}
				}
			}
		}
	}
}

if !collision_rectangle(bbox_left,bbox_top,bbox_right,bbox_top+5,obj_WaterCollision,true,true) {
	if oxygen < 100 {
		oxygen+=global.gameSpeed*5 
	}
}

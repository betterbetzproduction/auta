/// @description Insert description here
// You can write your code in this editor
scr_StandardSetup()

mustDrop = false
maxSpeed = 0.6

type = "slice"

class = "medium"

drops[0,0] = obj_Coin
drops[1,0] = obj_Coin
drops[2,0] = noone
drops[3,0] = obj_HealthPickup
drops[4,0] = noone

nudgeWeight = 2

shootTimer = 0

grabbable = false
maxhp = 15
hp = maxhp

recovercounter = 0

flinchThreshold = 5

hInst = instance_create_layer(x,y,layer,obj_Firebreather_Head)
layer = layer_get_id("Collision")
with hInst {
	bInst = other.id	
	//mustDrop = other.mustDrop
}

lowxp = 4
highxp = 7
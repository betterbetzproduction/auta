/// @description Insert description here
// You can write your code in this editor
if state = "bye" {
	instance_destroy()	
}

if drop {
	if !place_meeting(x,y+1,obj_GroundParent) {
		ySpeed += 0.15
	}
}
if place_meeting(x,y+ySpeed,obj_GroundParent) {
	if ySpeed > 0 {
		ySpeed = -ySpeed/2
		xSpeed = xSpeed/2
	}
}

if drop {
	y+=ySpeed
	x+=xSpeed
}

stallTime --

if drop {
	if stallTime <= -600 {
		instance_destroy();	
	}
}
stallTime -= global.gameSpeed
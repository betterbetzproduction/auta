/// @description Insert description here
// You can write your code in this editor
if state = 1 {
	alpha += 0.01*global.gameSpeed
	if alpha >= 0.5 {
		state = 2
	}
}
if state = 2 {
	alpha -= 0.01*global.gameSpeed
	if alpha <= 0 {
		state = 3
	}
}
if state = 3 {
	alpha += 0.01*global.gameSpeed
	if alpha >= 0.5 {
		state = 4
	}
}
if state = 4 {
	alpha -= 0.01*global.gameSpeed
	if alpha <= 0 {
		instance_destroy()
		audio_stop_sound(snd_Alarm)
	}
}
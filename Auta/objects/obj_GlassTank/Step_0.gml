/// @description Insert description here
// You can write your code in this editor

scr_EnemyStandard(spr_GlassTank_Hurt);


if state = "normal" {
	if grounded {
		var accel = 0.15
		var stop = 0.1
		
	} else {
		var accel = 0
		var stop = 0
	}
	
	if xSpeed > 0 {
		if xSpeed > stop {
			xSpeed -= stop	
		} else {
			xSpeed = 0	
		}
	}
	if xSpeed < 0 {
		if xSpeed < -stop {
			xSpeed += stop	
		} else {
			xSpeed = 0	
		}
	}
	
	var range = 200
	if distance_to_object(obj_PlayerParent) < range {
		if grounded {
			
			waitCounter += global.gameSpeed
			if waitCounter >= 60 {
				sprite_index = spr_GlassTank_WindJump
				image_index = 0
				waitCounter = 0
			}
		}
	} 
	
	
	if grounded && sprite_index = spr_GlassTank_Air {
		sprite_index = spr_GlassTank_Land	
	}
	
	if !grounded && ySpeed > -0.4 && attacked = false {
		with instance_create_depth(x,y, depth-1, obj_MoltenBall) {
			if obj_PlayerParent.x > x {
				dir = 1	
			}
			else {dir = -1}
			xSpeed = random_range(2*dir,	3*dir)
			ySpeed = 0
			
		}
		attacked = true
	}
	
	//xSpeed = maxSpeed * image_xscale
}
if state = "hurt" {
	if grounded {
		state = "normal"	
		sprite_index = spr_Chad
	}
	if place_meeting(x+xSpeed,y,obj_Collision) {
		xSpeed = -xSpeed
	}
}

	
if state = "cannonball" {
	scr_EnemyCannonball(spr_Chad_Cannonball)
	var c = true
} else {
	var c = false
}
scr_Collision(c)	


/// @description Insert description here
// You can write your code in this editor
if actState = 3 {
	percentDraw = (80-aggressionTimer)/80
	draw_set_alpha(percentDraw+0.1)
	draw_sprite_ext(spr_TrainAgoraMouth_Arrow,0,x,y,percentDraw*1.5,1,point_direction(x,y,obj_PlayerParent.x,obj_PlayerParent.y),c_white,image_alpha)
	draw_set_alpha(1)
}	

if hp > 0 {
	var rot = point_direction(x,y,obj_TrainAgora.x,obj_TrainAgora.y)
	draw_sprite_ext(spr_TrainAgoraEye_Thread,1,x,y,distance_to_point(obj_TrainAgora.x,obj_TrainAgora.y)/24,1,rot,c_white,1)
}

scr_FocusDistort(c_red,c_blue)

scr_EnemyDraw();


/// @description Insert description here
// You can write your code in this editor




scr_EnemyStandard(spr_Chad_Hurt);

if hitstunTimer > 0 {exit};
if state = "normal" {
	if grounded {
		var accel = 0.15
		var stop = 0.1
		
	} else {
		var accel = 0
		var stop = 0
	}

	
	var range = 1000
	if distance_to_object(obj_PlayerParent) < range {
		if sprite_index = spr_Chad {
			image_index = 0
			sprite_index = spr_Chad_ToAgro	
		}
		
		
		if place_meeting(x+xSpeed,y,obj_PlayerParent){
			if scr_HurtKelly(4) = 1 {
				ySpeed = -0.5
				xSpeed = 0
				grounded = false
				sprite_index = spr_Chad_OutAgro
			}
		}
	}
	
	if place_meeting(x+xSpeed,y-5,obj_Collision) {
		scr_EnemyDie()	
	}
	
	if collision_rectangle(bbox_left+xSpeed,bbox_top,bbox_right+xSpeed,+bbox_bottom,obj_Chad_Kamikaze,false,true) {
		scr_EnemyDie()	
	}
	
	//xSpeed = maxSpeed * image_xscale
}
if state = "hurt" {
	if grounded {
		state = "normal"	
		sprite_index = spr_Chad
	}
	if place_meeting(x+xSpeed,y,obj_Collision) {
		xSpeed = -xSpeed
	}
}

	
if state = "cannonball" {
	scr_EnemyCannonball(spr_Chad_Cannonball)
	var c = true
} else {
	var c = false
}
scr_Collision(c)	


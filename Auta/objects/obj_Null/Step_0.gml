/// @description Insert description here
// You can write your code in this editor
var spd = global.gameSpeed
canJumpCount--


//y += ySpeed*spd
if ySpeed > 6 {
	ySpeed = 6	
}
if state = "normal" {
	if grounded {
		var accel = 0.2*spd	
		var stop = 0.4*spd
		
	} else {
		var accel = 0.1*spd	
		var stop = 0.1*spd
		
		if jumpUpwardsCancel {
			if ySpeed < -0.5 && !jumpheld {
				ySpeed += 0.1*spd
			}
			if ySpeed >= -0.5 {
				jumpUpwardsCancel = false	
			}
		}
	}
	
	if canJumpCount > 0 {
		if jumpkey && !downkey {
			ySpeed = jumpforce
			if place_meeting(x,y+4,obj_WaterCollision) && !place_meeting(x,y+4,obj_Collision){
				ySpeed = jumpforce*0.75	
			}
			
			grounded = false
			audio_play_sound(snd_Jump,4,false)
			jumpUpwardsCancel = true
			jumpkey = false
		}	
	}
	
	if rightkey {
		if xSpeed < maxSpeed {
			if !place_meeting(x+maxSpeed,y,obj_Collision) {
				xSpeed += accel	
			}
		}
		if xSpeed > maxSpeed {
			xSpeed = maxSpeed	
		}
	}
	if leftkey {
		if xSpeed > -maxSpeed {
			if !place_meeting(x-maxSpeed,y,obj_Collision) {
				xSpeed -= accel	
			}
		}
		if xSpeed < -maxSpeed {
			xSpeed = -maxSpeed	
		}
	}
	if !leftkey && !rightkey {
		if xSpeed > 0 {
			if xSpeed > stop {
				xSpeed -= stop	
			} else {
				xSpeed = 0	
			}
		}
		if xSpeed < 0 {
			if xSpeed < -stop {
				xSpeed += stop	
			} else {
				xSpeed = 0	
			}
		}
	}
}

scr_Collision(false);

if active = true && !instance_exists(obj_HelloTextbox) {
		obj_PlayerParent.talkTarg = noone
	obj_Auta.state = "normal"
	keyboard_clear(ord("A"))
	with obj_PlayerParent {
		keyboard_clear(ord("A"))	
	}
	active = false

}
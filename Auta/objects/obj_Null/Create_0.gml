/// @description Insert description here
// You can write your code in this editor
talkIndex = 0
talkID = ""
active = false

rightkey = false
leftkey = false
downkey = false
jumpkey = false
jumpheld = false

canOrb = true
canJumpCount = 0

atkCounter = 0

atkStateGrav = 0

canWallRun = global.canWallRun

aimAngle = 0

xSpeed = 0
ySpeed = 0
state = "normal"
animstate = "normal"
maxSpeed = 1.5
grounded = true
jumpforce = -4.2
grav = 0.1
riseGrav = 0.06
slidecounter = 0
attack = 2
knockback = 1
orbPower = 4.2

weapondisplay = 0
jumpUpwardsCancel = false

HP = 20

wallrundirect = 0

iframes = 0
hurtcounter = 0

comboNumber = 1
comboTimer = 0

trail = false
trailcounter = 0
///Init palette system.
//You need to do this in whatever object you are drawing with a palette.  You'll need this later.
//---------IMPORTANT---------//
my_shader=shd_color_swapper_22; 
//---------------------------//


current_pal=global.pal;

//Aberation

dis_u = shader_get_uniform(shd_aberration, "u_Distance");
dis = 0.2;

shader = 1;

//ColorReplace

_uniColor = shader_get_uniform(shd_ColorReplace, "u_color");
_color    = [1.0, 1.0, 0.0, 1.0];

nudge = false
nudgeWeight = 1
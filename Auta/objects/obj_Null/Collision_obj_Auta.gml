/// @description Insert description here
// You can write your code in this editor

if keyboard_check_pressed(ord("A")) {
	if !instance_exists(obj_HelloTextbox) {
		if obj_Auta.grounded = true {
			obj_PlayerParent.state = "cutscene"
			obj_PlayerParent.xSpeed = 0
			obj_PlayerParent.animstate = "normal"
			obj_PlayerParent.talkTarg = id
			
			with instance_create_layer(x,y,"Text",obj_HelloTextbox){
				name = other.name
			}	
			
			
			active = true
		}
	}
}


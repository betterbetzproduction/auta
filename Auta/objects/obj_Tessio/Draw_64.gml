/// @description Insert description here
// You can write your code in this editor
draw_set_alpha(1)
if global.speedrun {
	draw_set_color(c_white);	
	var secs = global.secs
	global.secs+= (delta_time/1000000)*room_speed 
	display_set_gui_size(320,180)
	draw_set_halign(fa_middle);
	var quicksec = secs div 60
	if quicksec >= 60 {
		do {quicksec-=60} until(quicksec<60)
	}	
	draw_text (160,25 , string(secs div 3600) + ":" + string(quicksec) + ":" + string(round(secs mod 60)))
	draw_sprite(spr_Controls,4,50,25)
}

draw_set_valign(fa_middle)

draw_set_font(fnt_Default)
draw_set_color(c_white);	
display_set_gui_size(320,180)
draw_text(20,20,"HP: " + string(HP))
/// @description Insert description here
// You can write your code in this editor


if state = "dead" {
	exit;	
}

scr_PlayerDefaultNormal(spr_TessioIdle,spr_TessioJump)

if state = "normal" && animstate = "normal" {
	if keyboard_check_pressed(ord("X")) {
		if keyboard_check(vk_right) {
			image_xscale = 1	
		}
		if keyboard_check(vk_left) {
			image_xscale = -1	
		}
		image_index = 0
		if global.currentweapon = "stick" {
			sprite_index = spr_KellyStick
			attack = 1
		}
		if global.currentweapon = "sword" {
			sprite_index = spr_TessioAttack
			attack = 2
		}
		if global.currentweapon = "firesword" {
			sprite_index = spr_KellyFireSword
			attack = 3
		}
		if global.currentweapon = "lasersword" {
			sprite_index = spr_KellyLaserSword
			attack = 2
			with instance_create_depth(x,y,depth+1,obj_PlayerParentProjectile) {
				grav = 0
				attack = 1
				knockback = 0.5
				xSpeed = 3*other.image_xscale
				ySpeed = 0
			}
		}
		if global.currentweapon = "icesword" {
			sprite_index = spr_KellyIceSword
			attack = 2
			with instance_create_depth(x,y,depth+1,obj_PlayerParentProjectile) {
				grav = 0.07
				sprite_index = spr_KellyIceSword_IceCube
				attack = 2
				knockback = 0.5
				xSpeed = 1*other.image_xscale
				ySpeed = -1.5
			}
		}
		with instance_create_layer(x,y,layer,obj_PlayerParentHitbox) {
			attack = other.attack
			knockback = other.knockback
			killsprite = other.sprite_index
			sprite_index = spr_TessioAttack_Hitbox
		}
		animstate = "onedone"
	}
	if grounded {
		if keyboard_check(vk_down) && jumpkey {
			//animstate = "constant"
			sprite_index = spr_TessioRoll
			global.screenshake = 1
			with instance_create_layer(x,y,layer,obj_OneTimeFX) {
				image_xscale = other.image_xscale
				sprite_index = spr_TessioRollParticle
			}
			state = "slide"
			mask_index = spr_TessioIdleShortMask 	
			xSpeed = 3*image_xscale
			slidecounter = 0
		}
	}
		
}



scr_HurtState()

scr_Collision(false);

if state = "slide" {
	mask_index = spr_TessioIdleShortMask
	if jumpkey && animstate = "constant"{
		ySpeed = jumpforce
		audio_play_sound(snd_Jump,4,false)
	}
	animstate = "constant"
	if grounded = false {
		grounded = false
		ySpeed = -0.5
		sprite_index = spr_TessioFall
		xSpeed = sign(xSpeed)*0.5
		state = "normal"
		animstate = "normal"
	}	
	if xSpeed = 0 {
		mask_index = spr_TessioIdle
		if place_meeting(x,y,obj_Collision) {
			mask_index = spr_TessioIdleShortMask 	
		} else {
			grounded = false
			ySpeed = -0.5
			sprite_index = spr_TessioFall
			xSpeed = sign(xSpeed)*0.5
			state = "normal"
			animstate = "normal"
		}
	}
	slidecounter++
	if slidecounter >= 10 {
		mask_index = spr_TessioIdle
		if place_meeting(x,y,obj_Collision) {
			mask_index = spr_TessioIdleShortMask 	
		} else {
			grounded = false
			ySpeed = -0.5
			sprite_index = spr_TessioFall
			xSpeed = sign(xSpeed)*0.5
			state = "normal"
			animstate = "normal"
		}
	}
}

if wallrundirect = 1 || wallrundirect = 0 {
	if place_meeting(x+maxSpeed,y,obj_Collision) {
		var gdinst = instance_place(x+maxSpeed,y,obj_Collision)
		if gdinst != noone {
			if gdinst.topOnly = false {
				if grounded = false {
					if keyboard_check(ord("C")) && keyboard_check(vk_right) {
						state = "wallrun"
						slidecounter = 0
						image_xscale = 1
						wallrundirect = -1
						
						with instance_create_layer(x,y,layer,obj_OneTimeFX) {
							image_xscale = other.image_xscale
							sprite_index = spr_TessioUpWall_Particle
						}
						
					}
				}
			}
		}
	}
}



if state = "wallrun" {
	animstate = "constant"
	sprite_index = spr_TessioUpWall
	ySpeed = -1
	slidecounter++
	if slidecounter >= 20 {
		ySpeed = -0.4	
	}
	if slidecounter >= 40 {
		ySpeed = -0.1	
	}
	if slidecounter >= 80 {
		state = "normal"
		animstate = "normal"
	}
	
	if jumpkey && animstate = "constant"{
		image_xscale = wallrundirect
		ySpeed = jumpforce*0.75
		jumpUpwardsCancel = true
		xSpeed = maxSpeed*wallrundirect
		x += 4*wallrundirect
		audio_play_sound(snd_Jump,4,false)
	}
		
	var gdinst = instance_place(x+maxSpeed*image_xscale,y,obj_Collision)
	if gdinst != noone {
		if gdinst.topOnly = false {
			if !keyboard_check(ord("C")) {
				state = "normal"
				animstate = "normal"
			}
		} else {
			state = "normal"
			animstate = "normal"
		}
	} else {
		state = "normal"
		animstate = "normal"
	}
	
		
		
		
}

if wallrundirect = -1 || wallrundirect = 0 {
	if place_meeting(x-maxSpeed,y,obj_Collision) {
		var gdinst = instance_place(x-maxSpeed,y,obj_Collision)
		if gdinst != noone {
			if gdinst.topOnly = false {
				if grounded = false {
					if keyboard_check(ord("C")) && keyboard_check(vk_left) {
						state = "wallrun"
						slidecounter = 0
						image_xscale = -1
						wallrundirect = 1
						
						with instance_create_layer(x,y,layer,obj_OneTimeFX) {
							image_xscale = other.image_xscale
							sprite_index = spr_TessioUpWall_Particle
						}
					}
				}
			}
		}
	}
}


if animstate = "normal" {
	if grounded {
		if xSpeed = 0 {
			sprite_index = spr_TessioIdle	
		} else {
			sprite_index = spr_TessioRun	
			if xSpeed > 0 {
				image_xscale = 1	
			}
			if xSpeed < 0 {
				image_xscale = -1	
			}
		}
	} else {
		if sprite_index != spr_TessioFall {
			if ySpeed < 0 {
				sprite_index = spr_TessioJump	
			} else {
				if sprite_index != spr_TessioJump {
					sprite_index = spr_TessioFall	
				}
				else {
					if ySpeed > 0.7 {
						sprite_index = spr_TessioFall
					}
				}
			}
		}
	}
}
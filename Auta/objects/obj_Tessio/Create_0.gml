/// @description Insert description here
// You can write your code in this editor
if global.speedrun && obj_FadeTrans.toRoom = rm_MagicLand && obj_FadeTrans.toY = 352 {
	global.secs = 0	
}
canJumpCount = 0

canWallRun = global.canWallRun

xSpeed = 0
ySpeed = 0
state = "normal"
animstate = "normal"
maxSpeed = 1.2
grounded = true
jumpforce = -1.9
grav = 0.05
riseGrav = 0.03
slidecounter = 0
attack = 2
knockback = 1

jumpUpwardsCancel = false

HP = 4

wallrundirect = 0

iframes = 0
hurtcounter = 0



///Init palette system.
//You need to do this in whatever object you are drawing with a palette.  You'll need this later.
//---------IMPORTANT---------//
my_shader=shd_color_swapper_22; 
//---------------------------//


current_pal=global.pal;
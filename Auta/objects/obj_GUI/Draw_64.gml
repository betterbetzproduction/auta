/// @description Insert description here
// You can write your code in this editor
display_set_gui_size(800,450)
if global.cutscene = false {
	if instance_exists(obj_textbox) {exit;}
	if state = 1 {
		baseY+=	20
		if baseY = -20 {
			state = 2	
		}
	
	}
	if state = 2 {holdOut--}
	if state = 3 {
		baseY-=	20
		if baseY = -100 {
			state = 0	
		}
	}


	
	var screenshake = global.screenshake
	var hpScale = 2
	var hpX = 10 +random_range(-screenshake,screenshake)
	var hpY = baseY +random_range(-screenshake,screenshake)

	draw_sprite_ext(spr_Healthbar,2,hpX,hpY,hpScale,hpScale,0,c_white,1);


	//for(var i = 0; i < obj_Auta.HP; i++) {
	//	var col = c_white
	//	if i%2 = 0 {
	//		col = c_aqua
	//	}	
	//	draw_sprite_ext(spr_Healthbar, 1, hpX+hpScale*i*4,hpY,hpScale,hpScale,0,col,1);
	//}
	draw_sprite_part_ext(spr_HealthbarBar,0,0,0,80*(obj_PlayerParent.HP/global.maxhp),6, hpX+49*hpScale , hpY+35*hpScale ,hpScale,hpScale,c_white,1)
	
	draw_sprite_part_ext(spr_MagicBar,0,0,0,80*(obj_PlayerParent.MP/global.maxmp),6, hpX+49*hpScale , hpY+50*hpScale ,hpScale,hpScale,c_white,1)
	
	
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)
	draw_set_color(c_black)
	draw_set_font(fnt_Default)
	
	draw_text(hpX+49*hpScale , hpY+35*hpScale, string(obj_PlayerParent.HP) + "/" + string(global.maxhp) )
	
	draw_sprite_ext(spr_Healthbar,0,hpX,hpY,hpScale,hpScale,0,c_white,1);

	if hurtcounter > 0 {
		if state = 0 {state = 1; }
		holdOut = 600
		
		hurtcounter-= 2
		draw_sprite_ext(spr_Healthbar,3,hpX,hpY,hpScale,hpScale,0,c_white,1);
	}



	   {
		   draw_set_color(c_white)
		   draw_set_font(fnt_Terminal)
	  // draw_text(32, 32, "FPS = " + string(fps));
	   }
   
	//if bossTarg != 0 {
	
	//	if bossScrollHP = -1 {
	//		bossScrollHP = bossTarg.hp	
	//	}
	//	if bossScrollHP > bossTarg.hp {
	//		bossScrollHP -= 0.2	
	//	}
	
	//	var x1 = 118
	//	var x2 = 685
	//	var y1 = 385
	//	var y2 = 415
	//	draw_sprite(spr_BossHealthbar,0,x1, y1)
	//	//draw_set_color(c_black)
	//	//draw_rectangle(x1,y1,x2,y2,false)
	//	draw_set_color(c_white)
	//	draw_rectangle(x1,y1,x1+((x2-x1)*(bossScrollHP/bossTarg.maxhp)),y2,false)
	//	draw_set_color(c_red)
	//	draw_rectangle(x1,y1,x1+((x2-x1)*(bossTarg.hp/bossTarg.maxhp)),y2,false)
	//}

	//display_set_gui_size(800,450)
	if bossTarg != 0 {
	
		if bossScrollHP = -1 {
			bossScrollHP = bossTarg.hp	
		}
		if bossScrollHP > bossTarg.hp {
			bossScrollHP -= 0.2	
		}
	
		var x1 = 715
		var x2 = 745
		var y1 = 50
		var y2 = 400
		//draw_sprite(spr_BossHealthbar,0,x1, y1)
		//draw_set_color(c_black)
		//draw_rectangle(x1,y1,x2,y2,false)
		draw_set_color(c_white)
		draw_rectangle(x1,y1,x2,y1+((y2-y1)*(bossScrollHP/bossTarg.maxhp)),false)
		draw_set_color(c_red)
		draw_rectangle(x1,y1,x2,y1+((y2-y1)*(bossTarg.hp/bossTarg.maxhp)),false)
	}
	
	image_alpha = 1
	draw_set_alpha(0.6)
	//Draw map
	var mapW = 7
	var mapH = 5
	
	//draw_sprite_ext(spr_Healthbar,2,200,50,3,3,0,c_white,1);
	var mapCenterX = 650
	var mapCenterY = 50
	
	draw_set_color(c_white)
	draw_rectangle(mapCenterX-(mapW/2)*32-5,mapCenterY-(mapH/2)*18-5,mapCenterX+(mapW/2)*32+5,mapCenterY+(mapH/2)*18+5,false)
	
	draw_set_color(c_black)
	draw_rectangle(mapCenterX-(mapW/2)*32,mapCenterY-(mapH/2)*18,mapCenterX+(mapW/2)*32,mapCenterY+(mapH/2)*18,false)
	
	draw_sprite_part_ext(spr_TC_Map, 0, (global.mapX-mapW/2)*16 +8 , (global.mapY-mapH/2)*9 , mapW*16, mapH*9 , mapCenterX-(mapW/2)*32,mapCenterY-(mapH/2)*18, 2, 2, c_white, 1)
	draw_sprite_ext(spr_TC_Map1,0,mapCenterX,mapCenterY,2,2,0,c_white,1)
	
	draw_set_alpha(1)
}


if global.cutscene = true {
	draw_set_color(c_black)
	draw_rectangle(0,0,800,50, false)
	draw_rectangle(0,400,800,450, false)
}	

if global.gameSpeed < 1 && global.gameSpeed > 0{
	display_set_gui_size(400,225)
	
	draw_sprite_ext(spr_SpeedLines,speedlinesframe,0,0,1,1,0,c_white,0.15)	
	speedlinesframe+=0.25
}

display_set_gui_size(800,450)
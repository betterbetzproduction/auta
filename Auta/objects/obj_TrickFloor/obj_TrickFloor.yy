{
  "spriteId": {
    "name": "sprite706",
    "path": "sprites/sprite706/sprite706.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": null,
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":4,"collisionObjectId":{"name":"obj_Auta","path":"objects/obj_Auta/obj_Auta.yy",},"parent":{"name":"obj_TrickFloor","path":"objects/obj_TrickFloor/obj_TrickFloor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"obj_TrickFloor","path":"objects/obj_TrickFloor/obj_TrickFloor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"parent":{"name":"obj_TrickFloor","path":"objects/obj_TrickFloor/obj_TrickFloor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Hangar",
    "path": "folders/Objects/LevelOBJs/LevelSpecific/Hangar.yy",
  },
  "resourceVersion": "1.0",
  "name": "obj_TrickFloor",
  "tags": [],
  "resourceType": "GMObject",
}
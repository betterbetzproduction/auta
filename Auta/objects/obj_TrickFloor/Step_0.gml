/// @description Insert description here
// You can write your code in this editor
if state = 1 {
	xSpeed = random_range(-2,2)
	ySpeed = 0
	var tInst = instance_place(x+1,y,obj_TrickFloor)
	if tInst != noone {
		if tInst.state = 0 {
			tInst.state = 1
		}	
	}
	var tInst = instance_place(x-1,y,obj_TrickFloor)
	if tInst != noone {
		if tInst.state = 0 {
			tInst.state = 1
		}	
	}
	state = 2
}
if state = 2 {
	x+=xSpeed
	y+=ySpeed
	ySpeed += 0.1
	alphacount-=0.1
	image_alpha = alphacount
	if alphacount = 0 {
		instance_destroy();	
	}
}
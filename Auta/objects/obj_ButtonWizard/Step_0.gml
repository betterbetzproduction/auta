/// @description Insert description here
// You can write your code in this editor
if global.cutscene {
	if !selectkey {
		
		if keyboard_check_pressed(vk_up) {
			mpos--	
			audio_play_sound(snd_MenuMove,4,false)
		}
		if keyboard_check_pressed(vk_down) {
			mpos++	
			audio_play_sound(snd_MenuMove,4,false)
		}
		if keyboard_check_pressed(ord("A")) {
			selectkey = true
			audio_play_sound(snd_MenuSelect,4,false)
			io_clear()
		}
	}
	if selectkey {
		
		if keyboard_check_pressed(vk_anykey) {
			audio_play_sound(snd_MenuSelect,4,false)
			var lastKey = keyboard_key
			selectkey = false
			io_clear()
			switch mpos {
				case 0:
					global.kJump = lastKey
					break;
				case 1:
					global.kAttack = lastKey
					break;
				case 2:
					global.kOrb = lastKey
					break;
				case 3:
					global.kInteract = lastKey
					break;
				case 4:
					global.kSpecial = lastKey
					break;
				case 5:
					global.kShield = lastKey
					break;
				case 6:
					global.kRight = lastKey
					break;
				case 7:
					global.kLeft = lastKey
					break;
				case 8:
					global.kUp = lastKey
					break;
				case 9:
					global.kDown = lastKey
					break;
			}
		}
	}
	
}
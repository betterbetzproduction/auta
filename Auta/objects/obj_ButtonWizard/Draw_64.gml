/// @description Insert description here
// You can write your code in this editor
draw_set_color(c_white)
display_set_gui_size(800,450)
draw_set_font(fnt_Terminal)
draw_text(20,20,"Choose your preferred button layout!")
draw_text(20,40,"Press backspace to toggle the button mapper. Press insert to continue.")
draw_text(20,60,"Press A to select.")

draw_text(20,80,"Jump: "		+ string(global.kJump)		+	" / " + string(global.pJump) + " (Default: A)")
draw_text(20,100,"Attack: "		+ string(global.kAttack)	+	" / " + string(global.pAttack) + " (Default: S)")
draw_text(20,120,"Orb: "		+ string(global.kOrb)		+	" / " + string(global.pOrb) + " (Default: D)")
draw_text(20,140,"Interact: "	+ string(global.kInteract)	+	" / " + string(global.pInteract) + " (Default: SPACE)")
draw_text(20,160,"Special: "	+ string(global.kSpecial)	+	" / " + string(global.pInteract) + " (Default: W)")
draw_text(20,180,"Shield: "		+ string(global.kShield)	+	" / " + string(global.pInteract) + " (Default: SHIFT)")
draw_text(20,200,"Right: "		+ string(global.kRight)		+	" / " + string(global.pRight2) + " (Default: Right)")
draw_text(20,220,"Left: "		+ string(global.kLeft)		+	" / " + string(global.pLeft2) + " (Default: Left)")
draw_text(20,240,"Up: "			+ string(global.kUp)		+	" / " + string(global.pUp2) + " (Default: Up)")
draw_text(20,260,"Down: "		+ string(global.kDown)		+	" / " + string(global.pDown2) + " (Default: Down)")

if global.cutscene {
	draw_sprite(spr_TessioIdle,0,10,85+(20*mpos))	
}
if selectkey {
	draw_text(20,280, "Press your preferred key.")
}
/// @description Insert description here
// You can write your code in this editor

if !active {
	if abs(obj_PlayerParent.x - x) < 200 {
		active = true	
		with instance_create_layer(x,y,layer,obj_DuoDroid_Walker) {
			dInst = other	
			other.wInst = id
			state = "hold"
			gauntletCounter = other.gauntletCounter
		}
	}
	exit;	
}


scr_EnemyStandard(spr_DuoDroid_Fall);

if hitstunTimer > 0 {exit};

if state = "fall" {
	xSpeed = -maxSpeed * image_xscale
	if place_meeting(x,y+64,obj_Collision) {
		state = "susp"
		sprite_index = spr_DuoDroid_Gun
	}
}
if state = "susp" {
	ySpeed -= 0.2
	if ySpeed <= 0 {
		state = "normal"	
		grav = 0
		
		if instance_exists(wInst) {
			wInst.state = "normal"
		}
	}

}
if state = "normal" {
	grav = 0
	if grounded {
		var accel = 0.05
		var stop = 0.1
		
	} else {
		var accel = 0.05
		var stop = 0
	}
	
	yTo = baseY//obj_PlayerParent.y - 96
	
	plusY = (yTo - y)/40
	if !place_meeting(x,y+plusY,obj_Collision) {
		y+=plusY	
	}
	
	if xSpeed > 0 {
		if xSpeed > stop {
			xSpeed -= stop	
		} else {
			xSpeed = 0	
		}
	}
	if xSpeed < 0 {
		if xSpeed < -stop {
			xSpeed += stop	
		} else {
			xSpeed = 0	
		}
	}
	shootCounter += spd
	if shootCounter >= 180 {
		instance_create_layer(	x,y+5,layer,obj_DirectionBullet) 
		shootCounter = 0
	}
	var range = 100
	if distance_to_object(obj_PlayerParent) < range {
		
		if obj_PlayerParent.x > x{
			if xSpeed < maxSpeed {
				if !place_meeting(x+maxSpeed,y,obj_Collision) {
					xSpeed += accel	
				}
			}
			if xSpeed > maxSpeed {
				xSpeed = maxSpeed	
			}
		}
		if obj_PlayerParent.x < x {
			if xSpeed > -maxSpeed {
				if !place_meeting(x-maxSpeed,y,obj_Collision) {
					xSpeed -= accel	
				}
			}
			if xSpeed < -maxSpeed {
				xSpeed = -maxSpeed	
			}
		}
		
		//if place_meeting(x+xSpeed,y,obj_PlayerParent){
		//	if scr_HurtKelly(4) = 1 {

		//	}
		//}
		
		
	}
	
	if place_meeting(x+xSpeed,y,obj_DuoDroid) {
				
		var inst = instance_place(x+xSpeed,y,obj_DuoDroid)
		if inst.state != "cannonball" {
			if x > inst.x {
				xSpeed = abs(xSpeed)
				inst.xSpeed = -abs(inst.xSpeed)
			}
			if x < inst.x {
				xSpeed = -abs(xSpeed)
				inst.xSpeed = abs(inst.xSpeed)
			}
		}
	}
	
	//xSpeed = maxSpeed * image_xscale
}
if state = "hurt" {
	state = "normal"
	ySpeed = 0
	sprite_index = spr_DuoDroid_Fly
	if instance_exists(wInst) {
		if wInst.state = "hold" {
			wInst.state = "normal"	
		}
	}
}
	
if state = "cannonball" {
	scr_EnemyCannonball(spr_DuoDroid_Fall)
	var c = true
} else {
	var c = false
}
scr_Collision(c)	


/// @description Insert description here
// You can write your code in this editor
//if distance_to_object(obj_PlayerParent) > 180 {
//	exit;	
//}
if !active {exit}
var spd = global.gameSpeed;

scr_EnemyStandard(spr_CrystalSpinnerIdle);

if hitstunTimer > 0 {exit};
if state = "normal" {
	grav = 0
	actCounter+=spd
	if actCounter >= countCutoff {
		if actState = 0 {
			if interest > 0 {
				actState = 1;	
				if place_meeting(x,y+16,obj_Collision) {
					rise = true	
				} else {
					rise = false	
				}
				actCounter = 0
				countCutoff = random_range(80,120);
			}
		}
		
	}
	if actCounter >= countCutoff {
		if actState = 1 {
			
			switch atkType {
				case 0:
					for(var i=0;i<4;i++) {
						with instance_create_depth(x,y,depth-1,obj_DirectionBullet) {
							direction = 90*i
							image_angle = 90*i
							speed = 1
							type = "slice"
							sprite_index = spr_CrystalSpinner_Shot
							makeFlinch = true
						}	
					}
					atkType = 1
					break;
				case 1:
					for(var i=0;i<4;i++) {
						with instance_create_depth(x,y,depth-1,obj_DirectionBullet) {
							direction = 90*i+45
							image_angle = 90*i+45
							speed = 1
							type = "slice"
							sprite_index = spr_CrystalSpinner_Shot
							makeFlinch = true
						}	
					}
					atkType = 0
					break;
			}
			
			actState = 0;	
			actCounter = 0
			countCutoff = random_range(60,100);
		}
		
	}
	
	switch actState {
		case 1:
			sprite_index = spr_CrystalSpinnerMove
			grounded = false
			var accel = maxSpeed
			if xSpeed = 0 && ySpeed = 0 {
				if lastPlayerX > x-3 {
					xSpeed += accel
					image_xscale = 1
					if xSpeed > maxSpeed {
						xSpeed = maxSpeed	
					}
				}
				if lastPlayerX < x-3 {
					xSpeed -= accel
					image_xscale = -1
					if xSpeed < -maxSpeed {
						xSpeed = -maxSpeed	
					}
				}
				if rise = true {
					ySpeed -= accel
					if ySpeed < -maxSpeed*0.75 {
						ySpeed = -maxSpeed*0.75	
					}
				} else {
					if lastPlayerY > y-3 {
						ySpeed += accel
						if ySpeed > maxSpeed*0.75 {
							ySpeed = maxSpeed*0.75	
						}
					}
					if lastPlayerY < y-3 {
						ySpeed -= accel
						if ySpeed < -maxSpeed*0.75 {
							ySpeed = -maxSpeed*0.75	
						}
					}
				}
			}
			break;
		case 0:
			sprite_index = spr_CrystalSpinnerIdle
			var stop = 0.3
			if xSpeed > 0 {
				xSpeed -= stop
				if xSpeed < 0 {
					xSpeed = 0	
				}
			}
			if xSpeed < 0 {
				xSpeed += stop
				if xSpeed > 0 {
					xSpeed = 0	
				}
			}
			if ySpeed > 0 {
				ySpeed -= stop
				if ySpeed < 0 {
					ySpeed = 0	
				}
			}
			if ySpeed < 0 {
				ySpeed += stop
				if ySpeed > 0 {
					ySpeed = 0	
				}
			}
			break;
			
	}
	if place_meeting(x+xSpeed,y,obj_GroundParent) {
		xSpeed = -xSpeed	
	}
	if place_meeting(x,y+ySpeed,obj_GroundParent) {
		ySpeed = -ySpeed	
	}
		

}
if state = "hurt" {
	state = "normal"	
	sprite_index = spr_CrystalSpinnerIdle
	actState = 0
	actCounter = 0
}

if state = "cannonball" {
	scr_EnemyCannonball(spr_CrystalSpinnerIdle)
	var c = true
} else {
	var c = false
}

if state = "normal" {
	c = false	
}

x+=xSpeed*global.gameSpeed
y+=ySpeed*global.gameSpeed
//scr_Collision(false)	



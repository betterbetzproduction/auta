{
  "spriteId": {
    "name": "spr_Alex1",
    "path": "sprites/spr_Alex1/spr_Alex1.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "obj_KNemyParent",
    "path": "objects/obj_KNemyParent/obj_KNemyParent.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"obj_Alex3Fireball","path":"objects/obj_Alex3Fireball/obj_Alex3Fireball.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"parent":{"name":"obj_Alex3Fireball","path":"objects/obj_Alex3Fireball/obj_Alex3Fireball.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Shooter",
    "path": "folders/Objects/Shooter.yy",
  },
  "resourceVersion": "1.0",
  "name": "obj_Alex3Fireball",
  "tags": [],
  "resourceType": "GMObject",
}
{
  "spriteId": {
    "name": "spr_ElectricBossWave",
    "path": "sprites/spr_ElectricBossWave/spr_ElectricBossWave.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "obj_PainCollision",
    "path": "objects/obj_PainCollision/obj_PainCollision.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"parent":{"name":"obj_ElectricBreakerWave","path":"objects/obj_ElectricBreakerWave/obj_ElectricBreakerWave.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"obj_ElectricBreakerWave","path":"objects/obj_ElectricBreakerWave/obj_ElectricBreakerWave.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "ElectricBreaker",
    "path": "folders/Objects/LevelOBJs/Enemies/Bosses/ElectricBreaker.yy",
  },
  "resourceVersion": "1.0",
  "name": "obj_ElectricBreakerWave",
  "tags": [],
  "resourceType": "GMObject",
}
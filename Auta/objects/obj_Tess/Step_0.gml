 /// @description Insert description here

if mouse_wheel_up() {
	global.gameSpeed+=0.1	
}
if mouse_wheel_down() {
	global.gameSpeed-=0.1	
}

spd = global.gameSpeed
image_speed = spd*imgSpd

nudge = true

// You can write your code in this editor
if stuntimer > 0 {
	stuntimer -= spd
	image_speed = 0
	exit;
	
}

jumpkey = false
jumpheld = false

if state != "cutscene" {
	var pad = global.pad
	
	aRight = false
	aLeft = false
	aUp = false
	aDown = false
	aRightTap = false
	aLeftTap = false
	aUpTap = false
	aDownTap = false
	aAttack = false
	aMenu = false
	aSpecial = false
	aOrb = false
	aInteract = false
	aShield = false
	
	if global.currentControl = obj_Tess {
	
		if gamepad_axis_value(pad, gp_axislh) > 0.5 
			{ aRight = true} 
		if aRight = false {
			aRight = keyboard_check(kRight) || gamepad_button_check(pad,pRight2)
			aRightTap = keyboard_check_pressed(kRight) || gamepad_button_check_pressed(pad,pRight2)
		}


		if gamepad_axis_value(pad, gp_axislh) < -0.5
			{ aLeft = true} 
		if aLeft = false {
			aLeft = keyboard_check(kLeft) || gamepad_button_check(pad,pLeft2)
			aLeftTap = keyboard_check_pressed(kLeft) || gamepad_button_check_pressed(pad,pLeft2)
		}
	
		if gamepad_axis_value(pad, gp_axislv) < -0.5
			{ aUp = true} 
		if aUp = false {
			aUp = keyboard_check(kUp) || gamepad_button_check(pad,pUp2)
			aUpTap = keyboard_check_pressed(kUp) || gamepad_button_check_pressed(pad,pUp2)
		}
	
		if gamepad_axis_value(pad, gp_axislv) > 0.5
			{ aDown = true} 
		if aDown = false {
			aDown = keyboard_check(kDown) || gamepad_button_check(pad,pDown2)
			aDownTap = keyboard_check_pressed(kDown) || gamepad_button_check_pressed(pad,pDown2)
		}
	
		aAttack		= keyboard_check_pressed(kAttack) || gamepad_button_check_pressed(pad,pAttack)
		aJump		= keyboard_check_pressed(kJump) || gamepad_button_check_pressed(pad,pJump)
		aOrb		= keyboard_check_pressed(kOrb) ||  gamepad_button_check_pressed(pad,pOrb)
		aSpecial	= keyboard_check_pressed(kSpecial) || gamepad_button_check_pressed(pad,pSpecial)
		aMenu		= keyboard_check_pressed(kMenu) ||  gamepad_button_check_pressed(pad,pMenu)
		aShield		= keyboard_check(kShield) || gamepad_button_check(pad,pShield)
		aSpecial	= keyboard_check_pressed(kSpecial) || gamepad_button_check_pressed(pad,pSpecial)
		aInteract	= keyboard_check_pressed(kInteract) || gamepad_button_check_pressed(pad,pInteract)
		jumpkey = keyboard_check_pressed(kJump) || gamepad_button_check_pressed(pad,pJump)
		jumpheld = keyboard_check(kJump) || gamepad_button_check(pad,pJump)
	}

}


scr_PlayerDefaultNormal(spr_Tess_Idle,spr_Tess_Ball);

if state != "dead" {
	scr_CollisionNew(false);
}

scr_AutaSuspend()

if animstate = "normal" {
	trail = false
	if grounded {
		if xSpeed = 0 {
			sprite_index = spr_Tess_Idle	
		} else {
			sprite_index = spr_Tess_Run	
			if xSpeed > 0 {
				image_xscale = 1	
			}
			if xSpeed < 0 {
				image_xscale = -1	
			}
		}
	} else {
		if sprite_index != spr_Tess_Fall {
			if ySpeed < 0 {
				sprite_index = spr_Tess_Ball
			} else {
				if sprite_index != spr_Tess_Ball && sprite_index != spr_Tess_BallToFall {
					sprite_index = spr_Tess_Fall	
				}
				else {
					if ySpeed > 0.8 && sprite_index = spr_Tess_Ball {
						image_index = 0
						sprite_index = spr_Tess_BallToFall
					}
				}
			}
		}
	}
}

if !collision_rectangle(bbox_left,bbox_top,bbox_right,bbox_top+5,obj_WaterCollision,true,true) {
	if oxygen < 100 {
		oxygen+=global.gameSpeed*5 
	}
}

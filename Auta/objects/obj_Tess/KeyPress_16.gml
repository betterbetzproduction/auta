/// @description Insert description here
// You can write your code in this editor
if global.speedrun {
	if instance_exists(obj_FadeTrans) {
		if obj_FadeTrans.toX != 16 {
			instance_destroy(obj_FadeTrans);
		}
	}
	with instance_create_depth(x,y,depth-100,obj_FadeTrans) {
		toRoom = rm_MagicLand
		global.speedrun = true
		global.currentweapon = "Brush"
		toX = 16
		toY = 352
	}
}
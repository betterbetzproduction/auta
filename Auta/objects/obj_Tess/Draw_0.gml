/// @description Insert description here
image_blend = c_white
// You can write your code in this editor
//draw_set_alpha(image_alpha)
//if (shader = 1) {
//  shader_set(shd_aberration);
//  shader_set_uniform_f(dis_u, dis);
//}

//pal_swap_set(my_shader,spr_KellyPallettes,current_pal,true);
//{
//    draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha);
//}
//pal_swap_reset();

draw_self();
shader_reset();
draw_set_alpha(1);

draw_self();

//var temp = image_alpha
//image_alpha = 0.5
//var xadd = 0.5
//x+=xadd
//shader_set(shd_ColorReplace);
//    shader_set_uniform_f_array(_uniColor, [0.1, 0.45, 0.45, 0]);
//    draw_self();
//shader_reset();

//x-=xadd*2
if sil || (iframes > 0 && state = "orb") {
	shader_set(shd_ColorReplace);
	shader_set_uniform_f_array(_uniColor, [0,1, 1, 0]);
	draw_self();
	shader_reset();
}

//x+=xadd
//draw_set_alpha(1)

//image_alpha = temp

//scr_DrawSides(1.5,0.5,c_red,c_blue,0.1)

draw_set_alpha(1);

if weapondisplay > 0 {
	draw_set_halign(fa_center)
	draw_set_valign(fa_center)
	draw_text(x,y-20,global.currentweapon)
	weapondisplay--
}

if state = "aim" {
	draw_set_color(c_red)
	var enInst = instance_place(x+8*image_xscale,y,obj_EnemyParent)
	//draw_rectangle(x+64*dcos(aimAngle)-5,y-64*dsin(aimAngle)-5,x+64*dcos(aimAngle)+5,y-64*dsin(aimAngle)+5,true)
	//draw_ellipse(x+64*dcos(aimAngle)-5,y-64*dsin(aimAngle)-5,x+64*dcos(aimAngle)+5,y-64*dsin(aimAngle)+5,true)
	var offset = 0
	draw_sprite_ext(spr_AutaGrab1,0,enInst.x+offset,enInst.y,1,1,aimAngle,c_white,1)
	draw_sprite_ext(spr_AutaGrab1,0,enInst.x+offset+2,enInst.y+2,1,1,aimAngle,c_fuchsia,1)
	
	draw_rectangle(aimX-5,aimY+5,aimX+5,aimY-5,false)
	//draw_rectangle(x+64*dcos(aimAngle)*image_xscale-5,y-64*dsin(aimAngle)-5,x+64*dcos(aimAngle)*image_xscale+5,y-64*dsin(aimAngle)+5,true)
	//draw_ellipse(x+64*dcos(aimAngle)*image_xscale-5,y-64*dsin(aimAngle)-5,x+64*dcos(aimAngle)*image_xscale+5,y-64*dsin(aimAngle)+5,true)
}

if oxygen < 100 && oxygen > 0 {
	//draw_set_color(c_black)
	//draw_rectangle(bbox_left,bbox_bottom+3,bbox_right,bbox_bottom+4,false)	
	
	var length = bbox_right-bbox_left+6
	
	draw_set_color(c_white)
	draw_rectangle(bbox_left-3,bbox_top-3,bbox_left+length*(oxygen/100),bbox_top-4,false)	
}
//draw_set_color(c_white)

//draw_rectangle(bbox_left+2,ySpeed+bbox_bottom,bbox_right-2,ySpeed+bbox_bottom+2,true)
//draw_rectangle(bbox_right+xSpeed,bbox_bottom-2-dsin(floorAngle)*16,bbox_left+xSpeed,bbox_top,true)

//draw_text(x,bbox_top-5,string(floorAngle))
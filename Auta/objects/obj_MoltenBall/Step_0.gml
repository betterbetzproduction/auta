/// @description Insert description here
// You can write your code in this editor
x+=xSpeed*global.gameSpeed
y+=ySpeed*global.gameSpeed
ySpeed += grav
image_speed = global.gameSpeed

if place_meeting(x,y,obj_Collision) {
	var gdinst = instance_place(x,y,obj_Collision)
	if gdinst != noone {
		if gdinst.topOnly = false {
			with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
				sprite_index = spr_VatB_MetalBall_Explode
				global.screenshake = 3
			}
			instance_destroy();
		} else {
			if collision_rectangle(x-10,y+10,x+10,y+8,obj_Collision,false,true) {
				if ySpeed > 0 {
					with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
						sprite_index = spr_VatB_MetalBall_Explode
						global.screenshake = 3
					}
					instance_destroy();
				}
			}
			
		}
	}
}	

//counter++
//if counter%10 = 0 {
//	with instance_create_depth(x,y,depth+1,obj_AfterShadow) {
//		sprite_index = other.sprite_index
//		image_index = other.image_index
//	}
//}

Xknockback = 4*sign(xSpeed)
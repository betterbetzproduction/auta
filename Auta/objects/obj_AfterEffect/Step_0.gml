/// @description Insert description here
// You can write your code in this editor
if counter = 1 {
	sprite_index = targ.sprite_index
	image_index = targ.image_index
	image_angle = targ.image_angle
	image_xscale = targ.image_xscale
	x = targ.x
	y = targ.y
	image_yscale = targ.image_yscale
	counter++
}
if counter = 0 {
	counter++
}
image_alpha -= 0.1
if image_alpha <= 0 {
	instance_destroy();	
}
if expand = true {
	image_xscale-=0.2
	image_yscale+=0.2
}
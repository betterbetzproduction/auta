/// @description Insert description here
// You can write your code in this editor
if place_meeting(x,y-1,obj_Auta) && state = 0 {
	obj_Auta.state = "cutscene"
	state = 1
}
if state = 1 {
	if obj_Auta.x < x {
		obj_Auta.rightkey = true
	}
	if obj_Auta.x > x {
		obj_Auta.leftkey = true	
	}
	state = 2
}
if state = 2 {
	if obj_Auta.rightkey = true && obj_Auta.x >= x {
		obj_Auta.rightkey = false
		state = 3
	}
	if obj_Auta.leftkey = true && obj_Auta.x <= x {
		obj_Auta.leftkey = false
		state = 3
	}
	
}
if state = 3 {
	sprite_index = spr_UpgradeMachine_On
	image_index = 0
	state = 4
}
if state = 5 {
	counter++
	if counter = 30 {
		switch index {
			
			case 1:
				obj_Auta.haveOrb = true
				global.haveOrb = true
				global.canActivateTerminals = true
				instance_destroy(inst_755A5CA2)
				with instance_create_layer(x,y,layer,obj_DialogueHelper) {
					var text = ""
					var mySpeaker = -1
					speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
	
					var t = 0//line 1
					text[t] = "Gained a new skill: Orb!";
					mySpeaker[t] = speaker[0]
					
					t++;
					
					text[t] = "Press the 'orb' key to launch yourself in any direction!";
					mySpeaker[t] = speaker[0]
					create_dialogue(text, mySpeaker,0,0,0,0,0,0,0);	
				}
				break;
			
			
			
			
		}
		state = 6
		canUpgrade = false
	}
	
}
/// @description Insert description here
// You can write your code in this editor
//if image_alpha < 1{
//	image_alpha+=0.1	
//	var back_id = layer_background_get_id("Background");
//	layer_background_alpha(back_id,layer_background_get_alpha(back_id)-0.1)
//	if global.beatgame && !global.hardwon {
//		var back_id = layer_background_get_id("Grass");
//		layer_background_blend(back_id,$AD68A8)
//		var back_id = layer_background_get_id("Hills");
//		layer_background_blend(back_id,$5D4D89)
//		var back_id = layer_background_get_id("Sky");
//		layer_background_blend(back_id,$AD68A8)
//	}
//	if global.hardwon {
//		var back_id = layer_background_get_id("Grass");
//		layer_background_blend(back_id,$292D66)
//		var back_id = layer_background_get_id("Hills");
//		layer_background_blend(back_id,$1F224F)
//		var back_id = layer_background_get_id("Sky");
//		layer_background_blend(back_id,$292D66)
//	}
//}
//draw_set_alpha(image_alpha)
//if global.besttime != 0 {
//	var secs = global.besttime
//	var quicksec = secs div 60
//	if quicksec >= 60 {
//		do {quicksec-=60} until(quicksec<60)
//	}	
//	moption[1] = "Speedrun Mode (Lvl. 1 & 2)  " + string(secs div 3600) + ":" + string(quicksec) + ":" + string(round(secs mod 60))
//}	
//if global.hardwon {
//	switch(global.pal) {
//		case 0:
//			moption[4] = "Palette: Original"	
//			break;
//		case 1:
//			moption[4] = "Palette: Casual"	
//			break;
//		case 2:
//			moption[4] = "Palette: Negative"	
//			break;
//		case 3:
//			moption[4] = "Palette: Candy"	
//			break;
//		case 4:
//			moption[4] = "Palette: Winter"	
//			break;
//		case 5:
//			moption[4] = "Palette: Ethereal"	
//			break;
//		case 6:
//			moption[4] = "Palette: Star"	
//			break;
//		case 7:
//			moption[4] = "Palette: Cool"
//			break;
//		case 8:
//			moption[4] = "Palette: Sunset"
//			break;
	
//	}
//}
draw_set_halign(fa_left)
for(i=0;i<array_length_1d(moption);i++) {
	draw_set_color(c_white)
	if mpos = i {
		draw_set_color(c_yellow)	
	}
	draw_text_transformed(x,y+10*i,moption[i],0.5,0.5,0)
}
draw_sprite_ext(menusprite,menuanimate,x-5,y+10*mpos,mspritescale,mspritescale,0,c_white,1)
menuanimate+=sprite_get_speed(menusprite)

if active {
	if keyboard_check_pressed(vk_up) {
		mpos--
		audio_play_sound(snd_MenuMove,4,false)
	}
	if keyboard_check_pressed(vk_down) {
		mpos++
		audio_play_sound(snd_MenuMove,4,false)
	}
	if mpos < 0 {
		mpos = array_length_1d(moption)-1
	}	
	if mpos >= array_length_1d(moption) {
		mpos = 0
	}	



	if keyboard_check_pressed(ord("Z")) || keyboard_check_pressed(vk_space) || keyboard_check_pressed(vk_enter) || keyboard_check_pressed(vk_shift) {
		active = false
		switch mpos {
			case 0:
				room = rm_AutaFalls
				break;
			case 1:
				room = rm_HangarTrainBattle
				break;
			case 2:
				room = rm_Hangar_WO_1
				break;
				//if file_exists("saveData.ini")
			   //{
			   //file_delete("saveData.ini");
			   //game_restart();
			   //}
			case 3:
				room = rm_Hangar_Pre_1
				break;
			case 4:
				global.pal++
				if global.pal > 8 {
					global.pal = 0	
				}
				
				ini_open("saveData.ini");
				ini_write_real("Variables","Palette",global.pal); //The third value here will set the score variable if there is no save file
				ini_close()
				active = true
				break;
		
		}
	}
}
draw_set_alpha(1)
/// @description Insert description here
// You can write your code in this editor
moption[0] = "Fall"
moption[1] = "Boss"
moption[2] = "WO"
moption[3] = "HangarPre"
if file_exists("saveData.ini") {
	moption[2] = "Erase Data"	
}
if global.beatgame {
	moption[3] = "Final Boss: Hard Mode"	
}
if global.hardwon {
	moption[4] = "Palette: Original"	
}
mpos = 0

menuanimate = 0
menusprite = spr_Tess_Run
mspritescale = 0.5

if global.hardwon {
	menuanimate = 0
	menusprite = spr_Alex_Weird_H
	mspritescale = 0.2
}

active = true

global.fbskip = false

image_alpha = 0

/*if file_exists("level.txt")
   {
   file_delete("level.txt");
   }*/
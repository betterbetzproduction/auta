/// @description Insert description here
// You can write your code in this editor
if sprite_index = spr_GunAug {
	if keyboard_check_pressed(ord("D")) {
		audio_play_sound(snd_BurstAttack,4,false)
		sprite_index = spr_GunAugFire
		with instance_create_depth(x,y,depth+1,obj_PlayerParentProjectile) {
			xSpeed = 4*dcos(other.gunAngle)*other.image_xscale
			ySpeed = -4*dsin(other.gunAngle)
			image_angle = other.image_angle
		}
		
	}
}
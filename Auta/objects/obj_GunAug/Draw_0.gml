/// @description Insert description here
// You can write your code in this editor
var radius = 20
ringAlpha = 0.5 - 0.5*dcos(ringAlphaCosCounter)
draw_set_color(c_white)
ringAlphaCosCounter+=3
draw_set_alpha(ringAlpha)
draw_circle(obj_Auta.x,obj_Auta.y,radius,true)
draw_set_alpha(1)

image_xscale = obj_Auta.image_xscale
image_yscale = obj_Auta.image_xscale

gunX = obj_Auta.x + radius*image_xscale*dcos(rot)
gunY = obj_Auta.y + radius*dsin(rot)


x += (gunX-x)*0.2
y += (gunY-y)*0.2

inst = instance_nearest(x,y,obj_EnemyParent)
if distance_to_object(inst) > 200 {
	inst = noone	
}
if inst != noone {
var tempAngle = point_direction(x,y,inst.x,inst.y)
	if tempAngle > 180 {
		tempAngle -= 360	
	}
}

else tempAngle = 0
gunAngle += (tempAngle-gunAngle)*0.2


image_angle = gunAngle + (180*(image_xscale=-1))

draw_self();

rot = 0 + 5*dcos(ringAlphaCosCounter)
//draw_line(obj_Auta.x,obj_Auta.y,x,y)
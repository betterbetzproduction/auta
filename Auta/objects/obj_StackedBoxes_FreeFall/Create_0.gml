/// @description Insert description here
// You can write your code in this editor
canTouch = true

ySpeed = -3
xSpeed = random_range(-2,2)

with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
	image_xscale = other.image_xscale
	sprite_index = spr_BashEffect
	audio_play_sound_at(snd_Slam,x,y,0,50,300,1,false,4);
}
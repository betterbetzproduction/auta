/// @description Insert description here
// You can write your code in this editor
if ySpeed > 0 {
	if canTouch = true {
		ySpeed = -0.5
		canTouch = false
		with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
			image_xscale = other.image_xscale
			sprite_index = spr_BashEffect
			audio_play_sound_at(snd_Crash,x,y,0,50,300,1,false,4);
		}
	}
}
/// @description Insert description here
// You can write your code in this editor
draw_set_alpha(image_alpha)

pal_swap_set(my_shader,spr_KellyPallettes,global.pal,true);
{
    draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha);
}
pal_swap_reset();

draw_set_alpha(1);
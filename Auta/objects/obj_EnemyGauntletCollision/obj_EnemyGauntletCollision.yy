{
  "spriteId": {
    "name": "spr_PainCollision",
    "path": "sprites/spr_PainCollision/spr_PainCollision.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": null,
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":4,"collisionObjectId":{"name":"obj_PlayerParent","path":"objects/obj_PlayerParent/obj_PlayerParent.yy",},"parent":{"name":"obj_EnemyGauntletCollision","path":"objects/obj_EnemyGauntletCollision/obj_EnemyGauntletCollision.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"obj_EnemyGauntletCollision","path":"objects/obj_EnemyGauntletCollision/obj_EnemyGauntletCollision.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Enemies",
    "path": "folders/Objects/LevelOBJs/Enemies.yy",
  },
  "resourceVersion": "1.0",
  "name": "obj_EnemyGauntletCollision",
  "tags": [],
  "resourceType": "GMObject",
}
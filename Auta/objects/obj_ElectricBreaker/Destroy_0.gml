/// @description Insert description here
// You can write your code in this editor
if global.storyBeat < -2 {
	global.storyBeat = -2
}
instance_destroy(inst_28865DE9)
layer_set_visible(layer_get_id("B"),false)

obj_Music.musictrack = mus_HomogenousStars_PW

with instance_create_layer(x,y,layer,obj_DialogueHelper) {
	var text = ""
	var mySpeaker = -1
	speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
	
	var t = 0//line 1
	text[t] = "Defeated Breaker!";
	mySpeaker[t] = speaker[0]
	
	t++
	text[t] = "...A door has opened somewhere.";
	mySpeaker[t] = speaker[0]
	create_dialogue(text, mySpeaker,0,0,0,0,0,0,0);	
}
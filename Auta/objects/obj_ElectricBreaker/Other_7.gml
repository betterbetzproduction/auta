/// @description Insert description here
// You can write your code in this editor
if sprite_index = spr_ElectricBossPumpIn {

	with instance_create_layer(x,y+8,layer,obj_ElectricBreakerWave) {
		xSpeed = 1.5*other.image_xscale
	}

	sprite_index = spr_ElectricBossPumpOut
	image_index = 0
	
	audio_play_sound_at(snd_Throw,x,y,0,100,100,1,false,4);
	exit;
}
if sprite_index = spr_ElectricBossPumpIn1 {
	state = "slide"
	sprite_index = spr_ElectricBossIdle
}
if sprite_index = spr_ElectricBossTurn {
	state = "normal"
	sprite_index = spr_ElectricBossIdle
	obj_Music.musictrack = mus_MinibossPW
}	
if sprite_index = spr_ElectricBossPumpOut {
	state = "normal"
	sprite_index = spr_ElectricBossIdle
}	

if sprite_index = spr_SpiderBelly_Roar {
	sprite_index = spr_SpiderBelly_Idle
	state = "normal"
}
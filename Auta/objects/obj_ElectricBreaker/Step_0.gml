/// @description Insert description here
// You can write your code in this editor




scr_EnemyStandard(spr_ChuckerHurt);

if hitstunTimer > 0 {exit};
if state = "normal" {
	if obj_PlayerParent.x > x {
		image_xscale = 1	
	} else {
		image_xscale = -1	
	}
	if grounded {
		var accel = 0.15
		var stop = 0.1
		
	} else {
		var accel = 0
		var stop = 0
	}
	
	if xSpeed > 0 {
		if xSpeed > stop {
			xSpeed -= stop	
		} else {
			xSpeed = 0	
		}
	}
	if xSpeed < 0 {
		if xSpeed < -stop {
			xSpeed += stop	
		} else {
			xSpeed = 0	
		}
	}
	shootTimer+= spd
	
	
	if dashCounter < 2 {
		if shootTimer > 90 && shootTimes = 0 {
		
			shootTimes++
			sprite_index = spr_ElectricBossPumpIn
			state = "shoot"
			image_index = 0
			shootTimer = 0
		}
	
		if shootTimer > 40 && shootTimes != 0 {
		
			shootTimes++
			sprite_index = spr_ElectricBossPumpIn
			state = "shoot"
			image_index = 0
			shootTimer = 0
		
			if shootTimes = 3 {
				shootTimes = 0
				dashCounter++
			}

		}
	} else {
		if shootTimer > 90 {
			shootTimes++
			sprite_index = spr_ElectricBossPumpIn1
			dashCounter++
			state = "charge"
			image_index = 0
			shootTimer = 0
		}
	}
}
if state = "charge" {
	if afterimagecount%3 = 0 {
		with instance_create_layer(x,y,layer,obj_EB_Afterimage2) {
			creator = other	
		}
	}
}
if state = "slide" {
	xSpeed = image_xscale*6	
	if place_meeting(x+xSpeed,y,obj_Collision) {
		xSpeed = -image_xscale*1
		global.screenshake = 3
		ySpeed = -2
		grounded = false
		state = "air"
	}
	if afterimagecount%3 = 0 {
		with instance_create_layer(x,y,layer,obj_EB_Afterimage) {
			creator = other	
		}
	}
}

if state = "air" {
	if grounded {
		state = "normal"
		timeToDash = ceil(random_range(3,5))
		dashCounter = 0
		shootTimes = 0
		image_xscale = -image_xscale
		global.screenshake = 1
		
	}
}

if state = "hurt" {
	//recovercounter -= spd
	//if recovercounter <= 0 {
		state = "normal"	
		sprite_index = spr_ElectricBossIdle
	//}
}
if place_meeting(x+xSpeed,y+ySpeed,obj_PlayerParent) {
			if scr_HurtKelly(4) = 1 {}
}

if state = "cannonball" {
	scr_EnemyCannonball(spr_Chad_Cannonball)
	var c = true
} else {
	var c = false
}
scr_Collision(c)	

afterimagecount+=1
if afterimagecount = 12 {
	afterimagecount = 0	
}

if hp <= 1 && state != "dying" {
	state = "dying"
	shootTimer = 0
	
}
if state = "dying" {
	shootTimer++
	if shootTimer%12 = 0 {
		with instance_create_depth(x+random_range(-10,10),y+random_range(-10,10),depth-1,obj_OneTimeFX) {
			sprite_index = spr_VatB_MetalBall_Explode
			global.screenshake = 3
			audio_play_sound(snd_Crash,4,false)
		}
	}
	if shootTimer >= 180 {
		instance_destroy()	
		with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
			sprite_index = spr_LargeBurst_Slow
			image_xscale = 1.5
			image_yscale = 1.5
			global.screenshake = 3
			audio_play_sound(snd_Crash,4,false)
		}
		
		
		
		instance_create_layer(x,y,layer,obj_HeartCollectible)
	}
}
/// @description Insert description here
// You can write your code in this editor
scr_StandardSetup()
maxSpeed = 0.6

type = "slice"

state = "idle"

afterimagecount = 0

drops[0,0] = obj_Coin
drops[1,0] = obj_Coin
drops[2,0] = noone
drops[3,0] = obj_HealthPickup
drops[4,0] = noone

nudgeWeight = 2

shootTimer = 0
flinchThreshold = 1000

grabbable = false
maxhp = 70
hp = maxhp

recovercounter = 0

enemyIndex = 0

shootTimes = 0
timeToDash = ceil(random_range(2,4))
dashCounter = 0

if global.storyBeat >= -2 {
	instance_destroy();	
}
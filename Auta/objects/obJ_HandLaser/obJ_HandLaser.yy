{
  "spriteId": {
    "name": "spr_AlexHead_Laser_Charge",
    "path": "sprites/spr_AlexHead_Laser_Charge/spr_AlexHead_Laser_Charge.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": null,
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"obJ_HandLaser","path":"objects/obJ_HandLaser/obJ_HandLaser.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"parent":{"name":"obJ_HandLaser","path":"objects/obJ_HandLaser/obJ_HandLaser.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Shooter",
    "path": "folders/Objects/Shooter.yy",
  },
  "resourceVersion": "1.0",
  "name": "obJ_HandLaser",
  "tags": [],
  "resourceType": "GMObject",
}
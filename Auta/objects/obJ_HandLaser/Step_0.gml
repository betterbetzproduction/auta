/// @description Insert description here
// You can write your code in this editor
counter++

if instance_exists(obj_FBAlex) {
	x = obj_FBAlex.x
	y = obj_FBAlex.y+65*orient

	if counter >= 40 && obj_FBAlex.phase = "head" {
		sprite_index = spr_AlexHead_Laser
		if counter = 43 {	
			with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
				direction = 180
				speed = 5
				sprite_index = spr_AlexHead_Laser_Fire
				image_xscale = 2
				image_yscale = 2
			}
			counter = 40
		}
	}
	if x > 300 && obj_FBAlex.phase = "offscreen" {
		instance_destroy();
	}	
}
if instance_exists(obj_FBAlex_Hardmode) {
	image_angle+=sign(angleto)/2
	if obj_FBAlex_Hardmode.hp <= obj_FBAlex_Hardmode.maxhp/2 {
		image_angle+=sign(angleto)/2
	}
	if abs(image_angle) >= abs(angleto) && sign(image_angle) = sign(angleto) {
		angleto = -angleto	
	}
	x = obj_FBAlex_Hardmode.x
	y = obj_FBAlex_Hardmode.y+65*orient

	if counter >= 40 && obj_FBAlex_Hardmode.phase = "head" {
		sprite_index = spr_AlexHead_Laser
		if counter = 43 {	
			with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
				direction = other.image_angle+180
				speed = 5
				sprite_index = spr_AlexHead_Laser_Fire
				image_xscale = 2
				image_yscale = 2
			}
			counter = 40
		}
	}
	if x > 300 && obj_FBAlex_Hardmode.phase = "offscreen" {
		instance_destroy();
	}	
}
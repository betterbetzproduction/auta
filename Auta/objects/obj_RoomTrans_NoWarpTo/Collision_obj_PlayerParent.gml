/// @description Insert description here
// You can write your code in this editor
var go = true
if pressUp && !keyboard_check_pressed(global.kInteract) {
	go = false	
}
if go {
	if active {
		with instance_create_depth(x,y,depth-100,obj_FadeTrans) {
			toRoom = other.toRoom
			other.state = "cutscene"
			if other.toInst = noone {
				toX = other.toX
				toY = other.toY
			} else {
				toInst = other.toInst
			}
	
		}
		if global.speedrun = true {
			if toRoom = rm_Menu {
				global.speedrun = false
				if global.secs < global.besttime || global.besttime = 0 {
					global.besttime = global.secs	
					ini_open("saveData.ini");
					ini_write_real("Variables", "BestTime", global.secs);
					ini_close()
				}
				global.secs = 0
			}
		}
	}
}
/// @description Insert description here
// You can write your code in this editor
xSpeed = spd*dcos(angle)*image_xscale
ySpeed = -spd*dsin(angle)

x+=xSpeed*global.gameSpeed
y+=ySpeed*global.gameSpeed
ySpeed+=grav

xSpeed = xSpeed*0.95

if counter >= 30 {
	instance_destroy();	
}
counter+=global.gameSpeed

if contacted {
	instance_destroy();
}
/// @description Insert description here
// You can write your code in this editor
if sprite_index = spr_SpiderBelly_Spit {

	with instance_create_layer(x-5,y-14,layer,obj_SpiderBelly_Egg) {
		xSpeed = random_range(-1.5,-3.5)
		ySpeed = -2
	}

	sprite_index = spr_SpiderBelly_CloseMouth
	
	audio_play_sound_at(snd_Throw,x,y,0,100,100,1,false,4);
}

if sprite_index = spr_SpiderBelly_CloseMouth {
	state = "normal"
	sprite_index = spr_SpiderBelly_Idle
}	

if sprite_index = spr_SpiderBelly_Roar {
	sprite_index = spr_SpiderBelly_Idle
	state = "normal"
}
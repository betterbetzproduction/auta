/// @description Insert description here
// You can write your code in this editor

if global.bigEnemy[enemyIndex] = 0 {
	instance_destroy();	
}

xSpeed = 0
ySpeed = 0

image_xscale = 1
scr_EnemyStandard(spr_ChuckerHurt);
grounded = true
xSpeed = 0
ySpeed = 0


if hitstunTimer > 0 {exit};
if state = "normal" {
	if grounded {
		var accel = 0.15
		var stop = 0.1
		
	} else {
		var accel = 0
		var stop = 0
	}
	
	if xSpeed > 0 {
		if xSpeed > stop {
			xSpeed -= stop	
		} else {
			xSpeed = 0	
		}
	}
	if xSpeed < 0 {
		if xSpeed < -stop {
			xSpeed += stop	
		} else {
			xSpeed = 0	
		}
	}
	shootTimer+= spd
	if shootTimer > 90 {
		sprite_index = spr_SpiderBelly_Spit
		state = "shoot"
		image_index = 0
		shootTimer = 0
	}
}
if state = "hurt" {
	//recovercounter -= spd
	//if recovercounter <= 0 {
		state = "normal"	
		sprite_index = spr_SpiderBelly_Idle
	//}
}

	
if state = "cannonball" {
	scr_EnemyCannonball(spr_Chad_Cannonball)
	var c = true
} else {
	var c = false
}
scr_Collision(c)	


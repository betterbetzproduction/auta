draw_set_alpha(1)
if darkness > visdark {
    visdark += 0.01
}
if darkness < visdark {
    visdark -= 0.01
}

if !surface_exists(surf) {
	surf = surface_create(room_width,room_height);	
}

surface_set_target(surf);

draw_clear(c_black);

//gpu_set_blendmode(bm_src_color);
//draw_rectangle(0,0,room_width,room_height,false)
//gpu_set_blendmode(bm_normal);


surface_reset_target();
draw_surface_ext(surf,0,0,1/surfScale,1/surfScale,0,c_white,visdark);
/*
if room = rm_CrystalCaverns_3{
draw_surface_ext(surf,0,0,1/surfScale,1/surfScale,0,c_white,(view_yview[0]+100)/768);
}
else {

}
//just in case, draw_set_blend_mode(bm_src_color); draw_set_blend_mode(bm_normal);
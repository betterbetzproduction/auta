/// @description Insert description here
// You can write your code in this editor
event_inherited()
dir = point_direction(x,y,obj_PlayerParent.x,obj_PlayerParent.y)
xSpeed = 1.5*dcos(dir)
ySpeed = 1.5*-dsin(dir)

grounded = false
touchground = false
type = "shock"

hurtAmount = 1

counter = 0

sys_particle = part_system_create()
part_system_layer(sys_particle, layer)
part_particle = part_type_create()
part_type_shape(part_particle, pt_shape_square)
part_type_size(part_particle, 0.02, 0.03, 0, 0)
part_type_scale(part_particle, 1, 1)
part_type_color3(part_particle, 10759003, 16738906, 4325631)
part_type_alpha3(part_particle, 0.8, 0.6, 0)
part_type_speed(part_particle, 0.8, 1, 0, 0)
part_type_direction(part_particle, 0, 90, 0, 0)
part_type_gravity(part_particle, 0.02, 270)
part_type_orientation(part_particle, 0, 360, 0, 0, 0)
part_type_blend(part_particle, 1)
part_type_life(part_particle, 20, 30)
/// @description Insert description here
// You can write your code in this editor
var lay_id = layer_get_id(myLayer);
var map_id = layer_tilemap_get_id(lay_id);

shader_set(shd_ColorReplace);

counter+=2
if counter > 360 {
	counter = 0	
}

baseX = 0.5 + wave*dsin(counter*2)
baseY = 0.5 + wave*dsin(counter)

shader_set_uniform_f_array(_uniColor, color1);
draw_tilemap(map_id, -baseX, -baseY);

shader_set_uniform_f_array(_uniColor, color2);
draw_tilemap(map_id, baseX, baseY);


shader_reset();

draw_set_alpha(1)
draw_tilemap(map_id, 0, 0);
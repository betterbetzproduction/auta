/// @description Insert description here
// You can write your code in this editor
if distance_to_object(inst_RalphActor) > 180 {
	exit;	
}
var spd = global.gameSpeed;



scr_EnemyStandard(spr_FlyGuyLaserHurt);

if hitstunTimer > 0 {exit};
if state = "normal" {
	
	if place_meeting(x+xSpeed,y,obj_Collision) {
		image_xscale = -image_xscale	
	}
		
	if ySpeed > 0.5 {
		ySpeed = 0.5
	}
	if ySpeed < -0.5 {
		ySpeed = -0.5	
	}
			
		
	if y > baseY {
		flying = true	
	}
	if flying = true {
		ySpeed -= grav*2	
		if y < baseY {
			flying = false	
		}
	}
	if distance_to_object(inst_RalphActor < 150) {
		if inst_RalphActor.x > x {
			if !place_meeting(x+maxSpeed,y,obj_Collision) {
				xSpeed += 0.02	
				image_xscale = 1
			}
		}
	
		if inst_RalphActor.x < x {
			if !place_meeting(x-maxSpeed,y,obj_Collision) {
				xSpeed -= 0.02	
				image_xscale = -1
			}
		}
		if abs(xSpeed) > maxSpeed {
			xSpeed = maxSpeed * sign(xSpeed)
		}
	
		shoottimer += global.gameSpeed
	
		if shoottimer > 120 {
			 with instance_create_layer(	x,y,layer,obj_DirectionBullet)  {
				direction = point_direction(x,y,inst_RalphActor.x,inst_RalphActor.y)	 
			 }
			shoottimer = 0
		}
	
	} else {
		xSpeed = 0	
	}
	
	
}
if state = "hurt" {
	if grounded {
		state = "normal"	
		sprite_index = spr_FlyGuyLaser
	}
	if place_meeting(x+xSpeed,y,obj_Collision) {
		xSpeed = -xSpeed
	}
}

if state = "cannonball" {
	scr_EnemyCannonball(spr_FlyGuyLaserHurt)
	var c = true
} else {
	var c = false
}
if state = "normal" {
	c = true	
}
scr_Collision(c)	



/// @description Insert description here
// You can write your code in this editor

if global.canActivateTerminals {
	if on {
		if keyboard_check_pressed(global.kInteract) {
			if !instance_exists(obj_TerminalTextbox) {
				if obj_Auta.grounded = true {
					obj_PlayerParent.state = "cutscene"
					obj_PlayerParent.xSpeed = 0
					obj_PlayerParent.animstate = "normal"
					obj_PlayerParent.talkTarg = id
					obj_DefaultCamera.follow = noone
					obj_DefaultCamera.xTo = obj_PlayerParent.x+100
					obj_DefaultCamera.yTo = obj_PlayerParent.y
			
			
					with instance_create_layer(x,y,"Text",obj_TerminalTextbox){
						type = other.type
					}	
			
			
					active = true
				}
			}
		}
	} else {
		if keyboard_check_pressed(global.kInteract) {
			if !instance_exists(obj_TerminalTextbox) {
				if obj_Auta.grounded = true {
					obj_PlayerParent.state = "cutscene"
					obj_PlayerParent.xSpeed = 0
					obj_PlayerParent.animstate = "normal"
					obj_PlayerParent.talkTarg = id
					turnOnScene = true
					
			
					active = true
				}
			}
		}
	}
} else {
	if keyboard_check_pressed(global.kInteract) {
		with instance_create_layer(x,y,layer,obj_DialogueHelper) {
			var text = ""
			var mySpeaker = -1
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
	
			var t = 0//line 1
			text[t] = "The terminal appears to be dead.";
			mySpeaker[t] = speaker[0]
	
			create_dialogue(text, mySpeaker,0,0,0,0,0,0,0);	
		}
	}
}
/// @description Insert description here
// You can write your code in this editor
if active = true && !instance_exists(obj_TerminalTextbox) && turnOnScene != true {
		obj_PlayerParent.talkTarg = noone
		obj_DefaultCamera.follow = obj_Auta
	obj_Auta.state = "normal"
	keyboard_clear(global.kInteract)
	with obj_PlayerParent {
		keyboard_clear(global.kInteract)	
	}
	active = false

}


if turnOnScene {
	if !on {
		 if obj_Auta.xSpeed = 0 {
			 obj_PlayerParent.animstate = "cutscene"
			obj_Auta.sprite_index = spr_Auta_Smek
			
			if obj_Auta.image_index > 3 && on = false {
				sprite_index = spr_Terminal_PowerOn
				on = true
				part_particles_create(psys,x+random_range(-5,20),y+random_range(20,10),particle1,1);
				part_particles_create(psys,x+random_range(-5,20),y+random_range(20,10),particle1,1);
				with instance_create_depth(x-4,y,depth-1,obj_OneTimeFX) {
					sprite_index = spr_Smek
					fade = true
				}
			}
		 }
	}
	 if on && obj_Auta.sprite_index = spr_AutaIdle {
		turnOnScene = false
		obj_PlayerParent.animstate = "normal"
		
		obj_DefaultCamera.follow = noone
		obj_DefaultCamera.xTo = obj_PlayerParent.x+100
		obj_DefaultCamera.yTo = obj_PlayerParent.y
					
		with instance_create_layer(x,y,"Text",obj_TerminalTextbox){
			type = other.type
		}		 
	 }
}
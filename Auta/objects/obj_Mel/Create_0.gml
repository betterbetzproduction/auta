/// @description Insert description here
// You can write your code in this editor
scr_StandardSetup()
maxSpeed = 0.6

type = "slice"
actCounter = 0
actState = 0
countCutoff = random_range(40,80);
doAttack = true
maxhp = 500
hp = 500

drops[0,0] = obj_Coin
drops[1,0] = obj_Coin
drops[2,0] = noone
drops[3,0] = obj_HealthPickup
drops[4,0] = noone

image_xscale = 0.5
image_yscale = 0.5

scounter = 0
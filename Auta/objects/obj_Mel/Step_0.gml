/// @description Insert description here
// You can write your code in this editor

if grounded {
	var accel = 0.15
	var stop = 0.1
		
} else {
	var accel = 0
	var stop = 0
}
	
if xSpeed > 0 {
	if xSpeed > stop {
		xSpeed -= stop	
	} else {
		xSpeed = 0	
	}
}
if xSpeed < 0 {
	if xSpeed < -stop {
		xSpeed += stop	
	} else {
		xSpeed = 0	
	}
}
if state = "hurt" {
	if hitstunTimer <= 0 {
	
		state = "idle"
		var rand = choose(0,1,2,3,4,5,6,7,8)
		if rand = 6 {
			sprite_index = spr_Mel_Hurt1
		}
		if rand = 7 {
			sprite_index = spr_Mel_Hurt2
		}
		if rand = 8 {
			sprite_index = spr_Mel_Hurt3
		}
		//show_debug_message("Ow")
		scounter = 40
	}
}
if scounter > 0 {
	scounter--
	if scounter = 0 {
		sprite_index = spr_MelIdle
	}
}

scr_EnemyStandard(spr_MelIdle);

	
if state = "cannonball" {
	scr_EnemyCannonball(spr_MelIdle)
	var c = true
} else {
	var c = false
}
scr_Collision(c)	


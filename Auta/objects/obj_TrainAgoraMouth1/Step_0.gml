/// @description Insert description here
// You can write your code in this editor
if distance_to_object(obj_PlayerParent) > 180 {
	exit;	
}
var spd = global.gameSpeed;


scr_EnemyStandard(spr_TrainAgoraMouth);

if hitstunTimer > 0 {exit};
if state = "normal" {
	grav = 0
	actCounter+=spd
	if actCounter >= countCutoff {
		if actState = 0 {
			if interest > 0 {
				actState = 1;	
				if place_meeting(x,y+16,obj_Collision) {
					rise = true	
				} else {
					rise = false	
				}
				switch reverse {
					case true:
						reverse = false
						break;
					case false:
						//reverse = true
						if distance_to_object(obj_PlayerParent) > 32 {
							reverse = false
						}	
						if !canSeePlayer {
							reverse = false	
						}
						break;
				}
				actCounter = 0
				countCutoff = random_range(40,80);
			}
		}
		
	}
	if actCounter >= countCutoff {
		if actState = 1 {
			actState = 0;	
			actCounter = 0;
			
			//if canSeePlayer {
			//	sprite_index = spr_FlyGuyLaser_Charge
			//	if obj_PlayerParent.x > x {
			//		image_xscale = 1
			//	}
			//	if obj_PlayerParent.x < x {
			//		image_xscale = -1
			//	}
			//}
			countCutoff = random_range(20,40);
		}
		
	}
	
	
	switch actState {
		case 1:
			var accel = 0.1
			if obj_PlayerParent.x > x-3 {
				xSpeed += accel
				image_xscale = 1
				if xSpeed > maxSpeed {
					xSpeed = maxSpeed	
				}
			}
			if obj_PlayerParent.x < x-3 {
				xSpeed -= accel
				image_xscale = -1
				if xSpeed < -maxSpeed {
					xSpeed = -maxSpeed	
				}
			}
			if rise = true {
				ySpeed -= accel
				if ySpeed < -maxSpeed*0.75 {
					ySpeed = -maxSpeed*0.75	
				}
			} else {
				if obj_PlayerParent.y > y-3 {
					ySpeed += accel
					if ySpeed > maxSpeed*0.75 {
						ySpeed = maxSpeed*0.75	
					}
				}
				if obj_PlayerParent.y < y-3 {
					ySpeed -= accel
					if ySpeed < -maxSpeed*0.75 {
						ySpeed = -maxSpeed*0.75	
					}
				}
			}
			break;
		case 0:
			var stop = 0.01
			if xSpeed > 0 {
				xSpeed -= stop
				if xSpeed < 0 {
					xSpeed = 0	
				}
			}
			if xSpeed < 0 {
				xSpeed += stop
				if xSpeed > 0 {
					xSpeed = 0	
				}
			}
			if ySpeed > 0 {
				ySpeed -= stop
				if ySpeed < 0 {
					ySpeed = 0	
				}
			}
			if ySpeed < 0 {
				ySpeed += stop
				if ySpeed > 0 {
					ySpeed = 0	
				}
			}
			break;
			
	}
	if place_meeting(x+xSpeed,y,obj_Collision) {
		xSpeed = -xSpeed	
	}
	if place_meeting(x,y+ySpeed,obj_Collision) {
		ySpeed = -ySpeed	
	}
		
	if place_meeting(x+xSpeed,y,obj_FlyGuyParent) {
				
		var inst = instance_place(x+xSpeed,y,obj_FlyGuyParent)
		if inst.state != "cannonball" {
			if x > inst.x {
				xSpeed = abs(xSpeed)
				inst.xSpeed = -abs(inst.xSpeed)
			}
			if x < inst.x {
				xSpeed = -abs(xSpeed)
				inst.xSpeed = abs(inst.xSpeed)
			}
		}
	}
		
	if place_meeting(x+xSpeed,y+ySpeed,obj_PlayerParent) {
		if scr_HurtKelly(3) = 1 {
			xSpeed = -xSpeed	
		}
	}

}
if state = "hurt" {
	sprite_index = spr_TrainAgoraMouth
	state = "normal"
	
	if obj_PlayerParent.x > x {
		xSpeed = -1
	}
	if obj_PlayerParent.x < x {
		xSpeed = 1
	}
}

if state = "cannonball" {
	scr_EnemyCannonball(spr_TrainAgoraMouth)
	var c = true
} else {
	var c = false
}
if state = "normal" {
	c = true	
}
scr_Collision(c)	



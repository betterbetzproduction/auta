
if flicker = true {
    var z = choose(0.05,-0.05,0);
    lightSizeX += z;
    
    lightSizeX = clamp(lightSizeX, originSizeX, originSizeX + flickerdepth);
} else {
	lightSizeX = originSizeX	
}
if flicker = true {
    var z = choose(0.05,-0.05,0);
    lightSizeY += z;
    
    lightSizeY = clamp(lightSizeY, originSizeY, originSizeY + flickerdepth);
} else {
	lightSizeY = originSizeY	
}

/*if lightStrength != 1 {
    lightStrength += 0.01;
}


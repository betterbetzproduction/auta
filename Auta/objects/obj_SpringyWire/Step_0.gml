/// @description Insert description here
// You can write your code in this editor
image_yscale = 1
bX = x
bY = y
eX = x+16*image_xscale*dcos(image_angle)
eY = y-16*image_xscale*dsin(image_angle)
cX = bX/2+eX/2
cY = bY/2+eY/2

if place_meeting(x,y,obj_PlayerParent) {
	contacted = true	
}
if contacted {
	if !place_meeting(x,y,obj_PlayerParent) {
		if obj_PlayerParent.ySpeed >= 0 {
			
			obj_PlayerParent.xSpeed = -pwr*dsin(image_angle)
			obj_PlayerParent.ySpeed = -pwr*dcos(image_angle)
			
			if abs(obj_PlayerParent.xSpeed) > 2 {
				obj_PlayerParent.leftRightLock = 5
			}
		}
		contacted = false
	}
	
}	
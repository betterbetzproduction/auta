/// @description Insert description here
// You can write your code in this editor

if delay = 0 {
	delay = -1
	_enemyList = ds_list_create()
	_enemyNum = collision_rectangle_list(bbox_left,bbox_top,bbox_right,bbox_bottom,obj_EnemyGauntletSpawner,false,true,_enemyList, false)
}
if delay = 1{
	delay = 0	
}
if active {
	if _enemyNum > 0 {
		var subtract = true
	    for (var i = 0; i < _enemyNum; ++i;) {
	        var qInst = ds_list_find_value(_enemyList,i)
			if instance_exists(qInst) {
				if qInst.visible = true {
					subtract = false
				}
			} else {
				ds_list_delete(_enemyList,i)
				_enemyNum--
				i--
			}
		}
		if _enemyNum = 0 {
			instance_destroy();
			ds_list_destroy(_enemyList)
		}
		if subtract = true {
			for (var i = 0; i < _enemyNum; ++i;) {
		        var qInst = ds_list_find_value(_enemyList,i)
				if instance_exists(qInst) {
					qInst.gauntletCounter--
				}
			}
		}
    }
	show_debug_message(_enemyList)
}
if active {
	if !place_meeting(x,y,obj_EnemyGauntletSpawner) {
		instance_destroy();	
	}
}
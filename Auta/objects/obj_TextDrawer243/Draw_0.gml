/// @description Insert description here
// You can write your code in this editor
if !near {
	draw_set_font(fnt_Terminal)
	draw_set_color(c_white)
	draw_set_halign(fa_middle)
	draw_set_valign(fa_center)
	draw_text_transformed(x,y,text,image_xscale*0.5,image_yscale*0.5,image_angle)
}
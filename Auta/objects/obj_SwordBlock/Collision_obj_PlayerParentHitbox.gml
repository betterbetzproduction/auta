/// @description Insert description here
// You can write your code in this editor
audio_play_sound(snd_BlockBreak,4,false)
var __time = current_time;  
		while current_time-__time < 25 {  
	}  
global.screenshake = 1
with instance_create_layer(x,y,layer,obj_BreakableBlockFrags) {
	image_index = 0
	xSpeed = -1
	ySpeed = -1
	sprite_index = spr_SwordBlock_Frags
}
with instance_create_layer(x,y,layer,obj_BreakableBlockFrags) {
	image_index = 1
	xSpeed = 1
	ySpeed = -1
	sprite_index = spr_SwordBlock_Frags
}
with instance_create_layer(x,y,layer,obj_BreakableBlockFrags) {
	image_index = 2
	xSpeed = -1
	ySpeed = -0.5
	sprite_index = spr_SwordBlock_Frags
}
with instance_create_layer(x,y,layer,obj_BreakableBlockFrags) {
	image_index = 3
	xSpeed = 1
	ySpeed = -0.5
	sprite_index = spr_SwordBlock_Frags
}
if global.currentweapon = "stick" {
	with instance_create_depth(x,y,depth+1,obj_SwordPickup)	{
		swordid = "sword"
		sprite_index = spr_SwordPickup
	}
}
else {
	var rand = choose(0,1,2)
	if global.speedrun {
		rand = 1	
	}
	if rand = 0 {
		with instance_create_depth(x,y,depth+1,obj_SwordPickup)	{
			swordid = "firesword"
			sprite_index = spr_FireSwordPickup
		}
	}
	if rand = 1 {
		with instance_create_depth(x,y,depth+1,obj_SwordPickup)	{
			swordid = "lasersword"
			sprite_index = spr_LaserSwordPickup
		}
	}
	if rand = 2 {
		with instance_create_depth(x,y,depth+1,obj_SwordPickup)	{
			swordid = "icesword"
			sprite_index = spr_IceSwordPickup
		}
	}
} 
instance_destroy()
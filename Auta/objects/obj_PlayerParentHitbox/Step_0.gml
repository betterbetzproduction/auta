/// @description Insert description here
// You can write your code in this editor
if !selfDependant {
	x = obj_PlayerParent.x
	y = obj_PlayerParent.y
	
	image_index = obj_PlayerParent.image_index
	if obj_PlayerParent.sprite_index != killsprite {
		instance_destroy()	
	}
	if stop {
		obj_Auta.stuntimer = round(4*(sqrt( power(Xknockback,2) + power(Yknockback,2) )))
		//obj_Auta.canAttackBoost = true
	}
	
}

if !onetimething {
	if sprite_index = spr_AutaAttackBurst_Hitbox {
		if image_index >= 2 {
			global.screenshake = 1.5
			onetimething = true	
			audio_play_sound(snd_BurstAttack,4,false)
		}
	}
		
}

if stopme {  //if it can stop
	if stop { //if it will stop
		obj_PlayerParent.xSpeed = 0
		obj_PlayerParent.ySpeed = 0
		obj_PlayerParent.atkStateGrav = 0
		
		if obj_Auta.sprite_index = spr_AutaStomp {
			obj_PlayerParent.ySpeed = -4
			obj_PlayerParent.xSpeed = -1*image_index
			obj_PlayerParent.state = "normal"
			obj_PlayerParent.animstate = "normal"
		}	
	}
}

if sprite_index = spr_AutaAttackBurst_Hitbox {
	if image_index > 2 {
		selfDependant = true	
	}
}

stop = false
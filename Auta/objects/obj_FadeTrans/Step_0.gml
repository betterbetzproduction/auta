/// @description Insert description here
// You can write your code in this editor
if state = 0 {
	image_alpha += 0.05	
	if image_alpha >= 1 {
		state = 1	
	}
}
if state = 3 {
	if toInst != noone {
		toX = toInst.x
		toY = toInst.y
	}
	if instance_exists(obj_PlayerParent) {
		obj_PlayerParent.x = toX
		obj_PlayerParent.y = toY
	}
	if instance_exists(obj_DefaultCamera) {
		obj_DefaultCamera.x = toX
		obj_DefaultCamera.y = toY
	}
	state = 2
}

if state = 1 {
	room_goto(toRoom)
	state = 3
}
if state = 2 {
	if delay {
		image_alpha-=0.01	
	} else {
		image_alpha -= 0.05
	}
	if image_alpha <= 0 {
		instance_destroy()	
	}
}
/// @description Insert description here
// You can write your code in this editor

if other.aInteract {
	if !instance_exists(obj_textbox) {
		if obj_Auta.grounded = true {
			obj_PlayerParent.state = "cutscene"
			obj_PlayerParent.xSpeed = 0
			obj_PlayerParent.animstate = "normal"
			obj_PlayerParent.talkTarg = id
			
			global.respawnRoom = room
			global.respawnx = obj_Auta.x
			global.respawny = obj_Auta.y
			
			with instance_create_layer(x,y,layer,obj_DialogueHelper) {
				var text = ""
				var mySpeaker = -1
				speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
	
				var t = 0//line 1
				//text[t] = "...What? You again, kid?";
				//mySpeaker[t] = speaker[0]
	
				//t++
				//text[t] = "I guess this may now be a common occurance.";
				//mySpeaker[t] = speaker[0]
				
				//t++
				//text[t] = "Hey. Might as well settle down and chat with me a bit. Tell me what you've been up to.";
				//mySpeaker[t] = speaker[0]
				
				text[t] = "Weird place we got here...";
				mySpeaker[t] = speaker[0]
				
				t++
				text[t] = "So, tell me what you've been up to!";
				mySpeaker[t] = speaker[0]
				
				t++
				text[t] = "(Game saved!)";
				mySpeaker[t] = speaker[0]
				
				t++
				text[t] = "Looks like you've got a long road ahead of you.";
				mySpeaker[t] = speaker[0]
				
				t++
				text[t] = "I'm interested to see how it goes. Feel free to chat with me whenever, kid.";
				mySpeaker[t] = speaker[0]
				
				t++
				text[t] = "'Til next time...";
				mySpeaker[t] = speaker[0]
				create_dialogue(text, mySpeaker,0,0,0,0,0,0,0);	
			}
			
			
			active = true
		}
	}
}


/// @description Insert description here
// You can write your code in this editor

if state = "bye" {
	display_set_gui_size(800,450)
	
	var scale = 4
	var scale2 = 4
	if offset <= 0 && animaccel < 0 {
		instance_destroy();	
	}
	
	draw_sprite_ext(spr_ItemTag,0,400,400,scale,scale,0,c_black,1)
	
	gpu_set_blendmode(bm_add)
	draw_sprite_ext(spr_ItemTag,0,400-offset,400-offset,scale2,scale2,0,c_aqua,1)
	draw_sprite_ext(spr_ItemTag,0,400+offset,400+offset,scale2,scale2,0,c_fuchsia,1)
	
	gpu_set_blendmode(bm_normal)
	offset += animaccel
	if animaccel > 0.5 {
		animaccel-=0.2	
	} else if animaccel > 0 {
		animaccel-=0.02
	} else {
		animaccel-=0.4	
	}
	draw_set_font(fnt_Menu)
	draw_set_halign(fa_middle)
	draw_set_color(c_white)
	draw_set_valign(fa_center)
	draw_text(400,400,"Got " + item)
}
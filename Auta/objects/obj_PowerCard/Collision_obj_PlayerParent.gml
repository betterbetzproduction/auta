/// @description Insert description here
// You can write your code in this editor
if stallTime < 0 {
	if state != "bye" {
		obj_PlayerParent.HP += 2
		audio_play_sound(snd_Powerup,4,false)
	
		if obj_PlayerParent.HP > 20 {
			obj_PlayerParent.HP = 20	
		}
		scr_AddItem(item, 0)
	}
	state = "bye"
	
	if global.gottenFirstCard = false {
		global.gottenFirstCard = true
		instance_destroy();
		global.gameSpeed = 0	
		with instance_create_layer(x,y,layer,obj_DialogueHelper) {
			unpause = true
			
			var text = ""
			var mySpeaker = -1
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
	
			var t = 0//line 1
			text[t] = "You got an augment card!";
			mySpeaker[t] = speaker[0]
			
			var t = 0//line 1
			text[t] = "Augments give you new abilities to work with, whether they be mobility or combat focused.";
			mySpeaker[t] = speaker[0]
	
			t++
			text[t] = "Open your inventory with ENTER and then equip it. Then press the subweapon button and see what happens!";
			mySpeaker[t] = speaker[0]
				
			create_dialogue(text, mySpeaker,0,0,0,0,0,0,0);	
		}
	}
}
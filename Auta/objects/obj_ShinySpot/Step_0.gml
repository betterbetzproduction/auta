/// @description Insert description here
// You can write your code in this editor
if checkDelay = 1 {
	if index != -1 {
		if global.chest[index] = 0 {
			image_index = 1
			hits = -1000	
			instance_destroy();
		}
	}
	checkDelay = 0
}
x = baseX + random_range(-xAdd,xAdd)
y = baseY + random_range(-yAdd, yAdd)

if xAdd > 0 {
	xAdd -= 0.2
}
if yAdd > 0 {
	yAdd -= 0.2	
}

if !place_meeting(x,y,obj_PlayerParentHitbox) {
	invincible = false
}

if !place_meeting(baseX,baseY+1,obj_Collision) {
	baseY += 4
}
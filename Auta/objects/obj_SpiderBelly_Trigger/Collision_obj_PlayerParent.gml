/// @description Insert description here
// You can write your code in this editor
if instance_exists(obj_SpiderBelly) {
	if obj_SpiderBelly.state = "idle" {
		obj_SpiderBelly.sprite_index = spr_SpiderBelly_Roar
		instance_destroy();
		audio_play_sound_at(snd_SpiderBellyRoar, obj_SpiderBelly.x, obj_SpiderBelly.y,0,100,100,1,false,4);
	}	
}

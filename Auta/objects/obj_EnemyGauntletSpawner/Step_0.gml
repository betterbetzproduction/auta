/// @description Insert description here
// You can write your code in this editor
if gauntletCounter = 0 {
	FXInst = instance_create_layer(x,y,layer,obj_OneTimeFX)
	with FXInst {
		sprite_index = spr_EnemySpawn
	}
	
	gauntletCounter = -25
	visible = true
}
if gauntletCounter = -50 {
	if !instance_exists(inst) {
		instance_destroy() 
	}
}
if gauntletCounter = -25 && !instance_exists(FXInst) {
	inst = instance_create_layer(x,y,layer,enemy)
	gauntletCounter = -50
}

show_debug_message(gauntletCounter)
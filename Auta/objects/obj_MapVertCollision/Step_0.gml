/// @description Insert description here
// You can write your code in this editor

if !place_meeting(x,y,obj_Auta) {
	dirFrom = (y > obj_PlayerParent.y ? -1:1)
}

if place_meeting(x,y,obj_Auta) {
	if active = false {
		active = true	
		
		dirMoved = dirFrom
		global.mapY -= dirMoved
	}
} else {
	if active = true {
		active = false
		dirFrom = (y > obj_PlayerParent.y ? -1:1)
		
		if dirFrom = dirMoved {
			global.mapY += dirMoved
		}	
	}
}
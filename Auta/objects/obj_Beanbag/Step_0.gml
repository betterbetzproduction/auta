/// @description Insert description here
// You can write your code in this editor

scr_EnemyStandard(spr_Beanbag);

sprite_index = spr_Beanbag

if state = "normal" {
	if grounded {
		var accel = 0.15
		var stop = 0.1
		
	} else {
		var accel = 0
		var stop = 0
	}
	
	if xSpeed > 0 {
		if xSpeed > stop {
			xSpeed -= stop	
		} else {
			xSpeed = 0	
		}
	}
	if xSpeed < 0 {
		if xSpeed < -stop {
			xSpeed += stop	
		} else {
			xSpeed = 0	
		}
	}
	
	if act = "jump" {
		if grounded {
			grounded = false
			ySpeed = -6
		}
		
	}
	
	var range = 0
	if distance_to_object(obj_PlayerParent) < range {
		if sprite_index = spr_Chad {
			image_index = 0
			sprite_index = spr_Chad_ToAgro	
		}
		
		if obj_PlayerParent.x > x{
			if xSpeed < maxSpeed {
				if !place_meeting(x+maxSpeed,y,obj_Collision) {
					xSpeed += accel	
				}
			}
			if xSpeed > maxSpeed {
				xSpeed = maxSpeed	
			}
		}
		if obj_PlayerParent.x < x {
			if xSpeed > -maxSpeed {
				if !place_meeting(x-maxSpeed,y,obj_Collision) {
					xSpeed -= accel	
				}
			}
			if xSpeed < -maxSpeed {
				xSpeed = -maxSpeed	
			}
		}
		
		if place_meeting(x+xSpeed,y,obj_PlayerParent){
			if scr_HurtKelly(3) = 1 {
				ySpeed = -0.5
				xSpeed = 0
				grounded = false
				sprite_index = spr_Chad_OutAgro
			}
		}
		
		
	} else {
		if sprite_index = spr_Chad_Agro {
			sprite_index = spr_Chad_OutAgro	
			image_index = 0
		}
	}
	
	//xSpeed = maxSpeed * image_xscale
}
if state = "hurt" {
	if grounded {
		state = "normal"	
		sprite_index = spr_Chad
	}
	if place_meeting(x+xSpeed,y,obj_Collision) {
		xSpeed = -xSpeed
	}
}

	
if state = "cannonball" {
	scr_EnemyCannonball(spr_Chad_Cannonball)
	var c = true
} else {
	var c = false
}
scr_Collision(c)	


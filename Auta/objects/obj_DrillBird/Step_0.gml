/// @description Insert description here
// You can write your code in this editor

scr_EnemyStandard(spr_Drillbird_Roost);
if hitstunTimer > 0 {exit};
if state = "roost" {
	var range = 100
	if distance_to_object(obj_PlayerParent) < range {
		state = "normal"
		sprite_index = spr_Drillbird_FllyUp
	}
}

if state = "normal" {
	grounded = false
	if grounded {
		var accel = 0.15
		var stop = 0.1
		
	} else {
		var accel = 0.02
		var stop = 0
	}
	
	if xSpeed > 0 {
		if xSpeed > stop {
			xSpeed -= stop	
		} else {
			xSpeed = 0	
		}
	}
	if xSpeed < 0 {
		if xSpeed < -stop {
			xSpeed += stop	
		} else {
			xSpeed = 0	
		}
	}
	
	if place_meeting(x+xSpeed,y,obj_Collision) {
		xSpeed = -xSpeed	
	}
	var range = 150
	if distance_to_object(obj_PlayerParent) < range {
		
		if y > obj_PlayerParent.y - 64 {
			ySpeed -= grav*1.5	
			if ySpeed < -1 {
				ySpeed = -1	
			}
		}
		if ySpeed > 1 {
			ySpeed = 1	
		}

		if obj_PlayerParent.x > x{
			if xSpeed < maxSpeed {
				if !place_meeting(x+maxSpeed,y,obj_Collision) {
					xSpeed += accel	
				}
			}
			if xSpeed > maxSpeed {
				xSpeed = maxSpeed	
			}
		}
		if obj_PlayerParent.x < x {
			if xSpeed > -maxSpeed {
				if !place_meeting(x-maxSpeed,y,obj_Collision) {
					xSpeed -= accel	
				}
			}
			if xSpeed < -maxSpeed {
				xSpeed = -maxSpeed	
			}
		}
		
		if abs((obj_PlayerParent.x+obj_PlayerParent.xSpeed*10) - x) < 5 && y-obj_PlayerParent.y < -32 {
			
			var highEnough = true
			for (var i = 0; i < 8; i++) {
				if place_meeting(x,y+8*i,obj_Collision) {
					highEnough = false
				}
			}
			if highEnough {
				state = "toDrill"	
			}
		}
		
		if place_meeting(x+xSpeed,y,obj_PlayerParent){
			if scr_HurtKelly(3) = 1 {

			}
		}
		
		
	} else {
		if grounded {
			sprite_index = spr_Drillbird_Roost
			state = "roost"
		}
	}
	
	//xSpeed = maxSpeed * image_xscale
}
if state = "toDrill" {
	image_angle += 18
	y-=0.2
	xSpeed = 0
	if image_angle = 180 {
		state = "Drill"	
		sprite_index = spr_Drillbird_Drill
		ySpeed = 4
	}
}
if state = "Drill" {
	xSpeed = 0;
	if place_meeting(x+xSpeed,y+ySpeed,obj_PlayerParent){
		if scr_HurtKelly(4) = 1 {
			
		}
	}
	if grounded {
		audio_play_sound_at(snd_Slam,x,y,0,50,300,1,false,4);
		state = "stun"
		ySpeed = 0
		nudgeWeight = 2
		nudge = true
		sprite_index = spr_Drillbird_Stuck
		//vulnState = true
	}
}

if state = "hurt" {
	if grounded {
		state = "normal"	
		sprite_index = spr_Drillbird_FllyUp
	}
	if place_meeting(x+xSpeed,y,obj_Collision) {
		xSpeed = -xSpeed
	}
}

	
if state = "cannonball" {
	scr_EnemyCannonball(spr_Drillbird_Hurt)
	image_angle = point_direction(x,y,x+xSpeed,y+ySpeed)-90
	var c = true
} else {
	var c = false
}

if state = "normal" {
	var c = false	
}

if state != "roost" && state != "toDrill" {
	scr_Collision(c)	
}



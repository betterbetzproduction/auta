/// @description Insert description here
// You can write your code in this editor

if !active {
	exit;	
}


scr_EnemyStandard(spr_DuoDroid_Walker_Leg);

if hitstunTimer > 0 {exit};

if state = "hold" {
	y = dInst.y+15
	ySpeed = 0
	x = dInst.x
}

if state = "normal" {
	if grounded {
		var accel = 0.15
		var stop = 0.1
		
	} else {
		var accel = 0
		var stop = 0
	}
	
	if xSpeed > 0 {
		if xSpeed > stop {
			xSpeed -= stop	
		} else {
			xSpeed = 0	
		}
	}
	if xSpeed < 0 {
		if xSpeed < -stop {
			xSpeed += stop	
		} else {
			xSpeed = 0	
		}
	}

	if grounded {
		actTimer+=spd
	}
	
	if place_meeting(x+xSpeed,y,obj_PlayerParent){
		if scr_HurtKelly(4) = 1 {

		}
	}

	if grounded && sprite_index = spr_DuoDroid_Spin {
		ySpeed = -2
		grounded = false
		image_index = 0
		sprite_index = spr_DuoDroid_Walker_ToLeg
		xSpeed = xSpeed/2
	}
	
	var range = 200
	if actTimer > 60 {
		if distance_to_object(obj_PlayerParent) < range {
			if sprite_index = spr_DuoDroid_Walker_Leg {
				image_index = 0
				sprite_index = spr_DuoDroid_Walker_Run	
			}
		
			if obj_PlayerParent.x > x{
				image_xscale = -1
				if xSpeed < maxSpeed {
					if !place_meeting(x+maxSpeed,y,obj_Collision) {
						xSpeed += accel	
					}
				}
				if xSpeed > maxSpeed {
					xSpeed = maxSpeed	
				}
			}
			if obj_PlayerParent.x < x {
				image_xscale = 1
				if xSpeed > -maxSpeed {
					if !place_meeting(x-maxSpeed,y,obj_Collision) {
						xSpeed -= accel	
					}
				}
				if xSpeed < -maxSpeed {
					xSpeed = -maxSpeed	
				}
			}
		
			
		}
		
		if distance_to_object(obj_PlayerParent) < 50 {
			xSpeed = 0
			sprite_index = spr_DuoDroid_Walker_JumpPrep
			actTimer = 0
			image_index = 0
		}
		if actTimer > 180 && sprite_index = spr_DuoDroid_Walker_Run{
			xSpeed = 0
			sprite_index = spr_DuoDroid_Walker_JumpPrep
			actTimer = 0
			image_index = 0
		}
		
	}
	
	//xSpeed = maxSpeed * image_xscale
}
if state = "hurt" {
	if grounded {
		state = "normal"	
		sprite_index = spr_DuoDroid_Walker_Leg
	}
	if place_meeting(x+xSpeed,y,obj_Collision) {
		xSpeed = -xSpeed
	}
}

	
if state = "cannonball" {
	scr_EnemyCannonball(spr_DuoDroid_Spin)
	var c = true
} else {
	var c = false
}
scr_Collision(c)	


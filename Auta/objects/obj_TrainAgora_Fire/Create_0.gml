/// @description Insert description here
// You can write your code in this editor
// Setup
sys_particle = part_system_create()
part_system_layer(sys_particle, layer)
part_particle = part_type_create()
part_type_shape(part_particle, pt_shape_square)
part_type_size(part_particle, 0, 0.2, 0, 0)
part_type_scale(part_particle, 1, 1)
part_type_color3(part_particle, 10759003, 16738906, 4325631)
part_type_alpha3(part_particle, 0, 0.2, 0)
part_type_speed(part_particle, 0, 0.1, 0, 0)
part_type_direction(part_particle, 0, 360, 0, 0)
part_type_gravity(part_particle, 0.02, 90)
part_type_orientation(part_particle, 0, 360, 0, 0, 0)
part_type_blend(part_particle, 1)
part_type_life(part_particle, 60, 184)
emit_particle = part_emitter_create(sys_particle)


// To Use Particle:




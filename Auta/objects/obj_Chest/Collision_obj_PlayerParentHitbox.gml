/// @description Insert description here
// You can write your code in this editor

if hits > 0 {
	if !invincible {
		invincible = true
		other.stop = true
		hits--	
		xAdd = 2
		yAdd = 2
	}
}
if hits = 0 {
	if !invincible {
		
		global.chest[index] = 0
		 
		image_index = 1
		other.stop = true
		for(i = 0; i < array_length_1d(drops);i++) {
			with instance_create_depth(x,y,depth-1,drops[i]) {
				ySpeed = -2
				xSpeed = random_range(1,-1);
				drop = true
			}
		}
		xAdd = 3
		yAdd = 3
		hits = -1000
	}
}

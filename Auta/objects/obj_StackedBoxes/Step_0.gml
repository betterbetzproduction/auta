/// @description Insert description here
// You can write your code in this editor
if shaking { 
	x = basex + random_range(-1,1)
	y = basey + random_range(-1,1)
	
	shakeCounter--
	if shakeCounter = 0 {
		instance_destroy();
		instance_create_depth(x,y-1,depth,obj_StackedBoxes_FreeFall)
	}
}
if collision_rectangle(bbox_left-1,bbox_top-1,bbox_right+1,bbox_bottom+1,obj_PlayerParent,false,true) {
	shaking = true	
}
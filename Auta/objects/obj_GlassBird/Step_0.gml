/// @description Insert description here
// You can write your code in this editor

scr_EnemyStandard(spr_GlassBird);

grav = 0.08

nudge = true

if state = "normal" {
	if grounded {
		var accel = 0.1
		var stop = 0.1
		
	} else {
		var accel = 0.05
		var stop = 0
	}
	
	if xSpeed > 0 {
		if xSpeed > stop {
			xSpeed -= stop	
		} else {
			xSpeed = 0	
		}
	}
	if xSpeed < 0 {
		if xSpeed < -stop {
			xSpeed += stop	
		} else {
			xSpeed = 0	
		}
	}
	
	var range = 200
	if distance_to_object(obj_PlayerParent) < range {
	
		if grounded {
			actCounter += spd
			sprite_index = spr_GlassBirdSpin
			if actCounter > 60 {
				sprite_index = spr_GlassBirdSpin
				image_speed = spd*(actCounter-60)/10
			}
			if actCounter > 120 {
				grounded = false
				ySpeed = -2
				actCounter = 0
			}
		
		} else {
			sprite_index = spr_GlassBirdFastSpin	
			grav = 0.03
				
			if obj_PlayerParent.x > x{
				if xSpeed < maxSpeed {
					if !place_meeting(x+maxSpeed,y,obj_Collision) {
						xSpeed += accel	
					}
				}
				if xSpeed > maxSpeed {
					xSpeed = maxSpeed	
				}
			}
			if obj_PlayerParent.x < x {
				if xSpeed > -maxSpeed {
					if !place_meeting(x-maxSpeed,y,obj_Collision) {
						xSpeed -= accel	
					}
				}
				if xSpeed < -maxSpeed {
					xSpeed = -maxSpeed	
				}
			}
			
			if ySpeed > 1 {
				ySpeed = 1	
			}
			
			if place_meeting(x+xSpeed,y,obj_GlassBird) {
				
				var inst = instance_place(x+xSpeed,y,obj_GlassBird)
				if inst.state != "cannonball" {
					if x > inst.x {
						xSpeed = abs(xSpeed)
						inst.xSpeed = -abs(inst.xSpeed)
					}
					if x < inst.x {
						xSpeed = -abs(xSpeed)
						inst.xSpeed = abs(inst.xSpeed)
					}
				}
			}
			nudge = false
			
		}
		
	}
	
	
	//xSpeed = maxSpeed * image_xscale
}
if state = "hurt" {
	if grounded {
		state = "normal"	
		sprite_index = spr_GlassBird
	}
	if place_meeting(x+xSpeed,y,obj_Collision) {
		xSpeed = -xSpeed
	}
}

	
if state = "cannonball" {
	scr_EnemyCannonball(spr_GlassBird_Cannonball)
	var c = true
} else {
	var c = false
}
scr_Collision(c)	


/// @description Insert description here
// You can write your code in this editor

if place_meeting(x,y,obj_Collision) {
	if sprite_index = spr_TrainAgoraEye_AttackParticle1 {
		with instance_create_depth(x,y,depth,obj_OneTimeFX) {
			sprite_index = spr_TrainAgoraEye_AttackParticle_Crash
			if audio_is_playing(snd_LightBat_OrbExplode) {audio_stop_sound(snd_LightBat_OrbExplode)}
			audio_play_sound(snd_LightBat_OrbExplode,4,false)
		}
	}
	instance_destroy()	
}

speed = global.gameSpeed*1.5
counter += global.gameSpeed

if sprite_index = spr_TrainAgoraEye_AttackParticle1 {
	image_angle = direction	
}

if counter > 240 {
	instance_destroy();	
}
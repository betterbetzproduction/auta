/// @description Insert description here
// You can write your code in this editor
aSelect		= keyboard_check_pressed(kJump) || gamepad_button_check_pressed(pad,pJump)
aCancel		= keyboard_check_pressed(kAttack) || gamepad_button_check_pressed(pad,pAttack)
aUp			= keyboard_check_pressed(kUp) || gamepad_button_check_pressed(pad,pUp2)
aLeft		= keyboard_check_pressed(kLeft) || gamepad_button_check_pressed(pad,pLeft2)
aRight		= keyboard_check_pressed(kRight) || gamepad_button_check_pressed(pad,pRight2)
aDown		= keyboard_check_pressed(kDown) || gamepad_button_check_pressed(pad,pDown2)
aMenu		= keyboard_check_pressed(kMenu) || gamepad_button_check_pressed(pad,pMenu)

if closeDelay = 0 {
	if aMenu {
		instance_destroy()
		global.cutscene = false
		global.gameSpeed = 1
		global.currentControl.state = "normal"
		global.inventory = inventory
	}
} else {closeDelay--}
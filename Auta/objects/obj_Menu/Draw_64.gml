/// @description Insert description here
// You can write your code in this editor
if state = "Inventory" {
	draw_set_valign(fa_middle)
	display_set_gui_size(800,450)
	draw_set_color(c_fuchsia)
	draw_set_font(fnt_Menu)
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)
	offset = 5
	draw_rectangle(450-offset,100+offset,725-offset,400+offset,false)
	draw_set_color(c_black)
	draw_rectangle(450,100,725,400,false)
	
	
	display_set_gui_size(800,450)
	draw_set_color(c_fuchsia)
	draw_set_font(fnt_Menu)
	offset = 5
	draw_rectangle(75-offset,200+offset,350-offset,400+offset,false)
	draw_set_color(c_black)
	draw_rectangle(75,200,350,400,false)
	
	draw_sprite_ext(spr_Inventory,0,587,100,4,4,0,c_white,1)
	
	inventoryFirst = 1
	
	for(var q=0; q<clamp(array_length(inventory)-1,0,8); q++) {
		draw_set_color(c_white)
		if mPos-1 = q {
			draw_set_color(c_aqua)	
		}
		draw_text(495,125+35*q,inventory[q+1][0])	
		draw_text(675,125+35*q,"x" + string(inventory[q+1][1]))
		spr = scr_SpriteForItem(inventory[q+1][0])
		draw_sprite_ext(spr,0,455,128+35*q,1.5,1.5,1,c_white,1)
	}
	
	
	if aUp {
		mPos -= 1
		
	}
	if aDown {
		mPos += 1
		
	}
	if mPos < 1 {
			mPos = array_length(inventory)-1	
	}
	if mPos > array_length(inventory)-1 {
		mPos = 1	
	}
	

	var sel = false
	if aSelect {
		var sel = true
		audio_play_sound(snd_MenuSelect,4,false)
	}
	
	if aCancel {
		state = "Default"
		aCancel = false
		mPos = 0
	}
	/*global.inventory[0] = "Sword of Eternity"
global.inventory[1] = "Unseasoned Xmas Tree"
global.inventory[2] = "Saxman Statue"
global.inventory[3] = "Hat Collection"
global.inventory[4] = "Certificate of Lunch"
global.inventory[5] = "Sunflower Shells"
global.inventory[6] = "Balloon Cream"
global.inventory[7] = "Gummy Vitamins"
*/

//0 - Weapon
//1 - Throw
//2 - Armor
//3 - Mobility
//4 - Shield
//10 - Acc1
//11 - Acc2
//12 - Acc3

//scr_EquipInit
	switch(inventory[mPos][0]) {
													//WEAPONS
		case "Brush Blade":
			text = @"
+1 ATK 
A short ranged blue beam Which 
can slice up foes."
		if sel { inventory = scr_Equip(inventory[mPos][0],0,mPos); sel = false }
		break;
		
		case "Soft-Coded Blade":
			text = @"
+2 ATK 
A brush blade with some light 
optimization."
		if sel { inventory = scr_Equip(inventory[mPos][0],0,mPos); sel = false}
		break;
		
		case "Swift Blade":
			text = @"
+0 ATK 
Shorter and Quicker."
		if sel { inventory = scr_Equip(inventory[mPos][0],0,mPos); sel = false}
		break;
		
		case "Heavy Blade":
			text = @"
+3 ATK 
Takes longer to Swing."
		if sel { inventory = scr_Equip(inventory[mPos][0],0,mPos); sel = false}
		break;
		
		
		case "Piercer":
			text = @"
+1 ATK 
A long-ranged piercing shot."
		if sel { inventory = scr_Equip(inventory[mPos][0],0,mPos); sel = false}
		break;
		
		case "Gauntlets":
			text = @"
+0 ATK 
Quick and strong hits."
		if sel { inventory = scr_Equip(inventory[mPos][0],0,mPos); sel = false}
		break;
		
		case "Cannon":
			text = @"
+4 ATK 
An explosive punch attack."
		if sel { inventory = scr_Equip(inventory[mPos][0],0,mPos); sel = false}
		break;

		case "Fizzle Card":
			text = @"
A ground-tracing fireball that 
rises to hit aerial foes."
		if sel { inventory = scr_Equip(inventory[mPos][0],5,mPos); sel = false}
		break;
		
		case "Smolder Card":
			text = @"
A small, semi-ranged blast of 
fire."
		if sel { inventory = scr_Equip(inventory[mPos][0],5,mPos); sel = false}
		break;
											//REGULAR ITEMS
		case "Rusty Scraps":
			text = @"
A lackluster collection of scrap 
metal. 
Not usable in it's current form, 
but could be used as material
by the right person."
		break;
		if sel { sel = false}
		case "Metallic Scraps":
			text = @"
A collection of scrap 
metal. 
Not usable in it's current form, 
but could be used as material
by the right person."
		break;
		
		if sel { sel = false}
		case "Strong Scraps":
			text = @"
A hardy collection of scrap 
metal. 
Not usable in it's current form, 
but could be used as material
by the right person."
		if sel { sel = false}
		break;
		
		case "Mohs Crystal":
			text = @"
A little crystal capable of 
scratching most anything.
Not usable in it's current form, 
but could be used as material
by the right person."
		if sel { sel = false}
		break;
											//KEY ITEMS
		case "Staircase Key":
			text = @"
A key good for accessing anything 
society deems to be a staircase."
		if sel { sel = false}
		break;
		
	}
	
	if array_length(inventory) = 1 {
		state = "Default"
		aCancel = false
		mPos = 0
	}
	
	if mPos < 1 {
			mPos = array_length(inventory)-1	
	}
	if mPos > array_length(inventory)-1 {
		mPos = 1	
	}
	
	
	draw_set_valign(fa_top)
	draw_set_color(c_white)
	draw_set_halign(fa_center)
	
	draw_text(212,195, inventory[mPos][0])
	
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)
	draw_set_color(c_white)
	draw_text(85,200, text)
	
	draw_sprite_ext(scr_SpriteForItem(inventory[mPos][0]),0,180,360,2,2,0,c_white,1)
	draw_text(220,360,"x" + string(inventory[mPos][1]))
	
}
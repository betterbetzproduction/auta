/// @description Insert description here
// You can write your code in this editor

if state = "Default" {
	for(var i=0; i <= 3; i++) {
		var yDiff = 15
		draw_set_alpha(alpha-0.2*i)
		if i = 0 && array_length(inventory) <= 1 {
			draw_set_alpha(draw_get_alpha()-0.5)		
		}
		draw_sprite(spr_Menu,i*2+(mPos=i),x+xOffset*(2/3*i)+i*yDiff,y+i*yDiff)
		
	
	}
	if xOffset < 0 {
		xOffset += 5
	}
	if alpha < 1.4 {
		alpha += 0.2	
	}

	if aUp {
		mPos -= 1
		if mPos < 0 {
			mPos = 2	
		}
	}
	if aDown {
		mPos += 1
		if mPos > 2 {
			mPos = 0	
		}
	}
	
	if aSelect {
		switch mPos {
			case 0:
				if array_length(inventory) > 1 {
					audio_play_sound(snd_Sel,4,false)
					state = "Inventory"
				} else {
					audio_play_sound(snd_Err,4,false)
				}
				break;
		}
		aSelect = false
		mPos = 0
	}
	
}

//draw_self();
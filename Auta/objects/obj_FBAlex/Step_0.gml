/// @description Insert description here
// You can write your code in this editor

if hp > 0 {
	var danger = false
	if hp <= maxhp/2 {
		danger = true
	}
	image_blend = c_white
	if redcounter > 0 {
		redcounter--
		image_blend = c_red
	}
	if state = "enter" {
		image_xscale += 0.4*sign(image_xscale)
		image_yscale -= 2
		if abs(image_xscale) = 4 {
			state = "dormant"	
		}
	}
	if state = "dormant" {
		counter++
		image_angle = 0 +3*sin(counter/25)
	}

	aftercount++
	if aftercount = 6 {
		with instance_create_depth(x,y,depth+1,obj_AfterEffect)	{
			targ = obj_FBAlex	
			if other.hp <= other.maxhp/2 {
				expand = true
				image_blend = c_yellow
			}
		}
		aftercount = 0
	}

	if state = "active" {
		counter++
	
		if phase = "fullfloat" {
			if x < 248 {
				image_angle = 0 +3*sin(counter/25)
				shootcounter++
				if shootcounter >= 35 || (shootcounter >= 25 && danger) {
					audio_play_sound(snd_3orbShot,4,false)
					with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
						direction = 155	
					}
					with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
						direction = 180	
					}
					with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
						direction = 205
					}
					shootcounter = 0
				}
				if subphase = 0 {
					y-=1.5
					if y <= 48 {
						subphase = 1	
					}
				}
				if subphase = 1 {
					y+=1.5
					if y >= 136 {
						subphase = 0	
					}
				}
				if counter > 200 || danger {
					if counter%100 = 0 {
					transform = -image_yscale
					}
					if transform != 0 {
						image_yscale += 0.2*sign(transform)
					}
					if image_yscale = transform {
						transform = 0	
					}
				}
				if counter > 600 || (counter > 450 && danger) {
					phase = "offscreen"
					subphase = 0
				}
			} else {
				x-=4
				sprite_index = spr_Alex_Weird
			}
		}
		if phase = "spin" {
			//if x < 248 {
			//	counter++
			//	if counter < randomgototime {
			//		var range = 25
			//		if transform = 0 {
			//			image_angle += 3
			//			if image_angle >= 90+range {
			//				transform = -1	
			//			}
			//		}
			//		if transform = -1 {
			//			image_angle -= 3
			//			if image_angle <= 90-range {
			//				transform = 0	
			//			}
			//		}
			//	} else {
			//		speed = -6	
			//		direction = image_angle+270
			//		if x < -30 {
			//			x = 340
			//			y = 80
			//			speed = 0
			//			image_angle = 90
			//			counter = 0
			//			transform = 0
			//			randomgototime = random_range(80,160)
			//		}
			//	}
			//} else {
			//	x-=4
			//	sprite_index = spr_Alex_Spin
			//}
			if x = 340 {
				sprite_index = spr_Alex_Spin
				var range = 15
				image_angle = random_range(90+range,90-range)
				direction = image_angle+270
				speed = -7
				if danger {
					speed = -5	
				}
				audio_play_sound(snd_Spin,4,false)
				if subphase = 5 {
					phase = "offscreen"	
					speed = 0
				}
			
			}
			if speed = -5  && danger {
				image_angle -= 20	
			}
			if x < -30 {
				x = 340
				y = 80
				speed = 0
				subphase++
			}
		
		
		}
		
		
		if phase = "teleport" {
			sprite_index = spr_Alex_Weird
			if (transform = 0) {
				transform = 1
				gotox = round(random_range(62,245))
				gotoy = round(random_range(28,156))
				
			} if transform = 1 {
				visible = false
				x += (gotox - x)/8;
				y += (gotoy - y)/8;
				if round(x) = gotox {
					transform = 2	
					counter = 0
				}
			}
			if transform = 2 {
				//visible = true
				shootcounter++
				if shootcounter >= 15 {
					audio_play_sound(snd_3orbShot,4,false)
					transform = 3
					counter = 0
					shootcounter = 0 
					for(i=0;i<8;i++) {
						with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
							direction = 0+45*other.i
							sprite_index = spr_Alex_Weird_StarBullets
						}
					}
					//with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
					//	direction = 155	
					//}
					//with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
					//	direction = 180	
					//}
					//with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
					//	direction = 205
					//}
						
					
				}
				
			}
			if transform = 3 {
				counter++
				if counter >= 15 {
					subphase++
					if subphase = 5 {
						phase = "offscreen"	
						subphase = 0
					} else {
						counter = 0
						transform = 0
						shootcounter = 0
					}
				}
			}
		}
	
		if phase = "head" {
			
			if (x < 248 || transform = 1) {
				transform = 1
				image_angle = 0 +3*sin(counter/25)
				x += sin(counter/25)
				shootcounter++
				if shootcounter >= 50 || (shootcounter >= 40 && danger) {
					instance_create_depth(x,y,depth++,obj_AlexSpinBall)
					audio_play_sound(snd_OrbShot,4,false)
					shootcounter = 0
				}
			
				if subphase = 0 {
					y-=1.5
					if y <= 48 {
						subphase = 1	
					}
				}
				if subphase = 1 {
					y+=1.5
					if y >= 136 {
						subphase = 0	
					}
				}

				if counter > 450 {
					phase = "offscreen"
					subphase = 0
					
				}
			} else {
				x-=4
				sprite_index = spr_AlexHead

			}
		}
	
		if phase = "offscreen" {
			if subphase = 0 {
				x+=4
			}
			if x >= 340 {
				image_angle = 0
				visible = true
				x = 340
				image_xscale = -4
				image_yscale = 4
				y = 80
				var rand = 0
				if !danger {
				do {rand = choose(0,1,2)} until (rand != justrand)
				} else {
				do {rand = choose(0,1,2,3)} until (rand != justrand)
				}
				justrand = rand
				if rand = 0 {
					phase = "fullfloat"
					subphase = 0
					counter = 0
					transform = 0
				}
				if rand = 1 {
					phase = "spin"
					image_angle = 90
					subphase = 0
					counter = 0
					transform = 0
					randomgototime = random_range(100,160)
				}
				if rand = 2 {
					phase = "head"
					with instance_create_depth(x,y,depth-1,obJ_HandLaser) {
						orient = 1
					}
					with instance_create_depth(x,y,depth-1,obJ_HandLaser) {
						orient = -1
					}
					subphase = 0
					counter = 0
					transform = 0
					randomgototime = random_range(100,160)
				}
				if rand = 3 {
					phase = "teleport"
					subphase = 0
					counter = 0
					shootcounter = 0
					transform = 0
					randomgototime = random_range(100,160)
				}
			}
		}
	
	}
}
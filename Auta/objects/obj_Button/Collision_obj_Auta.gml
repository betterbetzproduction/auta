/// @description Insert description here
// You can write your code in this editor
if image_index = 0 {
	image_index = 1
	
	with killInst {
		var ex = x+sprite_width/2*image_xscale
		var ey = y+sprite_height/2*image_yscale
		with instance_create_depth(ex,ey,depth-1,obj_OffScreenFX) {
			baseX = ex
			baseY = ey
			sprite_index = spr_Click
		}		
	}
	
	killInst.x = -100
	killInst.button = self
	
	if other.state = "propeller" {
		with other {
			sprite_index = spr_AutaJump
			state = "normal"
			animstate = "normal"
		
			with instance_create_depth(x,y-5,depth-1,obj_OneTimeFX) {
				sprite_index = spr_LargeBurst	
				image_xscale = 0.5
				image_yscale = 0.5
				audio_play_sound_at(snd_EnemyDeath,x,y,0,50,300,1,false,4);
			}
			canOrb = true	
		}
	}
	
	audio_play_sound(snd_Sel,4,false)
}


/// @description Insert description here
if hp > 0 {
	var danger = false
	if hp <= maxhp/2 {
		danger = true
	}
	image_blend = c_white
	if redcounter > 0 {
		redcounter--
		image_blend = c_red
	}
	if state = "enter" {
		image_xscale += 0.4*sign(image_xscale)
		image_yscale -= 2
		if abs(image_xscale) = 4 {
			state = "dormant"	
		}
	}
	if state = "dormant" {
		counter++
		image_angle = 0 +3*sin(counter/25)
	}

	aftercount++
	if aftercount = 3 {
		with instance_create_depth(x,y,depth+1,obj_AfterEffect)	{
			targ = obj_FBAlex_Hardmode
			if other.hp <= other.maxhp/2 {
				expand = true
				image_blend = c_yellow
			}
		}
		aftercount = 0
	}

	if state = "active" {
		counter++
	
		if phase = "fullfloat" {
			if x < 248 {
				image_angle = 0 +3*sin(counter/25)
				shootcounter++
				if shootcounter >= 25 || (shootcounter >= 15 && danger) {
					audio_play_sound(snd_3orbShot,4,false)
					with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
						direction = 155	
					}
					with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
						direction = 180	
					}
					with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
						direction = 205
					}
					shootcounter = 0
				}
				if subphase = 0 {
					y-=2
					if y <= 48 {
						subphase = 1	
					}
				}
				if subphase = 1 {
					y+=2
					if y >= 136 {
						subphase = 0	
					}
				}
				if counter > 200 || danger {
					if counter%100 = 0 {
					transform = -image_yscale
					}
					if transform != 0 {
						image_yscale += 0.2*sign(transform)
					}
					if image_yscale = transform {
						transform = 0	
					}
				}
				if counter > 600 || (counter > 450 && danger) {
					phase = "offscreen"
					subphase = 0
				}
			} else {
				x-=4
				sprite_index = spr_Alex_Weird_H
				image_xscale = -3
				image_yscale = 3
			}
		}
		if phase = "spin" {
			if x = 340 {
				transform = 0
				sprite_index = spr_Alex_Spin
				var range = 15
				image_angle = choose(75,65,115,125)
				direction = image_angle+270
				transform = 1
				y = 80
				
				
				
			
				audio_play_sound(snd_Spin,4,false)
				if subphase = 5 {
					phase = "offscreen"	
					speed = 0
				}
			
			}
			if x < 250 {
				transform = 2	
			}
			if transform = 1 || transform = 2 {
				counter++
				if transform = 2 {
					if(place_meeting(x-6*cos((direction/180)*pi), y, obj_Collision)) {
					    direction = -direction + 180;
						
					}
					if(place_meeting(x, y + 6*sin((direction/180)*pi), obj_Collision)) {
					    direction = -direction;
				
					}
				}
				shootcounter++
				if shootcounter >= 30 {
					audio_play_sound(snd_3orbShot,4,false)
					shootcounter = 0 
					for(i=0;i<3;i++) {
						with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
							direction = 0+90*other.i
							sprite_index = spr_Alex_Weird_StarBullets
						}
					}
				}
				
				

				//Vertical bounce
				
				x+=-6*cos((direction/180)*pi)
				y+=6*sin((direction/180)*pi)
				
				if counter >= 500 {
					phase = "offscreen"	
					transform = 3
				}
			}
			if danger {
				image_angle -= 20	
				image_xscale = 3
				image_yscale = 3
			}
			if x < -30 {
				x = 340
				y = 80
				speed = 0
				subphase++
			}
		
		
		}
		
		
		if phase = "teleport" {
			sprite_index = spr_Alex_Weird_H
			image_xscale = -3
			image_yscale = 3
			if (transform = 0) {
				transform = 1
				gotox = round(random_range(62,245))
				gotoy = round(random_range(28,156))
				
			} if transform = 1 {
				visible = false
				x += (gotox - x)/8;
				y += (gotoy - y)/8;
				if abs(x-gotox)< 6 {
					transform = 2	
					counter = 0
				}
			}
			if transform = 2 {
				//visible = true
				shootcounter++
				if shootcounter >= 15 {
					audio_play_sound(snd_3orbShot,4,false)
					transform = 3
					counter = 0
					shootcounter = 0 
					for(i=0;i<8;i++) {
						with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
							direction = 0+45*other.i
							sprite_index = spr_Alex_Weird_StarBullets
						}
					}
					//with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
					//	direction = 155	
					//}
					//with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
					//	direction = 180	
					//}
					//with instance_create_depth(x,y,depth++,obj_Alex3Fireball) {
					//	direction = 205
					//}
						
					
				}
				
			}
			if transform = 3 {
				counter++
				if counter >= 15 {
					subphase++
					if subphase = 5 {
						phase = "offscreen"	
						subphase = 0
					} else {
						counter = 0
						transform = 0
						shootcounter = 0
					}
				}
			}
		}
	
		if phase = "head" {
			
			if (x < 248 || transform = 1) {
				transform = 1
				image_angle = 0 +3*sin(counter/25)
				x += sin(counter/25)
				shootcounter++
				if shootcounter >= 50 || (shootcounter >= 40 && danger) {
					instance_create_depth(x,y,depth++,obj_AlexSpinBall)
					audio_play_sound(snd_OrbShot,4,false)
					shootcounter = 0
				}
			
				if subphase = 0 {
					y-=1.5
					if y <= 48 {
						subphase = 1	
					}
				}
				if subphase = 1 {
					y+=1.5
					if y >= 136 {
						subphase = 0	
					}
				}

				if counter > 450 {
					phase = "offscreen"
					subphase = 0
					
				}
			} else {
				x-=4
				sprite_index = spr_AlexHead

			}
		}
	
		if phase = "offscreen" {
			if subphase = 0 {
				x+=4
			}
			if x >= 340 {
				image_angle = 0
				visible = true
				x = 340
				image_xscale = -4
				image_yscale = 4
				y = 80
				var rand = 0
				if !danger {
				do {rand = choose(0,1,2)} until (rand != justrand)
				} else {
				do {rand = choose(0,1,2,3)} until (rand != justrand)
				}
				justrand = rand
				if rand = 0 {
					phase = "fullfloat"
					subphase = 0
					counter = 0
					transform = 0
				}
				if rand = 1 {
					phase = "spin"
					image_angle = 90
					subphase = 0
					counter = 0
					transform = 0
					shootcounter = 0
					randomgototime = random_range(100,160)
				}
				if rand = 2 {
					phase = "head"
					with instance_create_depth(x,y,depth-1,obJ_HandLaser) {
						orient = 1
					}
					with instance_create_depth(x,y,depth-1,obJ_HandLaser) {
						orient = -1
					}
					subphase = 0
					counter = 0
					transform = 0
					randomgototime = random_range(100,160)
				}
				if rand = 3 {
					phase = "teleport"
					subphase = 0
					counter = 0
					shootcounter = 0
					transform = 0
					randomgototime = random_range(100,160)
				}
			}
		}
	
	}
}
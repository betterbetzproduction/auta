/// @description Insert description here
// You can write your code in this editor
draw_self()

if hp <= 0 {
	draw_set_alpha(1)
	visible = true
	speed = 0
	draw_set_color(c_white)
	draw_ellipse(x+xmin,y+ymin,x+xmax,y+ymax,false)	
	xmin-=4
	xmax+=4
	ymin-=4
	ymax+=4
	if xmin = -4 {
		audio_play_sound(snd_AlexDeath,4,false)	
		obj_Music.musictrack = snd_NoSound
	}
	
	if xmin <= -500 {
		with instance_create_depth(x,y,depth-100,obj_FadeTrans) {
			toRoom = rm_KellyArt
			toX = 0
			toY = 0
		}	
	}
}
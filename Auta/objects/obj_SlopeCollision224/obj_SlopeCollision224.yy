{
  "spriteId": {
    "name": "spr_SlopeCollision865",
    "path": "sprites/spr_SlopeCollision865/spr_SlopeCollision865.yy",
  },
  "solid": false,
  "visible": false,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "obj_Collision",
    "path": "objects/obj_Collision/obj_Collision.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"obj_SlopeCollision224","path":"objects/obj_SlopeCollision224/obj_SlopeCollision224.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Collision",
    "path": "folders/Objects/Environment/Collision.yy",
  },
  "resourceVersion": "1.0",
  "name": "obj_SlopeCollision224",
  "tags": [],
  "resourceType": "GMObject",
}
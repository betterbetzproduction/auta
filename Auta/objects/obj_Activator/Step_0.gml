/// @description Insert description here
// You can write your code in this editor
if active {
	if instance_exists(obj_ActivationPlatform) {
		with obj_ActivationPlatform {
			active = true	
		}
		with obj_ActivationPlatform_Spikes {
			active = true	
		}
	}
	if instance_exists(obj_AssetsRedrawer) {
		with obj_AssetsRedrawer {
			active = true	
		}
	}
}
active = false
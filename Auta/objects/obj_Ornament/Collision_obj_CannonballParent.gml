/// @description Insert description here
// You can write your code in this editor
if other.state = "cannonball" {
	rand = choose(0,1,2,3,4,5,6,7,8)

	var __time = current_time;  
	while current_time-__time < 20 {  
	} 

	if rand = 6 {
		instance_create_layer(x,y,layer,obj_HealthPickup)
	} else {
		instance_create_layer(x,y,layer,obj_PowerGem)
	}

	instance_destroy()
	global.screenshake = 1

	scr_PlaySFX(snd_BreakBG)

	for(i=0;i<4;i++) {
		with instance_create_layer(x,y,layer,obj_BambooParticle) {
			image_blend = other.color	
		}
	}
}
/// @description Insert description here
// You can write your code in this editor
x+=xSpeed*global.gameSpeed
y+=ySpeed*global.gameSpeed

if !place_meeting(x,y+1,obj_GroundParent) {
	var i = 0
	do {
		y+=0.2
		i+=0.2
	} until(place_meeting(x,y+1,obj_GroundParent) || i = 16)
}
if !place_meeting(x,y+16,obj_GroundParent) {
	instance_destroy();	
}
if place_meeting(x+xSpeed,y-1,obj_Collision) {
	instance_destroy()	
}
if counter >= 120 {
	instance_destroy();	
}
counter+=global.gameSpeed

if contacted {
	instance_destroy();
}

if collision_line(x,y,x,y-128,obj_EnemyParent,false,true) {
	instance_create_layer(x,y,layer,obj_Fizzle3)
	
	with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
		sprite_index = spr_FizzleGroundFX
		var i = 0
		do {
			y+=0.2
			i+=0.2
		} until(place_meeting(x,y+1,obj_GroundParent) || i = 16)
	}
	instance_destroy();
}
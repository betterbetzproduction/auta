/// @description Insert description here
// You can write your code in this editor
if sprite_index = spr_ChuckerShoot {
	var addAngle = 0
	if image_xscale = -1 {
		addAngle = 180	
	}
	with instance_create_depth(x,y,depth+1,obj_DirectionBullet) {
		direction = 0+addAngle
	}
	//with instance_create_depth(x,y,depth+1,obj_DirectionBullet) {
	//	direction = 25+addAngle	
	//}
	//with instance_create_depth(x,y,depth+1,obj_DirectionBullet) {
	//	direction = -25+addAngle
	//}
	sprite_index = spr_ChuckerCloseMouth
}

if sprite_index = spr_ChuckerCloseMouth {
	state = "normal"
	sprite_index = spr_ChuckerIdle
}	
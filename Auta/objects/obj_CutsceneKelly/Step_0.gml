/// @description Insert description here
// You can write your code in this editor
if state = "dead" {
	ySpeed += grav
	x += xSpeed
	y+=ySpeed
	image_angle += 5
	
}
if y >= room_height+80 {
	with instance_create_depth(x,y,depth-100,obj_FadeTrans) {
		toRoom = room
		toX = global.respawnx
		toY = global.respawny
		global.currentweapon = "sword"
	}
}

if state = "dead" {
	exit;	
}


y += ySpeed
if ySpeed > 3 {
	ySpeed = 3	
}
if state = "disappear" {
	image_xscale -= 0.1*sign(image_xscale)
	image_yscale += 0.5
	if image_xscale = 0 {
		visible = false
	}
}
if state = "normal" {
	mask_index = spr_KellyIdle
	if grounded {
		var accel = 0.2	
		var stop = 0.4
		if jump {
			ySpeed = jumpforce
			audio_play_sound(snd_Jump,4,false)
		}
	} else {
		var accel = 0.1	
		var stop = 0.1
		if ySpeed < -0.5 && !jump {
			ySpeed += 0.1
		}
	}
	if moveright {
		if xSpeed < maxSpeed {
			if !place_meeting(x+maxSpeed,y,obj_Collision) {
				xSpeed += accel	
			}
		}
		if xSpeed > maxSpeed {
			xSpeed = maxSpeed	
		}
	}
	if moveleft {
		if xSpeed > -maxSpeed {
			if !place_meeting(x-maxSpeed,y,obj_Collision) {
				xSpeed -= accel	
			}
		}
		if xSpeed < -maxSpeed {
			xSpeed = -maxSpeed	
		}
	}
	if !moveleft && !moveright {
		if xSpeed > 0 {
			if xSpeed > stop {
				xSpeed -= stop	
			} else {
				xSpeed = 0	
			}
		}
		if xSpeed < 0 {
			if xSpeed < -stop {
				xSpeed += stop	
			} else {
				xSpeed = 0	
			}
		}
	}
	//if animstate = "normal" {
	//	if keyboard_check_pressed(vk_space) {
	//		if global.currentweapon = "stick" {
	//			sprite_index = spr_KellyStick
	//			attack = 1
	//		}
	//		if global.currentweapon = "sword" {
	//			sprite_index = spr_KellySword
	//			attack = 2
	//		}
	//		if global.currentweapon = "firesword" {
	//			sprite_index = spr_KellyFireSword
	//			attack = 3
	//		}
	//		if global.currentweapon = "lasersword" {
	//			sprite_index = spr_KellyLaserSword
	//			attack = 1
	//			with instance_create_depth(x,y,depth+1,obj_PlayerParentProjectile) {
	//				grav = 0
	//				attack = 1
	//				knockback = 0.5
	//				xSpeed = 3*other.image_xscale
	//				ySpeed = 0
	//			}
	//		}
	//		with instance_create_layer(x,y,layer,obj_PlayerParentHitbox) {
	//			attack = other.attack
	//			knockback = other.knockback
	//			killsprite = other.sprite_index
	//			sprite_index = spr_KellySword_Hitbox
	//		}
	//		animstate = "onedone"
	//	}
	//	if grounded {
	//		if keyboard_check(vk_down) && keyboard_check_pressed(ord("Z")) {
	//			//animstate = "constant"
	//			sprite_index = spr_KellySlide
	//			state = "slide"

	//			xSpeed = 1.5*image_xscale
	//			slidecounter = 0
	//		}
	//	}
		
	//}
}

if place_meeting(x+xSpeed,y,obj_Collision) {
	var gdinst = instance_place(x+xSpeed,y,obj_Collision)
	if gdinst != "noone" {
		if gdinst.topOnly = false {
			if xSpeed > 0 {
				x -= 5
				var q = 0
				do {
					x+=0.2
					q+=0.2
				} until (place_meeting(x+1,y,obj_Collision) || q = 7)
			}
			if xSpeed < 0 {
				x += 5
				var q = 0
				do {
					x-=0.2
					q+=0.2
				} until (place_meeting(x-1,y,obj_Collision) || q = 7)
			}
			xSpeed = 0
		} else {
			x+=xSpeed
		}
	} else {
		x+=xSpeed
	}
} else {
	x+=xSpeed
}

if place_meeting(x,y+ySpeed,obj_Collision) && ySpeed < 0 {
  var gdinst = instance_place(x,y+ySpeed,obj_Collision)
	if gdinst != "noone" {
		if gdinst.topOnly = false {
			ySpeed = 0.1
		} else {

		}
	} else {

	}
} else {

}

if state = "hurt" {
	iframes = 60
	if grounded {
		state = "normal"
		animstate = "normal"
	}
}
if iframes > 0 {
	iframes--
	image_alpha = 0.5
} else {
	image_alpha = 1	
}
if grounded = true {
	var q = 0
	do {
		y-=0.1	
		q+=0.1
	} until(place_meeting(x,y-0.5,obj_Collision) || q = 3.5)
	
	var q = 0
	do {
		y+=0.1	
		q+=0.1
	} until(place_meeting(x,y+0.5,obj_Collision) || q = 7)
	if q = 7 {
		y-=3.5	
	} else {
		var gdinst = instance_place(x,y+0.5,obj_Collision)
		if gdinst != "noone" {
			x += gdinst.xSpeed
			y += gdinst.ySpeed
		}	
		
	}
	
}
if collision_rectangle(x-2,y+ySpeed+6,x+2,y+ySpeed+8,obj_Collision,true,false) && ySpeed = 0 {
	grounded = true	
	
} else {
	grounded = false
	ySpeed += grav
}
if ySpeed > 0 && collision_rectangle(x-2,y+ySpeed+6,x+2,y+ySpeed+8,obj_Collision,true,false) {
	var q = 0
	do {
		y+=0.1	
		q+=0.1
	} until(place_meeting(x,y+0.5,obj_Collision) || q = 10)
	ySpeed = 0
	grounded = true
	if q = 10 {
		y -= 10	
	}
	
}
if state = "slide" {
	mask_index = spr_KellyIdle_ShortMask
	if keyboard_check_pressed(ord("Z")) && animstate = "constant"{
		ySpeed = jumpforce
		audio_play_sound(snd_Jump,4,false)
	}
	animstate = "constant"
	if grounded = false {
		state = "normal"
		animstate = "normal"
	}	
	if xSpeed = 0 {
		mask_index = spr_KellyIdle
		if place_meeting(x,y,obj_Collision) {
			mask_index = spr_KellyIdle_ShortMask 	
		} else {
			state = "normal"
			animstate = "normal"
		}
	}
	slidecounter++
	if slidecounter >= 20 {
		mask_index = spr_KellyIdle
		if place_meeting(x,y,obj_Collision) {
			mask_index = spr_KellyIdle_ShortMask 	
		} else {
			state = "normal"
			animstate = "normal"
		}
	}
}

if animstate = "normal" {
	if grounded {
		if xSpeed = 0 {
			sprite_index = spr_KellyIdle	
		} else {
			sprite_index = spr_KellyRun	
			if xSpeed > 0 {
				image_xscale = 1	
			}
			if xSpeed < 0 {
				image_xscale = -1	
			}
		}
	} else {
		sprite_index = spr_KellyJump	
	}
}
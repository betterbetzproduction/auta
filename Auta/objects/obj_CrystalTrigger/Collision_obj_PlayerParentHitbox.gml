/// @description Insert description here
// You can write your code in this editor
if canHit {
	canHit = false
	global.screenshake = 1
	
	global.aberTime = 30
	
	var instance = id
	
	with obj_CrystalPlatform {
		////show_debug_message(other.range)
		////show_debug_message(distance_to_object(other))
		//crystalTime = 180
		//show_debug_message("--------------")
		//show_debug_message(instance.x)
		//show_debug_message(instance.y)
		//show_debug_message(id)
		//show_debug_message(point_distance(x,y,instance.x,instance.y))
		if point_distance(x,baseY,instance.x,instance.y) < 160 {
			//show_debug_message("ASDASDASD")
			crystalTime = 180
			
			image_alpha = 1
			visible = true
			
		}
	}
	circleSize = 0
	circleAlpha = 0.4
	other.contacted = true
	audio_play_sound(snd_CrystalDing,4,false)
	
	if other.type = "slice" {
			
		with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
			image_xscale = other.image_xscale
			sprite_index = spr_Slice
			audio_play_sound_at(snd_MagicSlice2,x,y,0,50,300,1,false,4);
		}
		other.stop = true
	}
	if other.type = "sliceNoPull" {
			
		with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
			image_xscale = other.image_xscale
			sprite_index = spr_Slice
			audio_play_sound_at(snd_MagicSlice2,x,y,0,50,300,1,false,4);
		}
		other.stop = true
			
	}
	if other.type = "lightSpeed" {
		with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
			image_xscale = other.image_xscale
			sprite_index = spr_Slice
			audio_play_sound_at(snd_MagicSlice2,x,y,0,50,300,1,false,4);
		}

	}
	if other.type = "cannonball" {
		with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
			image_xscale = other.image_xscale
			sprite_index = spr_BashEffect
			audio_play_sound_at(snd_Slam,x,y,0,50,300,1,false,4);
		}
	}
}
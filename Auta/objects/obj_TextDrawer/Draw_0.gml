/// @description Insert description here
// You can write your code in this editor
delay--
if delay = 80 {
	audio_play_sound(snd_CH_Hit,4,false)
}
if delay = -20 {
	audio_play_sound(snd_CH_Suspense,4,false)
}
if delay < 80 && delay > -300 {
	draw_set_alpha(1)
	var tempx = basex+random_range(-shake,shake)
	var tempy = basey+random_range(-shake,shake)
	draw_set_font(fnt_Terminal)
	draw_set_color(c_white)
	draw_set_halign(fa_middle)
	draw_set_valign(fa_center)
	draw_text_transformed(tempx,tempy,text,image_xscale*0.75,image_yscale*0.75,image_angle)
	
	draw_set_alpha(alpha)
	draw_text_transformed(tempx,tempy+50,sub,image_xscale/2,image_yscale/2,image_angle)
	
}

if delay <  -20 {
	alpha+=0.02	
}

if delay < -400 {
	switch text {
		case ("CHAPTER 1-2"):
			room_goto(rm_AutaFalls)
			global.storyBeat = 5
			break;
	}
}
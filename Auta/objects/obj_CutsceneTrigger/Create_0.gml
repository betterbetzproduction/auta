/// @description Insert description here
// You can write your code in this editor
tl = 0;
timeline_running = false
specActor[0] = noone
emoteInst = noone

suspendState = ""
suspendAnimState = ""

reset_dialogue_defaults();

myPortrait			= spr_portrait_examplechar;
myVoice				= snd_voice1;
myFont				= fnt_dialogue;
myName				= "Blue";

myPortraitTalk		= spr_portrait_examplechar_mouth;
myPortraitTalk_x	= 26;
myPortraitTalk_y	= 44;
myPortraitIdle		= spr_portrait_examplechar_idle;
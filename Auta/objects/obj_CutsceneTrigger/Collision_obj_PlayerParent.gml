 /// @description Insert description here
// You can write your code in this editor
if timeline_running = false {
	global.cutscene = true
	suspendState = obj_PlayerParent.state
	suspendAnimState = obj_PlayerParent.state
	obj_PlayerParent.state = "cutscene"
	obj_PlayerParent.xSpeed = 0
	obj_PlayerParent.animstate = "normal"

	timeline_index = tl
	timeline_running = true
	timeline_loop = false
	timeline_position = 0
	timeline_speed = 1
}
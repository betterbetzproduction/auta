/// @description Insert description here
// You can write your code in this editor
display_set_gui_size(800,450)
//draw_sprite_ext(spr_Hello,1,400,100,2.5,2.5,0,c_white,1);

//draw_set_halign(fa_center)
//draw_set_color(c_red)
//draw_set_font(fnt_Marker)
//draw_text_transformed(400,115,name,1.5,1.5,15)

//draw_set_halign(fa_left)

draw_set_color(c_white)
var x1 = 400
var x2 = 750
var y1 = 50
var y2 = 400
draw_rectangle(x1,y1,x2,y2,false)

draw_set_color(c_black)
x1+=5
x2-=5
y1+=5
y2-=5
draw_rectangle(x1,y1,x2,y2,false)
draw_set_halign(fa_left)
draw_set_valign(fa_top)
draw_set_color(c_white)
draw_set_font(fnt_Terminal)
draw_text(x1+10,y1+10,string_copy(type[index],0,tPos))

if tPos < string_length(type[index]) { tPos++}
else {
	if keyboard_check_pressed(global.kInteract) || gamepad_button_check_pressed(global.pad, global.pInteract) {
		keyboard_clear(global.kInteract)	
		if index = array_length_1d(type)-1 {
			instance_destroy();	
		} else {
			index++
			tPos = 0
		}
	}
}

if keyboard_check_pressed(global.kInteract) || gamepad_button_check_pressed(global.pad, global.pInteract) {
	if tPos < string_length(type[index]) {
		tPos = string_length(type[index])
	}
}
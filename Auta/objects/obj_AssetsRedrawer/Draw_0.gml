/// @description Insert description here
// You can write your code in this editor

if active {
	gpu_set_blendmode(bm_add)
	var a = layer_get_all_elements(myLayer);
	layer_set_visible(myLayer,false)
	for (var i = 0; i < array_length_1d(a); i++;)
	   {

	   draw_sprite_ext(layer_sprite_get_sprite(a[i]),layer_sprite_get_index(a[i]),layer_sprite_get_x(a[i]),layer_sprite_get_y(a[i]),layer_sprite_get_xscale(a[i]),layer_sprite_get_yscale(a[i]),layer_sprite_get_angle(a[i]),layer_sprite_get_blend(a[i]),layer_sprite_get_alpha(a[i]))
	   }
   
	gpu_set_blendmode(bm_normal)
} else {
	layer_set_visible(myLayer,false)
}

if instance_exists(obj_Activator) {
	active = obj_Activator.active	
}
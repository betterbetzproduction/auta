instance_destroy(obj_TessActor1)
var t = 0
text[t] = "(And just like that, they're gone...)";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "(What's a little kid doing out here?)";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);

timeline_speed = 1
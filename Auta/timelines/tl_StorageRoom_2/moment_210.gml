inst_4CF109D9.sprite_index = spr_ChiefMask_Talk


speaker[0] = scr_CustomDialogueInit( snd_voice1, -1)
speaker[1] = scr_AutaDialogueInit()
myScript = 0
myNextline = 0
myTypes = 0

var t = 0
text[t] = "...What he said.";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myTypes[t] = 0
myNextLine[t] = t+1


t++
text[t] = "Perhaps it'd be better just to show you. Follow me.";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myTypes[t] = 0
myNextLine[t] = t+1

create_dialogue(text, mySpeaker,myEffects,0,myTypes,myNextLine,myScript,0,myEmotion);
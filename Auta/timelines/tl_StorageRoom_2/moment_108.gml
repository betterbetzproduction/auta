inst_4EB75559.sprite_index = spr_TallMask_Walk

speaker[0] = scr_CustomDialogueInit( snd_voice1, -1)
speaker[1] = scr_AutaDialogueInit()
myScript = [0,0,0,0,0,0,0,0,0,0]
myNextline = 0
myTypes = 0

var t = 0
text[t] = "-!";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myTypes[t] = 0
myNextLine[t] = t+1


t++
text[t] = "Ah, we were just about to come get you! I didn't think you would wake up so soon.";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myTypes[t] = 0
myNextLine[t] = t+1
myScript[t] = [scr_OrientCharacter,inst_4EB75559,-1,spr_TallMask,true]
t++
text[t] = ["Who are you?", "Did you take me here?"]
mySpeaker[t] = speaker[1]
myEmotion[t] = 1
myEffects[t] = 0
myScript[t] = [[scr_OrientCharacter,inst_4CF109D9,-1,spr_ChiefMask_Talk,true],[scr_OrientCharacter,inst_4CF109D9,-1,spr_ChiefMask_Talk,true]]
myTypes[t] = 1
myNextLine[t] = [3,3]

t++
text[t] = "Bah! They always suspect us.";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myTypes[t] = 0
myNextLine[t] = t+1

t++
text[t] = "So missy, I suppose you're another refugee, huh?";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myScript[t] = [scr_OrientCharacter,inst_4CF109D9,-1,spr_ChiefMask_Drag,true]
myTypes[t] = 0
myNextLine[t] = t+1

t++
text[t] = "Escaping from that 'Chaos Syndrome' or whatever madness is going on out there?";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myTypes[t] = 0
myNextLine[t] = t+1

t++
text[t] = "Well I..."
mySpeaker[t] = speaker[1]
myEmotion[t] = 1
myEffects[t] = 0
myScript[t] = 0
myTypes[t] = 0
myNextLine[t] = t+1

t++

text[t] = "I'm sorry to tell you, miss, but things ain't much better on this side of the galaxy.";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myScript[t] = [scr_OrientCharacter,inst_4EB75559,1, spr_TallMask_Walk,false]
myTypes[t] = 0
myNextLine[t] = t+1

t++
text[t] = "Oh, captain! You have to tell her the truth.";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myTypes[t] = 0
myNextLine[t] = t+1
myScript[t] = [scr_OrientCharacter,inst_4CF109D9,-1,spr_ChiefMask_Drag,true]

create_dialogue(text, mySpeaker,myEffects,0,myTypes,myNextLine,myScript,0,myEmotion);
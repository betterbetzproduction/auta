inst_4CF109D9.sprite_index = spr_ChiefMask_Shock
inst_4CF109D9.xSpeed = 1
obj_Auta.animstate = "cutscene"
obj_Auta.sprite_index = spr_AutaHurt
obj_Auta.xSpeed = -1
audio_play_sound(snd_Oop,4,false)
speaker[0] = scr_CustomDialogueInit( snd_voice1, -1)
text = 0
mySpeaker = 0
myEmotion = 0
myEffects = 0

var t = 0
text[t] = "YOU CAN NEVER LEAVE!";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = [1,1]

inst_4EB75559.behaviour = "bounce"
create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);

timeline_speed = 1


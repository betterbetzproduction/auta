inst_4EB75559.sprite_index = spr_TallMask
inst_4EB75559.image_xscale = -1

inst_4CF109D9.sprite_index = spr_ChiefMask_Talk

text = 0
mySpeaker = 0
myEmotion = 0
myEffects = 0


var t = 0

text[t] = "Aye.";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "Well.";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "The thing is...";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "Now that you're here...";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0


create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
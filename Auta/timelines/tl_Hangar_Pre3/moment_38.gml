with obj_Auta {
	aAttack		= keyboard_check_pressed(kAttack) || gamepad_button_check_pressed(pad,pAttack)
}
if obj_Auta.aAttack {
	with obj_Auta {
		state = "normal"
		animstate = "normal"
		atkCounter = 5
		keyboard_clear(vk_up)
		keyboard_clear(vk_down)
		keyboard_clear(vk_left)
		keyboard_clear(vk_right)
		scr_AutaAttack()
	}
	global.gameSpeed = 1
}	


if global.gameSpeed = 0 {timeline_position--; exit;}
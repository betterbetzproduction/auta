inst_632B546E.sprite_index = spr_ChiefMaskCough

var t = 0
text[t] = "Aw-! Gimme a break!";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = [1,1]

speaker[1] = scr_AutaDialogueInit()

t++
text[t] = "Are you alright?";
mySpeaker[t] = speaker[1]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "It's that scamp again!";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = [1,1]

t++
text[t] = "Argh... she's always causing trouble.";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = [1,1]

create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
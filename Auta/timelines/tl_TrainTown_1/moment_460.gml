text = 0

speaker[1] = scr_AutaDialogueInit()
obj_Music.musictrack = mus_SecondTransmission
var t = 0
text[t] = "I'll find her for you.";
mySpeaker[t] = speaker[1]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "You should go sit down, maybe.";
mySpeaker[t] = speaker[1]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "Aye.";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "This'll be a good time to meet the people around here, too. Go ahead and introduce yourself.";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
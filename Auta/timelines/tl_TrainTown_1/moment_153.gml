speaker[0] = scr_CustomDialogueInit( snd_voice1, -1)
speaker[1] = scr_AutaDialogueInit()

var t = 0
text[t] = "One day, trains stopped coming into the station from the city.";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "Not long after that, we found out none of our trains were reaching the city either."
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "Whatever did THIS was snatching them up. Never saw those crews again."
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "Meanwhile, people just kept piling in here, and I guess that's what draws 'em in because soon they were snatching people up right outside this station."
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "No way...";
mySpeaker[t] = speaker[1]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "Eyup. Ever since then we've boarded up the windows and blockaded all entrances and exits."
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "And we've started wearing masks too, cuz uh..."
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "Well..."
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "This is more or less heresay, but a couple of guys have gone on record saying they've seen the snatchers wearing people's faces."
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "Snatched people's faces."
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
speaker[0] = scr_CustomDialogueInit( snd_voice1, -1)
text = 0
inst_632B546E.image_xscale = -1
var t = 0
text[t] = "I'm sorry miss. Ain't no way we're opening up the blockades to let anyone out.";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

t++
text[t] = "Until we figure something out, well... you're stuck here like the rest of us."
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0

speaker[1] = scr_AutaDialogueInit()

t++
text[t] = "...";
mySpeaker[t] = speaker[1]
myEmotion[t] = 1
myEffects[t] = 0

create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
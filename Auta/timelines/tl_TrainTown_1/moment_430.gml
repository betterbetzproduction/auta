inst_632B546E.sprite_index = spr_ChiefMask_Talk
inst_632B546E.image_speed = 1.5
inst_632B546E.image_xscale = -1
var t = 0
text[t] = "That little rascal stole my keychain!";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = [1,1]

t++

text[t] = "Urgh. I know you just got here, but mind tracking her down for me?";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0


speaker[1] = scr_AutaDialogueInit()

t++
text[t] = "Oh, um...";
mySpeaker[t] = speaker[1]
myEmotion[t] = 1
myEffects[t] = 0


create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
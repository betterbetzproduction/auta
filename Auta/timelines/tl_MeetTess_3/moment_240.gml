obj_TessActor3.image_xscale = -1
obj_Auta.animstate = "cutscene"
text = 0

var t = 0
text[t] = "Don't worry, though. I'm not saying anything.";
mySpeaker[t] = speaker[1]
myEmotion[t] = 0
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "There's nothing in it for me anyway!";
mySpeaker[t] = speaker[1]
myEmotion[t] = 4
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "...";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "So you're a good robot, huh...?";
mySpeaker[t] = speaker[1]
myEmotion[t] = 1
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "That's right."
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "Huh.";
mySpeaker[t] = speaker[1]
myEmotion[t] = 1
myEffects[t] = 0
myScripts[t] = [scr_OrientCharacter,obj_TessActor3,-1,spr_Tess_Idle,false]

t++
text[t] = "Prove it.";
mySpeaker[t] = speaker[1]
myEmotion[t] = 4
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "...?"
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "Good robots help people, right? Well, you could help me.";
mySpeaker[t] = speaker[1]
myEmotion[t] = 4
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "You're right, I do know a way out of here. I've been stocking up supplies before I get out for good."
mySpeaker[t] = speaker[1]
myEmotion[t] = 0
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "Buuut, there's somewhere I need to go first."
mySpeaker[t] = speaker[1]
myEmotion[t] = 0
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "And you're gonna be my bodyguard!"
mySpeaker[t] = speaker[1]
myEmotion[t] = 4
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "Okay...?"
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myScripts[t] = 0


create_dialogue(text, mySpeaker,myEffects,0,0,0,myScripts,0,myEmotion);
obj_TessActor3.image_xscale = -1
obj_Auta.animstate = "cutscene"
text = 0

var t = 0
text[t] = "Personally, I think I'm doing you a pretty big favor already.";
mySpeaker[t] = speaker[1]
myEmotion[t] = 0
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "What?";
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "You're not really what you seem, are you?";
mySpeaker[t] = speaker[1]
myEmotion[t] = 1
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "You're actually a--!";
mySpeaker[t] = speaker[1]
myEmotion[t] = 0
myEffects[t] = 0
myScripts[t] = [scr_OrientCharacter,obj_Auta,1,spr_AutaIdle,false]

t++
text[t] = "SHHH!";
mySpeaker[t] = speaker[0]
myEmotion[t] = 3
myEffects[t] = 0
myScripts[t] = 0

t++
text[t] = "I knew it!";
mySpeaker[t] = speaker[1]
myEmotion[t] = 2
myEffects[t] = 0
myScripts[t] = [scr_OrientCharacter,obj_TessActor3,1,spr_Tess_Idle,true]

t++
text[t] = "Man, those guys would freak if they knew...";
mySpeaker[t] = speaker[1]
myEmotion[t] = 3
myEffects[t] = 0
myScripts[t] = 0

create_dialogue(text, mySpeaker,myEffects,0,0,0,myScripts,0,myEmotion);
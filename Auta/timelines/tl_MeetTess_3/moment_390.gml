timeline_speed = 0.8

obj_TessActor3.image_xscale = -1
obj_TessActor3.sprite_index = spr_Tess_Idle
obj_TessActor3.behaviour = "idle"

obj_Auta.image_xscale = -1

text = 0

var t = 0
text[t] = "But at least tell me one thing."
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myScripts[t] = 0

t++

text[t] = "Why in the galaxy would you want to go there?"
mySpeaker[t] = speaker[0]
myEmotion[t] = 1
myEffects[t] = 0
myScripts[t] = 0
create_dialogue(text, mySpeaker,myEffects,0,0,0,myScripts,0,myEmotion);
function scr_HurtKelly(argument0) {
	if !place_meeting(x,y,obj_PlayerParentHitbox) {
		with obj_PlayerParent {
			if state = "cutscene" { exit };
			if iframes <= 0 && HP > 0{
				global.gameSpeed = 1
				var shield = false
				if instance_exists(obj_AutaShield) {
					if place_meeting(x,y,obj_AutaShield) {
						if (other.x > obj_Auta.x && obj_AutaShield.image_xscale = 1) || (other.x < obj_Auta.x && obj_AutaShield.image_xscale = -1)	 {
							shield = true	
						}
					}
				}
		
		
				if shield = false {
					if other.type = "slice" {
						with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
							image_xscale = other.image_xscale
							//image_blend = make_color_rgb(255, 181, 181)
							sprite_index = spr_Slice
							audio_play_sound(snd_MagicSlice2,4,false);
						}
					}
					if other.type = "burn" {
						with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
							image_xscale = other.image_xscale
							sprite_index = spr_Burn
							audio_play_sound(snd_BurnHurt,4,false);
						}
					}
					if other.type = "shock" {
						with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
							image_xscale = other.image_xscale
							sprite_index = spr_ElectricHurt
							audio_play_sound(snd_Oop,4,false);
						}
					}
		
					var atk = argument0
		
					//audio_play_sound(snd_PlayerHurt,4,false)
		
		
					global.screenshake = 2
					xSpeed+= 1*(other.x > x ? -1:1)
					var __time = current_time;  
						while current_time-__time < 80 {  
					}  
		
					scr_CreateHurtParticles(sign(xSpeed),atk/2)
		
					//ySpeed-= 1
					grounded = false
					animstate = "hurt"
					state = "hurt"
					
					switch object_index {
						case(obj_Tessio): 
							sprite_index = spr_TessioHurt
							break;
						case(obj_Auta):
							sprite_index = spr_AutaHurt
							break;
					}
					iframes = 60
					global.aberTime = 60
					hurtcounter = 15
					
					if other.makeFlinch = true {
						state = "knockback"	
						sprite_index = Auta_Knockback_Turn
						image_index = 0
						ySpeed = -1.5
						xSpeed = (other.x > x ? -1:1)
						image_xscale = -sign(xSpeed)
					}
					if other.type = "shock" {
						state = "Attack"
						sprite_index = spr_Auta_ElectricHurt
						image_index = 0
						xSpeed = 0
						ySpeed = 0
						animstate = "onedone"
					}
		
			
				} else {
					var atk = floor(argument0/4)
					scr_CreateHurtParticles(sign(xSpeed),atk/2)
					global.screenshake = 2
					xSpeed = 1*sign(other.image_xscale)
					var __time = current_time;  
						while current_time-__time < 50 {  
					}  
					iframes = 20
			
				}
		
				obj_GUI.hurtcounter = 60
		
				if HP-atk <= 0 {
					state = "dead"
					obj_Music.musictrack = snd_NoSound
					global.screenshake = 2
					var __time = current_time;  
						while current_time-__time < 50 {  
					}  
					HP = 0
					global.gameSpeed = 0
				
					alarm[1] = 3*room_speed
				
					ySpeed = -1
					sprite_index = spr_AutaHurt
				
				
				} else {
					HP -= atk
				}
				return 1;
				//switch(global.currentweapon) {
				//	case "stick":
				//		state = "dead"
				//		global.screenshake = 2
				//		var __time = current_time;  
				//			while current_time-__time < 50 {  
				//		}  
				//		break;
				//	case "sword":
				//		global.currentweapon = "stick"
				//		break;
				//	case "firesword":
				//		global.currentweapon = "sword"
				//		break;
				//	case "lasersword":
				//		global.currentweapon = "sword"
				//		break;
				//	case "icesword":
				//		global.currentweapon = "sword"
				//		break;
				//}
				//return 1;	
			} else {
				return 0;	
			}
		}
	}


}

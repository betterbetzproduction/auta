///@arg direction
///@arg number
function scr_CreateHurtParticles(argument0, argument1) {
	var dir				= argument0
	var degRange		= 0 + 40*(argument1-1);
	var degIncrement	= round(degRange/argument1);
	var degBegin		= 0-degIncrement/2+90 - 45*dir

	for(var i=0; i < argument1; i++) {
			toAngle = degBegin + degIncrement*i
			ind = i%2
			with instance_create_depth(x,y,depth-1,obj_HitStunParticles) {
				image_angle = other.toAngle
				image_index = other.ind
			}
	}
	if argument1 = 0 {
		toAngle = degBegin
		with instance_create_depth(x,y,depth-1,obj_HitStunParticles) {
			image_angle = other.toAngle
			sprite_index = spr_HurtParticles1
		}
	}





}

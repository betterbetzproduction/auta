function scr_TessDialogueInit() {
	inst = instance_create_depth(0,0,1,obj_Speaker);

	with inst {
		reset_dialogue_defaults();

		myPortrait			= spr_Portrait_Tess
		myVoice				= snd_voice1;
		myFont				= fnt_dialogue;
		myName				= "None";

		myPortraitTalk		= spr_Blank
		myPortraitTalk_x	= 26;
		myPortraitTalk_y	= 44;
		myPortraitIdle		= spr_Blank
	}

	return inst;


}

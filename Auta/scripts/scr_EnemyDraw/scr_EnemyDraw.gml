function scr_EnemyDraw() {
	image_blend = c_white
	if vulnState && grabbable {
		scr_DrawOutline(c_aqua)
		image_blend = c_aqua
		if round(stunCounter/8)%2 = 0 {
			image_blend = c_black	
		}
		draw_self()	
		image_blend = c_white
	
	}

	if seenCounter > 0 {
		//draw_sprite(spr_Danger,0,x,y)
		if class = "small" {
			gpu_set_blendmode(bm_add)
			draw_sprite_ext(spr_Danger,1,x-seenCounter*2,y-seenCounter*2,1,1,0,c_aqua,1)
			draw_sprite_ext(spr_Danger,2,x+seenCounter*2,y+seenCounter*2,1,1,0,c_fuchsia,1)
	
			gpu_set_blendmode(bm_normal);
			draw_sprite(spr_Danger,1,x-seenCounter,y-seenCounter)
			draw_sprite(spr_Danger,2,x+seenCounter,y+seenCounter)
		}
		if class = "medium" {
			var offset = 8
			draw_sprite(spr_Danger,0,x,y)
		
			gpu_set_blendmode(bm_add)
			draw_sprite_ext(spr_Danger,1,x-seenCounter*2-offset,y-seenCounter*2-offset,1,1,0,c_aqua,1)
			draw_sprite_ext(spr_Danger,2,x+seenCounter*2+offset,y+seenCounter*2+offset,1,1,0,c_fuchsia,1)
	
			gpu_set_blendmode(bm_normal);
			draw_sprite(spr_Danger,1,x-seenCounter-offset,y-seenCounter-offset)
			draw_sprite(spr_Danger,2,x+seenCounter+offset,y+seenCounter+offset)
		}
	
		seenCounter-= global.gameSpeed
	} else if seenCounter > -60 {
		if class = "small" {
	
			draw_sprite(spr_Danger,1,x,y)
			draw_sprite(spr_Danger,2,x,y)
		}
		if class = "medium" {
			var offset = 8
			draw_sprite(spr_Danger,0,x,y)
		
			draw_sprite(spr_Danger,1,x-offset,y-offset)
			draw_sprite(spr_Danger,2,x+offset,y+offset)
		
		}
	
		seenCounter-= global.gameSpeed
	}

	//if hp > 0 && seen && drawHealthBar {
	//	draw_set_color(c_black)
	//	draw_rectangle(bbox_left,bbox_bottom+3,bbox_right,bbox_bottom+4,false)	
	
	//	var length = bbox_right-bbox_left
	
	//	draw_set_color(c_red)
	//	draw_rectangle(bbox_left,bbox_bottom+3,bbox_left+length*(hp/maxhp),bbox_bottom+4,false)	
	//}


}

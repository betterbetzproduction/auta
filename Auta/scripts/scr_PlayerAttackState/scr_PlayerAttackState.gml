function scr_PlayerAttackState(argument0) {

	if state = "attack" {
		if ySpeed > 0 {
			//ySpeed -= grav*atkStateGrav*spd
		}
		//mask_index = argument0
		if grounded {
			var accel = 0.1
			var stop = 0.1
		
		} else {
			var accel = 0.1
			var stop = 0.0
		
			if sprite_index = spr_AutaStomp {
				stop = 0	
			}
		}
	
		if equip[0] = "Swift Blade" {
			accel*=0.5
		}
	
		if !grounded {
			if aRight {
				if xSpeed < maxSpeed {
					if !place_meeting(x+maxSpeed,y-4,obj_Collision) {
						xSpeed += accel	
					}
				} else {
					if xSpeed > maxSpeed+stop {
						xSpeed -= stop	
					} else {
						xSpeed = maxSpeed
					}	
				}
				
			}
			if aLeft {
				if xSpeed > -maxSpeed {
					if !place_meeting(x-maxSpeed,y-4,obj_Collision) {
						xSpeed -= accel	
					}
				} else {
					if xSpeed < -maxSpeed-stop {
						xSpeed += stop	
					} else {
						xSpeed = -maxSpeed	
					}
				}
			}
			if !aLeft && !aRight {
				if xSpeed > 0 {
					if xSpeed > stop {
						xSpeed -= stop	
					} else {
						xSpeed = 0	
					}
				}
				if xSpeed < 0 {
					if xSpeed < -stop {
						xSpeed += stop	
					} else {
						xSpeed = 0	
					}
				}
			}
		}
		if (!aLeft && !aRight) || grounded{
			if xSpeed > 0 {
				if xSpeed > stop {
					xSpeed -= stop	
				} else {
					xSpeed = 0	
				}
			}
			if xSpeed < 0 {
				if xSpeed < -stop {
					xSpeed += stop	
				} else {
					xSpeed = 0	
				}
			}
		}
		//if xSpeed > 0 {
		//	if xSpeed > stop {
		//		xSpeed -= stop	
		//	} else {
		//		xSpeed = 0	
		//	}
		//}
		//if xSpeed < 0 {
		//	if xSpeed < -stop {
		//		xSpeed += stop	
		//	} else {
		//		xSpeed = 0	
		//	}
		//}
	
		if grounded && sprite_index = spr_AutaStomp {
		
			with instance_create_layer(x+5*image_xscale,y,layer,obj_PlayerParentHitbox) {
				selfDependant = true
				image_xscale = other.image_xscale
				attack = 1
				Xknockback = 1
				Yknockback = 2
				killsprite = other.sprite_index
				sprite_index = spr_AutaStomp_Land
				visible = true
				type = "sliceNoPull"
			
			}
			global.screenshake = 2
			audio_play_sound(snd_Crash,4,false);
			sprite_index = spr_AutaStomp_Recovery
			xSpeed = 0
			image_index = 0
			state = "attack"
			animstate = "onedone"
			
		}
	}


}

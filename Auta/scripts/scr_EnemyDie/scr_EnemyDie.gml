function scr_EnemyDie() {
	instance_destroy();
	with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
		sprite_index = spr_LargeBurst	
		if audio_is_playing(snd_EnemyDeath) {
			audio_stop_sound(snd_EnemyDeath)	
		}
		audio_play_sound_at(snd_EnemyDeath,x,y,0,50,300,1,false,4);
	}

	var rand = floor(random_range(0,array_height_2d(drops)))
	if rand = array_height_2d(drops) {
		rand--
	}


	for(i = 0; i < array_length_2d(drops, rand);i++) {
		if drops[rand,i] != noone {
			with instance_create_depth(x,y,depth-1,drops[rand,i]) {
				drop = true
				ySpeed = -2
			}
		}
	}
	var expDrop = ceil(random_range(lowxp, highxp))
	for(i = 0; i < expDrop;i++) {
		instance_create_layer(x,y,layer,obj_PowerGem)	
	}


}

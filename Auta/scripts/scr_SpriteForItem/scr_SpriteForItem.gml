// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_SpriteForItem(){
	var sprite = spr_SwordIcon
	switch argument0 {
		case "Brush Blade":
			sprite = spr_SwordIcon
		break;
		
		case "Soft-Coded Blade":
			sprite = spr_SwordIcon
		break;
		
		case "Swift Blade":
			sprite = spr_SwordIcon
		break;
		
		case "Heavy Blade":
			sprite = spr_SwordIcon
		break;
		
		
		case "Piercer":
			sprite = spr_SwordIcon
		break;
		
		case "Cannon":
			sprite = spr_SwordIcon
		break;
		
		case "Gauntlets":
			sprite = spr_SwordIcon
		break;

		case "Fizzle Card":
			sprite = spr_FizzleCardIcon
		break;
		
		case "Smolder Card":
			sprite = spr_FizzleCardIcon
		break;
											//REGULAR ITEMS
		case "Metallic Scraps":
			sprite = spr_ScrapsIcon
			break; 
			
		case "Strong Scraps":
			sprite = spr_StrongScrapsIcon
		break;
		
		case "Mohs Crystal":
			sprite = spr_MohlsCrystalIcon
		break;
											//KEY ITEMS
		case "Staircase Key":
			sprite = spr_KeyIcon
		break;
		
	}
	return sprite
}
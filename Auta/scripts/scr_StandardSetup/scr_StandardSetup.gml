function scr_StandardSetup() {
	xSpeed = 0
	ySpeed = 0
	maxSpeed = 0.5
	state = "normal"
	grounded = true
	grav = 0.1
	maxhp = 9
	hp = maxhp
	invincible = false
	contactby = obj_init

	cannonballStall = 0

	dis_u = shader_get_uniform(shd_aberration, "u_Distance");
	dis = 1;

	_uniColor = shader_get_uniform(shd_ColorReplace, "u_color");
	_color    = [1.0, 1.0, 0.0, 1.0];

	vulnState = false //State of stun when HP = 0
	grabbable = true //Can be grabbed when stunned

	nudge = true //Does this object nudge with other objects?
	nudgeWeight = 1
	unstoppable = false //Is this object unmovable?

	gauntletCounter = -1

	cannonballredirect = true //Does this object bounce when it hits something?
	cannonballdmg = 10
	cannonballXknockback = 2
	cannonballYknockback = 1.5
	cannonballtimes = 60
	stun = true
	stunCounter = 0

	flinchThreshold = 0 //Minimum knockback needed to flinch

	drops[0,0] = obj_HealthPickup
	drops[1,0] = obj_Coin
	drops[2,0] = noone
	drops[3,0] = noone
	drops[4,0] = noone

	drawHealthBar = true
	seen = false
	seenCounter = -100

	canSeePlayer = false
	lastPlayerX = 0
	lastPlayerY = 0
	maxInterest = 300
	interest = 0
	viewDist = 150

	hitstunTimer = -100

	trailcounter = 0

	class = "small"

	active = true

	lowxp = 1
	highxp = 3

	ddInst = noone
	
	makeFlinch = false

	baseY = y
	
}

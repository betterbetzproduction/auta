function scr_EnemyCannonball(argument0) {
	sprite_index = argument0
	image_angle+=50*sign(xSpeed+0.1)*spd

	nudge = false

	grav = 0.1
	if cannonballStall > 0 {
		grav = 0
		cannonballStall-=global.gameSpeed*0.8
	}

	cannonballtimes -= global.gameSpeed
	if cannonballtimes <= 0 {
		scr_EnemyDie();
	}
	
	if distance_to_object(obj_EnemyParent) < 64 {
		toX = instance_nearest(x,y,obj_EnemyParent).x	
		toY = instance_nearest(x,y,obj_EnemyParent).x	
		
		xSpeed+=0.05*(x > toX ? -1:1)
		ySpeed+=0.05*(y > toY ? -1:1)
	}
	
	

	if place_meeting(bbox_right,y,obj_Collision) {
		if xSpeed > 0 {
			if xSpeed > 0 {
				gdinst = instance_place(bbox_right,y,obj_Collision) 
				if gdinst.object_index != obj_HardtoBreakBlock && gdinst.object_index != obj_BreakableBlock  {
					with instance_create_depth(x,y,depth+1,obj_OneTimeFX) {
						sprite_index = spr_WallSmash
						global.screenshake = 2
						image_angle = 90
						if audio_is_playing(snd_Strike) {
							audio_stop_sound(snd_Strike)
						}	
						var ad = audio_play_sound_at(snd_Strike,x,y,0,50,300,1,false,4);
						audio_sound_gain(ad,0.5,0)
						var q = 0
						do {
							x+=2	
							q++
						}
						until(place_meeting(x+1,y,obj_Collision) || q = 16)
						if q = 16 {instance_destroy()};
					}
				
					xSpeed = -xSpeed
					x-=5
				
				}
			
			}
		
		
		}
	}
	if place_meeting(bbox_left,y,obj_Collision) {
		if xSpeed < 0 {
			if xSpeed < 0 {
				gdinst = instance_place(bbox_left,y,obj_Collision) 
					if gdinst.object_index != obj_HardtoBreakBlock && gdinst.object_index != obj_BreakableBlock  {
					with instance_create_depth(x,y,depth+1,obj_OneTimeFX) {
						sprite_index = spr_WallSmash
						global.screenshake = 2
						image_angle = -90
						if audio_is_playing(snd_Strike) {
							audio_stop_sound(snd_Strike)
						}	
						var ad = audio_play_sound_at(snd_Strike,x,y,0,50,300,1,false,4);
						audio_sound_gain(ad,0.5,0)
						var q = 0
						do {
							x-=2	
							q++
						}
						until(place_meeting(x-1,y,obj_Collision) || q = 16)
						if q = 16 {instance_destroy()};
					}
				
					xSpeed = -xSpeed
					x+=5
				}
			}
		
		
		}
	}

	if place_meeting(x,bbox_bottom,obj_Collision) {
		if ySpeed > 0 {
			if ySpeed > 0 {
				gdinst = instance_place(x,bbox_bottom,obj_Collision) 
				if gdinst.object_index != obj_HardtoBreakBlock && gdinst.object_index != obj_BreakableBlock {
					with instance_create_depth(x,y,depth+1,obj_OneTimeFX) {
						sprite_index = spr_WallSmash
						global.screenshake = 2
						if audio_is_playing(snd_Strike) {
							audio_stop_sound(snd_Strike)
						}	
						var ad = audio_play_sound_at(snd_Strike,x,y,0,50,300,1,false,4);
						audio_sound_gain(ad,0.5,0)
						var q = 0
						do {
							y+=2	
							q++
						}
						until(place_meeting(x,y+1,obj_Collision) || q = 16)
						if q = 16 {instance_destroy()};
					}
				
					ySpeed = -ySpeed
					y-=5
				}
			}
		
		
		}
	}
	if place_meeting(x,bbox_top,obj_Collision) {
		if ySpeed < 0 {
			if ySpeed < 0 {
				gdinst = instance_place(x,bbox_top,obj_Collision) 
				if gdinst.object_index != obj_HardtoBreakBlock && gdinst.object_index != obj_BreakableBlock {
					with instance_create_depth(x,y,depth+1,obj_OneTimeFX) {
						sprite_index = spr_WallSmash
						global.screenshake = 2
						image_yscale = -1
						if audio_is_playing(snd_Strike) {
							audio_stop_sound(snd_Strike)
						}	
						var ad = audio_play_sound_at(snd_Strike,x,y,0,50,300,1,false,4);
						audio_sound_gain(ad,0.5,0)
						var q = 0
						do {
							y-=2	
							q++
						}
						until(place_meeting(x,y-1,obj_Collision) || q = 16)
						if q = 16 {instance_destroy()};
					}
				
					ySpeed = -ySpeed
					y-=5
				}
			}
		
		
		}
	}


	trailcounter+=1*spd

	if trailcounter>3 {
		with instance_create_depth(x,y,depth+1,obj_Auta_Afterimage)	{
			creator = other.id
		}
		trailcounter = 0
	}


}

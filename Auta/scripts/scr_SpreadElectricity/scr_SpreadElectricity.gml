function scr_SpreadElectricity() {
	var _list = ds_list_create();

	var _num = collision_rectangle_list(x - 8, y - 8, x + 8*(image_xscale+1), y + 8*(image_yscale+1), obj_ShockCollision, false, true, _list, false);
	
	if _num > 0
	{
		for (var i = 0; i < _num; ++i;)
			{
			if _list[| i].activated = false {
				_list[| i].activated = true
				_list[| i].deactiveDelay = 2
				with _list[| i] {
					scr_SpreadElectricity()	
				}
			}
		}
	}
		
	ds_list_destroy(_list);


}

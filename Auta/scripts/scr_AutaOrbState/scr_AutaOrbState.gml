function scr_AutaOrbState() {
	var pad = global.pad
	var haxis = gamepad_axis_value(pad, gp_axislh);
	var vaxis = gamepad_axis_value(pad, gp_axislv);
	var joystick = sqrt(power(haxis,2) + power(vaxis,2)) > 0.5

	if state = "orb" {
		
		if gamepad_axis_value(pad, gp_axislh) > 0.1
			{ aRight = true} 
		if aRight = false {
			aRight = keyboard_check(kRight) || gamepad_button_check(pad,pRight2)
			aRightTap = keyboard_check_pressed(kRight) || gamepad_button_check_pressed(pad,pRight2)
		}


		if gamepad_axis_value(pad, gp_axislh) < -0.1
			{ aLeft = true} 
		if aLeft = false {
			aLeft = keyboard_check(kLeft) || gamepad_button_check(pad,pLeft2)
			aLeftTap = keyboard_check_pressed(kLeft) || gamepad_button_check_pressed(pad,pLeft2)
		}
		
		
		if hangTime > 0 {
			hangTime-=global.gameSpeed
			ySpeed -= grav*global.gameSpeed
		}
		orbDelay--
		mask_index = spr_AutaOrb

		var accel = 0.06*spd	
		var stop = 0.06*spd
	
		if aRight {
			if xSpeed < maxSpeed {
				if !place_meeting(x+maxSpeed,y-4,obj_Collision) {
					xSpeed += accel	
				}
			}
		}
		if aLeft {
			if xSpeed > -maxSpeed {
				if !place_meeting(x-maxSpeed,y-4,obj_Collision) {
					xSpeed -= accel	
				}
			}
		}
	
		if !aLeft && !aRight {
			if xSpeed > 0 {
				if xSpeed > stop {
					xSpeed -= stop	
				} else {
					xSpeed = 0	
				}
			}
			if xSpeed < 0 {
				if xSpeed < -stop {
					xSpeed += stop	
				} else {
					xSpeed = 0	
				}
			}
		}
	
	
	
		animstate = "constant"
		ySpeed += grav*0.5*spd
	
		if place_meeting(x+xSpeed,y,obj_Collision) {
			xSpeed = -xSpeed	
			if ySpeed < 0 {
				ySpeed -= 1.5
				ySpeed = clamp(ySpeed,-3,-1.5)
			}
		}
		mask_index = spr_AutaIdle
		image_xscale = sign(image_xscale)*4
		image_yscale = image_yscale*2
		var enInst = instance_place(x+xSpeed,y,obj_EnemyParent)
		if place_meeting(x+xSpeed,y,obj_EnemyParent) {
		
			if enInst.grabbable = true && enInst.vulnState = true {
				orbDelay = 20
				global.gameSpeed = 0.15	
				global.screenshake = 1
				state = "aim"
				aimX = x+30*image_xscale/4
				aimY = y
				xSpeed = 0
				ySpeed = -1.5
			
				image_xscale = sign(image_xscale)
				image_yscale = image_yscale/2
				mask_index = spr_AutaOrb
			
				y=enInst.y
				x=enInst.x-8*image_xscale/2
			

				with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
					sprite_index = spr_GrabEffect	
					audio_play_sound(snd_Grab,4,false)
				}
		
				enInst.state = "grabbed"
				aimAngle = 0
				if image_angle = -1 {
					aimAngle = 180	
				}
			}
		
		}
		if place_meeting(x+xSpeed,y,obj_Propeller) {
		
			var enInst = instance_place(x+xSpeed,y,obj_Propeller)
			with enInst {
				visible = false
			
				with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
					sprite_index = spr_GrabEffect	
					audio_play_sound(snd_Grab,4,false)
				}
			}
		
			image_xscale = sign(image_xscale)
			image_yscale = image_yscale/2
			mask_index = spr_AutaOrb
		
			y = enInst.y
			x = enInst.x
		
		
			xSpeed = 0
			ySpeed = 0.5
			state = "propeller"
			sprite_index = spr_AutaPropeller
			animstate = "constant"
			specCounter = 0
		

		}
	
		image_xscale = sign(image_xscale)
		image_yscale = image_yscale/2
		mask_index = spr_AutaOrb
		if grounded {
			//if !keyboard_check(global.kOrb) && !gamepad_button_check(global.pad, global.pOrb) {
				state = "normal"
				animstate = "normal"
				mask_index = spr_AutaIdle
				xSpeed = 0
				with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
					sprite_index = spr_AutaMaterialize
					audio_play_sound(snd_Materialize,4,false)
				}
			//} else {
			//	grounded = false
			//}
		}
		if !keyboard_check(global.kOrb) && !gamepad_button_check(global.pad, global.pOrb) && orbDelay <= 0 {
			if aLeft {
				image_xscale = -1
			}
			if aRight {
				image_xscale = 1	
			}
			state = "normal"
			animstate = "onedone"
			sprite_index = spr_AutaFlipToAir
			mask_index = spr_AutaJump
			with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
				sprite_index = spr_AutaMaterialize
				audio_play_sound(snd_Materialize,4,false)
			}
		
			var angle = 0
		
			if image_xscale = 1 {
				angle = 10	
				if !grounded {
					angle = 0	
				}
			}
			if image_xscale = -1 {
				angle = 170	
				if !grounded {
					angle = 180	
				}
			}
		
			if !joystick {
				if aRight {
					angle = 10	
					if !grounded {
						angle = 0	
					}
				}
				if aLeft {
					angle = 170	
					if !grounded {
						angle = 180	
					}
				}
				if aUp {
					angle = 90
				}
				if aDown {
					angle = 270
				}
		
				if aRight && aDown {
					angle = 305	
				}
				if aLeft && aDown {
					angle = 225	
				}
		
				if aRight && aUp {
					angle = 45	
				}
				if aLeft && aUp {
					angle = 135	
				}
			}
			if joystick  {
				var haxis = gamepad_axis_value(pad, gp_axislh);
				var vaxis = gamepad_axis_value(pad, gp_axislv);
				angle = point_direction(0, 0, haxis, vaxis);	
			}
		
			if aUp || aDown || aLeft || aRight {
				xSpeed = dcos(angle)*2
				ySpeed = -dsin(angle)*2
			}
		} if aAttack {
			state = "normal"
			animstate = "normal"
			mask_index = spr_AutaJump
			with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
				sprite_index = spr_AutaMaterialize
				audio_play_sound(snd_Materialize,4,false)
			}
			scr_AutaAttack()
		}
		//if xSpeed != 0 || ySpeed != 0 {
		//	with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
		//		blend = bm_add
		//		sprite_index = spr_AutaOrb_Light
		//		image_angle = point_direction(other.x,other.y,other.x+other.xSpeed,other.y+other.ySpeed)+90
		//		fade = true
		//	}
		//}
	
	
	}

	if state = "aim" {
		sprite_index = spr_AutaGrab	
		var enInst = instance_place(x+8*image_xscale,y,obj_EnemyParent)
		if enInst != noone {
			enInst.y = y
		} else {
			global.gameSpeed = 1
			state = "normal"
		}
	
		if aimMode = 1 {
			if keyboard_check(vk_up) {
				aimAngle += 5
			}	

			if keyboard_check(vk_down) {
				aimAngle -= 5
			}	
			if keyboard_check(vk_up) {
				aimAngle = 90	
			}
			if keyboard_check(vk_right) {
				aimAngle = 0	
			}
			if keyboard_check(vk_left) {
				aimAngle = 180	
			}
			if keyboard_check(vk_down) {
				aimAngle = 270	
			}
	
			if keyboard_check(vk_up) && keyboard_check(vk_right) {
				aimAngle = 45	
			}
			if keyboard_check(vk_up) && keyboard_check(vk_left) {
				aimAngle = 135	
			}
	
			if keyboard_check(vk_down) && keyboard_check(vk_right) {
				aimAngle = 315	
			}
			if keyboard_check(vk_down) && keyboard_check(vk_left) {
				aimAngle = 225	
			}
		
			var haxis = gamepad_axis_value(pad, gp_axislh);
			var vaxis = gamepad_axis_value(pad, gp_axislv);
		
			if sqrt(power(haxis,2) + power(vaxis,2)) > 0.5 {
				direction = point_direction(0, 0, haxis, vaxis);
			}
		
		}
		if aimMode = 0 {
			if !joystick {
				if aUp {aimY -= 5}	
				if aDown {aimY += 5}
				if aRight {aimX += 5}
				if aLeft {aimX -= 5}
			}
		
			if joystick {
				var haxis = gamepad_axis_value(pad, gp_axislh);
				var vaxis = gamepad_axis_value(pad, gp_axislv);
		
				dir = point_direction(0, 0, haxis, vaxis);
				sp = point_distance(0 ,0, haxis, vaxis) * 5;
				aimX += sp*dcos(dir)
				aimY -= sp*dsin(dir)
			}
		
			aimAngle = point_direction(enInst.x,enInst.y,aimX,aimY)
		}
	
	
	
	
		orbDelay--
		if !(keyboard_check(global.kOrb) || gamepad_button_check(global.pad, global.pOrb)) && orbDelay <= 0 {
			canOrb = true
			trail = false
			with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
				sprite_index = spr_ThrowEffect_Front
				image_angle = other.aimAngle
			}
			with instance_create_depth(x,y,depth+1,obj_OneTimeFX) {
				sprite_index = spr_ThrowEffect_Back
				image_angle = other.aimAngle
			}
			global.screenshake = 2
		
			if enInst != noone {
				enInst.x = x
				enInst.y = y-3
				enInst.xSpeed = 3.5*dcos(aimAngle)
				enInst.ySpeed = -3.5*dsin(aimAngle)
				enInst.cannonballStall = 20
				enInst.state = "cannonball"
			}
		
			xSpeed = -3*dcos(aimAngle)
			ySpeed = clamp(5*dsin(aimAngle),-3,1);
		
			image_xscale = (aimX < x ? -1:1)
			global.gameSpeed = 1
			state = "attack"
			animstate = "onedone"
			sprite_index = spr_AutaFlipToAir
			audio_play_sound(snd_Throw,4,false)
			image_index = 0
	
		}
	}


}

function scr_CustomDialogueInit(argument0, argument1) {
	inst = instance_create_depth(0,0,1,obj_Speaker);

	with inst {
		reset_dialogue_defaults();

		myPortrait			= argument1;
		myVoice				= argument0;
		myFont				= fnt_dialogue;
		myName				= "None";

		myPortraitTalk		= spr_Blank
		myPortraitTalk_x	= 26;
		myPortraitTalk_y	= 44;
		myPortraitIdle		= spr_Blank
	}

	return inst;


}

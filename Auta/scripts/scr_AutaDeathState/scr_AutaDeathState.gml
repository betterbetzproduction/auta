function scr_AutaDeathState() {
	if state = "dead" {
		
		grav = 0.02
		global.gameSpeed = 1
		scr_Collision(false)
		global.gameSpeed = 0
		obj_DefaultCamera.follow = noone
		
		if grounded {
			if sprite_index = spr_AutaHurt {
				global.screenshake = 2	
			}
			sprite_index = spr_AutaCollapse	
		
		
			xSpeed = 0
			ySpeed = 0
		}
	
		xSpeed = clamp(xSpeed,-1,1)
		ySpeed = clamp(ySpeed,-1,1)
	}


}

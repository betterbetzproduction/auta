function scr_AutaSlide() {
	if state = "slide" {
		trail = true
		mask_index = spr_AutaIdle_ShortMask
		animstate = "constant"
	
		if jumpkey && animstate = "constant"{
			state = "normal"
			animstate = "normal"
			sprite_index = spr_AutaJump
			ySpeed = jumpforce
			audio_play_sound(snd_Jump,4,false)
		}
	
		if atkKey {
			image_index = 0
			xSpeed = 0.5*image_xscale
			sprite_index = spr_AutaUppercutWindup
			state = "attack"
		}
		//if grounded = false {
		//	grounded = false
		//	xSpeed = 0
		//	state = "normal"
		//	animstate = "normal"
		//}	
		if xSpeed = 0 {
			mask_index = spr_AutaIdle
			if place_meeting(x,y,obj_Collision) {
				mask_index = spr_AutaIdle_ShortMask 	
			} else {
				grounded = false
				state = "normal"
				animstate = "normal"
			}
		}
		slidecounter += spd
		if slidecounter >= 20 {
			mask_index = spr_AutaIdle
			if place_meeting(x,y,obj_Collision) {
				mask_index = spr_AutaIdle_ShortMask 	
			} else {
				grounded = false
				state = "normal"
				animstate = "normal"
			}
		}
	}



}

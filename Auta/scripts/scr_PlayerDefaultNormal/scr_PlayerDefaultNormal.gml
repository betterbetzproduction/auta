function scr_PlayerDefaultNormal(argument0, argument1) {
	var char = object_index
	var spd = global.gameSpeed
	canJumpCount--

	if y >= room_height+80 {
		if !instance_exists(obj_FadeTrans) {
			with instance_create_depth(x,y,depth-100,obj_FadeTrans) {
				toRoom	= room
				toX		= global.respawnx
				toY		= global.respawny
				global.currentweapon = "Brush"
			}
		}
	}
	if MP < global.maxmp {
		MP += 0.013
	}
	
	if ySpeed < 0 {
		ySpeed += grav*0.5*spd	
	}
	
	if place_meeting(x,y,obj_LadderCollision) {
		if state != "ladder" {
			if jumpkey {
				state = "ladder"	
			}
		} else {
			var ladderParent = instance_place(x,y,obj_LadderCollision)
			x =ladderParent.x+8
			ySpeed = 0
			xSpeed = 0
			grounded = false
			if aUp {
				if place_meeting(x,y-1,obj_LadderCollision) && !place_meeting(x,y-1,obj_Collision) {
					y -= 1	
				}
			}
			if aDown {
				if place_meeting(x,y+1,obj_LadderCollision) && !place_meeting(x,y+1,obj_Collision) {
					y += 1	
				}
			}
			if aRight || aLeft {
				state = "normal"	
			}
		}
	}
	if bufferjump > 0 {
		bufferjump--	
	}
	if jumpkey {
		bufferjump = 10	
	}
	//y += ySpeed*spd
	if ySpeed > 6 {
		ySpeed = 6	
	}
	if state = "normal" {
		if aMenu {
			state = "pause"
			global.cutscene = true
			instance_create_depth(x,y,0,obj_Menu)
			aMenu = false
		}
		
		
		mask_index = argument0
		if grounded {
			var accel = 0.2*spd	
			var stop = 0.4*spd
		
		} else {
			var accel = 0.15*spd	
			var stop = 0.1*spd
		
			if jumpUpwardsCancel {
				if ySpeed < -0.5 && !jumpheld {
					ySpeed += 0.1*spd
				}
				if ySpeed >= -0.5 {
					jumpUpwardsCancel = false	
				}
			}
		}
	
		if canJumpCount > 0 {
			if bufferjump && !aDown {
				ySpeed = jumpforce
				animstate = "onedone"
				image_index = 0
				switch char {
					case obj_Auta:
						if aLeft || aRight {
							sprite_index = spr_AutaRun_To_Jump
						} else {
							sprite_index = spr_AutaToJump
						}
						break;
					case obj_Tess:
						sprite_index = spr_Tess_Ball
						break;
				}
				
				if aRight {
					obj_Auta.image_xscale = 1
				}
				if aLeft {
					obj_Auta.image_xscale = -1	
				}
				
				
				if place_meeting(x,y+4,obj_WaterCollision) && !place_meeting(x,y+4,obj_Collision){
					ySpeed = jumpforce*0.75	
				}
			
				grounded = false
				audio_play_sound(snd_Jump,4,false)
			
				jumpUpwardsCancel = true
				bufferjump = 0
				jumpkey = false
			}	
		}
	
		if grounded && (aDown || aShield) {
			state = "duck"
			animstate = "other"
			sprite_index = spr_AutaToCrouch
			image_index = 0
			mask_index = spr_AutaIdle_ShortMask
		}
	
		if leftRightLock <= 0 {
			if aRight {
				if xSpeed < maxSpeed {
					if !place_meeting(x+maxSpeed,y-4,obj_Collision) {
						xSpeed += accel	
					}
				} else {
					if xSpeed > maxSpeed+stop {
						xSpeed -= stop	
					} else {
						xSpeed = maxSpeed
					}	
				}
			}
			if aLeft {
				if xSpeed > -maxSpeed {
					if !place_meeting(x-maxSpeed,y-4,obj_Collision) {
						xSpeed -= accel	
					}
				} else {
					if xSpeed < -maxSpeed-stop {
						xSpeed += stop	
					} else {
						xSpeed = -maxSpeed	
					}
				}
			}
			if !aLeft && !aRight {
				if xSpeed > 0 {
					if xSpeed > stop {
						xSpeed -= stop	
					} else {
						xSpeed = 0	
					}
				}
				if xSpeed < 0 {
					if xSpeed < -stop {
						xSpeed += stop	
					} else {
						xSpeed = 0	
					}
				}
			}
		} else {
			leftRightLock -= spd	
		}
	}


	if state = "duck" {
		mask_index = spr_AutaIdle_ShortMask
		var stop = 0.2
		if xSpeed > 0 {
			if xSpeed > stop {
				xSpeed -= stop	
			} else {
				xSpeed = 0	
			}
		}
		if xSpeed < 0 {
			if xSpeed < -stop {
				xSpeed += stop	
			} else {
				xSpeed = 0	
			}
		}
		
		if !aDown && !aShield{
			sprite_index = spr_AutaIdle
			animstate = "normal"
			image_index = 0
			state = "normal"
			mask_index = spr_AutaIdle
		}
	
		if aRight {
			//sprite_index = spr_AutaFlip
			//image_index = 0
			image_xscale = 1
			//state = "roll"
			//xSpeed = 2.5*image_xscale	
		}
		if aLeft {
			//sprite_index = spr_AutaFlip
			//image_index = 0
			image_xscale = -1
			//state = "roll"
			//xSpeed = 2.5*image_xscale	
		}
		if char = obj_Auta {
			if aAttack {
		
				if equip[0] = "Brush Blade" || equip[0] = "Soft-Coded Blade" || equip[0] = "Swift Blade" || equip[0] = "Heavy Blade" {
					var weaponType = "Sword"	
				}
				if equip[0] = "Piercer" {
					var weaponType = "Pierce"	
				}
				if equip[0] = "Cannon" {
					var weaponType = "Cannon"	
				}
				if equip[0] = "Gauntlets" {
					var weaponType = "Gauntlets"	
				}
				if weaponType = "Gauntlets" {
					image_index = 0
					sprite_index = spr_Auta_Gauntlet_DownTilt_Windup
					state = "attack"
				}
				if weaponType = "Sword" {
					var sweepKick = false
					if ds_list_size(inputList) >= 3 {
						if ds_list_find_value(inputList,ds_list_size(inputList)-1) = "down" {
							if ds_list_find_value(inputList,ds_list_size(inputList)-2) = "right" || ds_list_find_value(inputList,ds_list_size(inputList)-2) = "left" {
								if ds_list_find_value(inputList,ds_list_size(inputList)-3) = "down" {
										sweepKick = true
								}
							}
					
						}
					}
			
			
					if sweepKick {
						global.gameSpeed = 0.2
						iframes = 10
						image_index = 0
						sprite_index = spr_AutaCrouch_SpecialAttack1
						atkStateGrav = 0
						state = "attack"
						animstate = "onedone"
						audio_play_sound(snd_MagicSwing,4,false);
						with instance_create_layer(x,y+6,layer,obj_PlayerParentProjectile) {
							xSpeed = -3
							ySpeed = 0
							attack = other.Atk
							Yknockback = 2.5
							Xknockback = 0.3
							image_xscale = -1
						}
						with instance_create_layer(x,y+6,layer,obj_PlayerParentProjectile) {
							xSpeed = 3
							ySpeed = 0
							attack = other.Atk
							Yknockback = 2.5
							Xknockback = 0.3
							image_xscale = 1
						}
				
						with instance_create_layer(x,y-2,layer,obj_PlayerParentProjectile) {
							xSpeed = -3
							ySpeed = 0
							attack = other.Atk
							Yknockback = 2.5
							Xknockback = 0.3
							image_xscale = -1
						}
						with instance_create_layer(x,y-2,layer,obj_PlayerParentProjectile) {
							xSpeed = 3
							ySpeed = 0
							attack = other.Atk
							Yknockback = 2.5
							Xknockback = 0.3
							image_xscale = 1
						}
				
						with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
							attack = other.Atk
							Xknockback = 0.3
							Yknockback = 2.5
							image_xscale = obj_PlayerParent.image_xscale
							killsprite = other.sprite_index
							type = "slice"
							sprite_index = spr_AutaCrouch_SpecialAttack_Hitbox
							visible = true
							image_xscale *= 0.85
							if other.equip[0] = "Swift Blade" {
								other.imgSpd = 1.15
								image_xscale *= 0.75
							}
							if other.equip[0] = "Heavy Blade" {
								other.imgSpd = 0.7
							}
						}
				
					} else {
						image_index = 0
						sprite_index = spr_AutaCrouch_Attack
						atkStateGrav = 0
						state = "attack"
						animstate = "onedone"
						audio_play_sound(snd_MagicSwing,4,false);
						with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
							attack = other.Atk
							Xknockback = 0.8
							Yknockback = 1.6
							image_xscale = obj_PlayerParent.image_xscale
							killsprite = other.sprite_index
							type = "slice"
							sprite_index = spr_AutaCrouch_Attack_Hitbox
							visible = true
							image_xscale *= 1
							if other.equip[0] = "Swift Blade" {
								other.imgSpd = 1.15
								image_xscale *= 0.75
							}
							if other.equip[0] = "Heavy Blade" {
								other.imgSpd = 0.7
							}
						}
					}
					
				}
			}
		}
	
	
	}

	if state = "roll" {
		var stop = 0.05
		if xSpeed > 0 {
			if xSpeed > stop {
				xSpeed -= stop	
			} else {
				xSpeed = 0	
			}
		}
		if xSpeed < 0 {
			if xSpeed < -stop {
				xSpeed += stop	
			} else {
				xSpeed = 0	
			}
		}
	}

	if state = "cutscene" {
	
		mask_index = argument0
		if grounded {
			var accel = 0.2*spd	
			var stop = 0.4*spd
		
		} else {
			var accel = 0.1*spd	
			var stop = 0.1*spd
		
			if jumpUpwardsCancel {
				if ySpeed < -0.5 && !jumpheld {
					ySpeed += 0.1*spd
				}
				if ySpeed >= -0.5 {
					jumpUpwardsCancel = false	
				}
			}
		}
	
		if canJumpCount > 0 {
			if jumpkey && !keyboard_check(vk_down) {
				ySpeed = jumpforce
				if place_meeting(x,y+4,obj_WaterCollision) && !place_meeting(x,y+4,obj_Collision){
					ySpeed = jumpforce*0.75	
				}
			
				grounded = false
				audio_play_sound(snd_Jump,4,false)
				sprite_index = argument1
				jumpUpwardsCancel = true
				jumpkey = false
				
				if aRight {
					obj_Auta.image_xscale = 1
				}
				if aLeft {
					obj_Auta.image_xscale = -1	
				}
			}	
		}
	
		if rightkey {
			if xSpeed < maxSpeed {
				if !place_meeting(x+maxSpeed,y-4,obj_Collision) {
					xSpeed += accel	
				}
			}
			if xSpeed > maxSpeed {
				xSpeed = maxSpeed	
			}
		}
		if leftkey {
			if xSpeed > -maxSpeed {
				if !place_meeting(x-maxSpeed,y-4,obj_Collision) {
					xSpeed -= accel	
				}
			}
			if xSpeed < -maxSpeed {
				xSpeed = -maxSpeed	
			}
		}
		if !leftkey && !rightkey {
			if xSpeed > 0 {
				if xSpeed > stop {
					xSpeed -= stop	
				} else {
					xSpeed = 0	
				}
			}
			if xSpeed < 0 {
				if xSpeed < -stop {
					xSpeed += stop	
				} else {
					xSpeed = 0	
				}
			}
		}
	
		if talkTarg != noone {
			if talkTarg.image_xscale = -1 {
				if abs(x-talkTarg.x) < 12 {
					leftkey = true	
				} else {
					leftkey = false
					image_xscale = 1
				}
			}
		
			if talkTarg.image_xscale = 1 {
				if abs(x-talkTarg.x) < 12 {
					rightkey = true	
				} else {
					rightkey = false
					image_xscale = -1
				}
			}
		}
	
	
	}

	if comboTimer > 0 && state = "normal"{
		comboTimer -= global.gameSpeed	
		if comboTimer <= 0 {
			comboNumber = 1	
		}
	}

	if trail {
		trailcounter+=1*spd
		if state = "prismDash" {
			if trailcounter>1 {
				with instance_create_depth(x,y,depth+1,obj_Auta_Afterimage)	{
					creator = other	
					image_index = other.image_index
				}
				trailcounter = 0
			}
		}
		if trailcounter>3 {
			with instance_create_depth(x,y,depth+1,obj_Auta_Afterimage)	{
				creator = other	
				image_index = other.image_index
			}
			trailcounter = 0
		}
	}

	if image_yscale > 1 {
		image_yscale-=0.02	
	}
	if image_yscale < 1 {
		image_yscale+=0.05	
	}
	if char = obj_Auta {
		if aShield && grounded{
			if !instance_exists(obj_AutaShield) {
				instance_create_depth(x,y,depth-1,obj_AutaShield)	
			}
		}
	}



}

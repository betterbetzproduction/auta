///Controller Setup
function scr_Controls() {

	kRight		= global.kRight
	kLeft		= global.kLeft
	kUp			= global.kUp
	kDown		= global.kDown
	kJump		= global.kJump
	kAttack		= global.kAttack
	kOrb		= global.kOrb	
	kMenu		= vk_enter
	kInteract	= global.kInteract
	kShield		= global.kShield
	kSpecial	= global.kSpecial

	pRight		= gp_axislh
	pLeft		= gp_axislh
	pUp			= gp_axislv
	pDown		= gp_axislv
	pRight2		= global.pRight2
	pLeft2		= global.pLeft2
	pUp2		= global.pUp2	
	pDown2		= global.pDown2
	pJump		= global.pJump
	pAttack		= global.pAttack	
	pOrb		= global.pOrb	
	pInteract	= global.pInteract
	pMenu		= gp_start
	pShield		= global.pShield
	pSpecial	= global.pSpecial

	aRight		= false
	aLeft		= false
	aUp			= false
	aDown		= false
	aAttack		= false
	aOrb		= false
	aJump		= false
	aMenu		= false
	aShield		= false
	aInteract	= false
	aSpecial = false

	aLeftTap = false
	aRightTap = false
	aUpTap = false
	aDownTap = false


}

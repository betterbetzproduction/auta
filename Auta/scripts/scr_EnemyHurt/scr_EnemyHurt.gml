/// @description Insert description here
function scr_EnemyHurt(argument0) {
	// You can write your code in this editor

	if state != "cannonball" {
		if invincible = false {
		
			var flinch = (sqrt( power(other.Xknockback,2) + power(other.Yknockback,2) )) > flinchThreshold
		
			if other.type = "slice" {
			
				with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
					image_xscale = other.image_xscale
					sprite_index = spr_Slice
					audio_play_sound_at(snd_MagicSlice2,x,y,0,50,300,1,false,4);
				}
				other.stop = true
			
				if flinch {
					if nudgeWeight != 2 {
						y += (other.y-y)*0.2
					}
				}
			}
			if other.type = "sliceNoPull" {
			
				with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
					image_xscale = other.image_xscale
					sprite_index = spr_Slice
					audio_play_sound_at(snd_MagicSlice2,x,y,0,50,300,1,false,4);
				}
				other.stop = true
			
			}
			if other.type = "lightSpeed" {
				with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
					image_xscale = other.image_xscale
					sprite_index = spr_Slice
					audio_play_sound_at(snd_MagicSlice2,x,y,0,50,300,1,false,4);
				}

			}
			if other.type = "cannonball" {
				with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
					image_xscale = other.image_xscale
					sprite_index = spr_BashEffect
					audio_play_sound_at(snd_Slam,x,y,0,50,300,1,false,4);
				}
			}
			if other.type = "strike" {
				with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
					image_xscale = other.image_xscale
					sprite_index = spr_BashEffect
					
				}
				audio_play_sound_at(snd_Punch,x,y,0,50,300,1,false,4);
				if flinchThreshold < 2 {
					y += (other.y-y)*0.4
				}
				other.stop = true
			}
			if other.type = "burn" {
				with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
					image_xscale = other.image_xscale
					sprite_index = spr_Burn
					audio_play_sound(snd_BurnHurt,4,false);
				}
			}
	
			var __time = current_time;  
				while current_time-__time < 10 {  
			}  
			image_angle = 0
			global.screenshake = 1
			invincible = true
			xSpeed = other.image_xscale*other.Xknockback
			hp -= other.attack
			ySpeed = -other.Yknockback
			grounded = false
			scr_CreateHurtParticles(sign(xSpeed),other.attack/2)
		
			if !instance_exists(ddInst) {
				ddInst = scr_DD(0-other.attack,c_red)
		
			} else {
				ddInst.dmg -= other.attack
				ddInst.x = x
				ddInst.y = bbox_top
				ddInst.counter = 0
				ddInst.yaccel = 1
			}
		
			contactby = other
			other.contacted = true
			//vulnState = false
	
			if hp <= 0 {
				hitstunTimer = round(5*(sqrt( power(other.Xknockback,2) + power(other.Yknockback,2) )))+5
				global.screenshake = 2	
				var __time = current_time;  
				while current_time-__time < 20 {  
				} 
			
				if other.type = "cannonball" {
					if grabbable {
						var aimAngle = point_direction(x,y,other.x,other.y)
			
						xSpeed = other.cannonballXknockback*dcos(aimAngle)*image_xscale
						ySpeed = -other.cannonballXknockback*dsin(aimAngle)
						y-=5
						state = "cannonball"
					}
					else {
						instance_destroy();
						with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
							sprite_index = spr_LargeBurst
							if audio_is_playing(snd_MagicSlice2) {
								audio_stop_sound(snd_MagicSlice2)	
							}
							if audio_is_playing(snd_Slam) {
								audio_stop_sound(snd_Slam)	
							}
							audio_play_sound_at(snd_EnemyDeath,x,y,0,50,300,1,false,4);
						}
					}
			
			
				} else {
					if vulnState = true {
						if audio_is_playing(snd_MagicSlice2) {
							audio_stop_sound(snd_MagicSlice2)	
						}
						if audio_is_playing(snd_Slam) {
							audio_stop_sound(snd_Slam)	
						}
					
						scr_EnemyDie()

					} else {
						if grabbable {
							state = "stun"
							vulnState = true
							if other.object_index = obj_PlayerParentHitbox {
								obj_Auta.canOrb = true	
							}
						
						} else {
							if audio_is_playing(snd_MagicSlice2) {
							audio_stop_sound(snd_MagicSlice2)	
							}
							if audio_is_playing(snd_Slam) {
								audio_stop_sound(snd_Slam)	
							}
					
						scr_EnemyDie()
						}
					}
				}
			} else {
				if flinch {
					state = "hurt"
					sprite_index = argument0
				
					hitstunTimer = round(5*(sqrt( power(other.Xknockback,2) + power(other.Yknockback,2) )))+5
					if other.stun = false {
						hitstunTimer = 1	
					}
				}	
			}
	
			with other {
				if object_index = obj_PlayerParentProjectile {
					instance_destroy();	
				}
			}
		
			return 1;
		} else {
			return 0;	
		}
	} else {
		return 0;	
	}


}

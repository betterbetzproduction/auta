///wrap(value,min,max)
function wrap() {
	//returns the value wrapped.  If it is above or below the threshold it will wrap around
	var _val=argument[0];
	var _max = argument[2];
	var _min = argument[1];
	while(_val > _max || _val < _min)
	{
	    if(_val > _max)
	    {
	        _val=_min + _val - _max -1;
	    }
	    else if (_val < _min)
	    {
	        _val=_max + _val - _min +1;
	    }
	    else
	        _val=_val;
	}
	return(_val);


}

function scr_Equip(argument0, argument1, argument2) {
	var item	= argument0
	var index	= argument1
	var mPos	= argument2
	var temp	= equip[index]

	equip[index] = item
	if mPos != -1 {
		inventory = scr_RemoveItem(mPos)
	}
	if temp != "Nothing" {
		inventory = scr_AddItem(temp,0)
	}
	global.equipment = equip
	scr_EquipInit()

	return inventory


}

function scr_AddItem(argument0, argument1) {
	item = argument0
	itemSpecial = argument1
	var inventory = global.inventory
	if scr_HasItem(item) = -1 {
		array_resize(inventory,array_length(inventory)+1)
		inventory[array_length(inventory)-1][0] = item
		inventory[array_length(inventory)-1][1] = 1
		inventory[array_length(inventory)-1][2] = itemSpecial
		
	} else {
		inventory[scr_HasItem(item)][1] += 1
	}
	global.inventory = inventory
	return inventory;
	
	

}

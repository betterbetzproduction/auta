///pal_swap_get_color(palette sprite,pal index, color index)
function pal_swap_get_pal_color() {
	//returns the color for the given palette for the given palette sprite.
	var _palettes = ds_map_find_value(Pal_Map,argument[0]);
	var _current_pal=ds_list_find_value(_palettes,argument[1]);
	return(ds_list_find_value(_current_pal,argument[2]));


}

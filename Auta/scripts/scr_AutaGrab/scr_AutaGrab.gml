function scr_AutaGrab() {
	if state = "grab" {
	
		trail = true
	
		ySpeed = 0
		animstate = "constant"
	
		if xSpeed = 0 {
			mask_index = spr_AutaIdle
			grounded = false
			state = "attack"
			sprite_index = spr_AutaOutPrismDash
			animstate = "onedone"
			trail = false
		}
		slidecounter += spd
		if slidecounter >= 20 {
			mask_index = spr_AutaIdle
			grounded = false
			state = "attack"
			sprite_index = spr_AutaOutPrismDash
			animstate = "onedone"
			xSpeed = 2*sign(xSpeed)
			trail = false
		}
		var enInst = instance_place(x+xSpeed,y,obj_EnemyParent)
		if place_meeting(x+xSpeed,y,obj_EnemyParent) {
		
			if enInst.grabbable = true {
				global.gameSpeed = 0.1	
				global.screenshake = 1
				state = "aim"
				aimX = x+30*image_xscale
				aimY = y
				xSpeed = 0
				ySpeed = -1
				y=enInst.y
				x=enInst.x-8*image_xscale
			

				with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
					sprite_index = spr_GrabEffect	
					audio_play_sound(snd_Grab,4,false)
				}
		
				enInst.state = "grabbed"
				aimAngle = 0
			}
		
		}
	}


}

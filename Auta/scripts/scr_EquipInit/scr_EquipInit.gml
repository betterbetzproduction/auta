function scr_EquipInit() {
	//HP = obj_Auta.maxhp
	with obj_Auta {
		Atk = global.baseAtk
		Def = 4
		Spd = 0

		equip = global.equipment
		//0 - Weapon
		//1 - Throw
		//2 - Armor
		//3 - Mobility
		//4 - Shield
		//5 - Subweapon
		//10 - Acc1
		//11 - Acc2
		//12 - Acc3
		switch equip[0] {
											//Swords
			case "Brush Blade":
				Atk += 1
				break;
			case "Soft-Coded Blade":
				Atk += 2
				break;
			case "Swift Blade":
				Atk += 0
				//Attrib: Fast Swing
				break;
			case "Static Blade":
				Atk += 2
				//Attrib: 1/8 chance of Electric
				break;
			case "Heavy Blade":
				Atk += 3
				Def += 1
				//Attrib: Slow Swing
				break;
		
		
												//Piercers
			case "Piercer":
				Atk += 1
				break;
			
			//Gauntlet
			case "Gauntlets":
				Atk += 0
				break;
			
			//Cannons
			case "Cannon":
				Atk += 4
				break;
		
		}
	}


}

function scr_AutaSkate() {
	//if state = "normal" || animstate = "normal" {
	//	//if keyboard_check(vk_shift) {
	//		animstate = "constant"
	//		state = "skateWindup"
	//		xSpeed = 0
	//		sprite_index = spr_AutaGrounded
		
	//		alarm[0] = (room_speed * 0.1);
	//	}
	//}

	if state = "skate" {
		if canJumpCount > 0 {
			if jumpkey && !keyboard_check(vk_down) {
				ySpeed = jumpforce
				if place_meeting(x,y+4,obj_WaterCollision) && !place_meeting(x,y+4,obj_Collision){
					ySpeed = jumpforce*0.75	
				}
			
				grounded = false
				audio_play_sound(snd_Jump,4,false)
				jumpUpwardsCancel = true
				jumpkey = false
			}	
		}
	
		if grounded {
			sprite_index = spr_AutaSkate
		} else {
			sprite_index = spr_AutaSkate_Jump
		
			if jumpUpwardsCancel {
				if ySpeed < -0.5 && !jumpheld {
					ySpeed += 0.1*spd
				}
				if ySpeed >= -0.5 {
					jumpUpwardsCancel = false	
				}
			}
		}
	}


}

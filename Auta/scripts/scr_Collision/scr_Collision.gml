function scr_Collision(argument0) {
	if abs(obj_DefaultCamera.x-x) > 300 || abs(obj_DefaultCamera.y-y) > 250 {
		exit;	
	}

	if state = "suspension" {
		exit;
	}

	var spd = global.gameSpeed
	var player = (object_get_parent(object_index) = obj_PlayerParent)
	var tempAngle = image_angle
	image_angle = 0

	y += ySpeed*spd
	if player {
		fallDist += clamp(ySpeed,0,100)
	}
	if ySpeed > 3 {
		if state != "attack" {
			ySpeed = 3	
		}
	}

	var collideOnly = argument0 //if true, the object will not "land" 

	// CEILING COLLISION

	if place_meeting(x,y+ySpeed,obj_GroundParent) && ySpeed < 0 {
	  var gdinst = instance_place(x,y+ySpeed,obj_GroundParent)
		if gdinst != noone {
			if gdinst.topOnly = false {
				var p = 0
				do {
					y+=0.1
					p+=0.1
				}
				until !place_meeting(x,y-2,obj_GroundParent) || p = 5
			
				if !(player && state = "orb" and ySpeed > 1) {
					ySpeed = 0
				}
			}
		}
	}
	
	//if grounded {
		var leftY = bbox_bottom-2
		var q = 0
		while(!position_meeting(bbox_left+2,leftY,obj_GroundParent) && q < 32) {
			leftY+=0.2	
			q+=0.2
		}
		if q = 16 {
		leftY = bbox_bottom
		}
		
		var rightY = bbox_bottom-2
		var q = 0
		while(!position_meeting(bbox_right-2,rightY,obj_GroundParent) && q < 32) {
			rightY+=0.2	
			q+=0.2
		}
		if q = 16 {
		rightY = bbox_bottom	
		}
		
		floorAngle = point_direction(bbox_left+2,leftY,bbox_right-2,rightY)
		if floorAngle > 180 {
			floorAngle = 360-floorAngle	
		}
		
		floorAngle = abs(floorAngle)
		if image_xscale = -1 && rightY < leftY {
			floorAngle = -floorAngle	
		}
		if image_xscale = 1 && rightY > leftY {
			floorAngle = -floorAngle	
		}
		
	//} else {
		//floorAngle = 0	
	//}
	
	var tempFloor = floorAngle
	if !grounded {
		floorAngle = 0	
	}

	// WALL COLLISION
	//if place_meeting(x+xSpeed,y-abs(xSpeed)-maxSpeed,obj_Collision) {
	if collision_rectangle(bbox_right+xSpeed,bbox_bottom-2-dsin(floorAngle)*16,bbox_left+xSpeed,bbox_top,obj_GroundParent,true,true) {
		var gdinst = instance_place(x+xSpeed,y,obj_GroundParent)
		if gdinst != noone {
			if gdinst.topOnly = false {
				xSpeed = 0
				if place_meeting(bbox_left-maxSpeed,y,obj_GroundParent) {
					x += 5
					var q = 0
					do {
						x-=0.2
						q+=0.2
					} until (place_meeting(x-1,y,obj_GroundParent) || q = 7)

				}
			
				if place_meeting(bbox_right+maxSpeed,y,obj_GroundParent) {
					x -= 5
					var q = 0
					do {
						x+=0.2
						q+=0.2
					} until (place_meeting(x+1,y,obj_GroundParent) || q = 7)

				}
			
			
			} else {
				x+=xSpeed*spd*dcos(abs(floorAngle))
				y -= abs(xSpeed) * spd * dsin(floorAngle)
			}
		} else {
			x+=xSpeed*spd*dcos(abs(floorAngle))
			y -= abs(xSpeed) * spd * dsin(floorAngle)
		}
	} else {
		x+=xSpeed*spd*dcos(abs(floorAngle))
		y -= abs(xSpeed) * spd * dsin(floorAngle)
	}


	floorAngle = tempFloor



	// FLOOR COLLISION / GROUNDED CHECKING

	// INITIATE WITH UPPER SHIFT

	if collideOnly = false {
		if grounded = true {
			if player {
				canJumpCount = 15
				wallrundirect = 0
				canAttackBoost = true
				canOrb = true

			}
		
			var inGround = false
			if collision_rectangle(bbox_left+2,ySpeed+bbox_bottom,bbox_right-2,ySpeed+bbox_bottom-2,obj_GroundParent,true,false) {
				inGround = true	
			}
			var q = 0
			do {
				y-=0.1	
				q+=0.1
		
				if !collision_rectangle(bbox_left+2,ySpeed+bbox_bottom,bbox_right-2,ySpeed+bbox_bottom-2,obj_GroundParent,true,false) {
					inGround = false
				}
			} until((collision_rectangle(bbox_left+2,ySpeed+bbox_bottom,bbox_right-2,ySpeed+bbox_bottom-2.1,obj_GroundParent,true,false) && inGround = false) || q = 8)

		}
	}









	if collideOnly = false {

	
		//TERMINATE WITH RETURN TO GROUND
	

	
		// LANDING
		if ySpeed > 0 && collision_rectangle(bbox_left+2,ySpeed+bbox_bottom,bbox_right-2,ySpeed+bbox_bottom+2,obj_GroundParent,true,false) {
			//if grounded = false {
				var q = 0
				do {
					y+=0.1	
					q+=0.1
				} until(place_meeting(x,y+0.4,obj_GroundParent) || q = 10)

				if player {
					if !grounded && ySpeed > 1 {
						image_yscale = 0.8
					
						if state = "normal" {
							if aRight {
								obj_Auta.image_xscale = 1
							}
							if aLeft {
								obj_Auta.image_xscale = -1	
							}
								
							if fallDist > 80 && abs(xSpeed) > 0.1 {
								obj_Auta.sprite_index = spr_AutaFlipLand
								
								obj_Auta.xSpeed = 2*image_xscale
							} else {
								obj_Auta.sprite_index = spr_AutaLand
							}
							obj_Auta.animstate = "onedone"
						
						}
					
						fallDist = 0
					}
				}
				if player && state = "orb" && ySpeed > 1{
					ySpeed = -ySpeed
					grounded = false
				} else {
					if abs(floorAngle) < 50 {
						grounded = true
		
						ySpeed = 0
					}
				}
				if q = 10 {
					y -= 10	
				}
		
			//}
	
		}
	
		if grounded = true {
			ySpeed = 0
			var q = 0
			do {
				y+=0.05	
				q+=0.05
			} until(place_meeting(x,y+0.5,obj_GroundParent) || q = 16)
			if q = 16 {
				y-=8	
			} else {
				var gdinst = instance_place(x,y+0.5,obj_GroundParent)
				if gdinst != noone {
					x += gdinst.xSpeed*spd
					y += gdinst.ySpeed*spd
				}	
		
			}
	
		}
	
	} else {
		ySpeed += grav*spd
	}
	
	


	if collideOnly = false {
		if collision_rectangle(bbox_left+2,bbox_bottom,bbox_right-2,bbox_bottom+2,obj_GroundParent,true,false) && ySpeed = 0 {
			grounded = true	
	
		} else {
	
			if grounded = true {
				if !collision_rectangle(bbox_left+2,bbox_bottom,bbox_right-2  ,bbox_bottom+2,obj_GroundParent,true,false) {
					grounded = false
				}
			}
			ySpeed += grav*spd
		
			if player {
			
				canJumpCount--
				if ySpeed < -0.5 {
					ySpeed += riseGrav*spd	
				}
			}
	
		}
	}



	//if nudge {

	//	if place_meeting(x,y,obj_NudgeParent) {
	
	//		gdinst = instance_place(x,y,obj_NudgeParent)
	
	
	
	//		if gdinst != noone {
	//			if gdinst.nudge {
		
	//				var tempNudgeOther = gdinst.nudgeWeight
	//				var tempNudge = nudgeWeight
	
	//				if nudgeWeight = 2 {
	//					gdinst.nudgeWeight = 0
	//				}
	//				if gdinst.nudgeWeight = 2 {
	//					nudgeWeight = 0
	//				}	
		
		
	//				if x > gdinst.x {
	//					if !place_meeting(x+2,y,obj_Collision) {
	//						if gdinst.nudgeWeight > 0 {
	//							x+=2
	//						}
	//					}
	//					with gdinst {
	//						if !place_meeting(x-2,y,obj_Collision) {
	//							if other.nudgeWeight > 0 {
	//								x-=2
	//							}
	//						}	
	//					}
			
	//				} else {
	//					if !place_meeting(x-2,y,obj_Collision) {
	//						if gdinst.nudgeWeight > 0 {
	//							x-=2
	//						}
	//					}
	//					with gdinst {
	//						if !place_meeting(x+2,y,obj_Collision) {
	//							if other.nudgeWeight > 0 {
	//								x+=2
	//							}
	//						}	
	//					}
	//				}
		
	//				gdinst.nudgeWeight = tempNudgeOther
	//				nudgeWeight = tempNudge
	//			}
	//		}
	//	}
	//}


	image_angle = tempAngle


	//if state = "suspension" {exit;}

	//var spd = global.gameSpeed
	//var player = (object_get_parent(object_index) = obj_PlayerParent)
	//var tempAngle = image_angle
	//image_angle = 0

	//y += ySpeed*spd
	//if ySpeed > 3 && object_index != obj_CutsceneActor {
	//	if state != "attack" {
	//		ySpeed = 3	
	//	}
	//}

	//var collideOnly = argument0 //if true, the object will not "land" 

	//// CEILING COLLISION

	//if place_meeting(x,y+ySpeed,obj_GroundParent) && ySpeed < 0 {
	//  var gdinst = instance_place(x,y+ySpeed,obj_GroundParent)
	//	if gdinst != noone {
	//		if gdinst.topOnly = false {
	//			var p = 0
	//			do {
	//				y+=0.1
	//				p+=0.1
	//			}
	//			until !place_meeting(x,y-2,obj_GroundParent) || p = 5
	//			ySpeed = 0
	//		}
	//	}
	//}

	//// WALL COLLISION
	////if place_meeting(x+xSpeed,y-abs(xSpeed)-maxSpeed,obj_Collision) {
	//if collision_rectangle(bbox_right+xSpeed,bbox_bottom-2,bbox_left+xSpeed,bbox_top,obj_Collision,true,true) {
	//	var gdinst = instance_place(x+xSpeed,y,obj_Collision)
	//	if gdinst != noone {
	//		if gdinst.topOnly = false {
	//			xSpeed = 0
	//			if place_meeting(bbox_left-maxSpeed,y,obj_Collision) {
	//				x += 5
	//				var q = 0
	//				do {
	//					x-=0.2
	//					q+=0.2
	//				} until (place_meeting(x-1,y,obj_Collision) || q = 7)

	//			}
			
	//			if place_meeting(bbox_right+maxSpeed,y,obj_Collision) {
	//				x -= 5
	//				var q = 0
	//				do {
	//					x+=0.2
	//					q+=0.2
	//				} until (place_meeting(x+1,y,obj_Collision) || q = 7)

	//			}
			
			
	//		} else {
	//			x+=xSpeed*spd
	//		}
	//	} else {
	//		x+=xSpeed*spd
	//	}
	//} else {
	//	x+=xSpeed*spd
	//}




	//// FLOOR COLLISION / GROUNDED CHECKING

	//// INITIATE WITH UPPER SHIFT

	//if collideOnly = false {
	//	if grounded = true {
	//		if player {
	//			canJumpCount = 8
	//			wallrundirect = 0
	//			canAttackBoost = true
	//			canOrb = true
	//			if !canWallRun {
	//				wallrundirect = -2	
	//			}
	//		}
		
	//		var inGround = false
	//		if place_meeting(x,y-0.4,obj_GroundParent) {
	//			inGround = true	
	//		}
	//		var q = 0
	//		do {
	//			y-=0.1	
	//			q+=0.1
		
	//			if !place_meeting(x,y-0.4,obj_GroundParent) {
	//				inGround = false
	//			}
	//		} until((place_meeting(x,y-0.5,obj_GroundParent) && inGround = false) || q = 8)

	//	}
	//}



	//if collideOnly = false {

	
	//	//TERMINATE WITH RETURN TO GROUND
	

	
	//	// LANDING
	//	if ySpeed > 0 && collision_rectangle(bbox_left,ySpeed+bbox_bottom,bbox_right,ySpeed+bbox_bottom+2,obj_GroundParent,true,false) {
	//		//if grounded = false {
	//			var q = 0
	//			do {
	//				y+=0.1	
	//				q+=0.1
	//			} until(place_meeting(x,y+1,obj_GroundParent) || q = 10)
	//			if q!= 10 {
	//				gdinst = place_meeting(x,y+1,obj_GroundParent) {
	//					y+= (gdinst.y-round(gdinst.y))
	//				}
	//			}
	//			if player {
	//				if !grounded && ySpeed > 1 {
	//					//with instance_create_depth(x,y, depth-1,obj_OneTimeFX) {
	//					//	sprite_index = spr_LandParticles
	//					//	var q = 0
	//					//	do {
	//					//		y+=0.5
	//					//		q++
	//					//	} until (place_meeting(x,y+1,obj_GroundParent) || q = 20)
	//					//	if q = 20 {instance_destroy();}
			
	//					//}
	//				}
	//			}
	//			grounded = true
		
	//			ySpeed = 0
	//			if q = 10 {
	//				y -= 10	
	//			}
		
	//		//}
	
	//	}
	
	//	if grounded = true {
	
	//		var q = 0
	//		do {
	//			y+=0.05	
	//			q+=0.05
	//		} until(place_meeting(x,y+0.5,obj_GroundParent) || q = 16)
	//		if q = 16 {
	//			y-=8	
	//		} else {
	//			var gdinst = instance_place(x,y+0.5,obj_GroundParent)
	//			if gdinst != noone {
	//				x += gdinst.xSpeed*spd
	//				y += gdinst.ySpeed*spd
	//			}	
		
	//		}
	
	//	}
	
	//} else {
	//	ySpeed += grav*spd
	//}






	////if collideOnly = false {

	
	////	//TERMINATE WITH RETURN TO GROUND
	
	////	if grounded = true {
	
	////		var q = 0
	////		do {
	////			y+=0.1	
	////			q+=0.1
	////		} until(place_meeting(x,y+0.5,obj_GroundParent) || q = 16)
	////		if q = 16 {
	////			y-=8	
	////		} else {
	////			var gdinst = instance_place(x,y+0.5,obj_GroundParent)
	////			if gdinst != noone {
	////				x += gdinst.xSpeed*spd
	////				y += gdinst.ySpeed*spd
	////			}	
		
	////		}
	
	////	}
	
	////	// LANDING
	////	if ySpeed > 0 && collision_rectangle(bbox_left,ySpeed+bbox_bottom,bbox_right,ySpeed+bbox_bottom+2,obj_GroundParent,true,false) {
	////		//if grounded = false {
	////			var q = 0
	////			do {
	////				y+=0.1	
	////				q+=0.1
	////			} until(place_meeting(x,y+1,obj_GroundParent) || q = 10)
		
	////			grounded = true
		
	////			if player {
	////				image_yscale = 0.8
	////				if state = "normal" {
	////					obj_Auta.sprite_index = spr_AutaLand
	////					obj_Auta.animstate = "onedone"
	////				}
	////			}
	////			ySpeed = 0
	////			if q = 10 {
	////				y -= 10	
	////			}
		
	////		//}
	
	////	}
	////} else {
	////	ySpeed += grav*spd
	////}
	
	


	//if collideOnly = false {
	//	if collision_rectangle(bbox_left,bbox_bottom,bbox_right,bbox_bottom+2,obj_GroundParent,true,false) && ySpeed = 0 {
	//		grounded = true	
	
	//	} else {
	
	//		if grounded = true {
	//			if !collision_rectangle(bbox_left,bbox_bottom,bbox_right,bbox_bottom+2,obj_GroundParent,true,false) {
	//				grounded = false
	//			}
	//		}
	//		ySpeed += grav*spd
	//		if player {
	//			canJumpCount--
	//			if ySpeed < -0.5 {
	//				ySpeed += riseGrav*spd	
	//			}
	//		}
	
	//	}
	//}




	////if nudge {

	////	if place_meeting(x,y,obj_NudgeParent) {
	
	////		gdinst = instance_place(x,y,obj_NudgeParent)
	
	
	
	////		if gdinst != noone {
	////			if gdinst.nudge {
		
	////				var tempNudgeOther = gdinst.nudgeWeight
	////				var tempNudge = nudgeWeight
	
	////				if nudgeWeight = 2 {
	////					gdinst.nudgeWeight = 0
	////				}
	////				if gdinst.nudgeWeight = 2 {
	////					nudgeWeight = 0
	////				}	
		
		
	////				if x > gdinst.x {
	////					if !place_meeting(x+2,y,obj_Collision) {
	////						if gdinst.nudgeWeight > 0 {
	////							x+=2
	////						}
	////					}
	////					with gdinst {
	////						if !place_meeting(x-2,y,obj_Collision) {
	////							if other.nudgeWeight > 0 {
	////								x-=2
	////							}
	////						}	
	////					}
			
	////				} else {
	////					if !place_meeting(x-2,y,obj_Collision) {
	////						if gdinst.nudgeWeight > 0 {
	////							x-=2
	////						}
	////					}
	////					with gdinst {
	////						if !place_meeting(x+2,y,obj_Collision) {
	////							if other.nudgeWeight > 0 {
	////								x+=2
	////							}
	////						}	
	////					}
	////				}
		
	////				gdinst.nudgeWeight = tempNudgeOther
	////				nudgeWeight = tempNudge
	////			}
	////		}
	////	}
	////}


	//image_angle = tempAngle


//image_angle = floorAngle
}

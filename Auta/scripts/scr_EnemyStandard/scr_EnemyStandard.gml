///@arg Hurt sprite

function scr_EnemyStandard(argument0) {
	
	if global.cutscene {
		xSpeed = 0
		ySpeed = 0
	}

	spd = global.gameSpeed


	if invincible {

		if !instance_exists(contactby){
			invincible = false	
		}
		if instance_exists(contactby) {
			if !collision_rectangle(bbox_left-5,bbox_top-5,bbox_right+5,bbox_bottom+5,contactby,true,true) {
				invincible = false
			}
		}
	}

	if state = "stun" {
		sprite_index = argument0
		grav = 0.02
		ySpeed = clamp(ySpeed,-0.4,0.4)
		xSpeed = clamp(xSpeed,-0.4,0.4)
		if grounded {
			var accel = 0.1
			var stop = 0.1
		
		} else {
			var accel = 0
			var stop = 0
		}
	
		if xSpeed > 0 {
			if xSpeed > stop {
				xSpeed -= stop	
			} else {
				xSpeed = 0	
			}
		}
		if xSpeed < 0 {
			if xSpeed < -stop {
				xSpeed += stop	
			} else {
				xSpeed = 0	
			}
		}
	
		stunCounter+=spd
		if stunCounter = 90 {
			scr_EnemyDie()	
		}
	}

	if state = "grabbed" {
		if obj_PlayerParent.state != "aim" {
			state = "stun"	
		}
		xSpeed = 0
		ySpeed = 0
		grounded = false 
	}
	
	image_speed = spd

	if !seen && distance_to_object(obj_Auta) < 100 {
		seenCounter = 8
		seen = true	
	}

	if hitstunTimer > 5 {
	
		hitstunTimer -= spd
	} else if hitstunTimer > 0 {
		hitstunTimer = -1
		sprite_index = argument0
	}
	if interest > 0 {
		interest--
	}

	var player = obj_PlayerParent
	if distance_to_object(player) < viewDist {
		canSeePlayer = true

		//if (collision_line(x-sprite_get_xoffset(sprite_index)+sprite_get_bbox_right(sprite_index),y,player.x-sprite_get_xoffset(sprite_index)+sprite_get_bbox_right(player.sprite_index),player.y,obj_Collision,1,0)) {
		//	canSeePlayer = false
		//}
		//if (collision_line(x-sprite_get_xoffset(sprite_index)+sprite_get_bbox_left(sprite_index),y,player.x-sprite_get_xoffset(sprite_index)+sprite_get_bbox_left(player.sprite_index),player.y,obj_Collision,1,0)) {
		//	canSeePlayer = false
		//}
		//if (collision_line(x,y-sprite_get_yoffset(sprite_index)+sprite_get_bbox_top(sprite_index),player.x,player.y-sprite_get_yoffset(sprite_index)+sprite_get_bbox_top(player.sprite_index),obj_Collision,1,0)) {
		//	canSeePlayer = false
		//}
		//if (collision_line(x,y-sprite_get_yoffset(sprite_index)+sprite_get_bbox_bottom(sprite_index),player.x,player.y-sprite_get_yoffset(sprite_index)+sprite_get_bbox_bottom(player.sprite_index),obj_Collision,1,0)) {
		//	canSeePlayer = false
		//}
	
		if (collision_line(x-sprite_get_xoffset(sprite_index)+sprite_get_bbox_right(sprite_index),y,player.x,player.y,obj_Collision,1,0)) {
			canSeePlayer = false
		}
		if (collision_line(x-sprite_get_xoffset(sprite_index)+sprite_get_bbox_left(sprite_index),y,player.x,player.y,obj_Collision,1,0)) {
			canSeePlayer = false
		}
		if (collision_line(x,y-sprite_get_yoffset(sprite_index)+sprite_get_bbox_top(sprite_index),player.x,player.y,obj_Collision,1,0)) {
			canSeePlayer = false
		}
		if (collision_line(x,y-sprite_get_yoffset(sprite_index)+sprite_get_bbox_bottom(sprite_index),player.x,player.y,obj_Collision,1,0)) {
			canSeePlayer = false
		}

		//if (collision_line(x,y,player.x,player.y,obj_Collision,1,0)) {
		//	canSeePlayer = false	
		//}

		if canSeePlayer {
			interest = maxInterest
			lastPlayerX = obj_PlayerParent.x
			lastPlayerY = obj_PlayerParent.y
		}
	} else {
		canSeePlayer = false	
	}

	/*var canSeePlayer = false;
	if (collision_line(x-sprite_get_xoffset(sprite_index)+sprite_get_bbox_right(sprite_index),y,player.x-sprite_get_xoffset(sprite_index)+sprite_get_bbox_right(player.sprite_index),player.y,o_wall,1,0)) {
		canSeePlayer = false
	}
	if (collision_line(x-sprite_get_xoffset(sprite_index)+sprite_get_bbox_left(sprite_index),y,player.x-sprite_get_xoffset(sprite_index)+sprite_get_bbox_left(player.sprite_index),player.y,o_wall,1,0)) {
		canSeePlayer = false
	}
	if (collision_line(x,y-sprite_get_yoffset(sprite_index)+sprite_get_bbox_top(sprite_index),player.x,player.y-sprite_get_yoffset(sprite_index)+sprite_get_bbox_top(player.sprite_index),o_wall,1,0)) {
		canSeePlayer = false
	}
	if (collision_line(x,y-sprite_get_yoffset(sprite_index)+sprite_get_bbox_bottom(sprite_index),player.x,player.y-sprite_get_yoffset(sprite_index)+sprite_get_bbox_bottom(player.sprite_index),o_wall,1,0)) {
		canSeePlayer = false
	}
	*/
	if gauntletCounter > 0 {
		visible = false
		state = "suspend"
		//y = -100
	}
	if gauntletCounter = 0 {
		gauntletCounter = -1
		visible = true
		state = "normal"
		y = baseY
	}


}

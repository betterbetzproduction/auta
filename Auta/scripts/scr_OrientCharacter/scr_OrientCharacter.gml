///@description orient character
///@arg0 character
///@arg1 image_xscale
///@arg2 sprite_index
///@arg3 jump
function scr_OrientCharacter(argument0, argument1, argument2, argument3) {

	with argument0 {
		image_xscale = argument1
		sprite_index = argument2
		if argument3 = false {
			grounded = false
			ySpeed = -2
		}
		image_index = 0
	}


}

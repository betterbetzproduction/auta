function scr_CannonballHit(argument0) {
	if state != "cannonball" && visible{
		if other.state = "cannonball" {
			other.type = "cannonball"
		
			var angle = point_direction(other.x,other.y,x,y)
		
			other.Xknockback = other.cannonballXknockback*sign(other.xSpeed)
			other.Yknockback = other.cannonballYknockback
		
		
			other.attack = other.cannonballdmg
			scr_EnemyHurt(argument0)
	
			if other.cannonballredirect {
				if place_meeting(bbox_left,y,other) {
					other.xSpeed = -other.xSpeed
				}
				if place_meeting(bbox_right,y,other) {
					other.xSpeed = -other.xSpeed
				}
				if place_meeting(x,bbox_top,other) {
					other.ySpeed = -other.ySpeed
				}
				if place_meeting(x,bbox_bottom,other) {
					other.ySpeed = -other.ySpeed
				}
			}
		}
	}


}

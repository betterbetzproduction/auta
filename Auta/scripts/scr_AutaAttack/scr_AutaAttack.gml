function scr_AutaAttack() {
	if equip[0] = "Brush Blade" || equip[0] = "Soft-Coded Blade" || equip[0] = "Swift Blade" || equip[0] = "Heavy Blade" {
		var weaponType = "Sword"	
	}
	if equip[0] = "Piercer" {
		var weaponType = "Pierce"	
	}
	if equip[0] = "Cannon" {
		var weaponType = "Cannon"	
	}
	if equip[0] = "Gauntlets" {
		var weaponType = "Gauntlets"	
	}
	
	pad = global.pad
	atkKey = false
	if aAttack {
		atkCounter = 15
	}
	//if weaponType = "Gauntlets" && (keyboard_check(kAttack) || gamepad_button_check(global.pad, pAttack)) {
	//	atkCounter = 5	
	//}
	if atkCounter > 0 {
		atkCounter--
		atkKey = true
	}
	
	var haxis = gamepad_axis_value(pad, gp_axislh);
	var vaxis = gamepad_axis_value(pad, gp_axislv);
	var joystick = sqrt(power(haxis,2) + power(vaxis,2)) > 0.2
	keyPressTimer -= 1
	var doubleKeyPress = false
	if keyboard_check_pressed(vk_anykey) {
		if keyPressTimer > 0 && keyboard_key = keyPressLast {
			//doubleKeyPress = true	
		} else {
			keyPressLast = keyboard_key	
			keyPressTimer = 12
		}
	}	
	var it = 12

	inputTimer--

	if aRightTap {
		ds_list_add(inputList, "right")
		inputTimer = it
	}
	if aLeftTap {
		ds_list_add(inputList, "left")
		inputTimer = it
	}
	if aUpTap {
		ds_list_add(inputList, "up")
		inputTimer = it
	}
	if aDownTap {
		ds_list_add(inputList, "down")
		inputTimer = it
	}

	if inputTimer <= 0 {
		ds_list_clear(inputList)
	}


	if (state = "normal" ) || state = "skate" {
		//if place_meeting(x,y,obj_Collision) && grounded {
		//	mask_index = spr_AutaIdle_ShortMask 
		//	if !place_meeting(x,y,obj_Collision) {
		//		sprite_index = spr_AutaSlide
		//		global.screenshake = 1
		//		with instance_create_layer(x,y,layer,obj_OneTimeFX) {
		//			image_xscale = other.image_xscale
		//			sprite_index = spr_TessioRollParticle
		//		}
		//		state = "slide"
		//		mask_index = spr_AutaIdle_ShortMask 	
		//		xSpeed = maxSpeed*image_xscale
		//		slidecounter = 0
		//	}
		
		//}
	
		/// ORB --------------------------- ORB ---------------------------- ORB ///
		if aOrb {
			if canOrb && haveOrb {
				state = "orb"
				trail = true
				animstate = "constant"
				sprite_index = spr_AutaOrb
				orbDelay = 20
				iframes = 20
				canOrb = false
				
				hangTime = 10
		
				var angle = 0
		
				if image_xscale = 1 {
					angle = 10	
					if !grounded {
						angle = 0	
					}
				}
				if image_xscale = -1 {
					angle = 170	
					if !grounded {
						angle = 180	
					}
				}
		
				if !joystick {
					if aRight {
						angle = 10	
						if !grounded {
							angle = 0	
						}
					}
					if aLeft {
						angle = 170	
						if !grounded {
							angle = 180	
						}
					}
					if aUp {
						angle = 90
					}
					if aDown {
						angle = 270
					}
		
					if aRight && aDown {
						angle = 305	
					}
					if aLeft && aDown {
						angle = 225	
					}
		
					if aRight && aUp {
						angle = 45	
					}
					if aLeft && aUp {
						angle = 135	
					}
				}
				if joystick {
					var haxis = gamepad_axis_value(pad, gp_axislh);
					var vaxis = gamepad_axis_value(pad, gp_axislv);
					angle = point_direction(0, 0, haxis, vaxis);	
				}
		
				grounded = false
		
				xSpeed = dcos(angle)*orbPower
				ySpeed = -dsin(angle)*orbPower
		
				with instance_create_depth(x,y,depth-1,obj_OneTimeFX) {
					sprite_index = spr_AutaDisintegrate	
					audio_play_sound(snd_OrbThrow,4,false)
				}
		
				aOrb = false
			}
		
		
		}
	
		/// ATTACK --------------------------- ATTACK ---------------------------- ATTACK ///
	
		if atkKey && !place_meeting(x,y,obj_InteractDialogue){
			if aRight {
				image_xscale = 1	
				xSpeed = clamp(xSpeed,maxSpeed,maxSpeed*1.8)
			}
			else if aLeft {
				image_xscale = -1	
				xSpeed = clamp(xSpeed,-maxSpeed*1.8,-maxSpeed)
			} else {
				xSpeed = clamp(xSpeed,-maxSpeed*1.8,maxSpeed*1.8)
			}
			image_index = 0
		

			mask_index = spr_AutaIdle
			if weaponType = "Sword" {
				

				if comboNumber = 4 {
					comboNumber = 1	
				}
		
				if !aDown && !aUp {
					//if !grounded {
					//	comboNumber = 1	
					//}
					if comboNumber = 1 {
					
						sprite_index = spr_Auta_ComboAttackA
						atkStateGrav = 0
						state = "attack"
					
						audio_play_sound(snd_MagicSwing,4,false);
						with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
							attack = other.Atk
							Xknockback = 0.7
							Yknockback = 0.5
							image_xscale = obj_PlayerParent.image_xscale
							killsprite = other.sprite_index
							type = "slice"
							sprite_index = spr_Auta_ComboAttackA_Hitbox
							visible = true
							image_xscale *= 0.95
							if other.equip[0] = "Swift Blade" {
								other.imgSpd = 1.15
								image_xscale *= 0.75
							}
							if other.equip[0] = "Heavy Blade" {
								other.imgSpd = 0.7
							}
						}
					
					}
					if comboNumber = 2 {
						sprite_index = spr_Auta_ComboAttackB
						atkStateGrav = 0
						state = "attack"
						//xSpeed = 1.5*image_xscale*(aRight || aLeft)
						//ySpeed = 0
						audio_play_sound(snd_MagicSwing,4,false);
						with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
							attack = other.Atk
							Xknockback = 0.7
							Yknockback = 0.5
							image_xscale = obj_PlayerParent.image_xscale
							type = "slice"
							killsprite = other.sprite_index
							sprite_index = spr_Auta_ComboAttackB_Hitbox
							visible = true
							image_xscale *= 0.95
							if other.equip[0] = "Swift Blade" {
								other.imgSpd = 1.15
								image_xscale *= 0.75
							}
							if other.equip[0] = "Heavy Blade" {
								other.imgSpd = 0.7
							}
						}
					}
					if comboNumber = 3  {
						sprite_index = spr_AutaAerial
						atkStateGrav = 0
						state = "attack"
						//xSpeed = 1.5*image_xscale*(aRight || aLeft)
						//ySpeed = 0
						audio_play_sound(snd_MagicSwing,4,false);
						with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
							attack = other.Atk
							Xknockback = 2
							Yknockback = 0.9
							image_xscale = obj_PlayerParent.image_xscale
							type = "slice"
							killsprite = other.sprite_index
							sprite_index = spr_Auta_ComboAttackC_Hitbox
							visible = true
							image_xscale *= 0.95
							if other.equip[0] = "Swift Blade" {
								other.imgSpd = 1.15
								image_xscale *= 0.75
							}
							if other.equip[0] = "Heavy Blade" {
								other.imgSpd = 0.7
							}
						}
					}
				
				}
				if aDown && !grounded {
					sprite_index = spr_Auta_Sword_To_DownAerial
					image_index = 0
					state = "attack"
					//xSpeed = 2*image_xscale
					//ySpeed = -0.5
					//atkStateGrav = 1
					animstate = "onedone"
				}
				
			
				if aUp {
				
					var jumpkick = false
					if ds_list_size(inputList) >= 3 {
						if ds_list_find_value(inputList,ds_list_size(inputList)-1) = "up" {
							if ds_list_find_value(inputList,ds_list_size(inputList)-2) = "down" {
								if ds_list_find_value(inputList,ds_list_size(inputList)-3) = "up" {
									 jumpkick = true
								}
							}
						}
					}
				
					if !grounded {
						state = "attack"
						sprite_index = spr_Auta_AntiAerial
						atkStateGrav = 0
						if ySpeed > -1 && canAttackBoost {
							ySpeed = -1	
							canAttackBoost = false
						}
						audio_play_sound(snd_UpwardsAttack,4,false);
						with instance_create_layer(x,y,layer,obj_PlayerParentHitbox) {
							attack = other.Atk
							Xknockback = 0.5
							Yknockback = 2
							image_xscale *= 0.95
							type = "slice"
							killsprite = other.sprite_index
							sprite_index = spr_Auta_AntiAerial_Hitbox
							stopme = false
							visible = true
						
							if other.equip[0] = "Swift Blade" {
								other.imgSpd = 1.15
								image_xscale *= 0.75
							}
							if other.equip[0] = "Heavy Blade" {
								other.imgSpd = 0.7
							}
						}
					}
					if grounded {
					
						if jumpkick {
							image_index = 0
							xSpeed = 0.5*image_xscale
							sprite_index = spr_AutaUppercutWindup
							state = "attack"
						} else {
							
							sprite_index = spr_AutaUpTilt
							atkStateGrav = 0
							state = "attack"
							//xSpeed = 1.5*image_xscale*(aRight || aLeft)
							//ySpeed = 0
							audio_play_sound(snd_MagicSwing,4,false);
							with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
								attack = other.Atk
								Xknockback = 0.3
								Yknockback = 3
								image_xscale = obj_PlayerParent.image_xscale
								type = "slice"
								killsprite = other.sprite_index
								sprite_index = spr_AutaUpTilt_Hitbox
								visible = true
								image_xscale *= 0.95
								image_yscale*=1.25
								if other.equip[0] = "Swift Blade" {
									other.imgSpd = 1.15
									image_xscale *= 0.75
								}
								if other.equip[0] = "Heavy Blade" {
									other.imgSpd = 0.7
								}
							}
						}
					}
				}
			}
			
			if weaponType = "Gauntlets" {
				if comboNumber = 4 {
					comboNumber = 1	
				}
				if !aDown && !aUp {

					if comboNumber = 1 {
					
						sprite_index = spr_Auta_GauntletCombo_A
						if !grounded {
							sprite_index = spr_Auta_GauntletCombo_A_Air
						}
						atkStateGrav = 0
						state = "attack"
					
						audio_play_sound(snd_MagicSwing,4,false);
						with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
							attack = other.Atk
							Xknockback = 0.7
							Yknockback = 0.5
							image_xscale = obj_PlayerParent.image_xscale
							killsprite = other.sprite_index
							type = "strike"
							sprite_index = spr_Auta_GauntletCombo_A_Hitbox
							visible = true
							image_xscale *= 1
							if !other.grounded {
								image_angle = -12*other.image_xscale
								y+=1
							}
						}
					
					}
					if comboNumber = 2 {
						sprite_index = spr_Auta_GauntletCombo_B
						if !grounded {
							sprite_index = spr_Auta_GauntletCombo_B_Air
						}
						atkStateGrav = 0
						state = "attack"
						//xSpeed = 1.5*image_xscale*(aRight || aLeft)
						//ySpeed = 0
						audio_play_sound(snd_MagicSwing,4,false);
						with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
							attack = other.Atk
							Xknockback = 0.7
							Yknockback = 0.5
							image_xscale = obj_PlayerParent.image_xscale
							type = "strike"
							killsprite = other.sprite_index
							sprite_index = spr_Auta_GauntletCombo_B_Hitbox
							visible = true
							image_xscale *= 1
							if !other.grounded {
								image_angle = -12*other.image_xscale
								y+=1
							}
						}
					}
					if comboNumber = 3  {
						sprite_index = spr_Auta_GauntletCombo_C
						atkStateGrav = 0
						state = "attack"
						//xSpeed = 1.5*image_xscale*(aRight || aLeft)
						//ySpeed = 0
						audio_play_sound(snd_MagicSwing,4,false);
						with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
							attack = other.Atk
							Xknockback = 2
							Yknockback = 0.9
							image_xscale = obj_PlayerParent.image_xscale
							type = "strike"
							killsprite = other.sprite_index
							sprite_index = spr_Auta_GauntletCombo_C_Hitbox
							visible = true
						}
					}
				
				}
				if aDown && !grounded {
					sprite_index = spr_AutaStompWindup
					image_index = 0
					state = "attack"
					xSpeed = 2*image_xscale
					ySpeed = -1
					//atkStateGrav = 1
					animstate = "onedone"
				}
				
			
				if aUp {
					
					if grounded {
						state = "attack"
						sprite_index = spr_Auta_Gauntlet_UpTilt_Windup
						
						
					}
					if !grounded {
						state = "attack"
						sprite_index = spr_Auta_Gauntlet_UpAerial
						audio_play_sound(snd_UpwardsAttack,4,false);
						atkStateGrav = 0
						if ySpeed > -1 && canAttackBoost {
							ySpeed = -2
							canAttackBoost = false
						}
						with instance_create_layer(x,y,layer,obj_PlayerParentHitbox) {
							attack = other.Atk
							Xknockback = 0.5
							Yknockback = 3
							type = "strike"
							killsprite = other.sprite_index
							sprite_index = spr_Auta_Gauntlet_UpAerial_Hitbox
							stopme = false
							visible = true
							shiftY = -15
							shiftX = -7
						}
					}
				}
			}	
			if weaponType = "Pierce" {
				if comboNumber = 3 {
					comboNumber = 1	
				}
				if !aDown && !aUp {

					if comboNumber = 1 {
					
						sprite_index = spr_Auta_PierceCombo_A

						atkStateGrav = 0
						state = "attack"
					
						audio_play_sound(snd_MagicSwing,4,false);
						with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
							attack = other.Atk
							Xknockback = 0.7
							Yknockback = 0.5
							image_xscale = obj_PlayerParent.image_xscale
							killsprite = other.sprite_index
							type = "slice"
							sprite_index = spr_Auta_PierceCombo_A_Hitbox
							visible = true
							image_xscale *= 1

							point = instance_nearest(x,y,obj_EnemyParent)
							image_angle = point_direction(x,y,point.x,point.y)
							image_angle = clamp(image_angle,-35,35)
							if image_xscale = -1 {
								image_angle-=180	
							}

						}
					
					}
					if comboNumber = 2 {
						sprite_index = spr_Auta_PierceCombo_B

						atkStateGrav = 0
						state = "attack"
					
						audio_play_sound(snd_MagicSwing,4,false);
						with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
							attack = other.Atk
							Xknockback = 0.7
							Yknockback = 0.5
							image_xscale = obj_PlayerParent.image_xscale
							killsprite = other.sprite_index
							type = "slice"
							sprite_index = spr_Auta_PierceCombo_A_Hitbox
							visible = true
							image_xscale *= 1

							point = instance_nearest(x,y,obj_EnemyParent)
						
							image_angle = point_direction(x,y,point.x,point.y)
							image_angle = clamp(image_angle,-35,35)
							if image_xscale = -1 {
								//image_angle-=180	
							}

						}
					}
				
				}
				if aDown && !grounded {
					sprite_index = spr_AutaStompWindup
					image_index = 0
					state = "attack"
					xSpeed = 2*image_xscale
					ySpeed = -1
					//atkStateGrav = 1
					animstate = "onedone"
				}
				
			
				if aUp {
					
					if grounded {
						state = "attack"
						sprite_index = spr_Auta_Gauntlet_UpTilt_Windup
						
						
					}
					if !grounded {
						state = "attack"
						sprite_index = spr_Auta_Gauntlet_UpAerial
						audio_play_sound(snd_UpwardsAttack,4,false);
						atkStateGrav = 0
						if ySpeed > -1 && canAttackBoost {
							ySpeed = -2
							canAttackBoost = false
						}
						with instance_create_layer(x,y,layer,obj_PlayerParentHitbox) {
							attack = other.Atk
							Xknockback = 0.5
							Yknockback = 3
							type = "strike"
							killsprite = other.sprite_index
							sprite_index = spr_Auta_Gauntlet_UpAerial_Hitbox
							stopme = false
							visible = true
							shiftY = -15
							shiftX = -7
						}
					}
				}
			}
		
		
			
		
			if global.currentweapon = "Prism" {
				state = "suspend"
				animstate = "constant"
				sprite_index = spr_AutaToPrism
				image_index = 0
			
				keyboard_clear(ord("A"))
			}
			if weaponType = "Cannon" {
				if !aDown && !aUp {
					if ySpeed > 0 && canAttackBoost {
						ySpeed = 0
						canAttackBoost = false
					}
					sprite_index = spr_AutaAttackBurst
					atkStateGrav = 0
					state = "attack"
					xSpeed = 0
					audio_play_sound(snd_MagicSwing,4,false);
					with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
						selfDependant = false
						attack = other.Atk
						Xknockback = 1.5
						Yknockback = 1.5
						image_xscale = obj_PlayerParent.image_xscale
						killsprite = other.sprite_index
						type = "slice"
						stopme = false
						sprite_index = spr_AutaAttackBurst_Hitbox
						visible = true

					}
					
				
				}
				if aDown {
					if ySpeed > 0 && canAttackBoost {
						ySpeed = 0
						canAttackBoost = false
					}
					sprite_index = spr_AutaPiercer
					atkStateGrav = 0
					state = "attack"
					xSpeed = 0
					audio_play_sound(snd_MagicSwing,4,false);
					with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
						selfDependant = true
						attack = other.Atk
						Xknockback = 0.2
						Yknockback = 0.5
						image_angle = -45*other.image_xscale
						image_xscale = obj_PlayerParent.image_xscale
						killsprite = other.sprite_index
						type = "slice"
						stopme = false
						sprite_index = spr_AutaPiercer_Hitbox
						visible = true

					}
				}
				
			
				if aUp {
					if ySpeed > 0 && canAttackBoost {
						ySpeed = 0
						canAttackBoost = false
					}
					sprite_index = spr_AutaPiercer
					atkStateGrav = 0
					state = "attack"
					xSpeed = 0
					audio_play_sound(snd_MagicSwing,4,false);
					with instance_create_depth(x,y,depth-1,obj_PlayerParentHitbox) {
						selfDependant = true
						attack = other.Atk
						image_angle = 45*other.image_xscale
						Xknockback = 0.2
						Yknockback = 0.5
						image_xscale = obj_PlayerParent.image_xscale
						killsprite = other.sprite_index
						type = "slice"
						stopme = false
						sprite_index = spr_AutaPiercer_Hitbox
						visible = true

					}
				}
			}

		
			animstate = "onedone"
		}
		if doubleKeyPress {
			if keyPressLast = vk_left {
				state = "suspend"
				animstate = "constant"
				sprite_index = spr_AutaToPrism
				image_index = 0
				image_xscale = -1
			
				keyboard_clear(ord("A"))
			}
			if keyPressLast = vk_right {
				state = "suspend"
				animstate = "constant"
				sprite_index = spr_AutaToPrism
				image_index = 0
				image_xscale = 1
			
				keyboard_clear(ord("A"))
			}
		}
	
		/// SPECIAL --------------------------- SPECIAL ---------------------------- SPECIAL ///
	
		if aSpecial {
			if equip[5] != "Nothing" {
			
				switch equip[5] {
					case "Fizzle Card":
						if MP > 4 {
							if aLeft {image_xscale = -1}
							if aRight {image_xscale = 1}
				
							sprite_index = spr_AutaPiercer
							atkStateGrav = 0
							state = "attack"
							image_index = 0
							animstate = "onedone"
							with instance_create_layer(x+6*image_xscale,y-3,layer,obj_Fizzle1) {
								image_xscale = other.image_xscale
								xSpeed = 2*image_xscale
							}
						
							MP-=4
						}
				
						break;
					case "Smolder Card":
						if MP > 4 {
							if aLeft {image_xscale = -1}
							if aRight {image_xscale = 1}
				
							sprite_index = spr_AutaGenForwards
							atkStateGrav = 0
							state = "attack"
							image_index = 0
							xSpeed = 0
							animstate = "onedone"
							with instance_create_layer(x+6*image_xscale,y-3,layer,obj_SmolderGen) {
								image_xscale = other.image_xscale
								//xSpeed = 2*image_xscale
							}
						
							MP-=4
						}
				
						break;
				}
			}
		
		}
	
	
		//if keyboard_check_pressed(ord("S")) {
		//	state = "grab"
		//	sprite_index = spr_AutaRush
		//	xSpeed = 5*image_xscale
		//	animstate = "constant"
		//	slidecounter = 10
		//	keyboard_clear(ord("S"))
		
		//}
		//if grounded {
		//	if keyboard_check_pressed(vk_down) {
		//		//animstate = "constant"
		//		sprite_index = spr_AutaSlide
		//		global.screenshake = 1
		//		with instance_create_layer(x,y,layer,obj_OneTimeFX) {
		//			image_xscale = other.image_xscale
		//			sprite_index = spr_TessioRollParticle
		//		}
		//		state = "slide"
		//		mask_index = spr_AutaIdle_ShortMask 	
		//		xSpeed = 2.5*image_xscale
		//		slidecounter = 0
		//	}
		//}
		atkKey = false
		atkCounter = 0
		
	}

	if state = "prismDash" {
		iframes = 2
		nudge = false
		trail = true
	
		ySpeed = 0
		animstate = "constant"
	
		if xSpeed = 0 {
			mask_index = spr_AutaIdle
			grounded = false
			state = "attack"
			sprite_index = spr_AutaOutPrismDash
			animstate = "onedone"
			trail = false
		}
		slidecounter += spd
		if slidecounter >= 20 {
			mask_index = spr_AutaIdle
			grounded = false
			state = "attack"
			sprite_index = spr_AutaOutPrismDash
			animstate = "onedone"
			xSpeed = 2*sign(xSpeed)
			trail = false
		}
	}
	if grounded && (sprite_index = spr_Auta_AntiAerial || sprite_index = spr_Auta_Gauntlet_UpAerial) {
		sprite_index = spr_Auta_UpSword_Recovery
		image_index = 0
		state = "attack"
		animstate = "onedone"
	
	}
	if grounded && (sprite_index = spr_Auta_Sword_DownAerial) {
		sprite_index = spr_Auta_DownSword_Recovery
		image_index = 0
		state = "attack"
		animstate = "onedone"
	
	}

}

function scr_CollisionNew(argument0) {
	if abs(obj_DefaultCamera.x-x) > 300 || abs(obj_DefaultCamera.y-y) > 250 {
		exit;	
	}

	if state = "suspension" {
		exit;
	}

	var spd = global.gameSpeed
	var player = (object_get_parent(object_index) = obj_PlayerParent)
	var tempAngle = image_angle
	image_angle = 0
	
	y += ySpeed*spd
	
	if player {
		fallDist += clamp(ySpeed,0,100)
	}
	if ySpeed > 3 {
		if state != "attack" {
			ySpeed = 3	
		}
	}

	var collideOnly = argument0 //if true, the object will not "land" 

	// CEILING COLLISION

	if place_meeting(x,y+ySpeed,obj_GroundParent) && ySpeed < 0 {
	  var gdinst = instance_place(x,y+ySpeed,obj_GroundParent)
		if gdinst != noone {
			if gdinst.topOnly = false {
				var p = 0
				do {
					y+=0.1
					p+=0.1
				}
				until !place_meeting(x,y-2,obj_GroundParent) || p = 5
			
				if !(player && state = "orb" and ySpeed > 1) {
					ySpeed = 0
				}
			}
		}
	}
	
	//LeftPos
	var leftY = bbox_bottom-2
	var q = 0
		
	while(!position_meeting(bbox_left+2,leftY,obj_GroundParent) && q < 32) {
		leftY+=0.2	
		q+=0.2
	}
		
	if q = 32 {
		leftY = bbox_bottom
	}
		
	//RightPos
	var rightY = bbox_bottom-2
	var q = 0
		
	while(!position_meeting(bbox_right-2,rightY,obj_GroundParent) && q < 32) {
		rightY+=0.2	
		q+=0.2
	}
		
	if q = 32 {
		rightY = bbox_bottom	
	}
		
	//CalcFloorAngle
	floorAngle = point_direction(bbox_left+2,leftY,bbox_right-2,rightY)
		
	if floorAngle > 180 {
		floorAngle = 360-floorAngle	
	}
		
	floorAngle = abs(floorAngle)
	if image_xscale = -1 && rightY < leftY {
		floorAngle = -floorAngle	
	}
	if image_xscale = 1 && rightY > leftY {
		floorAngle = -floorAngle	
	}
		
		
	
	
	//Floor Collision
	
	if collision_rectangle(bbox_left+2,bbox_bottom+2,bbox_right-2,bbox_bottom-2 , obj_GroundParent, true, true) && ySpeed >= 0 {
		if abs(floorAngle) < 60 {
			
			if player {
				canJumpCount = 15
				wallrundirect = 0
				canAttackBoost = true
				canOrb = true
				if !grounded && ySpeed > 1 {
					image_yscale = 0.8
					switch object_index {
						case obj_Auta:
							if state = "normal" {
								image_index = 0
								if fallDist > 80 {
									obj_Auta.sprite_index = spr_AutaFlipLand
									
									if aRight {
										obj_Auta.image_xscale = 1
									}
									if aLeft {
										obj_Auta.image_xscale = -1	
									}
									obj_Auta.xSpeed = 2*image_xscale
								} else {
									obj_Auta.sprite_index = spr_AutaLand
								}
								obj_Auta.animstate = "onedone"
						
							}
							break;
						
						case obj_Tess:
							if state = "normal" {
								image_index = 0
								obj_Tess.sprite_index = spr_Tess_Land
								obj_Tess.animstate = "onedone"
						
							}
							break;
					
					}
					
					fallDist = 0
				}
			}
			
			if player && state = "orb" && ySpeed > 1{
				ySpeed = -ySpeed
				grounded = false
			} else {
				grounded = true
				ySpeed = 0
				
				var gdinst = instance_place(x,y+0.5,obj_GroundParent)
				if gdinst != noone {
					x += gdinst.xSpeed*spd
					y += gdinst.ySpeed*spd
				}	
			}
			
			y-=4
			while !collision_rectangle(bbox_left+2,bbox_bottom,bbox_right-2,bbox_bottom-2 , obj_GroundParent, true, true) && q < 16 {
				y+=0.02
				q+=0.02
			}
			if q = 16 {
				y-=12
			}
		}
	} else {
		grounded = false
		ySpeed+=grav*spd
		
		if player {
			canJumpCount--
			if ySpeed < -0.5 {
				ySpeed += riseGrav*spd	
			}
		}
	}
	
	
	//Wall Collision
	
	var bottomPadWallCheck = 4
	if grounded {
		bottomPadWallCheck = 4 + abs(xSpeed)*dsin(floorAngle)
	}
	if collision_rectangle(bbox_left+xSpeed,bbox_top,bbox_right+xSpeed,bbox_bottom-bottomPadWallCheck,obj_Collision,true,true) {
		xSpeed = 0
		
		//Push away from right wall
		if  collision_rectangle(bbox_left,bbox_top,bbox_right,bbox_bottom-bottomPadWallCheck,obj_Collision,true,true) {
			
			var goDir = 0
			var add = 0
			
			do {
				add++
				if !collision_rectangle(bbox_left-add,bbox_top,bbox_right-add,bbox_bottom-bottomPadWallCheck,obj_Collision,true,true) {
					goDir = -1
				}
				if !collision_rectangle(bbox_left+add,bbox_top,bbox_right+add,bbox_bottom-bottomPadWallCheck,obj_Collision,true,true) {
					goDir = 1
				}
			} until(goDir!=0 || add = 32)
			
			if add = 32 {
				y-=1	
			} else {
				if goDir = -1 {
					while collision_rectangle(bbox_left+1,bbox_top,bbox_right+1,bbox_bottom-bottomPadWallCheck,obj_Collision,true,true) {
						x-=0.1	
					}
				} else {
					while collision_rectangle(bbox_left-1,bbox_top,bbox_right-1,bbox_bottom-bottomPadWallCheck,obj_Collision,true,true) {
						x+=0.1	
					}
				}
			}
		}

		
	} else {
		if grounded {
			x+=xSpeed*dcos(abs(floorAngle))*spd
			y-=abs(xSpeed)*dsin(floorAngle)*spd
		} else {
			x+=xSpeed*spd
		}
	}



	//var tempFloor = floorAngle
	//if !grounded {
	//	floorAngle = 0	
	//}

	
}

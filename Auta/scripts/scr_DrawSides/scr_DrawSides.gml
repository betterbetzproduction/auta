function scr_DrawSides(argument0, argument1, argument2, argument3, argument4) {

	gpu_set_blendmode(bm_add)
	var temp = image_alpha

	var xadd = argument0
	var yadd = argument4

	image_alpha = argument1

	x+=xadd
	y+=yadd
	image_blend = argument2
	//image_xscale = distance_to_object(obj_DefaultCamera)/50
	draw_self()

	x-=xadd*2
	y-=yadd*2
	image_blend = argument3
	//image_xscale = distance_to_object(obj_DefaultCamera)/50
	draw_self()

	gpu_set_blendmode(bm_normal)
	x+=xadd
	y+=yadd

	image_blend = c_white
	image_alpha = temp


}

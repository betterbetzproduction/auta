function scr_HurtState() {
	if state = "hurt" {
		iframes = 60
		hurtcounter-=global.gameSpeed
		if grounded || hurtcounter = 0{
			state = "normal"
			animstate = "normal"
		}
	}
	if iframes > 0 {
		iframes-=global.gameSpeed
		image_alpha = 0.5
	} else {
		image_alpha = 1	
	}
	
	if state = "knockback" {
		iframes = 60
		
		if grounded {
			state = "attack"
			animstate = "onedone"
			sprite_index = spr_Auta_UpSword_Recovery
			image_index = 0
		}
		
	}


}

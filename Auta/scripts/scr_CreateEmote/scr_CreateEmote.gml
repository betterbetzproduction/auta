function scr_CreateEmote(argument0, argument1) {
	var object = argument0
	var sprite = argument1
	emoteInst = instance_create_layer(object.x,object.bbox_top-5,"Instances",obj_OneTimeFX)
	with emoteInst {
		sprite_index = sprite
		follow = object
	}	


}

function scr_AutaSpecial() {
	if state = "propeller" {
		
		sprite_index = spr_AutaPropeller	
		
		if place_meeting(x,y-5,obj_Collision) {
			specCounter = 90	
		}
		
		specCounter += global.gameSpeed*0.6
		if ySpeed > -1.5 {
			ySpeed -= 0.2
		} else {
			ySpeed = -1.5
		}
		if specCounter >= 90 {
			sprite_index = spr_AutaJump
			state = "normal"
			animstate = "normal"
		
			with instance_create_depth(x,y-5,depth-1,obj_OneTimeFX) {
				sprite_index = spr_LargeBurst	
				image_xscale = 0.5
				image_yscale = 0.5
				audio_play_sound_at(snd_EnemyDeath,x,y,0,50,300,1,false,4);
			}
			canOrb = true
		}
	
		var accel = 0.1*spd	
		var stop = 0.1*spd
	
		if keyboard_check(vk_right) {
			if xSpeed < maxSpeed {
				if !place_meeting(x+maxSpeed,y-4,obj_Collision) {
					xSpeed += accel	
				}
			}
			if xSpeed > maxSpeed {
				xSpeed = maxSpeed	
			}
		}
		if keyboard_check(vk_left) {
			if xSpeed > -maxSpeed {
				if !place_meeting(x-maxSpeed,y-4,obj_Collision) {
					xSpeed -= accel	
				}
			}
			if xSpeed < -maxSpeed {
				xSpeed = -maxSpeed	
			}
		}
		if !keyboard_check(vk_left) && !keyboard_check(vk_right) {
			if xSpeed > 0 {
				if xSpeed > stop {
					xSpeed -= stop	
				} else {
					xSpeed = 0	
				}
			}
			if xSpeed < 0 {
				if xSpeed < -stop {
					xSpeed += stop	
				} else {
					xSpeed = 0	
				}
			}
		}
	}


}

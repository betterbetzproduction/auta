function scr_DialogueList() {
	activated = true
	done = false
	var text = ""
	var mySpeaker = -1
	var myEmotion = 0
	var myScript = 0
	var myEffects = 0

	switch (textID) {
		case "LockedDoor":
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "The door is locked tight. You'll need a key.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			create_dialogue(text, mySpeaker,0,0,0,0,myScript,0,myEmotion);
			
			break;
		case "UnlockGate":
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "The gate swings open!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myScript[t] = [scr_OrientCharacter,inst_1C35C1C7,1,spr_SittingGuy,false]
			
			t++
			
			text[t] = "Oh, uh...";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myScript[t] = [scr_OrientCharacter,obj_Auta,-1,spr_AutaIdle,true]
			
			t++
			
			text[t] = "Watch your step in there.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myScript[t] = 0
			
			t++
			
			text[t] = "I've kind of been neglecting my cleaning duties lately...";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myScript[t] = 0
			
			create_dialogue(text, mySpeaker,0,0,0,0,myScript,0,myEmotion);
			
			break;
		case "RianTalk_1":
		
			speaker[0] = scr_RianDialogueInit()
		
			var t = 0//line 1
		
			text[t] = "Hey! Thanks for demoing this game.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++;//line 2
		
			text[t] = "Here's the controls: Arrow keys to move, up key to jump, down to slide, A to attack and S to throw your orb!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++;//line 2
		
			text[t] = "Remember, if an enemy turns blue after you attack it, you can throw your orb at it to grab it.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++;//line 3
		
			text[t] = "Please watch for glitches, things that need improving, etc.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++;//line 3
		
			text[t] = "And, if this is your first runthrough, Please set a stopwatch and report how long it takes.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++;//line 3
		
			text[t] = "Hope you have fun!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,0,0,0,0,myScript,0,myEmotion);
		
			break;
		case "RianSecret_1":
		
			speaker[0] = scr_RianDialogueInit()
		
			var t = 0//line 1
		
			text[t] = "Well, between you and me, I didn't know what kinda secret to put here yet.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++;//line 2
		
			text[t] = "So... let's just keep this room a secret?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,0,0,0,0,myScript,0,myEmotion);
		
			break;
		case "RianTalk_2":
		
			speaker[0] = scr_RianDialogueInit()
		
			var t = 0//line 1
		
			text[t] = "And that's all!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++;//line 2
		
			text[t] = "Please let me know your feedback as soon as you can.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,0,0,0,0,myScript,0,myEmotion);
		
			break;
		case "RianSecret_2":
		
			speaker[0] = scr_RianDialogueInit()
		
			var t = 0//line 1
		
			text[t] = "Also, yes. Yes I did self-insert.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++;//line 2
		
			text[t] = "I'm the CREATOR, bitch! I can do whatever I want!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 1
		
			create_dialogue(text, mySpeaker,0,0,0,0,myScript,0,myEmotion);
		
			break;
		
		
	///////////////////// HANGAR NPCS /////////////////////////		
		case "Tut_HoldOrb":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "USEFUL TIP!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++;//line 2
			
			text[t] = "Remember, you can stay in orb form for longer by holding down the orb button!"
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			create_dialogue(text, mySpeaker,0,0,0,0,0,0,myEmotion);
		
			break;

		case "HA_NPC_Ex1":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "Another one...?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myEffects[t] = [0, 0];
		
			t++;//line 2
		
			text[t] = "Where's your mask? You're gonna get snaaaatched.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myEffects[t] = [37, 2];
		
			create_dialogue(text, mySpeaker,0,0,0,0,0,0,myEmotion);
		
			break;
		
		case "HA_NPC_Ex3":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "...";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++;//line 2
			
			text[t] = "........";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++;//line 2
			
			text[t] = "I'm hungry..."
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,0,0,0,0,0,0,myEmotion);
		
			break;
		
		case "HA_NPC_Ex4":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "Where were you trying to go...?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++;//line 2
			
			text[t] = "I don't even remember..."
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++;//line 2
			
			text[t] = "What does it really matter, now?"
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,0,0,0,0,0,0,myEmotion);
		
			break;
		
		case "HA_NPC_Ex2":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "Sometimes you hear giggling at night around here.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++;//line 2
			
			text[t] = "Hope you're a heavy sleeper."
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++;//line 2
			
			text[t] = "I'm not... I haven't slept a wink since I got here..."
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,0,0,0,0,0,0,myEmotion);
		
			break;
		
		case "HA_NPC_Ex5":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "Some girl...?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++;//line 2
			
			text[t] = "Ran to the machinery room back here."
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			create_dialogue(text, mySpeaker,0,0,0,0,0,0,myEmotion);
		
			break;
	// Laughing NPC in Hangar Act 3
		case "HA_NPC_Laugh_1":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "This is my mushroom farm. Ain't it a thing of beauty?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myEffects[t] = [1,1]
		
			t++;
		
			text[t] = "Chief wants us to be self-sustainable, so I started this little project.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++;
		
			text[t] = "Stuff's ALMOST edible!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
		
			break;
		
		case "HA_NPC_Laugh_2":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "You want a bite?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
		
			break;
			
		case "chiefInTown":
			
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "Get my keychain back, will ya?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
		
			break;
		
		case "HA_NPC_Laugh_3":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "What a show!!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myEffects[t] = [1,1]
		
			t++;
		
			text[t] = "I haven't seen an eviction that good in ages!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++;
		
			text[t] = "How awful! You monster.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
		
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
		
			break;
		
		case "HA_NPC_Laugh_4":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "Guy was minding his own business and you just up and did him in!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
		
			break;
			
		case "HA_NPC_Choost":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "Choo choo.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++
		
			text[t] = "...";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++
		
			text[t] = "Oh my God.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
		
			break;
		
	// Sad guy in Hangar Act 3

		case "HA_NPC_Sad_1":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
			speaker[1] = scr_AutaDialogueInit()
		
			var t = 0//line 1
		
			text[t] = "Some little girl just ran past here and locked the gate to the staircase...";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "Dunno where she got the keys from, but as long as we have the captain's keychain that shouldn't be a problem.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++
		
			text[t] = "...";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "Oh...";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++
		
			text[t] = "Well, I hid a spare key down in the mushroom farm if you really need to get through here.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++
		
			text[t] = "Poke around until you see something shiny on the ground. Then go ahead and press INTERACT and pick it up.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
		
			break;
			
		case "HA_NPC_Sad_2":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "Oh, you found my spare key!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "Well, go ahead and let yourself in. Leave the key in the lock while you're at it.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
			break;
		
		case "HA_NPC_Sad_3":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "Careful in there.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
			break;
	// Strange Guy in Hangar Act 3

		case "HA_NPC_Strange_1":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "Whozzat?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "Ugh, you-!!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "*urp*";
			myEffects[t] = [1,1]
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "Can't I get a little privacy? Buzz off!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
		
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
		
			break;
		case "HA_NPC_Strange_2":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "Buzz off!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
		
			break;
		
		case "HA_NPC_Happy_1":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "I know my mask looks sad, but I'm actually quite the bundle of joy!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "That's why I've learned to express myself through words.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++
		
			text[t] = ":)";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
			break;
		
		case "NPC_HA_Barrel":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "Ahhh man I'm the barrel guy.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "Ahhh man. I hit the barrel. ";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			t++
		
			text[t] = "Ahhhhhhh.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
			break;
		
		case "HA_NPC_Holden":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "...";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++;
		
			text[t] = "Buy something. :)";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
		
			break;
		case "HA_NPC_Hardy":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
		
			var t = 0//line 1
		
			text[t] = "Say now, Holden! It's getting a little tricky out here, eh?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++;
		
			text[t] = "I've observed that this area is full of moving pieces that turn on and off again!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++;
		
			text[t] = "A lad's got to time themself to the T! No daydreaming, y'hear, Holden?!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,myEffects,0,0,0,0,0,myEmotion);
		
			break;


		case "HA_CKING_Meet":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
			speaker[1] = scr_AutaDialogueInit()
		
			var t = 0//line 1
		
			text[t] = "Halt, peasant!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "Thou shalt not pass these sacred grounds!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "What?";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 1
		
			t++
		
			text[t] = "Doth thou not realize they gazeth upon a mighty castle?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "A castle. Yes, of course. Made of... cardboard."
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "..."
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myScript[t] = [scr_OrientCharacter,inst_KR_King,-1,spr_KingAngry,true]
		
			t++
		
			text[t] = "HOLD THY TONGUE, KNAVE."
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
		
			t++
		
			text[t] = "Yes it is cardboard, and what of it?! My brethen erected their fine kingdoms from the stone that mother earth provided them!"
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "And I, too, build my castle from the sacred resources given me!"
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "Cardboard.";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 1
		
			t++
		
			text[t] = "..."
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myScript[t] = [scr_OrientCharacter,inst_KR_King,-1,spr_KingHappy,true]
		
			t++
		
			text[t] = "YES"
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0

		
			t++
		
			text[t] = "So can I pass?";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "NO"
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "I'll just pass through quickly.";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
			myScript[t] = [scr_OrientCharacter,inst_KR_King,-1,spr_KingAngry, true]
		
			t++
		
			text[t] = "YOU BETTER NOT!"
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
		
			t++
		
			text[t] = "(Subject clearly not a threat. Just go.)";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 1

		
			create_dialogue(text, mySpeaker,0,0,0,0,myScript,0,myEmotion);
		
			break;
		
		case "HA_CKING_Meet_2":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
			speaker[1] = scr_AutaDialogueInit()
		
			var t = 0//line 1
		
			text[t] = "ART YOUR EARS CLOGGED?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "CEASE YOUR TRESSPASSING AT ONCE!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "Calm down! You're going to fall!";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 1
		
			t++
		
			text[t] = "THIS CASTLE SHALL STAND FOREVER! IT SHALL NEVER--";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0

		
			create_dialogue(text, mySpeaker,0,0,0,0,myScript,0,myEmotion);
		
			break;
		
		case "HA_CKING_Meet_3":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
			speaker[1] = scr_AutaDialogueInit()
		
			var t = 0//line 1
		
			text[t] = "THOU HAST CAUSED MY KINGDOM TO FALL TO RUIN!";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "I didn't do anything...";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
		

		
			create_dialogue(text, mySpeaker,0,0,0,0,myScript,0,myEmotion);
		
			break;
		
		case "HA_CKING_Meet_4":
		
			speaker[0] = scr_CustomDialogueInit(snd_voice1,-1)
			speaker[1] = scr_AutaDialogueInit()
		
			var t = 0//line 1
		
			text[t] = "(An animation plays here where sparks fly out of the LED things and the lights go out.)";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++
		
			text[t] = "(The release of electricity causes the spark wires to activate all over the map.)";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0

			create_dialogue(text, mySpeaker,0,0,0,0,myScript,0,myEmotion);
		
			break;
	
		case "abc":
			speaker[0] = scr_AutaDialogueInit()
			speaker[1] = scr_CustomDialogueInit(snd_voice1,-1)

			var t = 0
			text[t] = "I was... just trying to get through here.";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 1
			myEffects[t] = 0
			myScript[t] = [scr_OrientCharacter,obj_Auta,1,obj_Auta.sprite_index,false]

			t++
			text[t] = "Did somebody put this blockade here? Why would they do that?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myEffects[t] = 0
			myScript[t] = [scr_OrientCharacter,obj_Auta,-1,obj_Auta.sprite_index,false]

			t++
			text[t] = "You a simpleton, buster?!";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0


			t++
			text[t] = "Am I a... huh?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myEffects[t] = 0
			myScript[t] = [scr_Screenshake,1]

			t++
			text[t] = "ARE. YOU. A. SIMPLETON.";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
			myEffects[t] = [1,1]


			t++
			text[t] = "Do you know what that means?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myEffects[t] = 0

			t++
			text[t] = "Listen buster.  Nobody gets through the barricade 'cuz the barricade doesn't move.";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
			myEffects[t] = 0


			t++
			text[t] = "That open crack to the world would cause panic. Hysteria! Catastrophe!";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
			myEffects[t] = 0


			t++
			text[t] = "Stay here, where it's safe. Don't be a simpleton.";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
			myEffects[t] = [28,1]


			t++
			text[t] = "Look, I... need to get through there. Who can I talk to about that?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myEffects[t] = 0
			myScript[t] = [scr_Screenshake,1]

			t++
			text[t] = "DID YOU HEAR A WORD I SAID, BUSTER?!";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
			myEffects[t] = [1,1]


			t++
			text[t] = "Ugh. If you want to get past there, you're gonna have to go up and talk to the big boss. Got that?";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0


			t++
			text[t] = "That should only take you... about 100 years?";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
			myEffects[t] = [29,1]
			myScript[t] = [scr_Screenshake,1]

			t++
			text[t] = "PAHAHAH, what am I even worried about?"
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
			myEffects[t] = [1,1]


			t++
			text[t] = "What?";
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
			myEffects[t] = 0
			myScript[t] = [scr_Screenshake,1]

			t++
			text[t] = "Good luck!"
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
			myEffects[t] = [1,1]

			create_dialogue(text, mySpeaker,myEffects,0,0,0,myScript,0,myEmotion);

			break;
	
		case "policeGuy":
			speaker[1] = scr_CustomDialogueInit(snd_voice1,-1)
	
			var t = 0
			text[t] = "Well, what are you waiting for?!";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,0,0,0,0,0,0,myEmotion);

			break;
			
		case "TrainStorage_TallMask":
			speaker[1] = scr_CustomDialogueInit(snd_voice1,-1)
	
			var t = 0
			text[t] = "Ohh, my head... Did I get overexcited again?";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,0,0,0,0,0,0,myEmotion);

			break;
		
		case "TrainStorage_TallMask_Uncons":
			speaker[1] = scr_CustomDialogueInit(snd_voice1,-1)
	
			var t = 0
			text[t] = "They're twitching a little.";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 0
		
			create_dialogue(text, mySpeaker,0,0,0,0,0,0,myEmotion);

			break;
		
		case "RalphMeeting":
		
		
			speaker[1] = scr_RalphDialogueInit()
		
			var t = 0//line 1
		
			text[t] = "CRIKEY MATE";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 3
		
			t++;//line 2
		
			text[t] = "...";
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 2
		
			t++;//line 2
		
			text[t] = "No goddammit we have to rewrite everything about this ahausdhlaisudhli"
			mySpeaker[t] =  speaker[1]
			myEmotion[t] = 0
		
			t++;//line 3
		
			text[t] = "Me neither! Some place this is, huh?"
			mySpeaker[t] =  speaker[0]
			myEmotion[t] = 0
		
			t++;//line 4
		
			text[t] = "You said it."
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 2
		
			t++;//line 5
		
			text[t] = "My name is Alex."
			mySpeaker[t] =  speaker[1]
			myEmotion[t] = 0
		
			t++;//line 6
		
			text[t] = "Auta."
			mySpeaker[t] = speaker[0]
			myEmotion[t] = 0
		
			t++;//line 7
		
			text[t] = "It's a pleasure."
			mySpeaker[t] = speaker[1]
			myEmotion[t] = 1
		
			create_dialogue(text, mySpeaker,0,0,0,0,0,0,myEmotion);
		
			break;
	
	}



}

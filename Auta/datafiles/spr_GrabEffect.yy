{
    "id": "d67a345b-d4e0-45dd-8391-3f34c9af2ee1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GrabEffect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7282cfeb-0127-40a4-82cf-67003b021f7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d67a345b-d4e0-45dd-8391-3f34c9af2ee1",
            "compositeImage": {
                "id": "782b3119-c2b2-4325-b8b3-d210c65e171b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7282cfeb-0127-40a4-82cf-67003b021f7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42e78e2b-3c6b-4d09-9096-61bc4116bbff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7282cfeb-0127-40a4-82cf-67003b021f7f",
                    "LayerId": "aa1ca05e-b611-4429-818c-b9d6f80c72bd"
                },
                {
                    "id": "ad8393a8-1cee-4b5f-9815-78e24497ea44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7282cfeb-0127-40a4-82cf-67003b021f7f",
                    "LayerId": "9e900577-b9ba-4cae-911d-14673e698ca2"
                }
            ]
        },
        {
            "id": "5e483522-de02-44f0-8470-c9922970dfb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d67a345b-d4e0-45dd-8391-3f34c9af2ee1",
            "compositeImage": {
                "id": "52a51759-09cb-4c11-90c9-806ccef03dc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e483522-de02-44f0-8470-c9922970dfb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f19ceca-abd2-4782-88e7-86e1763e1760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e483522-de02-44f0-8470-c9922970dfb0",
                    "LayerId": "9e900577-b9ba-4cae-911d-14673e698ca2"
                },
                {
                    "id": "b29aba0e-6964-41c0-bb28-5b65e42ff512",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e483522-de02-44f0-8470-c9922970dfb0",
                    "LayerId": "aa1ca05e-b611-4429-818c-b9d6f80c72bd"
                }
            ]
        },
        {
            "id": "688e5c0c-e902-4e5f-a510-bb73209d1e6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d67a345b-d4e0-45dd-8391-3f34c9af2ee1",
            "compositeImage": {
                "id": "1f9a8d97-6203-456c-8e5d-648446d38950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "688e5c0c-e902-4e5f-a510-bb73209d1e6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ebc7b0-0edc-4e6e-8eea-ec07adb4b40e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "688e5c0c-e902-4e5f-a510-bb73209d1e6e",
                    "LayerId": "9e900577-b9ba-4cae-911d-14673e698ca2"
                },
                {
                    "id": "58b25895-d7fc-47c5-a627-f598f04b0fa7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "688e5c0c-e902-4e5f-a510-bb73209d1e6e",
                    "LayerId": "aa1ca05e-b611-4429-818c-b9d6f80c72bd"
                }
            ]
        },
        {
            "id": "02b77eb1-3ca4-407b-b655-f57df19da2d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d67a345b-d4e0-45dd-8391-3f34c9af2ee1",
            "compositeImage": {
                "id": "20bb4992-041c-4a15-8c53-0a3b0e2d5ea8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02b77eb1-3ca4-407b-b655-f57df19da2d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8886f8d-cf6b-4f7a-bf61-2d66bbb23e1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02b77eb1-3ca4-407b-b655-f57df19da2d8",
                    "LayerId": "9e900577-b9ba-4cae-911d-14673e698ca2"
                },
                {
                    "id": "24a3bbeb-423b-450c-8afb-4c44994ddf38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02b77eb1-3ca4-407b-b655-f57df19da2d8",
                    "LayerId": "aa1ca05e-b611-4429-818c-b9d6f80c72bd"
                }
            ]
        },
        {
            "id": "600dd9ef-4a63-45b5-974e-3ab2e733f69e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d67a345b-d4e0-45dd-8391-3f34c9af2ee1",
            "compositeImage": {
                "id": "57ba6c8f-faec-426e-9d57-228acf945cb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "600dd9ef-4a63-45b5-974e-3ab2e733f69e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c25b0653-5d1d-4266-9dea-7929a5d5b73f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "600dd9ef-4a63-45b5-974e-3ab2e733f69e",
                    "LayerId": "9e900577-b9ba-4cae-911d-14673e698ca2"
                },
                {
                    "id": "d7360d69-16e5-4c7b-a298-b58edd50bfd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "600dd9ef-4a63-45b5-974e-3ab2e733f69e",
                    "LayerId": "aa1ca05e-b611-4429-818c-b9d6f80c72bd"
                }
            ]
        },
        {
            "id": "84b6d15b-40ff-4c09-9e16-512e98939101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d67a345b-d4e0-45dd-8391-3f34c9af2ee1",
            "compositeImage": {
                "id": "c1ba3ab4-32e6-4506-ba67-eaf0d169a0d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84b6d15b-40ff-4c09-9e16-512e98939101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca165e97-c3e2-4679-86f8-eb4791be9e01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84b6d15b-40ff-4c09-9e16-512e98939101",
                    "LayerId": "9e900577-b9ba-4cae-911d-14673e698ca2"
                },
                {
                    "id": "7eace952-30bf-4d7b-9829-4e4dce1e745f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84b6d15b-40ff-4c09-9e16-512e98939101",
                    "LayerId": "aa1ca05e-b611-4429-818c-b9d6f80c72bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "9e900577-b9ba-4cae-911d-14673e698ca2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d67a345b-d4e0-45dd-8391-3f34c9af2ee1",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "aa1ca05e-b611-4429-818c-b9d6f80c72bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d67a345b-d4e0-45dd-8391-3f34c9af2ee1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0.3,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 20,
    "yorig": 24
}